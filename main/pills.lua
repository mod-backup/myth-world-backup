AddPrefabFiles('alchmy_fur', 'mk_firefx', 'mk_cloudpuff')
AddPrefabFiles("myth_pills", "myth_buffs")
local easing = require("easing")

--免疫火
AddComponentPostInit("health", function(self)
	local old_DoFireDamage = self.DoFireDamage
	function self:DoFireDamage(...)
		local old_fds = self.fire_damage_scale
		if self.inst.components.inventory and self.inst.components.inventory:HasItemWithTag("heat_resistant_pill", 1) or self.inst.mythpill_forcefire then
			--local mult = self:GetFireDamageScale() * 0.25
			self.fire_damage_scale = self.fire_damage_scale * 0.15 -- 带避暑丹火烧伤害为原来的0.15
		end
		old_DoFireDamage(self, ...)
		self.fire_damage_scale = old_fds
	end
end)
--免疫水
AddComponentPostInit("moisture", function(self)
	local old_GetMoistureRate = self.GetMoistureRate
	local old_GetDryingRate = self.GetDryingRate
	function self:GetMoistureRate()
		if self.inst.components.inventory and self.inst.components.inventory:HasItemWithTag("cold_resistant_pill", 1) or self.inst.mythpill_forcecold then
			return old_GetMoistureRate(self) * 0.1 --带避寒丹时水分增加速度为原来的0.3
		end
		return old_GetMoistureRate(self)
	end

	function self:GetDryingRate(moisturerate, ...)
		if self.inst.components.inventory and self.inst.components.inventory:HasItemWithTag("cold_resistant_pill", 1) or self.inst.mythpill_forcecold then
			return old_GetDryingRate(self, moisturerate, ...) * 3 --带避寒丹时干燥速度为原来的3倍
		end
		return old_GetDryingRate(self, moisturerate, ...)
	end
end)
--温度控制
AddComponentPostInit("temperature", function(self)
	local old_SetTemperature = self.SetTemperature
	function self:SetTemperature(value, ...)
		if value < 25 and self.inst.components.inventory and self.inst.components.inventory:HasItemWithTag("cold_resistant_pill", 1) or self.inst.mythpill_forcecold then
			local c = self.current
			if c < 25 and value - c < 0 then
				value = c + (value - c) * 0.4 --避水丹
			end
		end
		if value > (self.overheattemp - 20) and self.inst.components.inventory and self.inst.components.inventory:HasItemWithTag("heat_resistant_pill", 1) or self.inst.mythpill_forcefire then
			local c = self.current
			if c > (self.overheattemp - 20) and value - c > 0 then
				value = c + (value - c) * 0.5 --避火丹
			end
		end
		return old_SetTemperature(self, value, ...)
	end
end)

local function PushFn(inst)
	TheWorld:PushEvent("myth_flower_ghostchange")
end

local function blood(inst, data)
	if not inst:HasTag("bloodthirsty_pill_buff") then
		return
	end
	if data and data.stimuli == "bramblefx_mk" then
		return
	end
	local delta = math.random(5, 10)
	if not (inst.components.health ~= nil and inst.components.health:IsDead()) then
		inst.components.health:DoDelta(delta, false, "bloodthirsty_pill_buff")
	end
end
local function OnCooldown(inst)
	inst._cdjingji = nil
end
local function onattacked(inst, data)
	if not inst.components.inventory then
		return
	end

	if inst._is_player_astral ~= nil and inst._is_player_astral:value() then
		return
	end

	if inst.components.inventory:Has("thorns_pill", 1) then
		if inst._cdjingji == nil and data ~= nil and not data.redirected then
			inst._cdjingji = inst:DoTaskInTime(.3, OnCooldown)

			SpawnPrefab("bramblefx_mk"):SetFXOwner(inst)

			if inst.SoundEmitter ~= nil then
				inst.SoundEmitter:PlaySound("dontstarve/common/together/armor/cactus")
			end
		end
	end
end

local function undertree(inst, under)
	inst.under_infantreeleaves:set(under)
	if under then
		if inst.components.moisture then
			inst.components.moisture.waterproofnessmodifiers:SetModifier("under_infantree", 0.6)
		end
		if inst.components.sanity then
			inst.components.sanity.externalmodifiers:SetModifier("under_infantree", 0.1)
		end
	else
		if inst.components.moisture then
			inst.components.moisture.waterproofnessmodifiers:RemoveModifier("under_infantree")
		end
		if inst.components.sanity then
			inst.components.sanity.externalmodifiers:RemoveModifier("under_infantree")
		end
	end
end

--玩家相关
AddPlayerPostInit(function(inst)
	inst.under_infantreeleaves = net_bool(inst.GUID, "under_infantreeleaves", "infantreeleavesdirty")
	if TheWorld.ismastersim then
		--[[if	inst.components.oldager and  inst.components.health then
			local old_DoDelta = inst.components.health.DoDelta
			inst.components.health.DoDelta = function(self, amount, overtime, cause,...)
				if cause and cause == "oldager_component" and self.mythinfant_fruit then
					return 0
				end
				return old_DoDelta(self,val,...)
			end
		end]]

		--新的监听器
		inst:ListenForEvent("onhitother", blood)
		inst:ListenForEvent("attacked", onattacked)
		--修改接口
		inst._myth_listener_blood = blood
		inst._myth_listener_attacked = onattacked
		inst.under_infantreeleaves_num = 0
		inst:ListenForEvent("ms_respawnedfromghost", PushFn)
		inst:ListenForEvent("ms_becameghost", PushFn)
		inst:ListenForEvent("onremove", function()
			TheWorld:DoTaskInTime(0.2, function(inst)
				PushFn(inst)
			end)
		end)
		inst:ListenForEvent("onchangeunderinfantreezone", undertree)
	end
end)
--免疫尖刺植物
--可能会导致一些奇怪的后果, 如发生冲突, 请联系我们
AddComponentPostInit('inventory', function(self)
	local old_has = self.EquipHasTag
	function self:EquipHasTag(tag, ...)
		if tag == "bramble_resistant" then
			if self.inst.components.inventory and self.inst.components.inventory:Has("thorns_pill", 1) then
				return true
			end
		end
		return old_has(self, tag, ...)
	end
end)

--免疫沙尘暴
AddClassPostConstruct("widgets/gogglesover", function(self)
	local oldToggleGoggles = self.ToggleGoggles
	function self:ToggleGoggles(show, ...)
		oldToggleGoggles(self, show, ...)
		if self.owner
			and (self.owner.replica.inventory and self.owner.replica.inventory:HasItemWithTag("dust_resistant_pill", 1)
				or self.owner:HasTag('mythpill_forcegoggles')) then
			self:Hide()
		end
	end
end)

--[[AddClassPostConstruct( "widgets/nutrientsover", function(self)
	local oldToggleNutrients =self.ToggleNutrients
	function self:ToggleNutrients(show,...)
		oldToggleNutrients(self,show,...)
		if self.owner and self.owner.replica.inventory and self.owner.replica.inventory:HasItemWithTag("dust_resistant_pill", 1) then
			self:Hide()
		end
	end
end)]]


---不要学我刷刷刷 这样很不好的 ！！！
AddComponentPostInit("playervision", function(self)
	self.inst:StartUpdatingComponent(self)
	function self:OnUpdate(dt)
		if self.inst.replica.inventory and self.inst.replica.inventory:HasItemWithTag("dust_resistant_pill", 1) or self.inst:HasTag('mythpill_forcegoggles') then
			self:ForceGoggleVision(true)
		else
			self:ForceGoggleVision(false)
		end
	end
end)

--壮骨 凝味丹会覆盖蒜末辣椒面料理buff

local noneedbuff = {
	buff_playerabsorption = "armor_pill_buff",
	buff_attack = "condensed_pill_buff",
}
AddComponentPostInit("debuffable", function(self)
	local old_AddDebuff = self.AddDebuff
	function self:AddDebuff(name, prefab, ...)
		if noneedbuff[name] ~= nil and self:HasDebuff(noneedbuff[name]) then
			self:RemoveDebuff(noneedbuff[name])
		end
		return old_AddDebuff(self, name, prefab, ...)
	end
end)
