
--[[
    --如果你想要添加可以炼丹炉炼制得产物 以下是配方参数

    配方产物 = {
        time = 960,
        recipe = {minotaurhorn =1,myth_rhino_blueheart =1,foliage =1,nitre =1},
        num = 3 , --炼制得到的数量
        prefn = nil, --前置满足的函数 比如判定doer 当前天气 之类的
        pruductfn = nil  --最终产物可以用这个修改 比如一个世界只能做一个牛铃铛 
        overridebuild = nil, 使用的贴图资源
        overridesymbolname = nil, 使用的贴图symbol (文件夹名字)
    }

    --例子
    AddSimPostInit(function()
        if TUNING.MYTH_PILL_RECIPES then
            TUNING.MYTH_PILL_RECIPES.spear = {
                time = 10,
                recipe = {twigs =15},
                overridebuild = "swap_spear",
                overridesymbolname = "swap_spear"
            }
        end
    end)
]]

TUNING.MYTH_PILL_RECIPES = {
    --_extension --表示后缀 可以重练
    --避暑
    heat_resistant_pill = {
        time = 960,
        recipe = {minotaurhorn =1,myth_rhino_blueheart =1,foliage =1,nitre =1},
    },
    heat_resistant_pill_extension = {
        time = 480,
        recipe = {heat_resistant_pill =1,bluegem= 1,foliage =1,nitre =1},
        pruductfn = function(inst)
            return "heat_resistant_pill"
        end,
    },
    --避寒
    cold_resistant_pill = {
        time = 960,
        recipe = {minotaurhorn =1,myth_rhino_redheart =1,foliage =1,nitre =1},
    },
    cold_resistant_pill_extension = {
        time = 480,
        recipe ={cold_resistant_pill =1,redgem = 1,foliage =1,nitre =1},
        pruductfn = function(inst)
            return "cold_resistant_pill"
        end,
    }, 

    --避尘
    dust_resistant_pill = {
        time = 960,
        recipe = {minotaurhorn =1,myth_rhino_yellowheart =1,foliage =1,nitre =1},
    },  
    dust_resistant_pill_extension = {
        time = 480,
        recipe = {dust_resistant_pill =1,yellowgem = 1,foliage =1,nitre =1},
        pruductfn = function(inst)
            return "dust_resistant_pill"
        end,
    },  

    --腾云
    fly_pill = {
        time = 60,
        recipe = {nitre =1,dragonfruit =1,honey =1,nightmarefuel =1},
        pruductfn = function(inst)
            return "fly_pill"
        end
    },

    --嗜血
    bloodthirsty_pill = {
        time = 60,
        recipe = {purplegem =1,livinglog =1,batwing =1,nightmarefuel =1},
    },

    --凝神
    condensed_pill = {
        time = 60,
        recipe = {gunpowder =1,stinger =1,durian =1,nightmarefuel =1},
    },

    --壮骨
    armor_pill = {
        time = 60,
        recipe = {garlic =1,rocks =1,boneshard =1,nightmarefuel =1},
    },

    --移山
    movemountain_pill = {
        time = 60,
        recipe = {houndstooth =1,townportaltalisman =1,asparagus =1,nightmarefuel=1},
    },
    --急急如律令
    laozi_sp = {
        time = 60,
        recipe = {papyrus =3,featherpencil =1,petals =3},
        num = 3, --一次给三个
    },

    --荆棘丹
    thorns_pill = {
        time = 180,
        recipe = {cactus_flower =1,waterplant_bomb =1,livinglog =1,lureplantbulb =1},
    },
    --[[
    --花翎
    {"mk_hualing",60,{goose_feather =3,redgem =1}},

    --火猿石心
    {"mk_huoyuan",60,{bigpeach =1,rocks =2,shadowheart=1}},
    --龙皮绸缎
    {"mk_longpi",60,{bearger_fur =1,dragon_scales =2,shroom_skin=1}},
    ]]--
    --紫金葫芦
    purple_gourd = {
        time = 960,
        recipe = {krampussack_sealed =1,goldnugget =1,orangegem =1,pill_bottle_gourd =1},
        overridebuild = "purple_gourd",
        overridesymbolname = "swap_fur",
    },
    --芭蕉宝扇
    bananafan_big = {
        time = 960,
        recipe = {laozi_sp =1,featherfan =1,lavae_egg =1,myth_banana_leaf = 6},
    },
    --玉净瓶
    myth_yjp = {
        time = 960,
        recipe = {opalpreciousgem =1,laozi_sp =1,moonglass =6,moonbutterfly = 1},
        overridebuild = "myth_yjp",
        overridesymbolname = "image",
    },
    --牛羚

    laozi_bell = {
        time = 240,
        recipe = {gnarwail_horn =1,horn =1,laozi_sp=1,lucky_goldnugget = 1},
        pruductfn = function(inst)
            return TheWorld.laozi_bell ~= nil and "beef_bell" or "laozi_bell"
        end   
    },
    laozi_bell_extension = {
        time = 120,
        recipe = {laozi_bell_broken =1,redgem =1,bluegem=1,purplegem = 1},
        pruductfn = function(inst)
            return "laozi_bell"
        end
    },
    --牛鞍
    saddle_qingniu = {
        time = 240,
        recipe = {saddle_race =1,succulent_picked =1,laozi_sp=1,lucky_goldnugget = 1},
    },

    --通天敕令
    myth_passcard_jie = {
        time = 240,
        recipe ={siving_rocks =1,lucky_goldnugget =1,walrus_tusk=1,myth_lotus_flower = 1},
    },

    --七星剑
    myth_qxj = {
        time = 960,
        recipe ={laozi_sp =1,siving_stone = 3},
    },
    --子圭战盔
    siving_hat = {
        time = 960,
        recipe ={siving_stone =2,myth_iron_helmet =1,livinglog = 1},
    },

    --子圭战甲
    armorsiving = {
        time = 960,
        recipe ={siving_stone =2,myth_iron_battlegear =1,livinglog = 1},
    },

    --子圭青金
    siving_stone = {
        time = 60,
        recipe ={siving_rocks =1,lucky_goldnugget =1,thulecite_pieces = 1},
    },

    --霜钺斧
    myth_weapon_syf = {
        time = 960,
        recipe ={moonglassaxe =1 ,myth_rhino_blueheart =1,moonrocknugget =1,bluegem = 1},
    },
    myth_weapon_syf_extension = {
        time = 480,
        recipe ={myth_weapon_syf =1 ,redgem =1,bluegem=1,purplegem=1},
        pruductfn = function(inst)
            return "myth_weapon_syf"
        end
    },

    --扢挞藤
    myth_weapon_gtt = {
        time = 960,
        recipe ={ruins_bat =1 ,myth_rhino_yellowheart =1,townportaltalisman =1,yellowgem = 1},
    },
    myth_weapon_gtt_extension = {
        time = 480,
        recipe ={myth_weapon_gtt =1 ,redgem =1,bluegem=1,purplegem=1},
        pruductfn = function(inst)
            return "myth_weapon_gtt"
        end
    },

    --暑熤刀
    myth_weapon_syd = {
        time = 960,
        recipe ={glasscutter =1 ,myth_rhino_redheart =1,goldnugget =1,redgem = 1},
    },
    myth_weapon_syd_extension = {
        time = 480,
        recipe ={myth_weapon_syd =1 ,redgem =1,bluegem=1,purplegem=1},
        pruductfn = function(inst)
            return "myth_weapon_syd"
        end
    },
    --浮尘
    myth_fuchen = {
        time = 60,
        recipe ={cane =1 , manrabbit_tail =3},
    },

    --金击子
    myth_gold_staff = {
        time = 960,
        recipe ={myth_ruyi =1 , lucky_goldnugget =4},
        overridebuild = "myth_gold_staff",
        overridesymbolname = "water",
    },
}


--人物mod得炼制部分
TUNING.MYTH_PLAYER_PILL_RECIPES = {
    --大圣锁子甲
    golden_armor_mk = {
        time = 240,
        recipe ={dragon_scales =2,armorruins =1,furtuft=15,minotaurhorn=1},
    },
    golden_armor_mk_extension = {
        time = 120,
        recipe ={golden_armor_mk =1,redgem =1,bluegem=1,purplegem=1},
        pruductfn = function(inst)
            return "golden_armor_mk"
        end
    },
    
    --锁子清源甲
    yangjian_armor = {
        time = 240,
        recipe ={furtuft =15,armorruins =1,malbatross_feather=10,walrus_tusk=1},
    },
    yangjian_armor_extension = {
        time = 120,
        recipe ={yangjian_armor =1,redgem =1,bluegem=1,purplegem=1},
        pruductfn = function(inst)
            return "yangjian_armor"
        end
    },

    --凤翅紫金冠
    golden_hat_mk = {
        time = 240,
        recipe ={redgem =2,ruinshat =1,minotaurhorn=1,goose_feather=6},
    },
    golden_hat_mk_extension = {
        time = 120,
        recipe ={golden_hat_mk =1,redgem =1,bluegem=1,purplegem=1},
        pruductfn = function(inst)
            return "golden_hat_mk"
        end
    },

    --三山飞凤冠
    yangjian_hair = {
        time = 240,
        recipe ={ruinshat =1,deerclops_eyeball =1,bluegem=2,goose_feather=6},
    },
    yangjian_hair_extension = {
        time = 120,
        recipe ={yangjian_hair =1,redgem =1,bluegem=1,purplegem=1},
        pruductfn = function(inst)
            return "yangjian_hair"
        end,
    },
    --酆都路引
    pass_commissioner_ylw = {
        time = 480,
        recipe ={myth_greenbamboo =2,myth_higanbana_item=1},
        overridebuild = "pass_commissioner",
        overridesymbolname = "lingpai",
    },
}