
local function checkbool(val) return val == nil or type(val) == "boolean" end
local function checknumber(val) return type(val) == "number" end
local function checkuint(val) return type(val) == "number" and tostring(val):find("%D") == nil end
local function checkstring(val) return type(val) == "string" end
local function checkentity(val) return type(val) == "table" end
AddPrefabPostInit("world", function(inst)
    if not TheWorld.ismastersim then
        return
    end
	inst:AddComponent("myth_saveinfo")
end)

AddShardModRPCHandler( "myth_saveinfo", "myth_saveinfo", function(worldid,value)
    if checkstring(value) then
        local myth_saveinfo = TheWorld and TheWorld.components.myth_saveinfo or nil
        if myth_saveinfo and  myth_saveinfo.AddPlayerInfo ~= nil then
            local success, b = pcall(json.decode,value)
            if success and b ~= nil then
                for k, v in pairs(b) do
                    myth_saveinfo:AddPlayerInfo(k, v)
                end
            end
        end
    end
end)

AddClientModRPCHandler( "commissioner_book_rpc", "commissioner_book_rpc", function(value,value1)
    if  checkstring(value) then
        local success, result = pcall(json.decode,value)
	    if result and ThePlayer then
            if checkentity(value1) then
                result.book = value1
            end
            ThePlayer:PushEvent("open_commissioner_book",result)
	    end
    end
end)

local function commissioner_book(inst,book,value1,value2)
    if checknumber(value1) and checkentity(book) then
        if book and book.UseSoulBook and book:IsValid() then
            if value1 == 1 and checkstring(value2) then --复活玩家
                if book.components.finiteuses then
                    book.components.finiteuses:Use(1)
                end
                SendModRPCToShard(SHARD_MOD_RPC["commissioner_book_respawn"]["commissioner_book_respawn"],nil,value2)  
            elseif value1 == 2 and checknumber(value2) then --复活玩家
                if book.UseSoulBook then
                    book:UseSoulBook(value1,value2,inst)
                end
            elseif value1 == 3 and checknumber(value2) then --复活玩家
                if book.UseSoulBook then
                    book:UseSoulBook(value1,value2,inst)
                end
            elseif value1 == 4 then --复活玩家
                book.inonbusy = false
            end
        end
    end
end
AddModRPCHandler("commissioner_book", "commissioner_book", commissioner_book)

AddShardModRPCHandler( "commissioner_book_respawn", "commissioner_book_respawn", function(worldid,value,book)
    if checkstring(value) then
        for i, v in ipairs(AllPlayers) do
            if v.userid == value and v:HasTag("playerghost")  then
                local book = SpawnPrefab("commissioner_flybook")
                v:PushEvent("respawnfromghost", { source = book })
                if v.components.health then
                    v.components.health:SetPercent(0.5)
                end
                if v.components.hunger then
                    v.components.hunger:SetPercent(0.5)
                end
                if v.components.sanity then
                    v.components.sanity:SetPercent(0.5)
                end
                book:DoTaskInTime(0,book.Remove)
            end
        end
    end
end)