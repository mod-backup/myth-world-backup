

modimport("main/myth_skinsapi.lua")

MakeItemSkinDefaultImage("myth_redlantern","images/inventoryimages/redlantern_myth_a.xml","redlantern_myth_a")
MakeItemSkinDefaultImage("alchmy_fur","images/monkey_king_item.xml","alchmy_fur")
MakeItemSkinDefaultImage("myth_interiors_ghg_groundlight","images/inventoryimages/myth_interiors_ghg_groundlight.xml","myth_interiors_ghg_groundlight")
MakeItemSkinDefaultImage("fence_bamboo_item","images/inventoryimages/fence_bamboo_item.xml","fence_bamboo_item")
MakeItemSkinDefaultImage("fence_gate_bamboo_item","images/inventoryimages/fence_gate_bamboo_item.xml","fence_gate_bamboo_item")
MakeItemSkinDefaultImage("myth_food_table","images/map_icons/myth_food_table.xml","myth_food_table")
MakeItemSkinDefaultImage("myth_redlantern_ground","images/inventoryimages/myth_redlantern_ground.xml","myth_redlantern_ground")
MakeItemSkinDefaultImage("myth_stool","images/inventoryimages/myth_stool.xml","myth_stool")
MakeItemSkinDefaultImage("mk_battle_flag_item","images/monkey_king_item.xml","mk_battle_flag_item")

GLOBAL.myth_redlantern_clear_fn = function(inst,skin)
	inst.AnimState:SetBuild("redlantern_myth_a")
    if inst.components.inventoryitem ~= nil then
        inst.components.inventoryitem.atlasname = "images/inventoryimages/redlantern_myth_a.xml"
        inst.components.inventoryitem:ChangeImageName("redlantern_myth_a")
    end
end

local function alchmy_fur_init_fn(inst,skin) 
	if inst.components.stewer_fur and inst.components.stewer_fur:IsCooking() then
		inst.components.stewer_fur:CheckSmoke()
		inst.AnimState:OverrideSymbol("pot", skin.."_work", "pot_work")
	end 
end

GLOBAL.alchmy_fur_clear_fn = function(inst,skin) 
	inst.AnimState:SetBuild("alchmy_fur")
	alchmy_fur_init_fn(inst,"alchmy_fur")
end

GLOBAL.myth_interiors_ghg_groundlight_clear_fn = function(inst,skin) 
	if inst.Light then
		inst.Light:SetColour(242/255,239/255,103/255)
	end
	if inst.fires ~= nil then
		for _, v in ipairs(inst.fires) do
			v:Remove()
		end
		inst.fires = nil
	end
end

local fires = {
	{"yama_fire_purple","purple"},
	{"yama_fire_green","yellow"},
}
local function myth_interiors_ghg_groundlight_init_fn(inst,r,g,b,hasfire)
	if inst.Light then
		inst.Light:SetColour(r/255,g/255,b/255)
	end
	if hasfire then
		inst.fires = {}
		inst:DoTaskInTime(0,function()
			for _, v in ipairs(fires) do
				local fx = SpawnPrefab(v[1])
				if fx then
					fx._light.Light:Enable(false)
					fx.entity:SetParent(inst.entity)
					fx.entity:AddFollower()
					fx.Follower:FollowSymbol(inst.GUID, v[2], 28, 65, 0)    
					table.insert(inst.fires,fx)    
				end  
			end
		end)
	end
end

GLOBAL.fence_bamboo_item_clear_fn = function(inst,build_name) 
	inst.linked_skinname = nil
	inst.AnimState:SetBuild("fence_bamboo")	
end
GLOBAL.fence_bamboo_clear_fn = function(inst,build_name) 
	inst.linked_skinname = nil
	inst.AnimState:SetBuild("fence_bamboo")		
end

GLOBAL.fence_gate_bamboo_item_clear_fn = function(inst,build_name) 
	inst.linked_skinname = nil
	inst.AnimState:SetBuild("fence_gate_bamboo")	
end
GLOBAL.fence_gate_bamboo_clear_fn = function(inst,build_name) 
	--inst.linked_skinname = nil
	--inst.AnimState:SetBuild("fence_gate_bamboo")	
	inst.dooranim.skin_id = nil
    inst.dooranim.AnimState:SetBuild("fence_gate_bamboo")	
end

GLOBAL.myth_food_table_clear_fn  = function(inst,build_name) 
	inst.AnimState:SetBuild("myth_food_table")	
	inst.AnimState:SetBank("myth_food_table")
end

GLOBAL.myth_redlantern_ground_clear_fn  = function(inst,build_name) 
	inst.AnimState:SetBuild("myth_redlantern_ground")	
	inst.AnimState:SetBank("myth_redlantern_ground")
end
----========================================

GLOBAL.myth_stool_clear_fn = function(inst,skin)
	inst.AnimState:SetBuild("myth_stool")
	inst.SitSymbol = nil
	inst.AnimState:ClearOverrideSymbol("wood")
end

GLOBAL.mk_battle_flag_clear_fn = function(inst,skin)
	inst.AnimState:SetBuild("mk_battle_flag")
	inst.linked_skinname = nil
end
GLOBAL.mk_battle_flag_item_clear_fn = function(inst,skin)
	inst.AnimState:SetBuild("mk_battle_flag")
	inst.linked_skinname = nil
end
---------------------------==========================================
MakeItemSkin("myth_redlantern","redlantern_myth_b",
{
	basebuild = "redlantern_myth_a",
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_REDLANTERN_MYTH_B,
    atlas = "images/inventoryimages/redlantern_myth_b.xml",
    image = "redlantern_myth_b",
})

MakeItemSkin("myth_redlantern","redlantern_myth_c",
{
	basebuild = "redlantern_myth_a",
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_REDLANTERN_MYTH_C,
    atlas = "images/inventoryimages/redlantern_myth_c.xml",
    image = "redlantern_myth_c",
})


MakeItemSkin("myth_redlantern","redlantern_myth_d",
{
	basebuild = "redlantern_myth_a",
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_REDLANTERN_MYTH_D,
    atlas = "images/inventoryimages/redlantern_myth_d.xml",
    image = "redlantern_myth_d",
})

MakeItemSkin("myth_redlantern","redlantern_myth_e",
{
	basebuild = "redlantern_myth_a",
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_REDLANTERN_MYTH_E,
    atlas = "images/inventoryimages/redlantern_myth_e.xml",
    image = "redlantern_myth_e",
})

MakeItemSkin("myth_redlantern","redlantern_myth_i",
{
	basebuild = "redlantern_myth_i",
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_REDLANTERN_MYTH_I,
    atlas = "images/inventoryimages/redlantern_myth_i.xml",
    image = "redlantern_myth_i",
})

MakeItemSkin("myth_redlantern","redlantern_myth_f",
{
	basebuild = "redlantern_myth_f",
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_REDLANTERN_MYTH_F,
    atlas = "images/inventoryimages/redlantern_myth_f.xml",
    image = "redlantern_myth_f",
})
MakeItemSkin("myth_redlantern","redlantern_myth_g",
{
	basebuild = "redlantern_myth_g",
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_REDLANTERN_MYTH_G,
    atlas = "images/inventoryimages/redlantern_myth_g.xml",
    image = "redlantern_myth_g",
})
MakeItemSkin("myth_redlantern","redlantern_myth_h",
{
	basebuild = "redlantern_myth_h",
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_REDLANTERN_MYTH_H,
    atlas = "images/inventoryimages/redlantern_myth_h.xml",
    image = "redlantern_myth_h",
})

--==================================
local alchmy_fur_skins = {"alchmy_fur_copper","alchmy_fur_ruins"}
for i, v in ipairs(alchmy_fur_skins) do
	MakeItemSkin("alchmy_fur",v,
	{
		rarity = "Loyal",
		type = "item",
		name = STRINGS["MYTH_SKIN_"..string.upper(v)],
		atlas = "images/inventoryimages/"..v..".xml",
		image = v,
		init_fn  = function(inst) alchmy_fur_init_fn(inst,v)  end,
	})
end

--=============================宫灯
MakeItemSkin("myth_interiors_ghg_groundlight","myth_interiors_ghg_groundlight_std",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_GROUNDLIGHT_STD,
    atlas = "images/inventoryimages/myth_interiors_ghg_groundlight_std.xml",
    image = "myth_interiors_ghg_groundlight_std",
	init_fn  = function(inst) myth_interiors_ghg_groundlight_init_fn(inst,255,239,169)  end,
})

MakeItemSkin("myth_interiors_ghg_groundlight","myth_interiors_ghg_groundlight_ryx",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_GROUNDLIGHT_RYX,
    atlas = "images/inventoryimages/myth_interiors_ghg_groundlight_ryx.xml",
    image = "myth_interiors_ghg_groundlight_ryx",
	init_fn  = function(inst) myth_interiors_ghg_groundlight_init_fn(inst,255,205,115)  end,
})

MakeItemSkin("myth_interiors_ghg_groundlight","myth_interiors_ghg_groundlight_qzh",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_GROUNDLIGHT_QZH,
    atlas = "images/inventoryimages/myth_interiors_ghg_groundlight_qzh.xml",
    image = "myth_interiors_ghg_groundlight_qzh",
	init_fn  = function(inst) myth_interiors_ghg_groundlight_init_fn(inst,255,240,144)  end,
})

MakeItemSkin("myth_interiors_ghg_groundlight","myth_interiors_ghg_groundlight_llt",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_GROUNDLIGHT_LLT,
    atlas = "images/inventoryimages/myth_interiors_ghg_groundlight_llt.xml",
    image = "myth_interiors_ghg_groundlight_llt",
	init_fn  = function(inst) myth_interiors_ghg_groundlight_init_fn(inst,255,219,111)  end,
})

MakeItemSkin("myth_interiors_ghg_groundlight","myth_interiors_ghg_groundlight_bgz",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_GROUNDLIGHT_BGZ,
    atlas = "images/inventoryimages/myth_interiors_ghg_groundlight_bgz.xml",
    image = "myth_interiors_ghg_groundlight_bgz",
	init_fn  = function(inst) myth_interiors_ghg_groundlight_init_fn(inst,255,102,169)  end,
})

MakeItemSkin("myth_interiors_ghg_groundlight","myth_interiors_ghg_groundlight_blz",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_GROUNDLIGHT_BLZ,
    atlas = "images/inventoryimages/myth_interiors_ghg_groundlight_blz.xml",
    image = "myth_interiors_ghg_groundlight_blz",
	init_fn  = function(inst) myth_interiors_ghg_groundlight_init_fn(inst,152,255,233)  end,
})

MakeItemSkin("myth_interiors_ghg_groundlight","myth_interiors_ghg_groundlight_gxy",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_GROUNDLIGHT_GXY,
    atlas = "images/inventoryimages/myth_interiors_ghg_groundlight_gxy.xml",
    image = "myth_interiors_ghg_groundlight_gxy",
	init_fn  = function(inst) myth_interiors_ghg_groundlight_init_fn(inst,255,176,2)  end,
})

MakeItemSkin("myth_interiors_ghg_groundlight","myth_interiors_ghg_groundlight_yhy",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_GROUNDLIGHT_YHY,
    atlas = "images/inventoryimages/myth_interiors_ghg_groundlight_yhy.xml",
    image = "myth_interiors_ghg_groundlight_yhy",
	init_fn  = function(inst) myth_interiors_ghg_groundlight_init_fn(inst,210,59,221,true)  end,
})
--翠镶红， 蛛设织
MakeItemSkin("myth_interiors_ghg_groundlight","myth_interiors_ghg_groundlight_crh",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_GROUNDLIGHT_CXH,
    atlas = "images/inventoryimages/myth_interiors_ghg_groundlight_crh.xml",
    image = "myth_interiors_ghg_groundlight_crh",
	init_fn  = function(inst) myth_interiors_ghg_groundlight_init_fn(inst,255,183,99)  end,
})

MakeItemSkin("myth_interiors_ghg_groundlight","myth_interiors_ghg_groundlight_zsz",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_SKIN_GROUNDLIGHT_ZSZ,
    atlas = "images/inventoryimages/myth_interiors_ghg_groundlight_zsz.xml",
    image = "myth_interiors_ghg_groundlight_zsz",
	init_fn  = function(inst) myth_interiors_ghg_groundlight_init_fn(inst,252,123,144)  end,
})

--=============================栅栏
MakeItemSkin("fence_bamboo_item","fence_bamboo_item_decolored",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_BOOINFO.ITEM_SKIN.fence_bamboo_item1,
    atlas = "images/inventoryimages/fence_bamboo_item_decolored.xml",
    image = "fence_bamboo_item_decolored",
	init_fn  = function(inst,build_name) 
		local build_name = string.gsub(build_name, "_item", "")
		inst.linked_skinname = build_name
		inst.AnimState:SetSkin(build_name, "fence_bamboo")
	end,
})

MakeItemSkin("fence_bamboo","fence_bamboo_decolored",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_BOOINFO.ITEM_SKIN.fence_bamboo_item1,
    atlas = "images/inventoryimages/fence_bamboo_item_decolored.xml",
    image = "fence_bamboo_item_decolored",
	init_fn  = function(inst,build_name)
	    inst.linked_skinname = build_name
    	inst.AnimState:SetSkin(build_name, "fence_bamboo")
	end,
})


MakeItemSkin("fence_gate_bamboo_item","fence_gate_bamboo_item_decolored",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_BOOINFO.ITEM_SKIN.fence_bamboo_item1,
    atlas = "images/inventoryimages/fence_gate_bamboo_item_decolored.xml",
    image = "fence_gate_bamboo_item_decolored",
	init_fn  = function(inst,build_name) 
		local build_name = string.gsub(build_name, "_item", "")
		inst.linked_skinname = build_name
		inst.AnimState:SetSkin(build_name, "fence_gate_bamboo")
	end,
})

MakeItemSkin("fence_gate_bamboo","fence_gate_bamboo_decolored",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_BOOINFO.ITEM_SKIN.fence_bamboo_item1,
    atlas = "images/inventoryimages/fence_gate_bamboo_item_decolored.xml",
    image = "fence_gate_bamboo_item_decolored",
	init_fn  = function(inst,build_name)
		if inst.components.placer == nil and not TheWorld.ismastersim then
			return
		end
		inst.dooranim.skin_id = inst.skin_id
		inst.dooranim.AnimState:SetSkin(build_name, "fence_gate_bamboo")
	end,
})

---================红木桌子
MakeItemSkin("myth_food_table","myth_food_table_star",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_BOOINFO.ITEM_SKIN.myth_stool1,
    atlas = "images/inventoryimages/myth_food_table_star.xml",
    image = "myth_food_table_star",
	init_fn  = function(inst,build_name)
    	inst.AnimState:SetBank(build_name)
	end,
})
MakeItemSkin("myth_food_table","myth_food_table_stone",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_BOOINFO.ITEM_SKIN.myth_stool2,
    atlas = "images/inventoryimages/myth_food_table_stone.xml",
    image = "myth_food_table_stone",
	init_fn  = function(inst,build_name)
    	inst.AnimState:SetBank(build_name)
	end,
})
---====================================

MakeItemSkin("myth_redlantern_ground","myth_redlantern_ground_wood",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_BOOINFO.ITEM_SKIN.myth_redlantern_ground1,
	bank = "myth_redlantern_ground_wood",
    atlas = "images/inventoryimages/myth_redlantern_ground_wood.xml",
    image = "myth_redlantern_ground_wood",
	--init_fn  = function(inst,build_name)
    --	inst.AnimState:SetBank(build_name)
	--end,
})

--local names = {"七星","墨玉","玄冥","卧熊","虎啸","凤台"}
local stoolskins = {"star","stone","bigstone","bear","tiger","golden"}
for i,v in ipairs(stoolskins) do
	MakeItemSkin("myth_stool","myth_stool_"..v,
	{
		rarity = "Loyal",
		type = "item",
		name = STRINGS.MYTH_BOOINFO.ITEM_SKIN["myth_stool"..i],
		nochangebuild = true,
		atlas = "images/inventoryimages/myth_stool_"..v..".xml",
		image = "myth_stool_"..v,
		init_fn  = function(inst,build_name)
			inst.AnimState:OverrideSymbol("wood", "myth_stool", v)
			inst.SitSymbol = v
		end,
	})
end
--=============================战旗
MakeItemSkin("mk_battle_flag_item","mk_battle_flag_item_golden",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_BOOINFO.ITEM_SKIN.mk_battle_flag1,
    atlas = "images/inventoryimages/mk_battle_flag_item_golden.xml",
    image = "mk_battle_flag_item_golden",
	init_fn  = function(inst,build_name) 
		local build_name = string.gsub(build_name, "_item", "")
		inst.linked_skinname = build_name
		inst.AnimState:SetSkin("mk_battle_flag_golden", "mk_battle_flag")
	end,
})

MakeItemSkin("mk_battle_flag","mk_battle_flag_golden",
{
	rarity = "Loyal",
	type = "item",
	name = STRINGS.MYTH_BOOINFO.ITEM_SKIN.mk_battle_flag1,
    atlas = "images/inventoryimages/mk_battle_flag_item_golden.xml",
    image = "mk_battle_flag_item_golden",
	init_fn  = function(inst,build_name)
	    inst.linked_skinname = build_name
    	inst.AnimState:SetSkin(build_name, "mk_battle_flag")
	end,
})