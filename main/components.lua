
AddComponentPostInit("hunger", function(self) --月饼不掉饥饿
	local oldDoDelta = self.DoDelta
	function self:DoDelta(...)
		if self.myth_mooncake_nuts then 
			return
		end
		return oldDoDelta(self,...)
	end
end)


AddComponentPostInit("mightiness", function(self)
	local oldDoDelta = self.DoDelta
	function self:DoDelta(delta,...)
		if delta < 0 and self.inst:HasTag("movemountain_pill_buff") then
			return
		end
		return oldDoDelta(self,delta,...)
	end
end)

AddComponentPostInit("health", function(self)
	if not self.inst.components.myth_hassoul then
		self.inst:AddComponent("myth_hassoul")
	end
end)

local function getfacing(doer)
	local angle  =  doer.Transform:GetRotation()
	return  math.floor(angle/90 +0.5 )*90
end

AddComponentPostInit("farmtiller", function(self)
	local oldTill = self.Till
	function self:Till(pt, doer,...)
		local old = oldTill(self,pt, doer,...)
		if old then
			if math.random() < 0.05 then
				local item = SpawnPrefab("myth_coin")
				item.Transform:SetPosition(pt:Get())
				if item.components.inventoryitem ~= nil and item.components.inventoryitem.ondropfn ~= nil then
					item.components.inventoryitem.ondropfn(item)
				end
			end
			if doer and doer.prefab == "pigsy" and  doer.components.hunger.current >= 250   then
				self.inst:DoTaskInTime(0.3,function()
					for k = -90,90,180 do
						local facing_angle = (getfacing(doer)+k) * DEGREES
						local newx = pt.x + 1.5 * math.cos(facing_angle)
						local new_z = pt.z - 1.5* math.sin(facing_angle)
						if TheWorld.Map:CanTillSoilAtPoint(newx, 0, new_z, false) then
							TheWorld.Map:CollapseSoilAtPoint(newx, 0, new_z, false)
							SpawnPrefab("farm_soil").Transform:SetPosition(newx, 0, new_z)
						end
					end
				end)
			end
		end
		return old
	end
end)

AddComponentPostInit("sanity", function(self)--月饼不掉脑残
	local oldDoDelta = self.DoDelta
	function self:DoDelta(...)
		if self.myth_mooncake_ice and not self.yj_change then 
			return
		end
		return oldDoDelta(self,...)
	end

	local old_RecalculatePenalty = self.RecalculatePenalty
	function self:RecalculatePenalty(...)
		local icemooncake = false
		if self.myth_mooncake_ice then 
			icemooncake = true
			self.myth_mooncake_ice = false
		end
		old_RecalculatePenalty(self,...)
		if icemooncake then
			self.myth_mooncake_ice = true
		end
	end
end)

AddComponentPostInit("moisture", function(self)--避水
	local oldGetWaterproofInventory= self.GetWaterproofInventory
	function self:GetWaterproofInventory(...)
		if self.myth_waterproofInventory  then 
			return true
		end
		return oldGetWaterproofInventory(self,...)
	end
end)

AddComponentPostInit("groomer", function(self)
	local oldBeginChanging= self.BeginChanging
	function self:BeginChanging(doer,...)
		if self.inst.components.hitcher ~= nil and self.inst.components.hitcher.hitched ~= nil 
			and  self.inst.components.hitcher.hitched:HasTag("laozi_qingniu") then 
			if doer and doer.components.talker then
				doer.components.talker:Say(STRINGS.CHARACTERS.WILLOW.ACTIONFAIL_GENERIC)
			end
			return false
		end
		return oldBeginChanging(self,doer,...)
	end
end)

AddComponentPostInit("edible", function(self)
	local old_GetSanity = self.GetSanity
	function self:GetSanity(eater,...)
		local old = old_GetSanity(self,eater,...)
		if eater then
			if old > 0 and eater.myth_delicious_buff then --美味buff
				old = 1.5 * old
			end
			if old < 0 and (eater.myth_mooncake_lotus or eater.prefab == "pigsy") then
				old = 0
			end
		end
		return old
	end
	local old_GetHunger = self.GetHunger
	function self:GetHunger(eater,...)
		local old = old_GetHunger(self,eater,...)
		if eater then
			if old > 0 and eater.myth_appetite_buff then --开胃buff
				old = 1.5 * old
			end	
			--人物专属
			if (eater.prefab == "pigsy" or eater.prefab == "monkey_king") and self.foodtype == FOODTYPE.MEAT then 
				old = old *(self.inst:HasTag("preparedfood") and 0.8 or 0.2)
			end
			if old < 0 and eater.myth_mooncake_lotus then
				old = 0
			end
		end
		return old
	end

	local old_GetHealth = self.GetHealth
	function self:GetHealth(eater,...)
		local old = old_GetHealth(self,eater,...)
		if eater  then
			if old > 0 and eater.myth_nutritional_buff then --营养buff
				old = 1.5 * old
			end	
			if old < 0 and eater.myth_mooncake_lotus then
				old = 0
			end
		end
		return old
	end
end)


--不可以传送大蛤蟆
AddPrefabPostInit("telestaff", function(inst)
	if inst.components.spellcaster then
		local old = inst.components.spellcaster.spell
		inst.components.spellcaster.spell = function(inst, target,...)
			if  target and target.prefab == "myth_goldfrog" then
				return false
			end
			if old ~= nil then
				return old(inst, target,...)
			end
		end
	end
end)

AddComponentPostInit("equippable", function(self) 
	local oldGetWalkSpeedMult = self.GetWalkSpeedMult
	function self:GetWalkSpeedMult(...)
		local old = oldGetWalkSpeedMult(self,...)
		local owner =  self.inst.components.inventoryitem and self.inst.components.inventoryitem.owner or nil
		if owner and owner:HasTag("movemountain_pill_buff") then
			return math.max(1,old)
		end
		return old
	end
end)


AddComponentPostInit("temperature", function(self) 
	local oldGetInsulation = self.GetInsulation
	function self:GetInsulation(...)
		local winterInsulation , summerInsulation = oldGetInsulation(self,...)
		if self.myth_food_tsj_buff or self.myth_warm_buff then
			return winterInsulation + 120 ,summerInsulation
		end
		return winterInsulation , summerInsulation
	end
end)


AddComponentPostInit("inventory", function(self) 
	local old_IsHeavyLifting = self.IsHeavyLifting
	function self:IsHeavyLifting(...)
		if self.inst:HasTag("movemountain_pill_buff") then
			return false
		end
		return old_IsHeavyLifting(self,...)
	end
end)

AddClassPostConstruct( "components/inventory_replica", function(self, inst)
	local old_IsHeavyLifting = self.IsHeavyLifting
	function self:IsHeavyLifting(...)
		if self.inst:HasTag("movemountain_pill_buff") then
			return false
		end
		return old_IsHeavyLifting(self,...)
	end
end)

AddClassPostConstruct( "components/inventoryitem_replica", function(self, inst)
	local old_GetWalkSpeedMult = self.GetWalkSpeedMult
	function self:GetWalkSpeedMult(...)
		local old = old_GetWalkSpeedMult(self,...)
		if ThePlayer and self:IsGrandOwner(ThePlayer) and ThePlayer:HasTag("movemountain_pill_buff")  then
			return math.max(1,old)
		end
		return old
	end
end)

AddComponentPostInit("armor", function(self)
	local oldTakeDamage = self.TakeDamage
	function self:TakeDamage(damage_amount,...)
		local owner = self.inst.components.inventoryitem and self.inst.components.inventoryitem.owner or nil
		if owner and owner.components.inventory and owner.components.inventory:ArmorHasTag("siving_hat") then
			damage_amount = damage_amount *0.75
		end
		oldTakeDamage(self,damage_amount,...)
	end
end)

AddComponentPostInit("weapon", function(self1)
	local attackwearmultipliers = self1.attackwearmultipliers
	local old_Get = attackwearmultipliers.Get
	function attackwearmultipliers:Get()
		local old = old_Get(self)
		local owner = self1.inst.components.inventoryitem and self1.inst.components.inventoryitem.owner or nil
		if owner and owner.components.inventory and owner.components.inventory:ArmorHasTag("siving_hat") then
			old = old *0.75
		end
		return old
	end
end)

local shadow_sanity = {
	"crawlinghorror","terrorbeak","shadow_knight","shadow_rook","shadow_bishop","crawlingnightmare","nightmarebeak",
	"stalker","stalker_forest","stalker_atrium","oceanhorror",
}

for _, v in ipairs(shadow_sanity) do
	AddPrefabPostInit(v,function(inst)
		inst:AddTag("myth_sving_sanity")
	end)
end

local IsFlying = function(inst) return inst and inst.components.mk_flyer and inst.components.mk_flyer:IsFlying()end

AddComponentPostInit("combat",function(self)
	local old_SetRetargetFunction = self.SetRetargetFunction
	function self:SetRetargetFunction(...)
		old_SetRetargetFunction(self,...)
		if self.targetfn ~= nil then
			local old_targetfn = self.targetfn
			self.targetfn = function(...)
				local newtarget, forcechange = old_targetfn(...)
				if newtarget then
					if self.inst.inwbfog and newtarget.prefab == "white_bone" then --白骨在雾气里面无法被发现
						newtarget = nil
					elseif newtarget.fly_hiding then --白骨隐身buff
						newtarget = nil
					elseif newtarget.myth_avoidtarget_buff then --有特殊的buff
						newtarget = nil
					end
				end
				return newtarget, forcechange
			end
		end
	end
	local old_ShareTarget = self.ShareTarget
	function self:ShareTarget(target,...)
		if self.inst.inwbfog and target and target.prefab == "white_bone" then
			return
		end
		old_ShareTarget(self,target,...)
	end
end)

AddClassPostConstruct( "components/combat_replica", function(self, inst)
	local old_IsValidTarget = self.IsValidTarget
	function self:IsValidTarget(target)
		--幽冥状态得无常无法被当成目标
		if target and target.prefab == "yama_commissioners" and IsFlying(target) and not TheWorld.state.isday then
			return false
		end
		return old_IsValidTarget(self,target)
	end
end)

local foodvalue = {
	"healthabsorption","hungerabsorption","sanityabsorption",
}
--双倍吃 并且不消耗食物 打死纠结鱼
AddComponentPostInit("eater", function(self)
	local old_Eat = self.Eat
	function self:Eat(food,...)
		local double = nil
		if self:PrefersToEat(food) and self.inst.components.inventory and self.inst.components.inventory:EquipHasTag("myth_nianhat") and math.random() < 0.25 then
			--能吃并且触发了概率
			local new = SpawnSaveRecord(food:GetSaveRecord())
			if new then
				if not self.eatwholestack and new.components.stackable ~= nil then --只要一个
					new.components.stackable:SetStackSize(1)
				end
				double = {}
				for _,v in ipairs(foodvalue) do
					table.insert(double,self[v])
					self[v] = self[v] * 2 --双倍
				end
				food = new --吃我的！
			end
		end
		local old = old_Eat(self,food,...)
		if double ~= nil then --还原倍率
			self:SetAbsorptionModifiers(unpack(double))
		end
		return old
	end
end)

