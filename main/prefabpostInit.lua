AddPrefabPostInit("firecrackers", function(inst)
    --inst:AddTag("myth_nianweidu")
end)

AddPrefabPostInit(
    "frog",
    function(inst)
        if inst.components.combat and inst.components.combat.targetfn then
            local old_targetfn = inst.components.combat.targetfn
            inst.components.combat.targetfn = function(inst, ...)
                local old_target,forcechange =  old_targetfn(inst, ...)
                if old_target ~= nil and old_target.prefab == "myth_siving_boss" then
                    return nil,false
                end
            end
        end
end)

local need = {
	laozi_bell = true,
}

local function Container_WithTag(self,tag)
    local containers = {}
    for i = 1, self.numslots do
        local item = self.slots[i]
        if item ~= nil then
            if need[item.prefab] then
                item:RemoveTag(tag)
            elseif item.components.container ~= nil then
                table.insert(containers, item)
            end
        end
    end
    for i, v in ipairs(containers) do
        Container_WithTag(v.components.container,tag)
    end
end

local function RemoveWithTag(self,tag)
    local containers = {}

    if self.activeitem ~= nil then
        if need[self.activeitem.prefab] then
            self.activeitem:RemoveTag(tag)
        elseif self.activeitem.components.container ~= nil then
            table.insert(containers, self.activeitem)
        end
    end

    for k = 1, self.maxslots do
        local v = self.itemslots[k]
        if v ~= nil then
            if need[v.prefab] then
                v:RemoveTag(tag)
            elseif v.components.container ~= nil then
                table.insert(containers, v)
            end
        end
    end

    for k, v in pairs(self.equipslots) do
        if need[v.prefab] then
            v:RemoveTag(tag)
        elseif v.components.container ~= nil then
            table.insert(containers, v)
        end
    end
    for i, v in ipairs(containers) do
        Container_WithTag(v.components.container,tag)
    end
end
local function onchuansong(inst,data)
    if data and data.player and data.player.components.inventory then
        RemoveWithTag(data.player.components.inventory,"irreplaceable")
    end
end
    
 AddPrefabPostInit("world", function(inst)
    if TheWorld.ismastersim then
        inst:ListenForEvent("ms_playerdespawnandmigrate", onchuansong)
    end
end)

local function ondeath(inst)
	if inst.IsUnchained ~= nil  and not inst:IsUnchained() then
		return
	end
    if TheWorld.state.season == "winter" then
        for i,v in ipairs(AllPlayers) do
            if v and v.components.myth_playernwd then
                v.components.myth_playernwd:DoDelta("winterboss_nwd",10)
            end
        end
    end
end

AddPrefabPostInit("deerclops", function(inst)
    if TheWorld.ismastersim then
        inst:ListenForEvent("death", ondeath)
    end
end)

AddPrefabPostInit("klaus", function(inst)
    if TheWorld.ismastersim then
        inst:ListenForEvent("death", ondeath)
    end
end)

local function onpicked(inst)
    local x,y,z = inst.Transform:GetWorldPosition()
    if math.random() < 0.1 then
        local item = SpawnPrefab("myth_coin")
        item.Transform:SetPosition(x, y, z)
        if item.components.inventoryitem ~= nil and item.components.inventoryitem.ondropfn ~= nil then
            item.components.inventoryitem.ondropfn(item)
        end
    end
end

AddPrefabPostInit("tumbleweed", function(inst)
    if inst.components.pickable then
        local old_pick = inst.components.pickable.onpickedfn
        inst.components.pickable.onpickedfn = function(inst,...)
            onpicked(inst)
            if old_pick then
                return old_pick(inst,...)
            end
        end
    end
end)

--牛肉面buff
AddPrefabPostInit(
    "beefalo",
    function(inst)
        if inst.components.combat and inst.components.combat.targetfn then
            local old_targetfn = inst.components.combat.targetfn
            inst.components.combat.targetfn = function(inst, ...)
                local old_target,forcechange =  old_targetfn(inst, ...)
                if old_target ~= nil and old_target.myth_beefalo_buff then
                    return nil,false
                end
                return old_target,forcechange
            end
        end
        if inst.components.brushable and inst.components.brushable.onbrushfn then
            local old_onbrushfn = inst.components.brushable.onbrushfn
            inst.components.brushable.onbrushfn = function(inst,doer,numprizes)
                old_onbrushfn(inst,doer,numprizes)
                if doer and doer.myth_beefalo_buff then --再来一份
                    if numprizes > 0 and inst.components.domesticatable ~= nil then
                        inst.components.domesticatable:DeltaDomestication(TUNING.BEEFALO_DOMESTICATION_BRUSHED_DOMESTICATION)
                        inst.components.domesticatable:DeltaObedience(TUNING.BEEFALO_DOMESTICATION_BRUSHED_OBEDIENCE)
                    end
                end
            end
        end
    end
)

AddPrefabPostInit("gunpowder", function(inst)
    if inst.components.explosive and inst.components.explosive.onexplodefn then
        local old_onexplodefn = inst.components.explosive.onexplodefn
        inst.components.explosive.onexplodefn= function(inst,...)
            old_onexplodefn(inst,...)
            local x, y, z = inst.Transform:GetWorldPosition()
            for i, v in ipairs(TheSim:FindEntities(x, y, z, TUNING.FIRECRACKERS_STARTLE_RANGE, {"nian"})) do
                v:PushEvent("startle_byexplode", { source = inst })
            end
        end
    end
end)
--猪王再来一份

local function launchitem(item, angle)
    local speed = math.random() * 4 + 2
    angle = (angle + math.random() * 60 - 30) * DEGREES
    item.Physics:SetVel(speed * math.cos(angle), math.random() * 2 + 8, speed * math.sin(angle))
end

local function ontradeforgold(inst, num, giver)
    local x, y, z = inst.Transform:GetWorldPosition()
    y = 4.5

    local angle
    if giver ~= nil and giver:IsValid() then
        angle = 180 - giver:GetAngleToPoint(x, 0, z)
    else
        local down = TheCamera:GetDownVec()
        angle = math.atan2(down.z, down.x) / DEGREES
        giver = nil
    end

    for k = 1, num do
        local nug = SpawnPrefab("goldnugget")
        nug.Transform:SetPosition(x, y, z)
        launchitem(nug, angle)
    end
end

AddPrefabPostInit("pigking", function(inst)
	if inst.components.trader then	
		local old_onaccept = inst.components.trader.onaccept
		inst.components.trader.onaccept = function(inst, giver, item)
			old_onaccept(inst, giver, item)
            if giver and  item.components.tradable ~=  nil and item.components.tradable.goldvalue > 0  then
			    if giver.prefab == "pigsy" and math.random() < 0.2    then --八戒先来
				    inst:DoTaskInTime(2 / 3, ontradeforgold, item.components.tradable.goldvalue, giver)
                elseif  giver.myth_nianbuff_lu and math.random() < 0.3 then
                    local num = math.min(2,math.ceil(math.random(item.components.tradable.goldvalue)))
                    inst:DoTaskInTime(2 / 3, ontradeforgold, num, giver)
                end
			end
		end
	end
end)

local function DigestFood(inst, food)
    if food.components.edible.foodtype == FOODTYPE.MEAT then
        if inst.components.occupiable and inst.components.occupiable:GetOccupant() and inst.components.occupiable:GetOccupant():HasTag("bird_mutant") then
            inst.components.lootdropper:SpawnLootPrefab("rottenegg")
        else
            inst.components.lootdropper:SpawnLootPrefab("bird_egg")
        end
    else
        if inst.components.occupiable and inst.components.occupiable:GetOccupant() and inst.components.occupiable:GetOccupant():HasTag("bird_mutant") then
            inst.components.lootdropper:SpawnLootPrefab("spoiled_food")
        else
            local seed_name = string.lower(food.prefab .. "_seeds")
            if Prefabs[seed_name] ~= nil then
    			inst.components.lootdropper:SpawnLootPrefab(seed_name)
            end
        end
    end
end

AddPrefabPostInit("birdcage", function(inst)
	if inst.components.trader then	
		local old_onaccept = inst.components.trader.onaccept
		inst.components.trader.onaccept = function(inst, giver, item)
			old_onaccept(inst, giver, item)
            if giver and giver.myth_nianbuff_lu and math.random() < 0.3 and
                item.components.edible ~= nil and
                (   item.components.edible.foodtype == FOODTYPE.MEAT
                    or item.prefab == "seeds"
                    or string.match(item.prefab, "_seeds")
                    or Prefabs[string.lower(item.prefab .. "_seeds")] ~= nil
                ) then
                inst:DoTaskInTime(60 * FRAMES, DigestFood, item)
            end
		end
	end
end)

local function TradeItem(inst)

    local item = inst.itemtotrade
    local giver = inst.tradegiver

    local x, y, z = inst.Transform:GetWorldPosition()
    y = 5.5

    local angle
    if giver ~= nil and giver:IsValid() then
        angle = 180 - giver:GetAngleToPoint(x, 0, z)
    else
        local down = TheCamera:GetDownVec()
        angle = math.atan2(down.z, down.x) / DEGREES
        giver = nil
    end

    local selected_index = math.random(1, #inst.trading_items)
    local selected_item = inst.trading_items[selected_index]
    local reward_count = math.min(2,math.random(selected_item.min_count, selected_item.max_count))
    local filler_min = 2
    local filler_max = 4

    for k = 1, reward_count do
        local reward_item = SpawnPrefab(selected_item.prefabs[math.random(1, #selected_item.prefabs)])
        reward_item.Transform:SetPosition(x, y, z)
        launchitem(reward_item, angle)
    end
end

AddPrefabPostInit("mermking", function(inst)
	if inst.TradeItem then	
		local old_TradeItem = inst.TradeItem
		inst.TradeItem = function(inst)
            if inst.tradegiver and inst.tradegiver.myth_nianbuff_lu and math.random() < 0.3 then
                TradeItem(inst)
            end
			old_TradeItem(inst)
		end
	end
end)