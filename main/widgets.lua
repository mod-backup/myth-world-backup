--ui相关得
local UIAnim = require "widgets/uianim"
local FumeOver = require "widgets/myth_fumeover"

local function setnewbuild(self,mythbuild)
    self.inst:DoTaskInTime(mythbuild and 0 or 1,function()
        for _,v in ipairs(self.myth_leafcanopy) do
            if v and v.GetAnimState ~= nil then
                v:GetAnimState():SetBuild(mythbuild and "infantree_leaves_canopy" or "leaves_canopy" )
            end
        end
    end)
end

local SignGenerator = require"signgenerator"
local zhanqi = {
    prompt = STRINGS.SIGNS.MENU.PROMPT,
    animbank = "ui_board_5x3",
    animbuild = "ui_board_5x3",
    menuoffset = Vector3(6, -70, 0),

    cancelbtn = { text = STRINGS.SIGNS.MENU.CANCEL, cb = nil, control = CONTROL_CANCEL },
    middlebtn = { text = STRINGS.SIGNS.MENU.RANDOM, cb = function(inst, doer, widget)
            widget:OverrideText( SignGenerator(inst, doer) )
        end, control = CONTROL_MENU_MISC_2 },
    acceptbtn = { text = STRINGS.SIGNS.MENU.ACCEPT, cb = nil, control = CONTROL_ACCEPT },
}

local writeables = require"writeables"
writeables.AddLayout("mk_battle_flag", zhanqi)

local FlareOver = require "widgets/myth_flareover"

local function AddNewUI(self)
	local old_CreateOverlays = self.CreateOverlays
	function self:CreateOverlays(owner,...)
		old_CreateOverlays(self,owner,...)
        self.myth_fumeover = self.overlayroot:AddChild(FumeOver(owner))
        
        --暂时懒得兼容别的mod 以后再说
        self.myth_leafcanopy = {}
        if self.leafcanopy then
            for i=1,10 do
                for k=1,10 do
                    if self.leafcanopy["leavesTop"..i.."_"..k] ~= nil then
                        table.insert(self.myth_leafcanopy,self.leafcanopy["leavesTop"..i.."_"..k])
                    end
                end
            end 
        end
        self.inst:ListenForEvent("infantreeleavesdirty",function(owner)
            setnewbuild(self,owner.under_infantreeleaves:value())
        end,owner)
        if owner.under_infantreeleaves and owner.under_infantreeleaves:value() then
            setnewbuild(self,owner.under_infantreeleaves:value())
        end

        ---爆竹
        self.myth_flareover = self.overlayroot:AddChild(FlareOver(owner))
	end
end
AddClassPostConstruct("screens/playerhud", AddNewUI)


local shengsibu = require "widgets/myth_shengsibu"
local function ssb(self)
	self.myth_shengsibu = self:AddChild(shengsibu(self.owner))
end
AddClassPostConstruct("widgets/controls", ssb)