--------------------------------------------------------------------------
--[[ 添加交易动作 ]]
--------------------------------------------------------------------------

AddStategraphState("wilson", State{
    name = "bbshop_start",
    tags = { "doing", "pausepredict" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.components.locomotor:Clear()

        inst.AnimState:PlayAnimation("idle_wardrobe1_pre")
        inst.AnimState:PushAnimation("idle_wardrobe1_loop", true)

        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:RemotePausePrediction()
            inst.components.playercontroller:EnableMapControls(false)
            inst.components.playercontroller:Enable(false)
        end
        inst.components.inventory:Hide()
        inst:ShowActions(false)

        inst.task_canshop = inst:DoPeriodicTask(0.5, function() --只要店铺无法开店了，就自动退出
            if inst.bbshop_m ~= nil and inst.bbshop_m.inst:HasTag("shopclosed") then
                inst.sg:GoToState("idle")
            end
        end, 0)
    end,
    timeline = {
        TimeEvent(8*FRAMES, function(inst)
            inst:PerformBufferedAction()
        end)
    },
    events = {
        EventHandler("ms_closepopup", function(inst, data)
            if data.popup == POPUPS.BAMBOOSHOP then
                inst.sg:GoToState("idle")
            end
        end)
    },
    onexit = function(inst)
        if inst.bbshop_m ~= nil then
            inst.bbshop_m:CloseTradeDialog()
        end

        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:EnableMapControls(true)
            inst.components.playercontroller:Enable(true)
        end
        inst.components.inventory:Show()
        inst:ShowActions(true)

        if inst.task_canshop ~= nil then
            inst.task_canshop:Cancel()
            inst.task_canshop = nil
        end
    end,
})

local TRADE_BBSHOP_M = Action({ priority = -1, encumbered_valid = true, })
TRADE_BBSHOP_M.id = "TRADE_BBSHOP_M"
TRADE_BBSHOP_M.str = STRINGS.ACTIONS.TRADE_BBSHOP_M
TRADE_BBSHOP_M.fn = function(act)
    if
        act.doer ~= nil and act.target ~= nil and
        act.target.components.bambooshop_myth ~= nil
    then
        local success, reason = act.target.components.bambooshop_myth:CanTrade(act.doer)
        if not success then
            act.doer.sg:GoToState("idle")
            return false, reason
        end

        act.target.components.bambooshop_myth:OpenTradeDialog(act.doer)
        return true
    end
end
AddAction(TRADE_BBSHOP_M)

AddComponentAction("SCENE", "bambooshop_myth", function(inst, doer, actions, right)
    if not inst:HasTag("shopclosed") then
        table.insert(actions, ACTIONS.TRADE_BBSHOP_M)
    end
end)

AddStategraphActionHandler("wilson", ActionHandler(ACTIONS.TRADE_BBSHOP_M, "bbshop_start"))

--------------------------------------------------------------------------
--[[ rpc交互 ]]
--------------------------------------------------------------------------

AddClientModRPCHandler("BBShop_m", "handleShop", function(handletype, data, ...)
    if handletype and type(handletype) == "number" then
        if handletype == 1 then --初始化店铺
            if data and type(data) == "string" then
                local success, result = pcall(json.decode, data)
                if result and ThePlayer and ThePlayer.HUD and ThePlayer.HUD.bambooshopdialog then
                    ThePlayer.HUD.bambooshopdialog:InitShop(result)
                end
            end
        elseif handletype == 2 then --让店铺npc说话
            if data and type(data) == "string" then
                local success, result = pcall(json.decode, data)
                if result and ThePlayer and ThePlayer.HUD and ThePlayer.HUD.bambooshopdialog then
                    if result.treasure == nil then
                        ThePlayer.HUD.bambooshopdialog:NpcSay(result.words, 3, result.anim or "anim_error", true)
                    else
                        --宝藏线索话语设置
                        ThePlayer.HUD.bambooshopdialog:GetClueWords(result.treasure)
                        ThePlayer.HUD.bambooshopdialog:NpcSay(
                            ThePlayer.HUD.bambooshopdialog.cluewords or result.words,
                            3, result.anim or "anim_error", true)
                    end
                end
            end
        end
    end
end)

AddModRPCHandler("BBShop_m", "handleTrade", function(player, handletype, data, data2, ...)
    if handletype and type(handletype) == "number" then
        if handletype == 1 then --玩家买东西
            if player and player.bbshop_m ~= nil and data and type(data) == "string" then
                local success, result = pcall(json.decode, data)
                if result then
                    player.bbshop_m:Buy(player, result)
                end
            end
        elseif handletype == 2 then --玩家卖东西
            if player and player.bbshop_m ~= nil and data and type(data) == "string" then
                local success, result = pcall(json.decode, data)
                if result then
                    player.bbshop_m:Sell(player, result)
                end
            end
        elseif handletype == 3 then --店铺的特殊功能
            if player and player.bbshop_m ~= nil and data and type(data) == "string" then
                local success, result = pcall(json.decode, data)
                if result then
                    player.bbshop_m:Serve(player, result, data2)
                end
            end
        end
    end
end)

--------------------------------------------------------------------------
--[[ 全局店铺数据管理API（给其他mod用的） ]]
--------------------------------------------------------------------------

function GLOBAL.AddBambooShopItems(shoptype, datalist)
    if BBSHOPDATA ~= nil and BBSHOPDATA[shoptype] ~= nil then
        if
            shoptype == "rareitem" or shoptype == "ingredient" or shoptype == "animals" or shoptype == "plants"
            or shoptype == "foods" or shoptype == "weapons" or shoptype == "numerology" or shoptype == "construct"
        then
            for k,v in pairs(datalist) do --!!!关于数据规范请看下面的数据注释
                BBSHOPDATA[shoptype][k] = v
            end
        end
    end
end

--------------------------------------------------------------------------
--[[ 全局店铺数据 ]]
--------------------------------------------------------------------------

--控制全局店铺npc产生的缓存
if TheNet:GetIsServer() or TheNet:IsDedicated() then --非客户端环境（主机或者云服）（其实也可以放theworld里）
    GLOBAL.BSHOPSTATE = {
        npc = { --当前世界存在的店铺NPC数量
            rareitem = 0,
            ingredient = 0,
            animals = 0,
            plants = 0,
            foods = 0,
            weapons = 0,
            numerology = 0,
            construct = 0,
        },
        shops = { --所有店铺的存在情况
            rareitem = {
                num = 0, -- 该店铺已经存在的数量
            },
            ingredient = { num = 0, },
            animals = { num = 0, },
            plants = { num = 0, },
            foods = { num = 0, },
            weapons = { num = 0, },
            numerology = { num = 0, },
            construct = { num = 0, },
        },
    }
end

--往官方全局表插入建造中店铺所需材料的数据
CONSTRUCTION_PLANS["myth_store_construction"] = {
    Ingredient("myth_bamboo", 6), Ingredient("boards", 2), Ingredient("papyrus", 6), Ingredient("rope", 4)
}

local chancemap = { 1, 3, 7, 10, 15 }

GLOBAL.BBSHOPDATA = {
    rareitem = {
        redgem = { --红宝石
            img_tex = nil, img_atlas = nil,
            buy = { --玩家买
                value = 10,             --价格。为空时代表不能被摆上货架，不为空时不能小于1
                chance = chancemap[4],  --摆上货架几率的权重(为空则不参与计算)
                count_min = 2,          --摆在货架上随机数量的最小值。不能小于1。和最大值相等时代表固定该数量
                count_max = 4,          --摆在货架上随机数量的最大值。必须不小于最小值
                stacksize = 20,         --最大摆放数量。不能小于1
                num_mix = nil,          --买到后，给予玩家的数量。为空则默认1，不能为0
            },
            sell = { --玩家卖
                value = 5,              --价格。为空时代表不能加入订购单，不为空时不能小于1
                chance = chancemap[4],  --加入订购单几率的权重(为空则不参与计算)
                count_min = 3,          --加入订购单随机数量的最小值。不能小于1。和最大值相等时代表固定该数量
                count_max = 6,         --加入订购单随机数量的最大值。必须不小于最小值
                stacksize = 20,         --最大订购数量。不能小于1
                num_mix = nil,          --需要达到这个数量，才能卖一次。为空则默认1，不能为0
            },
            nameoverride = nil, --名字覆盖（这样用的 STRINGS.NAMES[string.upper(item.nameoverride)]）
            desc = nil, --覆盖点击时npc会说的话
        },
        bluegem = { --蓝宝石
            buy = { value = 10, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 20, },
            sell = { value = 5, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
        },
        orangegem = { --橙宝石
            buy = { value = 40, chance = chancemap[3], count_min = 1, count_max = 3, stacksize = 20, },
            sell = { value = 20, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 20, },
        },
        yellowgem = { --黄宝石
            buy = { value = 40, chance = chancemap[3], count_min = 1, count_max = 3, stacksize = 20, },
            sell = { value = 20, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 20, },
        },
        greengem = { --绿宝石
            buy = { value = 40, chance = chancemap[3], count_min = 1, count_max = 3, stacksize = 20, },
            sell = { value = 20, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 20, },
        },
        purplegem = { --紫宝石
            buy = { value = 40, chance = chancemap[3], count_min = 1, count_max = 3, stacksize = 20, },
            sell = { value = 20, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 20, },
        },
        opalpreciousgem = { --彩红宝石
            buy = { value = 320, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
            sell = { value = 160, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
        },
        myth_toy_featherbundle = { --神话玩具：毽子
            img_tex = "myth_toy_featherbundle.tex", img_atlas = "images/inventoryimages/myth_toy_featherbundle.xml",
            buy = { value = nil, chance = chancemap[5], count_min = 5, count_max = 10, stacksize = 40, },
            sell = { value = 2, chance = chancemap[5], count_min = 5, count_max = 12, stacksize = 40, },
        },
        myth_toy_tigerdoll = { --神话玩具：布老虎
            img_tex = "myth_toy_tigerdoll.tex", img_atlas = "images/inventoryimages/myth_toy_tigerdoll.xml",
            buy = { value = nil, chance = chancemap[5], count_min = 5, count_max = 10, stacksize = 40, },
            sell = { value = 2, chance = chancemap[5], count_min = 5, count_max = 12, stacksize = 40, },
        },
        myth_toy_tumbler = { --神话玩具：土地不倒翁
            img_tex = "myth_toy_tumbler.tex", img_atlas = "images/inventoryimages/myth_toy_tumbler.xml",
            buy = { value = nil, chance = chancemap[5], count_min = 5, count_max = 10, stacksize = 40, },
            sell = { value = 2, chance = chancemap[5], count_min = 5, count_max = 12, stacksize = 40, },
        },
        myth_toy_twirldrum = { --神话玩具：拨浪鼓
            img_tex = "myth_toy_twirldrum.tex", img_atlas = "images/inventoryimages/myth_toy_twirldrum.xml",
            buy = { value = nil, chance = chancemap[5], count_min = 5, count_max = 10, stacksize = 40, },
            sell = { value = 2, chance = chancemap[5], count_min = 5, count_max = 12, stacksize = 40, },
        },
        myth_toy_chineseknot = { --神话玩具：中国结
            img_tex = "myth_toy_chineseknot.tex", img_atlas = "images/inventoryimages/myth_toy_chineseknot.xml",
            buy = { value = nil, chance = chancemap[5], count_min = 5, count_max = 10, stacksize = 40, },
            sell = { value = 2, chance = chancemap[5], count_min = 5, count_max = 12, stacksize = 40, },
        },
        amulet = { --生命护符
            buy = { value = 20, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 10, },
            sell = { value = 10, chance = chancemap[4], count_min = 1, count_max = 3, stacksize = 10, },
        },
        blueamulet = { --寒冰护符
            buy = { value = 20, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 10, },
            sell = { value = 10, chance = chancemap[4], count_min = 1, count_max = 3, stacksize = 10, },
        },
        purpleamulet = { --梦魇护符
            buy = { value = 80, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 10, },
            sell = { value = 40, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 10, },
        },
        orangeamulet = { --懒惰护符
            buy = { value = 80, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 10, },
            sell = { value = 40, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 10, },
        },
        yellowamulet = { --发光护符
            buy = { value = 80, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 10, },
            sell = { value = 40, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 10, },
        },
        greenamulet = { --建造护符
            buy = { value = 80, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 10, },
            sell = { value = 40, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 10, },
        },
        myth_rhino_redheart = { --犀牛心脏：红
            img_tex = "myth_rhino_redheart.tex", img_atlas = "images/inventoryimages/myth_rhino_redheart.xml",
            buy = { value = nil, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
            sell = { value = 600, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
        },
        myth_rhino_yellowheart = { --犀牛心脏：黄
            img_tex = "myth_rhino_yellowheart.tex", img_atlas = "images/inventoryimages/myth_rhino_yellowheart.xml",
            buy = { value = nil, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
            sell = { value = 600, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
        },
        myth_rhino_blueheart = { --犀牛心脏：蓝
            img_tex = "myth_rhino_blueheart.tex", img_atlas = "images/inventoryimages/myth_rhino_blueheart.xml",
            buy = { value = nil, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
            sell = { value = 600, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
        },
        minotaurhorn = { --犀牛角
            buy = { value = nil, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
            sell = { value = 300, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
        },
        goose_feather = { --鹅毛
            buy = { value = 10, chance = chancemap[3], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = 5, chance = chancemap[3], count_min = 4, count_max = 7, stacksize = 20, },
        },
        bearger_fur = { --熊皮
            buy = { value = 120, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 40, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
        },
        dragon_scales = { --龙鳞
            buy = { value = 120, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 40, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
        },
        deerclops_eyeball = { --巨鹿眼球
            buy = { value = 120, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 80, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
        },
        spiderhat = { --蜘蛛帽
            buy = { value = 60, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 10, },
            sell = { value = 40, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 10, },
        },
        hivehat = { --蜂后帽
            buy = { value = 120, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 80, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
        },
        shroom_skin = { --蛤蟆皮
            buy = { value = 120, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
            sell = { value = 80, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
        },
        malbatross_beak = { --邪天翁喙
            buy = { value = 120, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 80, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
        },
        gnarwail_horn = { --独角鲸角
            buy = { value = 80, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 40, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 5, },
        },
        singingshell_octave3 = { --贝壳钟1
            img_tex = "singingshell_octave3_1.tex", img_atlas = nil,
            buy = { value = nil, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = 5, chance = chancemap[5], count_min = 4, count_max = 7, stacksize = 20, },
        },
        singingshell_octave4 = { --贝壳钟2
            img_tex = "singingshell_octave4_1.tex", img_atlas = nil,
            buy = { value = nil, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = 5, chance = chancemap[5], count_min = 4, count_max = 7, stacksize = 20, },
        },
        singingshell_octave5 = { --贝壳钟3
            img_tex = "singingshell_octave5_1.tex", img_atlas = nil,
            buy = { value = nil, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = 5, chance = chancemap[5], count_min = 4, count_max = 7, stacksize = 20, },
        },
        deer_antler1 = { --鹿角1
            nameoverride = "deer_antler",
            buy = { value = 20, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 2, count_max = 3, stacksize = 5, },
        },
        deer_antler2 = { --鹿角2
            nameoverride = "deer_antler",
            buy = { value = 20, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 2, count_max = 3, stacksize = 5, },
        },
        deer_antler3 = { --鹿角3
            nameoverride = "deer_antler",
            buy = { value = 20, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 2, count_max = 3, stacksize = 5, },
        },
        walrus_tusk = { --海象牙
            buy = { value = 60, chance = chancemap[4], count_min = 1, count_max = 3, stacksize = 10, },
            sell = { value = 40, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 10, },
        },
        lightninggoathorn = { --电羊角
            buy = { value = 20, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 10, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 10, },
        },
        steelwool = { --钢羊毛
            buy = { value = 80, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 40, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 10, },
        },
        tentaclespots = { --触手皮
            buy = { value = 40, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 20, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 10, },
        },
        siving_rocks = { --子圭石
            img_tex = "siving_rocks.tex", img_atlas = "images/inventoryimages/siving_rocks.xml",
            buy = { value = 9, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 40, },
            sell = { value = 4, chance = chancemap[4], count_min = 4, count_max = 9, stacksize = 40, },
        },
        slurper_pelt = { --啜食者皮
            buy = { value = 40, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 20, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 10, },
        },
        fossil_piece = { --化石碎片
            buy = { value = 40, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 20, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, },
        },
        myth_greenbamboo = { --苍竹
            img_tex = "myth_greenbamboo.tex", img_atlas = "images/inventoryimages/myth_greenbamboo.xml",
            buy = { value = 15, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 10, },
        },
        townportaltalisman = { --沙之石
            buy = { value = 9, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 20, },
            sell = { value = 3, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 20, },
        },
        miniflare_myth = { --窜天猴（冬季限定）
            img_tex = "miniflare_myth.tex", img_atlas = "images/inventoryimages/miniflare_myth.xml",
            buy = { value = 2, chance = nil, count_min = 3, count_max = 5, stacksize = 30, tag = 1, },
            sell = { value = nil, chance = nil, count_min = 3, count_max = 5, stacksize = 30, },
        },
        firecrackers_myth = { --爆竹（冬季限定）
            img_tex = "firecrackers_myth.tex", img_atlas = "images/inventoryimages/firecrackers_myth.xml",
            buy = { value = 2, chance = nil, count_min = 3, count_max = 5, stacksize = 30, tag = 1, },
            sell = { value = nil, chance = nil, count_min = 3, count_max = 5, stacksize = 30, },
        },
    },
    ingredient = {
        watermelon = { --西瓜
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = 1, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
        },
        dragonfruit = { --火龙果
            buy = { value = 5, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
        },
        durian = { --榴莲
            buy = { value = 4, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 1, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
        },
        pomegranate = { --石榴
            buy = { value = 2, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 1, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
        },
        cave_banana = { --香蕉
            buy = { value = 2, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
            sell = { value = 1, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
        },
        berries = { --浆果
            buy = { value = 1, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 40, },
            sell = { value = nil, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 40, },
        },
        berries_juicy = { --多汁浆果
            buy = { value = 1, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 40, },
            sell = { value = nil, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 40, },
        },
        bird_egg = { --鸡蛋
            buy = { value = 2, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 20, },
            sell = { value = 1, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 20, },
        },
        tallbirdegg = { --高脚鸟蛋
            buy = { value = 6, chance = chancemap[3], count_min = 1, count_max = 3, stacksize = 10, },
            sell = { value = 3, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, },
        },
        potato = { --土豆
            buy = { value = 2, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
            sell = { value = nil, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
        },
        tomato = { --番茄
            img_tex = "quagmire_tomato.tex",
            buy = { value = 2, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
            sell = { value = nil, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
        },
        eggplant = { --茄子
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = 1, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
        },
        pepper = { --辣椒
            buy = { value = 5, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
        },
        corn = { --玉米
            buy = { value = 2, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
            sell = { value = nil, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
        },
        pumpkin = { --南瓜
            buy = { value = 3, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = 1, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
        },
        carrot = { --胡萝卜
            buy = { value = 2, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
            sell = { value = nil, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
        },
        onion = { --洋葱
            img_tex = "quagmire_onion.tex",
            buy = { value = 5, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
        },
        garlic = { --大蒜
            buy = { value = 5, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
        },
        asparagus = { --芦笋
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = 1, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
        },
        gourd = { --葫芦
            img_tex = "gourd.tex", img_atlas = "images/monkey_king_item.xml",
            buy = { value = 10, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 5, chance = chancemap[2], count_min = 3, count_max = 5, stacksize = 20, },
        },
        lotus_root = { --莲藕
            img_tex = "lotus_root.tex", img_atlas = "images/inventoryimages/lotus_root.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
            sell = { value = 2, chance = chancemap[5], count_min = 4, count_max = 6, stacksize = 30, },
        },
        rock_avocado_fruit = { --石果（生的）
            img_tex = "rock_avocado_fruit_rockhard.tex", img_atlas = nil,
            buy = { value = 1, chance = chancemap[4], count_min = 4, count_max = 7, stacksize = 40, },
            sell = { value = nil, chance = chancemap[4], count_min = 4, count_max = 8, stacksize = 40, },
        },
        cutlichen = { --洞穴苔藓
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 20, },
        },
        red_cap = { --红蘑菇
            buy = { value = 1, chance = chancemap[5], count_min = 4, count_max = 7, stacksize = 40, },
            sell = { value = nil, chance = chancemap[5], count_min = 4, count_max = 7, stacksize = 40, },
        },
        green_cap = { --绿蘑菇
            buy = { value = 1, chance = chancemap[5], count_min = 4, count_max = 7, stacksize = 40, },
            sell = { value = nil, chance = chancemap[5], count_min = 4, count_max = 7, stacksize = 40, },
        },
        blue_cap = { --蓝蘑菇
            buy = { value = 1, chance = chancemap[5], count_min = 4, count_max = 7, stacksize = 40, },
            sell = { value = nil, chance = chancemap[5], count_min = 4, count_max = 7, stacksize = 40, },
        },
        moon_cap = { --月亮蘑菇
            buy = { value = 10, chance = chancemap[3], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = nil, chance = chancemap[3], count_min = 3, count_max = 5, stacksize = 20, },
        },
        smallmeat = { --小肉
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 20, },
        },
        meat = { --大肉
            buy = { value = 10, chance = chancemap[4], count_min = 3, count_max = 4, stacksize = 20, },
            sell = { value = 5, chance = chancemap[4], count_min = 3, count_max = 4, stacksize = 20, },
        },
        monstermeat = { --怪物肉
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 20, },
        },
        fishmeat_small = { --小鱼肉
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 20, },
        },
        fishmeat = { --大鱼肉
            buy = { value = 10, chance = chancemap[4], count_min = 3, count_max = 4, stacksize = 20, },
            sell = { value = 5, chance = chancemap[4], count_min = 3, count_max = 4, stacksize = 20, },
        },
        pondeel = { --鳗鱼
            buy = { value = 8, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 4, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        froglegs = { --蛙腿
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 20, },
        },
        drumstick = { --鸡腿
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, },
        },
        batwing = { --蝙蝠翅膀
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, },
        },
        kelp = { --海带
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 30, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 30, },
        },
        barnacle = { --藤壶
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 30, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 30, },
        },
        plantmeat = { --叶肉
            buy = { value = 5, chance = chancemap[4], count_min = 2, count_max = 5, stacksize = 20, },
            sell = { value = 2, chance = chancemap[4], count_min = 2, count_max = 5, stacksize = 20, },
        },
        wormlight = { --发光浆果
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        wormlight_lesser = { --小发光浆果
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 4, stacksize = 10, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 4, stacksize = 10, },
        },
        cactus_meat = { --仙人掌
            buy = { value = 4, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 30, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 30, },
        },
        cactus_flower = { --仙人掌花
            buy = { value = 20, chance = chancemap[3], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = nil, chance = chancemap[3], count_min = 3, count_max = 5, stacksize = 20, },
        },
        butter = { --黄油
            buy = { value = 40, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 5, },
            sell = { value = 20, chance = chancemap[1], count_min = 1, count_max = 2, stacksize = 5, },
        },
        goatmilk = { --电羊奶
            buy = { value = 20, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 10, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        ice = { --冰块
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 40, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 40, },
        },
        pigskin = { --猪皮
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        honeycomb = { --蜂巢
            buy = { value = 20, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 5, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 5, },
        },
    },
    animals = {
        spider = { --蜘蛛
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 15, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 15, },
        },
        spider_warrior = { --蜘蛛战士
            buy = { value = 8, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 3, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 10, },
        },
        spider_dropper = { --穴居悬蛛
            buy = { value = 8, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 3, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        spider_water = { --海黾
            buy = { value = 8, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 3, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        spider_healer = { --护士蜘蛛
            buy = { value = 8, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 3, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        fireflies = { --萤火虫
            buy = { value = 10, chance = chancemap[4], count_min = 3, count_max = 7, stacksize = 20, },
            sell = { value = 5, chance = chancemap[4], count_min = 3, count_max = 7, stacksize = 20, },
        },
        butterfly = { --蝴蝶
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
        },
        moonbutterfly = { --月蛾
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        bee = { --蜜蜂
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
        },
        killerbee = { --杀人蜂
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
        },
        crow = { --乌鸦
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
        },
        robin = { --红雀
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
        },
        robin_winter = { --雪雀
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
        },
        canary = { --金丝雀
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        puffin = { --海鹦
            buy = { value = 6, chance = chancemap[5], count_min = 2, count_max = 5, stacksize = 10, },
            sell = { value = 3, chance = chancemap[5], count_min = 2, count_max = 5, stacksize = 10, },
        },
        rabbit = { --兔子
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
        },
        mole = { --鼹鼠
            buy = { value = 5, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 15, },
        },
        wobster_sheller_land = { --活龙虾
            buy = { value = 20, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 10, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        wobster_moonglass_land = { --活月玻璃龙虾
            buy = { value = 30, chance = chancemap[2], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 18, chance = chancemap[2], count_min = 2, count_max = 4, stacksize = 10, },
        },
        oceanfish_small_7_inv = { --花朵金枪鱼
            buy = { value = 20, chance = chancemap[2], count_min = 2, count_max = 3, stacksize = 10, },
            sell = { value = 10, chance = chancemap[2], count_min = 2, count_max = 3, stacksize = 10, },
        },
        oceanfish_small_8_inv = { --炽热太阳鱼
            buy = { value = 20, chance = chancemap[2], count_min = 2, count_max = 3, stacksize = 10, },
            sell = { value = 10, chance = chancemap[2], count_min = 2, count_max = 3, stacksize = 10, },
        },
        oceanfish_small_6_inv = { --落叶比目鱼
            buy = { value = 20, chance = chancemap[2], count_min = 2, count_max = 3, stacksize = 10, },
            sell = { value = 10, chance = chancemap[2], count_min = 2, count_max = 3, stacksize = 10, },
        },
        oceanfish_medium_8_inv = { --冰鲷鱼
            buy = { value = 20, chance = chancemap[2], count_min = 2, count_max = 3, stacksize = 10, },
            sell = { value = 10, chance = chancemap[2], count_min = 2, count_max = 3, stacksize = 10, },
        },
        oceanfish_small_1_inv = { --小孔雀鱼
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
        },
        oceanfish_small_2_inv = { --针鼻喷墨鱼
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
        },
        oceanfish_small_3_inv = { --小饵鱼
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
        },
        oceanfish_small_4_inv = { --三文鱼苗
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
        },
        oceanfish_small_5_inv = { --爆米花鱼
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
        },
        oceanfish_small_9_inv = { --口水鱼
            buy = { value = 5, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
            sell = { value = 2, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
        },
        oceanfish_medium_1_inv = { --泥鱼
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
        },
        oceanfish_medium_2_inv = { --斑鱼
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
        },
        oceanfish_medium_3_inv = { --浮夸狮子鱼
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
        },
        oceanfish_medium_4_inv = { --黑鲶鱼
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
        },
        oceanfish_medium_5_inv = { --玉米鳕鱼
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
        },
        oceanfish_medium_6_inv = { --花锦鲤
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
        },
        oceanfish_medium_7_inv = { --金锦鲤
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 5, stacksize = 10, },
        },
        oceanfish_medium_9_inv = { --甜味鱼
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = 5, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 10, },
        },
        lavae_egg = { --岩浆虫卵
            buy = { value = 40, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 20, chance = chancemap[2], count_min = 1, count_max = 2, stacksize = 5, },
        },
        mosquito = { --蚊子
            buy = { value = 5, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
            sell = { value = 2, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 15, },
        },
    },
    plants = {
        seeds = { --随机种子
            buy = { value = 1, chance = chancemap[5], count_min = 2, count_max = 5, stacksize = 15, num_mix = 4, },
            sell = { value = nil, chance = chancemap[5], count_min = 2, count_max = 5, stacksize = 15, num_mix = 4, },
        },
        potato_seeds = { --土豆种子
            buy = { value = 2, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
        },
        potato_oversized = { --巨型土豆
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        tomato_seeds = { --番茄种子
            buy = { value = 2, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
        },
        tomato_oversized = { --巨型番茄
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        eggplant_seeds = { --茄子种子
            buy = { value = 2, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
        },
        eggplant_oversized = { --巨型茄子
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        corn_seeds = { --玉米种子
            buy = { value = 2, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
        },
        corn_oversized = { --巨型玉米
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        carrot_seeds = { --胡萝卜种子
            buy = { value = 2, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
        },
        carrot_oversized = { --巨型胡萝卜
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        watermelon_seeds = { --西瓜种子
            buy = { value = 2, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[5], count_min = 2, count_max = 4, stacksize = 10, num_mix = 4, },
        },
        watermelon_oversized = { --巨型西瓜
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        pumpkin_seeds = { --南瓜种子
            buy = { value = 6, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
        },
        pumpkin_oversized = { --巨型南瓜
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        pomegranate_seeds = { --石榴种子
            buy = { value = 2, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
        },
        pomegranate_oversized = { --巨型石榴
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        asparagus_seeds = { --芦笋种子
            buy = { value = 2, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
        },
        asparagus_oversized = { --巨型芦笋
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        pepper_seeds = { --辣椒种子
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
        },
        pepper_oversized = { --巨型辣椒
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        onion_seeds = { --洋葱种子
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
        },
        onion_oversized = { --巨型洋葱
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        garlic_seeds = { --大蒜种子
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
        },
        garlic_oversized = { --巨型大蒜
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        dragonfruit_seeds = { --火龙果种子
            buy = { value = 12, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
        },
        dragonfruit_oversized = { --巨型火龙果
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        durian_seeds = { --榴莲种子
            buy = { value = 12, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
        },
        durian_oversized = { --巨型榴莲
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        gourd_seeds = { --葫芦种子
            img_tex = "gourd_seeds.tex", img_atlas = "images/monkey_king_item.xml",
            buy = { value = 20, chance = chancemap[2], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
            sell = { value = nil, chance = chancemap[2], count_min = 2, count_max = 3, stacksize = 10, num_mix = 4, },
        },
        gourd_oversized = { --巨型葫芦
            img_tex = "gourd_oversized.tex", img_atlas = "images/inventoryimages/gourd_oversized.xml",
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 2, stacksize = 5, },
        },
        lotus_seeds = { --莲子
            img_tex = "lotus_seeds.tex", img_atlas = "images/inventoryimages/lotus_seeds.xml",
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, },
        },
        pinecone = { --松果
            buy = { value = 1, chance = chancemap[5], count_min = 2, count_max = 5, stacksize = 20, },
            sell = { value = nil, chance = chancemap[5], count_min = 2, count_max = 5, stacksize = 20, },
        },
        twiggy_nut = { --多枝树种
            buy = { value = 1, chance = chancemap[5], count_min = 2, count_max = 5, stacksize = 20, },
            sell = { value = nil, chance = chancemap[5], count_min = 2, count_max = 5, stacksize = 20, },
        },
        acorn = { --桦树果
            buy = { value = 1, chance = chancemap[5], count_min = 2, count_max = 5, stacksize = 20, },
            sell = { value = nil, chance = chancemap[5], count_min = 2, count_max = 5, stacksize = 20, },
        },
        dug_sapling = { --树枝丛
            buy = { value = 5, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 15, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 15, },
        },
        dug_sapling_moon = { --月岛变异树枝丛
            buy = { value = 10, chance = chancemap[2], count_min = 1, count_max = 3, stacksize = 10, },
            sell = { value = nil, chance = chancemap[2], count_min = 1, count_max = 3, stacksize = 10, },
        },
        dug_grass = { --草根
            buy = { value = 5, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 15, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 15, },
        },
        dug_marsh_bush = { --荆棘
            buy = { value = 5, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 15, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 15, },
        },
        dug_berrybush = { --浆果丛
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 15, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 15, },
        },
        dug_berrybush2 = { --猪村浆果丛
            buy = { value = 12, chance = chancemap[2], count_min = 1, count_max = 3, stacksize = 10, },
            sell = { value = nil, chance = chancemap[2], count_min = 1, count_max = 3, stacksize = 10, },
        },
        dug_berrybush_juicy = { --多汁浆果丛
            buy = { value = 10, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 15, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 4, stacksize = 15, },
        },
        waterplant_planter = { --海芽插穗
            buy = { value = 20, chance = chancemap[2], count_min = 1, count_max = 3, stacksize = 10, },
            sell = { value = nil, chance = chancemap[2], count_min = 1, count_max = 3, stacksize = 10, },
        },
        dug_rock_avocado_bush = { --石果树
            buy = { value = 10, chance = chancemap[2], count_min = 2, count_max = 4, stacksize = 10, },
            sell = { value = nil, chance = chancemap[2], count_min = 2, count_max = 4, stacksize = 10, },
        },
        rock_avocado_fruit_sprout = { --石果树芽
            buy = { value = 20, chance = chancemap[2], count_min = 1, count_max = 3, stacksize = 10, },
            sell = { value = nil, chance = chancemap[2], count_min = 1, count_max = 3, stacksize = 10, },
        },
        mandrake = { --曼德拉草
            buy = { value = 300, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 1, },
            sell = { value = nil, chance = chancemap[1], count_min = 1, count_max = 1, stacksize = 1, },
        },
        foliage = { --蕨叶
            buy = { value = 1, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 30, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 6, stacksize = 30, },
        },
        succulent_picked = { --多肉植物
            buy = { value = 1, chance = chancemap[3], count_min = 3, count_max = 6, stacksize = 30, },
            sell = { value = nil, chance = chancemap[3], count_min = 3, count_max = 6, stacksize = 30, },
        },
        cutreeds = { --芦苇
            buy = { value = 10, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 15, num_mix = 4, },
            sell = { value = nil, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 15, num_mix = 4, },
        },
    },
    foods = {
        nightmarepie = { --恐怖国王派
            buy = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 10, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
        },
        voltgoatjelly = { --伏特羊果冻
            buy = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 25, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
        },
        glowberrymousse = { --发光慕斯
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = 8, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        frogfishbowl = { --蓝带鱼排
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = 8, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        dragonchilisalad = { --热龙椒沙拉
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        gazpacho = { --芦笋凉菜汤
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = 10, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        potatosouffle = { --蓬松土豆蛋奶酥
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = 8, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        monstertartare = { --怪物鞑靼
            buy = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = 5, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        freshfruitcrepes = { --鲜果可丽饼
            buy = { value = nil, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = 40, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 3, },
        },
        bonesoup = { --骨头汤
            buy = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 20, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
        },
        moqueca = { --海鲜杂烩
            buy = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = 20, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
        },
        myth_food_lxq = { --蕉叶龙虾球
            img_tex = "myth_food_lxq.tex", img_atlas = "images/inventoryimages/myth_food_lxq.xml",
            buy = { value = 8, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_fhy = { --覆海宴
            img_tex = "myth_food_fhy.tex", img_atlas = "images/inventoryimages/myth_food_fhy.xml",
            buy = { value = 12, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
        },
        myth_food_nx = { --香蕉奶昔
            img_tex = "myth_food_nx.tex", img_atlas = "images/inventoryimages/myth_food_nx.xml",
            buy = { value = 8, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        gourd_omelet = { --葫芦鸡蛋饼
            img_tex = "gourd_omelet.tex", img_atlas = "images/inventoryimages/gourd_omelet.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        gourd_soup = { --葫芦汤
            img_tex = "gourd_soup.tex", img_atlas = "images/inventoryimages/gourd_soup.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        honey_pie = { --蜂蜜素饼
            img_tex = "honey_pie.tex", img_atlas = "images/inventoryimages/honey_pie.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_zyoh = { --折月藕盒
            img_tex = "myth_food_zyoh.tex", img_atlas = "images/inventoryimages/myth_food_zyoh.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_zscr = { --竹笋炒肉
            img_tex = "myth_food_zscr.tex", img_atlas = "images/inventoryimages/myth_food_zscr.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        peach_banquet = { --蟠桃大会
            img_tex = "peach_banquet.tex", img_atlas = "images/inventoryimages/peach_banquet.xml",
            buy = { value = 12, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
        },
        myth_food_xjdmg = { --小鸡炖蘑菇
            img_tex = "myth_food_xjdmg.tex", img_atlas = "images/inventoryimages/myth_food_xjdmg.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_ztf = { --竹筒饭
            img_tex = "myth_food_ztf.tex", img_atlas = "images/inventoryimages/myth_food_ztf.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_tsj = { --屠苏酒
            img_tex = "myth_food_tsj.tex", img_atlas = "images/inventoryimages/myth_food_tsj.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_pgt = { --莲藕排骨汤
            img_tex = "myth_food_pgt.tex", img_atlas = "images/inventoryimages/myth_food_pgt.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_lzg = { --冰糖莲子羹
            img_tex = "myth_food_lzg.tex", img_atlas = "images/inventoryimages/myth_food_lzg.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_hbj = { --荷包鸡
            img_tex = "myth_food_hbj.tex", img_atlas = "images/inventoryimages/myth_food_hbj.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_hsy = { --麻辣红烧鱼
            img_tex = "myth_food_hsy.tex", img_atlas = "images/inventoryimages/myth_food_hsy.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_bbf = { --八宝饭
            img_tex = "myth_food_bbf.tex", img_atlas = "images/inventoryimages/myth_food_bbf.xml",
            buy = { value = 8, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        peach_wine = { --蟠桃素酒
            img_tex = "peach_wine.tex", img_atlas = "images/inventoryimages/peach_wine.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_hlbz = { --胡萝卜汁
            img_tex = "myth_food_hlbz.tex", img_atlas = "images/inventoryimages/myth_food_hlbz.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_sj = { --水饺
            img_tex = "myth_food_sj.tex", img_atlas = "images/inventoryimages/myth_food_sj.xml",
            buy = { value = 6, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_tr = { --糖人
            img_tex = "myth_food_tr.tex", img_atlas = "images/inventoryimages/myth_food_tr.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_cj = { --折月春卷
            img_tex = "myth_food_cj.tex", img_atlas = "images/inventoryimages/myth_food_cj.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        vegetarian_food = { --素斋
            img_tex = "vegetarian_food.tex", img_atlas = "images/inventoryimages/vegetarian_food.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
        },
        myth_food_bz = { --大肉包子
            img_tex = "myth_food_bz.tex", img_atlas = "images/inventoryimages/myth_food_bz.xml",
            buy = { value = 10, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
        },
        myth_food_lwhz = { --腊味合蒸
            img_tex = "myth_food_lwhz.tex", img_atlas = "images/inventoryimages/myth_food_lwhz.xml",
            buy = { value = 12, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
        },
        myth_food_thl = { --糖葫芦
            img_tex = "myth_food_thl.tex", img_atlas = "images/inventoryimages/myth_food_thl.xml",
            buy = { value = 6, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            desc = STRINGS.BBSHOP.DESC.myth_food_thl,
        },
        myth_food_nrlm = { --牛肉拉面
            img_tex = "myth_food_nrlm.tex", img_atlas = "images/inventoryimages/myth_food_nrlm.xml",
            buy = { value = 8, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            desc = STRINGS.BBSHOP.DESC.myth_food_nrlm,
        },
        myth_food_djyt = { --豆浆油条
            img_tex = "myth_food_djyt.tex", img_atlas = "images/inventoryimages/myth_food_djyt.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            desc = STRINGS.BBSHOP.DESC.myth_food_djyt,
        },
        myth_food_cdf = { --臭豆腐
            img_tex = "myth_food_cdf.tex", img_atlas = "images/inventoryimages/myth_food_cdf.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            desc = STRINGS.BBSHOP.DESC.myth_food_cdf,
        },
        myth_food_lrhs = { --驴肉火烧
            img_tex = "myth_food_lrhs.tex", img_atlas = "images/inventoryimages/myth_food_lrhs.xml",
            buy = { value = 6, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            desc = STRINGS.BBSHOP.DESC.myth_food_lrhs,
        },
        myth_food_lhyx = { --莲花血鸭
            img_tex = "myth_food_lhyx.tex", img_atlas = "images/inventoryimages/myth_food_lhyx.xml",
            buy = { value = 8, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            desc = STRINGS.BBSHOP.DESC.myth_food_lhyx,
        },
        myth_food_gqmx = { --过桥米线
            img_tex = "myth_food_gqmx.tex", img_atlas = "images/inventoryimages/myth_food_gqmx.xml",
            buy = { value = 8, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 5, },
            desc = STRINGS.BBSHOP.DESC.myth_food_gqmx,
        },
        myth_food_xcmt = { --咸菜馒头
            img_tex = "myth_food_xcmt.tex", img_atlas = "images/inventoryimages/myth_food_xcmt.xml",
            buy = { value = 4, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 8, },
            desc = STRINGS.BBSHOP.DESC.myth_food_xcmt,
        },
    },
    weapons = {
        myth_iron_broadsword = { --铸铁大刀
            img_tex = "myth_iron_broadsword.tex", img_atlas = "images/inventoryimages/myth_iron_broadsword.xml",
            buy = { value = 80, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 7, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 7, },
            fix = { value = 5, },
            desc = STRINGS.BBSHOP.DESC.myth_iron_broadsword,
        },
        myth_iron_helmet = { --铸铁头盔
            img_tex = "myth_iron_helmet.tex", img_atlas = "images/inventoryimages/myth_iron_helmet.xml",
            buy = { value = 80, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 7, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 7, },
            fix = { value = 5, },
            desc = STRINGS.BBSHOP.DESC.myth_iron_helmet,
        },
        myth_iron_battlegear = { --铸铁战甲
            img_tex = "myth_iron_battlegear.tex", img_atlas = "images/inventoryimages/myth_iron_battlegear.xml",
            buy = { value = 80, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 7, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 3, stacksize = 7, },
            fix = { value = 5, },
            desc = STRINGS.BBSHOP.DESC.myth_iron_battlegear,
        },
        lucky_goldnugget = { --元宝
            buy = { value = 10, chance = chancemap[5], count_min = 3, count_max = 7, stacksize = 35, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 7, stacksize = 35, },
        },
        spear_wathgrithr = { --女武神长矛
            buy = {}, sell = {},
            fix = {
                value = 5, --维修价格
            },
        },
        spear = { --长矛
            buy = {}, sell = {}, fix = { value = 5, },
        },
        nightstick = { --晨星
            buy = {}, sell = {}, fix = { value = 10, },
        },
        whip = { --猫尾鞭
            buy = {}, sell = {}, fix = { value = 5, },
        },
        boomerang = { --回旋镖
            buy = {}, sell = {}, fix = { value = 5, },
        },
        trap_teeth = { --狗牙陷阱
            buy = {}, sell = {}, fix = { value = 5, },
        },
        staff_tornado = { --旋风杖
            buy = {}, sell = {}, fix = { value = 10, },
        },
        trident = { --噪音三叉戟
            buy = {}, sell = {}, fix = { value = 10, },
        },
        nightsword = { --影刀
            buy = {}, sell = {}, fix = { value = 5, },
        },
        firestaff = { --火焰法杖
            buy = {}, sell = {}, fix = { value = 10, },
        },
        icestaff = { --冰冻法杖
            buy = {}, sell = {}, fix = { value = 10, },
        },
        ruins_bat = { --铥棒
            buy = {}, sell = {}, fix = { value = 25, },
        },
        glasscutter = { --月玻璃刀
            buy = {}, sell = {}, fix = { value = 10, },
        },
        wathgrithrhat = { --女武神头盔
            buy = {}, sell = {}, fix = { value = 5, },
        },
        cookiecutterhat = { --饼干切割机头盔
            buy = {}, sell = {}, fix = { value = 5, },
        },
        footballhat = { --猪皮头盔
            buy = {}, sell = {}, fix = { value = 5, },
        },
        ruinshat = { --铥头盔
            buy = {}, sell = {}, fix = { value = 25, },
        },
        armorgrass = { --草甲
            buy = {}, sell = {}, fix = { value = 5, },
        },
        armorwood = { --木甲
            buy = {}, sell = {}, fix = { value = 5, },
        },
        armormarble = { --大理石甲
            buy = {}, sell = {}, fix = { value = 5, },
        },
        armordragonfly = { --龙鳞甲
            buy = {}, sell = {}, fix = { value = 10, },
        },
        armor_sanity = { --影甲
            buy = {}, sell = {}, fix = { value = 5, },
        },
        armorruins = { --铥甲
            buy = {}, sell = {}, fix = { value = 25, },
        },
        farm_plow_item = { --耕地机
            buy = {}, sell = {}, fix = { value = 5, },
        },
        compass = { --指南针
            buy = {}, sell = {}, fix = { value = 5, },
        },
        heatrock = { --热能石
            buy = {}, sell = {}, fix = { value = 5, },
        },
        axe = { --斧头
            buy = {}, sell = {}, fix = { value = 5, },
        },
        goldenaxe = { --金斧头
            buy = {}, sell = {}, fix = { value = 5, },
        },
        pickaxe = { --镐子
            buy = {}, sell = {}, fix = { value = 5, },
        },
        goldenpickaxe = { --金镐子
            buy = {}, sell = {}, fix = { value = 5, },
        },
        shovel = { --铲子
            buy = {}, sell = {}, fix = { value = 5, },
        },
        goldenshovel = { --金铲子
            buy = {}, sell = {}, fix = { value = 5, },
        },
        farm_hoe = { --锄头
            buy = {}, sell = {}, fix = { value = 5, },
        },
        golden_farm_hoe = { --金锄头
            buy = {}, sell = {}, fix = { value = 5, },
        },
        hammer = { --锤子
            buy = {}, sell = {}, fix = { value = 5, },
        },
        pitchfork = { --草叉
            buy = {}, sell = {}, fix = { value = 5, },
        },
        wateringcan = { --水壶
            buy = {}, sell = {}, fix = { value = 1, },
        },
        premiumwateringcan = { --鸟嘴水壶
            buy = {}, sell = {}, fix = { value = 1, },
        },
        saddlehorn = { --取鞍器
            buy = {}, sell = {}, fix = { value = 5, },
        },
        saddle_basic = { --普通鞍
            buy = {}, sell = {}, fix = { value = 5, },
        },
        saddle_race = { --蝴蝶鞍
            buy = {}, sell = {}, fix = { value = 5, },
        },
        saddle_war = { --战争鞍
            buy = {}, sell = {}, fix = { value = 5, },
        },
        brush = { --毛刷
            buy = {}, sell = {}, fix = { value = 5, },
        },
        onemanband = { --个人乐队
            buy = {}, sell = {}, fix = { value = 5, },
        },
        goggleshat = { --时尚目镜
            buy = {}, sell = {}, fix = { value = 5, },
        },
        deserthat = { --防风目镜
            buy = {}, sell = {}, fix = { value = 5, },
        },
        multitool_axe_pickaxe = { --铥斧镐
            buy = {}, sell = {}, fix = { value = 25, },
        },
        moonglassaxe = { --月玻璃斧头
            buy = {}, sell = {}, fix = { value = 10, },
        },
        oar = { --船桨
            buy = {}, sell = {}, fix = { value = 5, },
        },
        oar_driftwood = { --漂浮木船桨
            buy = {}, sell = {}, fix = { value = 5, },
        },
        dragonheadhat = { --龙狮帽-头部
            buy = {}, sell = {}, fix = { value = 10, },
        },
        dragonbodyhat = { --龙狮帽-身体
            buy = {}, sell = {}, fix = { value = 10, },
        },
        dragontailhat = { --龙狮帽-尾巴
            buy = {}, sell = {}, fix = { value = 10, },
        },
    },
    numerology = {
        sweettea = { --舒缓茶
            buy = { value = 10, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
        },
        healingsalve = { --治疗药膏
            buy = { value = 10, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
        },
        tillweedsalve = { --犁地草药膏
            buy = { value = 10, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
        },
        bandage = { --蜂蜜药膏
            buy = { value = 20, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 6, stacksize = 20, },
        },
    },
    construct = {
        minisign_item = { --小木牌
            buy = { value = 1, chance = chancemap[4], count_min = 3, count_max = 4, stacksize = 15, num_mix = 4, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 4, stacksize = 15, num_mix = 4, },
        },
        fence_item = { --栅栏
            buy = { value = 1, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, num_mix = 3, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, num_mix = 3, },
        },
        fence_gate_item = { --栅栏门
            buy = { value = 1, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 15, },
            sell = { value = nil, chance = chancemap[4], count_min = 2, count_max = 4, stacksize = 15, },
        },
        wall_hay_item = { --草墙
            buy = { value = 1, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, num_mix = 3, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, num_mix = 3, },
        },
        wall_wood_item = { --木墙
            buy = { value = 1, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, num_mix = 3, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, num_mix = 3, },
        },
        wall_stone_item = { --石墙
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, num_mix = 3, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 20, num_mix = 3, },
        },
        wall_moonrock_item = { --月岩墙
            buy = { value = 5, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 2, },
            sell = { value = nil, chance = chancemap[3], count_min = 2, count_max = 3, stacksize = 10, num_mix = 2, },
        },
        wall_ruins_item = { --铥矿墙
            buy = { value = 20, chance = chancemap[1], count_min = 1, count_max = 2, stacksize = 4, num_mix = 2, },
            sell = { value = nil, chance = chancemap[1], count_min = 1, count_max = 2, stacksize = 4, num_mix = 2, },
        },
        rope = { --草绳
            buy = { value = 1, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 30, num_mix = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 30, num_mix = 3, },
        },
        boards = { --木板
            buy = { value = 1, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 30, num_mix = 2, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 30, num_mix = 2, },
        },
        cutstone = { --石砖
            buy = { value = 2, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 30, num_mix = 2, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 5, stacksize = 30, num_mix = 2, },
        },
        papyrus = { --莎草纸
            buy = { value = 2, chance = chancemap[5], count_min = 3, count_max = 4, stacksize = 15, num_mix = 2, },
            sell = { value = nil, chance = chancemap[5], count_min = 3, count_max = 4, stacksize = 15, num_mix = 2, },
        },
        transistor = { --电子元件
            buy = { value = 3, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 8, },
            sell = { value = nil, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 8, },
        },
        hammer = { --锤子
            buy = { value = 4, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 4, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 4, },
        },
        pitchfork = { --草叉
            buy = { value = 2, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 4, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 4, },
        },
        featherpencil = { --羽毛笔
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 4, stacksize = 15, num_mix = 3, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 4, stacksize = 15, num_mix = 3, },
        },
        turf_road = { --卵石路地皮
            buy = { value = 5, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 15, num_mix = 2, },
            sell = { value = nil, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 15, num_mix = 2, },
        },
        turf_woodfloor = { --木地板地皮
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 15, num_mix = 2, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 15, num_mix = 2, },
        },
        turf_checkerfloor = { --大理石地皮
            buy = { value = 5, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 15, num_mix = 2, },
            sell = { value = nil, chance = chancemap[4], count_min = 2, count_max = 3, stacksize = 15, num_mix = 2, },
        },
        turf_carpetfloor = { --地毯地皮
            buy = { value = 2, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 15, num_mix = 2, },
            sell = { value = nil, chance = chancemap[4], count_min = 3, count_max = 5, stacksize = 15, num_mix = 2, },
        },
        myth_house_bamboo_blueprint = { --苍竹瓦屋（蓝图）
            img_tex = "blueprint.tex", img_atlas = nil,
            nameoverride = "myth_house_bamboo",
            buy = { value = 100, chance = chancemap[2], count_min = 1, count_max = 1, stacksize = 2, },
            sell = { value = nil, chance = chancemap[2], count_min = 1, count_max = 1, stacksize = 2, },
        },
        wall_dwelling_item_blueprint = { --黑瓦白墙（蓝图）
            img_tex = "blueprint.tex", img_atlas = nil,
            nameoverride = "wall_dwelling_item",
            buy = { value = 50, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 3, },
            sell = { value = nil, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 3, },
        },
        fence_bamboo_item_blueprint = { --竹栅栏（蓝图）
            img_tex = "blueprint.tex", img_atlas = nil,
            nameoverride = "fence_bamboo_item",
            buy = { value = 40, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 3, },
            sell = { value = nil, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 3, },
        },
        fence_gate_bamboo_item_blueprint = { --竹门（蓝图）
            img_tex = "blueprint.tex", img_atlas = nil,
            nameoverride = "fence_gate_bamboo_item",
            buy = { value = 40, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 3, },
            sell = { value = nil, chance = chancemap[3], count_min = 1, count_max = 2, stacksize = 3, },
        },
        myth_stool_blueprint = { --红木椅（蓝图）
            img_tex = "blueprint.tex", img_atlas = nil,
            nameoverride = "myth_stool",
            buy = { value = 20, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 3, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 3, },
        },
        myth_food_table_blueprint = { --红木餐桌（蓝图）
            img_tex = "blueprint.tex", img_atlas = nil,
            nameoverride = "myth_food_table",
            buy = { value = 20, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 3, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 3, },
        },
        myth_rocktips_blueprint = { --鹅卵石（蓝图）
            img_tex = "blueprint.tex", img_atlas = nil,
            nameoverride = "myth_rocktips",
            buy = { value = 20, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 3, },
            sell = { value = nil, chance = chancemap[4], count_min = 1, count_max = 2, stacksize = 3, },
        },
        --[[
        madscience_lab_blueprint = { --疯狂药水台
            img_tex = "madscience_lab.tex", img_atlas = nil,
            buy = { value = 20, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 2, },
            sell = { value = nil, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 2, },
        },
        wintersfeastoven_blueprint = { --冬季盛宴烤箱壁炉
            img_tex = "wintersfeastoven.tex", img_atlas = nil,
            buy = { value = 20, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 2, },
            sell = { value = nil, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 2, },
        },
        table_winters_feast_blueprint = { --冬季盛宴餐桌
            img_tex = "table_winters_feast.tex", img_atlas = nil,
            buy = { value = 20, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 2, },
            sell = { value = nil, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 2, },
        },
        winter_treestand_blueprint = { --冬季盛宴圣诞树盆
            img_tex = "winter_treestand.tex", img_atlas = nil,
            buy = { value = 20, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 2, },
            sell = { value = nil, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 2, },
        },
        winch_blueprint = { --绞绞夹盘
            img_tex = "winch.tex", img_atlas = nil,
            buy = { value = 20, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 2, },
            sell = { value = nil, chance = chancemap[3], count_min = 1, count_max = 1, stacksize = 2, },
        },
        boat_item_blueprint = { --船
            img_tex = "boat_item.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        anchor_item_blueprint = { --锚
            img_tex = "anchor_item.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        mast_item_blueprint = { --桅杆
            img_tex = "mast_item.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        steeringwheel_item_blueprint = { --方向舵
            img_tex = "steeringwheel_item.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        fish_box_blueprint = { --鱼箱
            img_tex = "fish_box.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        treasurechest_blueprint = { --木箱
            img_tex = "treasurechest.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        arrowsign_post_blueprint = { --箭头木牌
            img_tex = "arrowsign_post.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        homesign_blueprint = { --木牌
            img_tex = "homesign.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        wardrobe_blueprint = { --衣柜
            img_tex = "wardrobe.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        pighouse_blueprint = { --猪房
            img_tex = "pighouse.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        rabbithouse_blueprint = { --兔房
            img_tex = "rabbithouse.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        birdcage_blueprint = { --鸟笼
            img_tex = "birdcage.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        scarecrow_blueprint = { --稻草人
            img_tex = "scarecrow.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        tacklestation_blueprint = { --钓具容器
            img_tex = "tacklestation.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        trophyscale_fish_blueprint = { --鱼类称重器
            img_tex = "trophyscale_fish.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        trophyscale_oversizedveggies_blueprint = { --作物称重器
            img_tex = "trophyscale_oversizedveggies.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        pottedfern_blueprint = { --蕨类盆栽
            img_tex = "pottedfern.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        dragonflychest_blueprint = { --龙鳞箱
            img_tex = "dragonflychest.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        cookpot_blueprint = { --烹饪锅
            img_tex = "cookpot.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        icebox_blueprint = { --冰箱
            img_tex = "icebox.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        saltbox_blueprint = { --盐箱
            img_tex = "saltbox.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        mushroom_farm_blueprint = { --蘑菇农场
            img_tex = "mushroom_farm.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        beebox_blueprint = { --蜂箱
            img_tex = "beebox.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        meatrack_blueprint = { --晾肉架
            img_tex = "meatrack.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        tent_blueprint = { --帐篷
            img_tex = "tent.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        siestahut_blueprint = { --凉棚
            img_tex = "siestahut.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        saltlick_blueprint = { --舔盐器
            img_tex = "saltlick.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        researchlab2_blueprint = { --科学机器2级
            img_tex = "researchlab2.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        seafaring_prototyper_blueprint = { --航海智囊团
            img_tex = "seafaring_prototyper.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        cartographydesk_blueprint = { --制图桌
            img_tex = "cartographydesk.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        sculptingtable_blueprint = { --陶轮
            img_tex = "sculptingtable.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        winterometer_blueprint = { --温度计
            img_tex = "winterometer.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        rainometer_blueprint = { --雨量计
            img_tex = "rainometer.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        lightning_rod_blueprint = { --避雷针
            img_tex = "lightning_rod.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        firesuppressor_blueprint = { --灭火器
            img_tex = "firesuppressor.tex", img_atlas = nil,
            buy = { value = 15, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
            sell = { value = nil, chance = chancemap[5], count_min = 1, count_max = 1, stacksize = 3, },
        },
        ]]--
    },
}

--淘气值相关获取 淘气值满值是48
TUNING.MYTH_KRAMPED = nil
local upvaluehelper = require "components/myth_upvaluehelper"
AddComponentPostInit("kramped",function(self)
    local _activeplayers = upvaluehelper.Get(self.GetDebugString, "_activeplayers")
    if _activeplayers then
        TUNING.MYTH_KRAMPED = _activeplayers
    end
end)
GLOBAL.Myth_GetPLayer_Kramped = function(player)
    if TUNING.MYTH_KRAMPED and TUNING.MYTH_KRAMPED[player] then
        return TUNING.MYTH_KRAMPED[player].actions or 0
    end
    return 0
end
--Myth_GetPLayer_Kramped(ThePlayer)
