local assets =
{
    Asset("ANIM", "anim/sporecloud.zip"),
    Asset("ANIM", "anim/sporecloud_base.zip"),
    Asset("ANIM", "anim/nian_sporecloud.zip"),
}

local prefabs =
{
    "sporecloud_overlay",
}

local AURA_EXCLUDE_TAGS = { "nian", "playerghost", "ghost", "shadow", "shadowminion", "noauradamage", "INLIMBO", "notarget", "noattack", "flight", "invisible" }

local FADE_FRAMES = 5
local FADE_INTENSITY = .8
local FADE_RADIUS = 1
local FADE_FALLOFF = .5

local function OnUpdateFade(inst)
    local k
    if inst._fade:value() <= FADE_FRAMES then
        inst._fade:set_local(math.min(inst._fade:value() + 1, FADE_FRAMES))
        k = inst._fade:value() / FADE_FRAMES
    else
        inst._fade:set_local(math.min(inst._fade:value() + 1, FADE_FRAMES * 2 + 1))
        k = (FADE_FRAMES * 2 + 1 - inst._fade:value()) / FADE_FRAMES
    end

    inst.Light:SetIntensity(FADE_INTENSITY * k)
    inst.Light:SetRadius(FADE_RADIUS * k)
    inst.Light:SetFalloff(1 - (1 - FADE_FALLOFF) * k)

    if TheWorld.ismastersim then
        inst.Light:Enable(inst._fade:value() > 0 and inst._fade:value() <= FADE_FRAMES * 2)
    end

    if inst._fade:value() == FADE_FRAMES or inst._fade:value() > FADE_FRAMES * 2 then
        inst._fadetask:Cancel()
        inst._fadetask = nil
    end
end

local function OnFadeDirty(inst)
    if inst._fadetask == nil then
        inst._fadetask = inst:DoPeriodicTask(FRAMES, OnUpdateFade)
    end
    OnUpdateFade(inst)
end

local function FadeOut(inst)
    inst._fade:set(FADE_FRAMES + 1)
    if inst._fadetask == nil then
        inst._fadetask = inst:DoPeriodicTask(FRAMES, OnUpdateFade)
    end
end

local function FadeInImmediately(inst)
    inst._fade:set(FADE_FRAMES)
    OnFadeDirty(inst)
end

local function FadeOutImmediately(inst)
    inst._fade:Set(FADE_FRAMES * 2 + 1)
    OnFadeDirty(inst)
end

local OVERLAY_COORDS =
{
    { 0,0,0,               1 },
    { 5/2,0,0,             0.8, 0 },
    { 2.5/2,0,-4.330/2,    0.8 , 5/3*180 },
    { -2.5/2,0,-4.330/2,   0.8, 4/3*180 },
    { -5/2,0,0,            0.8, 3/3*180 },
    { 2.5/2,0,4.330/2,     0.8, 1/3*180 },
    { -2.5/2,0,4.330/2,    0.8, 2/3*180 },
}

local function SpawnOverlayFX(inst, i, set, isnew)
    if i ~= nil then
        inst._overlaytasks[i] = nil
        if next(inst._overlaytasks) == nil then
            inst._overlaytasks = nil
        end
    end

    local fx = SpawnPrefab("nian_sporecloud_overlay")
    fx.entity:SetParent(inst.entity)
    fx.Transform:SetPosition(set[1] * .85, 0, set[3] * .85)
    fx.Transform:SetScale(set[4], set[4], set[4])
    if set[5] ~= nil then
        fx.Transform:SetRotation(set[4])
    end

    if not isnew then
        fx.AnimState:PlayAnimation("sporecloud_overlay_loop")
        fx.AnimState:SetTime(math.random() * .7)
    end

    if inst._overlayfx == nil then
        inst._overlayfx = { fx }
    else
        table.insert(inst._overlayfx, fx)
    end
end

local function CreateBase(isnew)
    local inst = CreateEntity()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst.AnimState:SetBank("sporecloud_base")
    inst.AnimState:SetBuild("sporecloud_base")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
    inst.AnimState:SetFinalOffset(3)

    if isnew then
        inst.AnimState:PlayAnimation("sporecloud_base_pre")
        inst.AnimState:PushAnimation("sporecloud_base_idle", false)
    else
        inst.AnimState:PlayAnimation("sporecloud_base_idle")
    end

    return inst
end

local function OnStateDirty(inst)
    if inst._state:value() > 0 then
        if inst._inittask ~= nil then
            inst._inittask:Cancel()
            inst._inittask = nil
        end
        if inst._state:value() == 1 then
            --if inst._basefx == nil then
            --    inst._basefx = CreateBase(false)
            --    inst._basefx.entity:SetParent(inst.entity)
            --end
        elseif inst._basefx ~= nil then
            inst._basefx.AnimState:PlayAnimation("sporecloud_base_pst")
        end
    end
end

local function OnAnimOver(inst)
    inst:RemoveEventCallback("animover", OnAnimOver)
    inst._state:set(1)
end

local function OnOverlayAnimOver(fx)
    fx.AnimState:PlayAnimation("sporecloud_overlay_loop")
end

local function KillOverlayFX(fx)
    fx:RemoveEventCallback("animover", OnOverlayAnimOver)
    fx.AnimState:PlayAnimation("sporecloud_overlay_pst")
end

local function DisableCloud(inst)
    --inst.components.aura:Enable(false)

    if inst._spoiltask ~= nil then
        inst._spoiltask:Cancel()
        inst._spoiltask = nil
    end

    inst:RemoveTag("nian_sporecloud")
end

local function DoDisperse(inst)
    if inst.dispersed then
        return
    end
    inst.dispersed = true

    if inst._inittask ~= nil then
        inst._inittask:Cancel()
        inst._inittask = nil
    end

    DisableCloud(inst)

    inst:RemoveEventCallback("animover", OnAnimOver)
    inst._state:set(2)
    FadeOut(inst)

    inst.AnimState:PlayAnimation("sporecloud_pst")
    inst.SoundEmitter:KillSound("spore_loop")
    inst.persists = false
    inst:DoTaskInTime(3, inst.Remove) --anim len + 1.5 sec

    if inst._basefx ~= nil then
        inst._basefx.AnimState:PlayAnimation("sporecloud_base_pst")
    end

    if inst._overlaytasks ~= nil then
        for k, v in pairs(inst._overlaytasks) do
            v:Cancel()
        end
        inst._overlaytasks = nil
    end
    if inst._overlayfx ~= nil then
        for i, v in ipairs(inst._overlayfx) do
            v:DoTaskInTime(i == 1 and 0 or math.random() * .5, KillOverlayFX)
        end
    end
end

local function OnTimerDone(inst, data)
    if data.name == "disperse" then
        DoDisperse(inst)
    end
end

local function FinishImmediately(inst)
    if inst.components.timer:TimerExists("disperse") then
        inst.components.timer:StopTimer("disperse")
        DoDisperse(inst)
    end
end

local function InitFX(inst)
    inst._inittask = nil

    if TheWorld.ismastersim then
        inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/infection_post")
    end

    --Dedicated server does not need to spawn the local fx
    if not TheNet:IsDedicated() then
        --inst._basefx = CreateBase(true)
        --inst._basefx.entity:SetParent(inst.entity)
    end
end

local function TryPerish(item)
    if item:IsInLimbo() then
        local owner = item.components.inventoryitem ~= nil and item.components.inventoryitem.owner or nil
        if owner == nil or
            (   owner.components.container ~= nil and
                not owner.components.container:IsOpen() and
                owner:HasTag("structure")   ) then
            return
        end
    end
    item.components.perishable:ReducePercent(TUNING.TOADSTOOL_SPORECLOUD_ROT)
end

local SPOIL_CANT_TAGS = { "small_livestock" }
local SPOIL_ONEOF_TAGS = { "fresh", "stale", "spoiled" }
local function DoAreaSpoil(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, TUNING.TOADSTOOL_SPORECLOUD_RADIUS, nil, SPOIL_CANT_TAGS, SPOIL_ONEOF_TAGS)
    for i, v in ipairs(ents) do
        TryPerish(v)
    end
end

local function updatedamage(player)
    if player.nian_sporecloud_fx and player.nian_sporecloud_fx > 0  then
        if player.components.combat ~= nil then
            player.components.combat.externaldamagetakenmultipliers:SetModifier("nian_sporecloud", 1.25)
            player.components.combat.externaldamagemultipliers:SetModifier("nian_sporecloud", 0.5)
        end
        if player.components.locomotor ~= nil then
            player.components.locomotor:SetExternalSpeedMultiplier(player, "nian_sporecloud", 0.75)
        end
        if player.components.efficientuser == nil then
            player:AddComponent("efficientuser")
        end
        player.components.efficientuser:AddMultiplier(ACTIONS.ATTACK, 1.5, "nian_sporecloud")
    else
        if player.components.combat ~= nil then
            player.components.combat.externaldamagemultipliers:RemoveModifier("nian_sporecloud")
            player.components.combat.externaldamagetakenmultipliers:RemoveModifier("nian_sporecloud")
        end
        if player.components.locomotor ~= nil then
            player.components.locomotor:RemoveExternalSpeedMultiplier(player, "nian_sporecloud")
        end
        if player.components.efficientuser then
            player.components.efficientuser:RemoveMultiplier(ACTIONS.ATTACK,"nian_sporecloud")
        end
    end
end

local EXPLODETARGET_MUST_TAGS = { "_health", "_combat" }
local EXPLODETARGET_CANT_TAGS = { "INLIMBO", "nian" }
local function DoDmage(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    SpawnPrefab("nian_sporecloud_explode").Transform:SetPosition(x, y, z)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/spore_explode")
    local ents = TheSim:FindEntities(x, y, z, TUNING.TOADSTOOL_SPORECLOUD_RADIUS, EXPLODETARGET_MUST_TAGS, EXPLODETARGET_CANT_TAGS)
    for i, v in ipairs(ents) do
        if v:IsValid() and not v:IsInLimbo() and
            v.components.combat ~= nil and not (v.components.health ~= nil and v.components.health:IsDead()) then
            v.components.combat:GetAttacked(inst, TUNING.MYTH_NIAN_BOOMDANAGE)
        end
    end
end

local function OnFar(inst, player)
    if player.nian_sporecloud_fx then   
        player.nian_sporecloud_fx = player.nian_sporecloud_fx - 1
        updatedamage(player)
    end
    inst.players[player] = nil
end

local function OnNear(inst,player)
    inst.players[player] = true
    player.nian_sporecloud_fx = (player.nian_sporecloud_fx or 0) + 1
    updatedamage(player)
end

local function OnRemove(inst)
    for player in pairs(inst.players) do
        if player:IsValid() then
            if player.nian_sporecloud_fx then   
                player.nian_sporecloud_fx = player.nian_sporecloud_fx - 1
                updatedamage(player)
            end
        end
    end
end

local function Spawn(inst)
    local fx = SpawnPrefab('ash')
    fx.Transform:SetPosition(inst:GetPosition():Get())
    DoDisperse(inst)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("sporecloud")
    inst.AnimState:SetBuild("nian_sporecloud")
    inst.AnimState:PlayAnimation("sporecloud_pre")
    inst.AnimState:SetLightOverride(.3)
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    inst.Light:SetFalloff(FADE_FALLOFF)
    inst.Light:SetIntensity(FADE_INTENSITY)
    inst.Light:SetRadius(FADE_RADIUS)
    inst.Light:SetColour(125 / 255, 200 / 255, 50 / 255)
    inst.Light:Enable(false)
    inst.Light:EnableClientModulation(true)

    --inst:AddTag("sporecloud")
    inst:AddTag("nian_sporecloud")
    inst:AddTag("bananafanworkable")

    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/toad_stool/spore_cloud_LP", "spore_loop")

    inst._state = net_tinybyte(inst.GUID, "nian_sporecloud._state", "statedirty")
    inst._fade = net_smallbyte(inst.GUID, "nian_sporecloud._fade", "fadedirty")

    inst._fadetask = inst:DoPeriodicTask(FRAMES, OnUpdateFade)

    inst._inittask = inst:DoTaskInTime(0, InitFX)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        inst:ListenForEvent("statedirty", OnStateDirty)
        inst:ListenForEvent("fadedirty", OnFadeDirty)

        return inst
    end

    inst:AddComponent("inspectable")

    inst.players = {}
    inst:AddComponent("playerprox")
    inst.components.playerprox:SetTargetMode(inst.components.playerprox.TargetModes.AllPlayers)
    inst.components.playerprox:SetDist(TUNING.TOADSTOOL_SPORECLOUD_RADIUS, TUNING.TOADSTOOL_SPORECLOUD_RADIUS)
    inst.components.playerprox:SetOnPlayerFar(OnFar)
    inst.components.playerprox:SetOnPlayerNear(OnNear)

    MakeSmallBurnable(inst)
    inst.components.burnable.onburnt = Spawn
    inst.components.burnable:SetBurnTime(1)

    inst._spoiltask = inst:DoPeriodicTask(TUNING.TOADSTOOL_SPORECLOUD_TICK, DoAreaSpoil, TUNING.TOADSTOOL_SPORECLOUD_TICK * .5)

    inst.AnimState:PushAnimation("sporecloud_loop", true)
    inst:ListenForEvent("animover", OnAnimOver)

    inst:AddComponent("timer")
    inst.components.timer:StartTimer("disperse", 120)

    inst:ListenForEvent("timerdone", OnTimerDone)
    inst:ListenForEvent("onremove", OnRemove)

    inst:AddComponent("entitytracker")

    inst.persists = false
    inst.DestroyByFan = DoDisperse
    inst.DoDmage = DoDmage

    inst.FadeInImmediately = FadeInImmediately
    inst.FinishImmediately = FinishImmediately

    inst._overlaytasks = {}
    for i, v in ipairs(OVERLAY_COORDS) do
        inst._overlaytasks[i] = inst:DoTaskInTime(i == 1 and 0 or math.random() * .7, SpawnOverlayFX, i, v, true)
    end

    return inst
end

local function overlayfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.Transform:SetTwoFaced()

    inst.AnimState:SetBank("sporecloud")
    inst.AnimState:SetBuild("nian_sporecloud")
    inst.AnimState:SetLightOverride(.2)

    inst.AnimState:PlayAnimation("sporecloud_overlay_pre")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:ListenForEvent("animover", OnOverlayAnimOver)

    inst.persists = false

    return inst
end


local function fxfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    inst.AnimState:SetBank("mushroombomb")
    inst.AnimState:SetBuild("mushroombomb")
    inst.AnimState:SetLightOverride(.2)
    inst.AnimState:PlayAnimation("explode")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    inst:ListenForEvent("animover", inst.Remove)

    inst.persists = false
    inst:DoTaskInTime(1,inst.Remove)
    return inst
end

return Prefab("nian_sporecloud", fn, assets, prefabs),
    Prefab("nian_sporecloud_explode", fxfn),
    Prefab("nian_sporecloud_overlay", overlayfn, assets)
