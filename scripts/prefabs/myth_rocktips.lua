local assets =
{
    Asset("ANIM", "anim/myth_rocktips.zip"),
    Asset("ATLAS", "images/inventoryimages/myth_rocktips.xml"),
}

local prefabs =
{

}

local function ondeploy(inst, pt, deployer)
    local tree = SpawnPrefab("myth_rocktips_ground")
    if tree ~= nil then
        tree.Transform:SetPosition(pt:Get())
        local num = math.random(6)
        tree.AnimState:PlayAnimation("rock"..num)  
        tree.animtype = num
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("myth_rocktips")
    inst.AnimState:SetBuild("myth_rocktips")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("quakedebris")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    -----------------
    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem:SetSinks(true)
    inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_rocktips.xml"

    inst:AddComponent("tradable")

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM

    inst:AddComponent("inspectable")

    inst:AddComponent("deployable")
    inst.components.deployable:SetDeploySpacing(DEPLOYSPACING.NONE)
    inst.components.deployable.ondeploy = ondeploy

    MakeHauntableLaunch(inst)

    return inst
end

local function onload(inst, data)
    if data ~= nil and data.animtype ~= nil then
        inst.animtype = data.animtype
        inst.AnimState:PlayAnimation("rock"..data.animtype)
    end
end

local function onsave(inst, data)
    data.animtype = inst.animtype
end

local function groundfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("myth_rocktips")
    inst.AnimState:SetBuild("myth_rocktips")
    inst.AnimState:PlayAnimation("rock1")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    inst.animtype = 1
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.DIG)
    inst.components.workable:SetWorkLeft(1)
    inst.components.workable:SetOnFinishCallback(inst.Remove)

    MakeHauntable(inst)

    inst.OnSave = onsave
    inst.OnLoad = onload

    return inst
end
return Prefab("myth_rocktips", fn, assets),
    Prefab("myth_rocktips_ground", groundfn),
    MakePlacer("myth_rocktips_placer", "myth_rocktips", "myth_rocktips", "rock1")
