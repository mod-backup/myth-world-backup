
local function onremove(inst)
    if inst.swinglight then
        inst.swinglight:Remove()
    end
	if inst.childrenspawned then
		inst.childrenspawned:Remove()
	end
end
local function updatelight(inst, phase)
    if phase == "day" then
		if inst.window then
			inst.AnimState:PlayAnimation("to_day")
			inst.AnimState:PushAnimation("day_loop", true)
		end
    elseif phase == "night" then
		if inst.window then
			inst.AnimState:PlayAnimation("to_night")
			inst.AnimState:PushAnimation("night_loop", true)
		end
    elseif phase == "dusk" then
		if inst.window then
			inst.AnimState:PlayAnimation("to_dusk")
			inst.AnimState:PushAnimation("dusk_loop", true)
		end
    end
end

local function windows(inst)
	inst.Transform:SetScale(0.9, 0.9, 0.9)
	inst.window = true
end

local function window_lightfn(inst)
	inst.Transform:SetScale(0.9, 0.9, 0.9)
	inst.window = true
end

local function updatewindow(inst, phase)
	if phase == "night" then
		if	inst._light ~= nil then
			inst._light:Show()
		end
    else
		if	inst._light ~= nil then
			inst._light:Hide()
		end
    end
end

local function makelights(name,build,bank,anim,light,common_fn,child,tags,persists)

	local prefabs =
	{
		"collapse_small",
	}
	
	local assets =
	{
		Asset("ANIM", "anim/myth_window.zip"),
		Asset("ANIM", "anim/myth_window_round.zip"),
	}
	
	local function fn()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddSoundEmitter()
		inst.entity:AddNetwork()

		inst.AnimState:SetBank(bank)
		inst.AnimState:SetBuild(build)
		inst.AnimState:PlayAnimation(anim,true)

		if  common_fn ~= nil then
			common_fn(inst)
		end
        for i, tag in ipairs(tags) do
            inst:AddTag(tag)
        end
		
		inst:AddTag("nonpackable")
		inst:AddTag("moistureimmunity")

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end
		
		if not persists then
			inst:AddComponent("inspectable")
		end

        inst:DoTaskInTime(0, function()
            if light ~= nil and not inst.sunraysspawned then
                inst.swinglight = inst:SpawnChild("myth_house_lights")
                inst.swinglight.setLightType(inst.swinglight, light)             
                inst.sunraysspawned = true
				if light == "natural" then
					inst.swinglight.setListenEvents(inst.swinglight)
				end
            end
			if child ~= nil then
                if not inst.childrenspawned then
                    local childprop = SpawnPrefab(child)
                    local pt = Vector3(inst.Transform:GetWorldPosition())
                    childprop.Transform:SetPosition(pt.x ,pt.y, pt.z)
                    childprop.Transform:SetRotation(inst.Transform:GetRotation())
                    inst.childrenspawned = childprop
                end			
			end
        end)
		
        inst:ListenForEvent("onremove", function() 
            onremove(inst)
        end)
		
		inst:WatchWorldState("phase", updatelight)

		updatelight(inst, TheWorld.state.phase)
		
		if persists then
			inst.persists = false
		end
		
		return inst
	end

	return Prefab(name, fn,assets,prefabs)
end

return makelights("myth_window_round", "myth_window_round", "myth_window" ,"day_loop",nil,windows,"myth_window_round_light",{"NOBLOCK","waterproofer"}),
		makelights("myth_window_round_light", "myth_window_round", "myth_window_light", "day_loop","natural",window_lightfn,nil,{"NOBLOCK","NOCLICK"},true)