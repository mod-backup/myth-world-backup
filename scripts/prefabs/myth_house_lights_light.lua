
local MULT = 0.8 

local lighttypes = {
    natural = {
        day = {rad=2,intensity=0.75,falloff=0.5,color={1*MULT,1*MULT,1*MULT}},
        dusk = {rad=1.5,intensity=0.75,falloff=0.5,color={1/1.8*MULT,1/1.8*MULT,1/1.8*MULT}},
        full = {rad=1.5,intensity=0.75,falloff=0.5,color={0.8/1.8*MULT,0.8/1.8*MULT,1/1.8*MULT}}
    },
    electric_1 = {
        day = {rad=14,intensity=0.9,falloff=.4,color={1*MULT,1*MULT,1*MULT}},
    },   
    electric_2 = {
        day = {rad=14,intensity=0.9,falloff=.4,color={251 / 255, 248 / 255, 103 / 255}},
    }, 
    electric_3 = {
        day = {rad=14,intensity=0.9,falloff=.4,color={255/255,229/255,138/255}},
    }, 	
}

local function turnoff(inst, light)
    if light then
        light:Enable(false)
    end
end

local phasefunctions = 
{
    day = function(inst, instant)
        local lights = lighttypes[inst.lighttype]
        if not inst:IsInLimbo() then inst.Light:Enable(true) end
        local time = 2
        if instant then time = 0 end
        inst.components.lighttweener:StartTween(nil, lights.day.rad, lights.day.intensity, lights.day.falloff, {lights.day.color[1],lights.day.color[2],lights.day.color[3]}, time)
    end,

    dusk = function(inst, instant) 
        local lights = lighttypes[inst.lighttype]
        if not inst:IsInLimbo() then inst.Light:Enable(true) end       
        local time = 2
        if instant then time = 0 end        
        inst.components.lighttweener:StartTween(nil, lights.dusk.rad, lights.dusk.intensity, lights.dusk.falloff, {lights.dusk.color[1],lights.dusk.color[2],lights.dusk.color[3]}, time)
    end,

    night = function(inst, instant) 
        local lights = lighttypes[inst.lighttype]
        if TheWorld.state.isfullmoon then
            local time = 4
            if instant then time = 0 end            
            inst.components.lighttweener:StartTween(nil, lights.full.rad, lights.full.intensity, lights.full.falloff, {lights.full.color[1],lights.full.color[2],lights.full.color[3]}, time)
        else
            inst.components.lighttweener:StartTween(nil, 0, 0, 1, {0,0,0}, 6, turnoff)
        end    
    end,
}

local function timechange(inst, instant) 
    if TheWorld.state.phase == "day" then
        if inst.Light then
            phasefunctions["day"](inst, instant)
        end
    elseif TheWorld.state.phase == "night" then
        if inst.Light then
            phasefunctions["night"](inst, instant)
        end
    elseif TheWorld.state.phase == "dusk" then
        if inst.Light then
            phasefunctions["dusk"](inst, instant)
        end
    end
end

local function updatelight(inst, phase)
	local lights = lighttypes[inst.lighttype]
	if phase == "night" then
		inst.Light:Enable(true)
		inst.components.lighttweener:StartTween(nil, lights.day.rad, lights.day.intensity, lights.day.falloff, {lights.day.color[1],lights.day.color[2],lights.day.color[3]}, 2)
	else
		inst.components.lighttweener:StartTween(nil, 0, 0, 1, {0,0,0}, 3.5, turnoff)
	end
end

local function setLightType(inst, lighttype)
    if lighttypes[lighttype] then
        inst.lighttype = lighttype
        inst.Light:SetIntensity(lighttypes[inst.lighttype].day.intensity)
        inst.Light:SetColour(lighttypes[inst.lighttype].day.color[1],lighttypes[inst.lighttype].day.color[2],lighttypes[inst.lighttype].day.color[3])
        inst.Light:SetFalloff( lighttypes[inst.lighttype].day.falloff )
        inst.Light:SetRadius( lighttypes[inst.lighttype].day.rad ) 
		if lighttype ~= "natural" then
			local lights = lighttypes[inst.lighttype]
			inst:AddComponent("lighttweener")
			inst.components.lighttweener:StartTween(inst.Light, lights.day.rad, lights.day.intensity, lights.day.falloff, {lights.day.color[1],lights.day.color[2],lights.day.color[3]}, 0)
			inst:WatchWorldState("phase", updatelight)
			updatelight(inst, TheWorld.state.phase)
		end
    end
end

local function setListenEvents(inst)
	inst:AddComponent("lighttweener")
    local lights = lighttypes[inst.lighttype]
    inst.components.lighttweener:StartTween(inst.Light, lights.day.rad, lights.day.intensity, lights.day.falloff, {lights.day.color[1],lights.day.color[2],lights.day.color[3]}, 0)
	inst.Light:Enable(true)
	
    inst:WatchWorldState("phase", timechange)

    timechange(inst)
end

local function swinglightobjectfn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
	inst.entity:AddNetwork()

    local light = inst.entity:AddLight()

    inst.lighttype = "natural"

    inst.Light:SetIntensity(lighttypes[inst.lighttype].day.intensity)
    inst.Light:SetColour(lighttypes[inst.lighttype].day.color[1],lighttypes[inst.lighttype].day.color[2],lighttypes[inst.lighttype].day.color[3])
    inst.Light:SetFalloff( lighttypes[inst.lighttype].day.falloff )
    inst.Light:SetRadius( lighttypes[inst.lighttype].day.rad )
	inst.Light:Enable(true)
	

    inst:AddTag("swinglight")
    inst:AddTag("NOBLOCK")
	inst:AddTag("lightsource")
	inst:AddTag("daylight")
	
	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst.persists = false
	
    inst.setLightType = setLightType
	inst.setListenEvents = setListenEvents

    return inst
end

return Prefab( "myth_house_lights", swinglightobjectfn)

