local prefs = {}

--------------------------------------------------------------------------
--[[ 酆都路引 ]]
--------------------------------------------------------------------------

local function MakePass(data)
	table.insert(prefs, Prefab(
		data.name,
		function()
			local inst = CreateEntity()

			inst.entity:AddTransform()
			inst.entity:AddSoundEmitter() --要用
			inst.entity:AddAnimState()
			inst.entity:AddLight()
			inst.entity:AddNetwork()
			-- inst.entity:AddMiniMapEntity()

			-- inst.MiniMapEntity:SetIcon(data.name..".tex")

			inst.Light:SetRadius(.6)
			inst.Light:SetFalloff(1)
			inst.Light:SetIntensity(.5)
			inst.Light:Enable(false)

			inst:AddTag("nobundling") --该标签使其不能被放进打包纸
			inst:AddTag("pass_commissioner")

			MakeInventoryPhysics(inst)

			inst.AnimState:SetBank("pass_commissioner")
			inst.AnimState:SetBuild("pass_commissioner")
			inst.AnimState:PlayAnimation("idle", true)

			inst.myth_use_needtag = "yama_commissioners"
			inst.MYTH_USE_TYPE = "DELIVER"
			inst.onusesgname = "give"

			if data.fn_common ~= nil then
				data.fn_common(inst)
			end

			inst.entity:SetPristine()
			if not TheWorld.ismastersim then
				if data.fn_between ~= nil then
					data.fn_between(inst)
				end
				return inst
			end

			inst:AddComponent("inspectable")

			inst:AddComponent("inventoryitem")
			inst.components.inventoryitem.imagename = "pass_commissioner"
			inst.components.inventoryitem.atlasname = "images/inventoryimages/pass_commissioner.xml"
			inst.components.inventoryitem:SetOnPickupFn(function(inst, pickupguy, src_pos)
				if pickupguy ~= nil then
					local needdrop = false
					if pickupguy:HasTag("player") then
						return
						-- if pickupguy:HasTag("yama_commissioners") then
						-- 	return
						-- end
						-- needdrop = true
					elseif
						pickupguy.prefab == "krampus" or
						pickupguy.prefab == "eyeplant" or pickupguy.prefab == "lureplant" or
						pickupguy.prefab == "monkey"
					then
						needdrop = true
					end

					if needdrop then
						if pickupguy.components.inventory ~= nil then
							pickupguy:DoTaskInTime(0, function()
								pickupguy.components.inventory:DropItem(inst, true, true)
							end)
						end
					end
				end
			end)

			inst:AddComponent("myth_use_inventory")
			inst.components.myth_use_inventory.canuse = true
			inst.components.myth_use_inventory.canusescene  = true

			------
			inst:AddComponent("bloomer")

			inst.isactived = false --是否处于传信状态
			inst.fn_mythuse = function(inst, doer, isactived,fx) --启用与关闭路引功能
				if isactived then
					inst.isactived = true
					if not inst.Is_Haunted  then
						inst.Is_Haunted = true
						inst.AnimState:SetHaunted(true)
						inst:DoTaskInTime(1,function()
							if inst.Is_Haunted then
								inst.AnimState:SetHaunted(false)
								inst.Is_Haunted = false
							end
						end)
					end
					inst.components.inventoryitem.canbepickedup = false --激活后，不能被捡起
					inst.components.bloomer:PushBloom(inst, "shaders/anim_bloom_ghost.ksh", -0.4)
					inst.AnimState:SetLightOverride(.1)
				else
					-- inst.AnimState:PlayAnimation("idle", true)
					inst.isactived = false
					if inst.Is_Haunted then
						inst.AnimState:SetHaunted(false)
						inst.Is_Haunted = false
					end
					inst.components.inventoryitem.canbepickedup = true
					inst.components.bloomer:PopBloom(inst)
					inst.AnimState:SetLightOverride(0)
				end
				inst.Light:Enable(inst.isactived)
				if fx then
					inst:DoTaskInTime(0.5,function()
						inst.fn_mythuse_detail(inst, doer, inst.isactived)
					end)
					inst:SpawnChild("commissioner_teleport")
				else
					inst.fn_mythuse_detail(inst, doer, inst.isactived)
				end
			end
			inst.components.myth_use_inventory:SetOnUseFn(function(inst, doer)
				--使用时，如果在物品栏，自动掉地上
				local owner = inst.components.inventoryitem:GetGrandOwner()
				if owner and owner.components.inventory ~= nil then
					owner.components.inventory:DropItem(inst)
					inst:DoTaskInTime(0.2, function()
						inst.fn_mythuse(inst, doer, true,true)
					end)
				else
					inst.fn_mythuse(inst, doer, not inst.isactived,true)
				end
				return true
			end)

			--捡起和丢下时，恢复未传信状态
			inst.components.inventoryitem:SetOnDroppedFn(function(inst)
				inst.fn_mythuse(inst, nil, false)
			end)
			inst.components.inventoryitem:SetOnPutInInventoryFn(function(inst, owner)
				inst.fn_mythuse(inst, nil, false)
			end)

			MakeHauntableLaunch(inst)

			if data.fn_server ~= nil then
				data.fn_server(inst)
			end

			return inst
		end,
		data.assets,
		data.prefabs
	))
end

------
local healvalue = {
    hunger = {5,5,5,15},
    sanity = {2,2,2,5},
    health = {5,5,5,8},
}
local function OnGetItemFromPlayer(inst, giver, item)
    if giver and giver:HasTag("player")  then
        giver.components.hunger:DoDelta(healvalue.hunger[3] or 0)
        giver.components.health:DoDelta(healvalue.sanity[3] or 0)
        giver.components.sanity:DoDelta(healvalue.health[3] or 0)
        if inst.components.constructionsite then
            inst.components.constructionsite:AddMaterial(item.prefab, 1)
        end
    end
end
------阎罗王
MakePass({
	name = "pass_commissioner_ylw",
	assets = {
		Asset("ANIM", "anim/pass_commissioner.zip"),
		Asset("ANIM", "anim/lavaarena_player_teleport.zip"),
	},
	prefabs = nil,
	fn_common = function(inst)
		inst.Light:SetColour(0/255, 185/255, 138/255)
		inst:AddTag("soulcontroller") --这个标签能让玩家携带时，不让24范围内善恶魂失去控制
	end,
	-- fn_between = function(inst)end,
	fn_server = function(inst)
		inst:AddComponent("trader")
		inst.components.trader.onaccept = OnGetItemFromPlayer
        inst.components.trader:SetAcceptTest(function(inst, item, giver)
			return
				inst.isactived and --激活状态
				giver and giver:HasTag("yama_commissioners") and --无常专用
				item:HasTag("soul_lost") --只要善恶魂
		end)
		inst.components.trader:Disable()
		inst.fn_mythuse_detail = function(inst, doer, isactived) --undo: 路引传信功能的开启与关闭
			if isactived then
				inst.components.trader:Enable()
				inst.AnimState:PlayAnimation("idle_ylw", true)
			else
				inst.components.trader:Disable()
				inst.AnimState:PlayAnimation("idle", true)
			end
		end
	end,
})

------魏征
MakePass({
	name = "pass_commissioner_wz",
	assets = {
		Asset("ANIM", "anim/pass_commissioner.zip"),
	},
	prefabs = nil,
	fn_common = function(inst)
		inst.Light:SetColour(0/255, 101/255, 190/255)
	end,
	-- fn_between = function(inst)end,
	fn_server = function(inst)
		inst:AddComponent("trader")
		inst.components.trader.onaccept = OnGetItemFromPlayer
        inst.components.trader:SetAcceptTest(function(inst, item, giver)
			return
				inst.isactived and --激活状态
				giver and giver:HasTag("yama_commissioners") and --无常专用
				item:HasTag("soul_lost") --只要善恶魂
		end)
		inst.components.trader:Disable()
		inst.fn_mythuse_detail = function(inst, doer, isactived) --undo: 路引传信功能的开启与关闭
			if isactived then
				inst.components.trader:Enable()
				inst.AnimState:PlayAnimation("idle_wz", true)
			else
				inst.components.trader:Disable()
				inst.AnimState:PlayAnimation("idle", true)
			end
		end
	end,
})

------钟馗
MakePass({
	name = "pass_commissioner_zk",
	assets = {
		Asset("ANIM", "anim/pass_commissioner.zip"),
	},
	prefabs = nil,
	fn_common = function(inst)
		inst.Light:SetColour(190/255, 0/255, 186/255)
	end,
	-- fn_between = function(inst)end,
	fn_server = function(inst)
		inst:AddComponent("trader")
		inst.components.trader.onaccept = OnGetItemFromPlayer
        inst.components.trader:SetAcceptTest(function(inst, item, giver)
			return
				inst.isactived and --激活状态
				giver and giver:HasTag("yama_commissioners") and --无常专用
				item:HasTag("soul_lost") --只要善恶魂
		end)
		inst.components.trader:Disable()
		inst.fn_mythuse_detail = function(inst, doer, isactived) --undo: 路引传信功能的开启与关闭
			if isactived then
				inst.components.trader:Enable()
				inst.AnimState:PlayAnimation("idle_zk", true)
			else
				inst.components.trader:Disable()
				inst.AnimState:PlayAnimation("idle", true)
			end
		end
	end,
})

local function getstatuscj(inst)
	return STRINGS.CHARACTERS.GENERIC.DESCRIBE[string.upper(inst.prefab)].."\n"..(STRINGS.NAMES.SOUL_SPECTER or "善").."："..inst.mysouls.w.." "..(STRINGS.NAMES.SOUL_GHAST or "恶").."："..inst.mysouls.b
end
------崔珏
--------TheInput:GetWorldEntityUnderMouse().mysouls.b = 98 TheInput:GetWorldEntityUnderMouse().mysouls.w = 98
local function InitMySouls(inst, bmax, wmax, itemname)

	inst.components.inspectable.descriptionfn = getstatuscj

	inst.mysouls = { b = 0, w = 0, bmax = bmax, wmax = wmax } --b恶魂、w善魂
	inst.fn_cangetsoul = function(inst, soultype)
		if not inst.isactived then
			return false
		end

		if soultype == "soul_ghast" then
			if inst.mysouls.b < inst.mysouls.bmax then
				return true
			end
		else
			if inst.mysouls.w < inst.mysouls.wmax then
				return true
			end
		end

		return false
	end
	inst.fn_getsoul = function(inst, soultype, count) --soultype=soul_ghast、soul_specter
		if soultype == "soul_ghast" then
			inst.mysouls.b = math.min(inst.mysouls.bmax, inst.mysouls.b+count)
		else
			inst.mysouls.w = math.min(inst.mysouls.wmax, inst.mysouls.w+count)
		end

		if inst.mysouls.b >= inst.mysouls.bmax and inst.mysouls.w >= inst.mysouls.wmax then
			local theta = math.random() * 2 * PI
			local pt = inst:GetPosition()
			local radius = 2
			local offset = FindWalkableOffset(pt, theta, radius, 6, true)
			if offset ~= nil then
				pt.x = pt.x + offset.x
				pt.z = pt.z + offset.z
			end
			local fx  = SpawnPrefab("commissioner_teleport")
			fx.Transform:SetPosition(pt.x, pt.y, pt.z)
			local light = SpawnPrefab("commissioner_booklight")
			light.Transform:SetPosition(pt.x, pt.y, pt.z)

			inst:DoTaskInTime(0.5,function()
				local new = SpawnPrefab(itemname)
				new.Transform:SetPosition(pt.x, pt.y, pt.z)
				inst.mysouls.b = 0
				inst.mysouls.w = 0
			end)
		end
	end

	inst.OnSave = function(inst, data)
		if inst.mysouls ~= nil and (inst.mysouls.b > 0 or inst.mysouls.w > 0) then
			data.mysouls = { b = inst.mysouls.b, w = inst.mysouls.w }
		end
	end
	inst.OnLoad = function(inst, data)
		if data ~= nil then
			if data.mysouls ~= nil then
				inst.mysouls.b = data.mysouls.b or 0
				inst.mysouls.w = data.mysouls.w or 0
			end
		end
	end
end

local function OnGetItemFromPlayerOther(inst, giver, item)
	inst.fn_getsoul(inst,item.prefab,1)
end

MakePass({
	name = "pass_commissioner_cj",
	assets = {
		Asset("ANIM", "anim/pass_commissioner.zip"),
	},
	prefabs = nil,
	fn_common = function(inst)
		inst.Light:SetColour(190/255, 9/255, 0/255)
		inst:AddTag("soulabsorber") --这个标签能使其优先吸收善恶魂
	end,
	-- fn_between = function(inst)end,
	fn_server = function(inst)
		InitMySouls(inst, 99, 99, "commissioner_book") --undo

		inst:AddComponent("trader")
		inst.components.trader.onaccept = OnGetItemFromPlayerOther
        inst.components.trader:SetAcceptTest(function(inst, item, giver)
			return
				inst.isactived and --激活状态
				giver and giver:HasTag("yama_commissioners") and --无常专用
				item:HasTag("soul_lost") --只要善恶魂
		end)
		inst.components.trader:Disable()
		inst.fn_mythuse_detail = function(inst, doer, isactived)
			if isactived then
				inst.components.trader:Enable()
				inst.AnimState:PlayAnimation("idle_cj", true)
			else
				inst.components.trader:Disable()
				inst.AnimState:PlayAnimation("idle", true)
			end
		end
	end,
})

------陆之道
MakePass({
	name = "pass_commissioner_lzd",
	assets = {
		Asset("ANIM", "anim/pass_commissioner.zip"),
	},
	prefabs = nil,
	fn_common = function(inst)
		inst.Light:SetColour(66/255, 178/255, 57/255)

		--trader (from trader component) added to pristine state for optimization
        inst:AddTag("trader")
	end,
	-- fn_between = function(inst)end,
	fn_server = function(inst)
		inst:AddComponent("trader")
        inst.components.trader:SetAcceptTest(function(inst, item, giver)
			return
				inst.isactived and --激活状态
				giver and giver:HasTag("yama_commissioners") and --无常专用
				item:HasTag("soul_lost") --只要善恶魂
		end)
        inst.components.trader.onaccept = function(inst, giver, item)
			if giver and giver.components.inventory ~= nil then
				local count = item.components.stackable ~= nil and item.components.stackable:StackSize() or 1
				local newitem = SpawnPrefab(item:HasTag("soul_specter") and "soul_ghast" or "soul_specter")
				if newitem ~= nil then
					if count > 1 then
						newitem.components.stackable:SetStackSize(count)
					end
					giver.components.inventory:GiveItem(newitem, nil, inst:GetPosition())
				end
			end
		end
        inst.components.trader.onrefuse = function(inst, giver, item)
			if giver and item then
				local saytype = nil
				if not giver:HasTag("yama_commissioners") then
					saytype = "ONLYYAMA"
				elseif not inst.isactived then
					saytype = "INACTIVE"
				elseif not item:HasTag("soul_lost") then
					saytype = "NOTSOUL"
				end
				if saytype ~= nil then
					if giver.components.talker ~= nil then
						giver.components.talker:Say(GetString(giver, "DESCRIBE", { "PASS_COMMISSIONER_LZD", saytype }))
					end
				end
			end
		end
        inst.components.trader.deleteitemonaccept = true
		inst.components.trader.acceptnontradable = false

		inst.fn_mythuse_detail = function(inst, doer, isactived)
			if isactived then
				inst.AnimState:PlayAnimation("idle_lzd", true)
				inst:AddTag("soulinvertor")
				inst.components.trader:Enable()
			else
				inst.AnimState:PlayAnimation("idle", true)
				inst:RemoveTag("soulinvertor")
				inst.components.trader:Disable()
			end
		end
	end,
})

------孟婆
MakePass({
	name = "pass_commissioner_mp",
	assets = {
		Asset("ANIM", "anim/pass_commissioner.zip"),
	},
	prefabs = nil,
	fn_common = function(inst)
		inst.Light:SetColour(0/255, 210/255, 235/255)
		inst:AddTag("soulabsorber") --这个标签能使其优先吸收善恶魂
	end,
	-- fn_between = function(inst)end,
	fn_server = function(inst)
		InitMySouls(inst, 20, 20, "commissioner_mpt")
		inst:AddComponent("trader")
		inst.components.trader.onaccept = OnGetItemFromPlayerOther
        inst.components.trader:SetAcceptTest(function(inst, item, giver)
			return
				inst.isactived and --激活状态
				giver and giver:HasTag("yama_commissioners") and --无常专用
				item:HasTag("soul_lost") --只要善恶魂
		end)
		inst.components.trader:Disable()
		inst.fn_mythuse_detail = function(inst, doer, isactived)
			if isactived then
				inst.components.trader:Enable()
				inst.AnimState:PlayAnimation("idle_mp", true)
			else
				inst.components.trader:Disable()
				inst.AnimState:PlayAnimation("idle", true)
			end
		end
	end,
})

--------------------
--------------------

return unpack(prefs)
