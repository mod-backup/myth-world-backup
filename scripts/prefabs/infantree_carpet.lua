local assets =
{
    Asset("ANIM", "anim/infantree_carpet.zip"),
    Asset("IMAGE", "images/inventoryimages/infantree_carpet.tex"),
    Asset("ATLAS", "images/inventoryimages/infantree_carpet.xml"),
}

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("infantree_carpet")
    inst.AnimState:SetBuild("infantree_carpet")
    inst.AnimState:PlayAnimation("idle")

    inst.Transform:SetScale(1.2, 1.2, 1.2)
	inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(-2)

    inst:AddTag("NOBLOCK")
    inst:AddTag("NOCLICK")
    
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    --[[inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.DIG)
    inst.components.workable:SetWorkLeft(1)
    inst.components.workable:SetOnFinishCallback(inst.Remove)]]

    local burnable_locator = inst:SpawnChild("locator_infantree_carpet")
	burnable_locator.carpet = inst

    return inst
end

local function posfn(inst)
    inst.AnimState:SetSortOrder(-2)
end

local function applytickdamage(inst)

end

local function onsmoldering(inst)
	inst:RemoveTag("NOCLICK")
end

local function onignite(inst)
	inst:RemoveTag("NOCLICK")
end

local function onstopsmoldering(inst)
	inst:AddTag("NOCLICK")
end

local function onextinguish(inst)
	inst:AddTag("NOCLICK")
end

local function onremove(inst)
    if inst.carpet then
        inst.carpet:Remove()
    end
end

local function fxfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst:AddTag("NOBLOCK")
    inst:AddTag("NOCLICK")
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    MakeLargeBurnable(inst)
	inst.components.burnable.extinguishimmediately = false
	inst.components.burnable:SetBurnTime(4)
	inst.components.burnable:SetOnIgniteFn(onignite)
	inst.components.burnable:SetOnExtinguishFn(onextinguish)
	inst.components.burnable:SetOnSmolderingFn(onsmoldering)
	inst.components.burnable:SetOnStopSmolderingFn(onstopsmoldering)
    inst.components.burnable.onburnt = onremove
	MakeLargePropagator(inst)

    return inst
end

return Prefab("infantree_carpet", fn, assets),
    Prefab("locator_infantree_carpet", fxfn),
    MakePlacer("infantree_carpet_placer", "infantree_carpet", "infantree_carpet", "idle",true,nil,nil,1.2,nil,nil,posfn)
