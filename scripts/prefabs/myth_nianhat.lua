local assets = {
    Asset("ANIM", "anim/myth_nianhat.zip"),
    Asset("IMAGE", "images/inventoryimages/myth_nianhat.tex"),
    Asset("ATLAS", "images/inventoryimages/myth_nianhat.xml"),
}


local function onequiphat(inst, owner)
    owner.AnimState:OverrideSymbol("SWAP_FACE", "myth_nianhat", "SWAP_FACE")
    owner.AnimState:OverrideSymbol("swap_hat", "myth_nianhat", "swap_hat")
    owner.AnimState:Show("HAT")
    owner.AnimState:Show("HAIR_HAT")
    owner.AnimState:Hide("HAIR_NOHAT")
    owner.AnimState:Hide("HAIR")

    if owner:HasTag("player") then
        owner.AnimState:Hide("HEAD")
        owner.AnimState:Show("HEAD_HAT")
    end
    if owner.components.health ~= nil then
        owner.components.health.externalfiredamagemultipliers:SetModifier(inst, 2)
    end
end

local function onunequiphat(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_hat")
    owner.AnimState:ClearOverrideSymbol("SWAP_FACE")
    owner.AnimState:Hide("HAT")
    owner.AnimState:Hide("HAIR_HAT")
    owner.AnimState:Show("HAIR_NOHAT")
    owner.AnimState:Show("HAIR")

    if owner:HasTag("player") then
        owner.AnimState:Show("HEAD")
        owner.AnimState:Hide("HEAD_HAT")
    end
    if owner.components.health ~= nil then
        owner.components.health.externalfiredamagemultipliers:RemoveModifier(inst)
    end
end

local function fn(Sim)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("myth_nianhat")
    inst.AnimState:SetBuild("myth_nianhat")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("hat")
    inst:AddTag("myth_nianhat")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_nianhat.xml"

    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.HEAD
    inst.components.equippable:SetOnEquip(onequiphat)
    inst.components.equippable:SetOnUnequip(onunequiphat)

    inst:AddComponent("armor")
    inst.components.armor:InitCondition(2000, 0.7)

    inst:AddComponent("tradable")

    MakeHauntableLaunch(inst)
    return inst
end

return Prefab("myth_nianhat", fn, assets)
