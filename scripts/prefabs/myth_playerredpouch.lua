local assets = {
    Asset("ANIM", "anim/myth_playerredpouch.zip"),
    Asset("ANIM", "anim/myth_playerredpouch_shinefx.zip"),
	Asset("ATLAS", "images/inventoryimages/myth_playerredpouch_normal.xml"),
    Asset("ATLAS", "images/inventoryimages/myth_playerredpouch_super.xml"),
}

local prefabs = {

}

local function shine(inst)
    inst.task = nil
    inst.task = inst:DoTaskInTime(2 + math.random() * 4, shine)
    if inst.components.inventoryitem.owner ~= nil then
        return
    end
    local fx = SpawnPrefab("myth_playerredpouch_shinefx")
    fx.entity:AddFollower()
    fx.Follower:FollowSymbol(inst.GUID, "images", 0, -40, 0)
end

local buffs = {"fu","lu","shou"}

local function OnUnwrapped(inst, pos, doer)
    if inst.burnt then
        SpawnPrefab("ash").Transform:SetPosition(pos:Get())
    else
        if inst.typeredpouch == "super" and doer ~=nil and doer.components.debuffable ~= nil and doer.components.debuffable:IsEnabled() then
            local buff = "myth_nianbuff_"..buffs[math.random(#buffs)]
            doer.components.debuffable:AddDebuff(buff, buff)
        end
        SpawnPrefab("redpouch_unwrap").Transform:SetPosition(pos:Get())
    end
    if doer ~= nil and doer.SoundEmitter ~= nil then
        doer.SoundEmitter:PlaySound("dontstarve/common/together/packaged")
    end
    inst:Remove()
end

local function makeredpouch(name)
    local function fn()
        local inst = CreateEntity()
        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.shadow = inst.entity:AddDynamicShadow()
        inst.entity:AddNetwork()

        MakeInventoryPhysics(inst)

        inst.shadow:SetSize( 0.8, .55 )

        inst.AnimState:SetBank("myth_playerredpouch")
        inst.AnimState:SetBuild("myth_playerredpouch")
        inst.AnimState:PlayAnimation(name,true)

        inst:AddTag("redpouch") --谁给我发个红包呢？

        inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

        MakeInventoryFloatable(inst, "med", nil, 0.68)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst.typeredpouch = name

        inst.AnimState:SetTime(math.random() * inst.AnimState:GetCurrentAnimationLength())

        inst:AddComponent("inventoryitem")
        inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_playerredpouch_"..name..".xml"

        inst:AddComponent("tradable")

        inst:AddComponent("inspectable")

        inst:AddComponent("unwrappable")
        inst.components.unwrappable:SetOnUnwrappedFn(OnUnwrapped)

        MakeHauntableLaunch(inst)

        shine(inst)

        return inst
    end

    return Prefab("myth_playerredpouch_"..name, fn, assets, prefabs)
end

return makeredpouch("normal"),makeredpouch("super")
