local assets =
{
    Asset("ANIM", "anim/firecrackers.zip"),
    Asset("ANIM", "anim/firecrackers_myth.zip"),
    Asset("IMAGE", "images/inventoryimages/miniflare_myth.tex"),
    Asset("ATLAS", "images/inventoryimages/miniflare_myth.xml"),
}

local prefabs =
{
    "explode_firecrackers",
}

local function DoPop(inst, remaining, total, level, hissvol)
    local x, y, z = inst.Transform:GetWorldPosition()
    SpawnPrefab("explode_firecrackers").Transform:SetPosition(x, y, z)

    for i, v in ipairs(TheSim:FindEntities(x, y, z, TUNING.FIRECRACKERS_STARTLE_RANGE, { "canbestartled" })) do
        v:PushEvent("startle", { source = inst })
    end

    if remaining > 1 then
        inst.AnimState:PlayAnimation("spin_loop"..tostring(math.random(3)))

        if hissvol > .5 then
            hissvol = hissvol - .1
            inst.SoundEmitter:SetVolume("hiss", hissvol)
        end

        local newlevel = 8 - math.ceil(8 * remaining / total)
        for i = level + 1, newlevel do
            inst.AnimState:Hide("F"..tostring(i))
        end

        local angle = math.random() * 2 * PI
        local spd = 1.5
        inst.Physics:Teleport(x, math.max(y * .5, .1), z)
        inst.Physics:SetVel(math.cos(angle) * spd, 8, math.sin(angle) * spd)

        --23 frames in spin_loop, so if the delay gets longer, loop the anim
        inst:DoTaskInTime(.3 + .3 * math.random(), DoPop, remaining - 1, total, newlevel, hissvol)

        if remaining/total <= 0.55 then --引爆一半时间后会自动引燃附近的鞭炮、爆竹和火药
            for i, v in ipairs(TheSim:FindEntities(x, y, z, 4, { "explosive" })) do
                if
                    v ~= inst and v.components.burnable ~= nil
                    and not v.components.burnable:IsSmoldering()
                    and v:HasTag("canlight")
                    and not ((v:HasTag("fueldepleted") and not v:HasTag("burnableignorefuel")) or v:HasTag("INLIMBO"))
                then
                    v.components.burnable:Ignite()
                end
            end
        end
    else
        inst:Remove()
    end
end

local function StartExploding(inst, count)
    inst:AddTag("NOCLICK")
    inst:AddTag("scarytoprey")
    inst.Physics:SetFriction(.2)
    DoPop(inst, count, count, 0, 1)
    if TheWorld.state.season == "winter" then
        local x, y, z = inst.Transform:GetWorldPosition()
        for i, v in ipairs(TheSim:FindEntities(x, y, z, 15, { "player" })) do
            if v and v.components.myth_playernwd then
                v.components.myth_playernwd:DoDelta("firecrackers_nwd",2)
            end
        end
    end
end

local function StartFuse(inst)
    inst.starttask = nil
    inst:RemoveComponent("burnable")

    inst.AnimState:PlayAnimation("burn")
    inst.SoundEmitter:PlaySound("dontstarve/common/blackpowder_fuse_LP", "hiss")

    inst:DoTaskInTime(inst.AnimState:GetCurrentAnimationLength(), StartExploding,
        math.floor(33.4 * math.sqrt(inst.components.stackable:StackSize() + 3) - 58.8 + .5) * 3) --3倍时间

    inst:RemoveComponent("stackable")
    inst.persists = false
end

local function OnIgniteFn(inst)
    if inst.starttask == nil then
        inst.starttask = inst:DoTaskInTime(0, StartFuse)
    end
    inst.components.inventoryitem.canbepickedup = false
end

local function OnExtinguishFn(inst)
    if inst.starttask ~= nil then
        inst.starttask:Cancel()
        inst.starttask = nil
        inst.components.inventoryitem.canbepickedup = true
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("firecrackers")
    inst.AnimState:SetBuild("firecrackers_myth")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("explosive")

    MakeInventoryFloatable(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "firecrackers_myth"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/firecrackers_myth.xml"

    inst:AddComponent("inspectable")

    inst:AddComponent("burnable")
    -- inst.components.burnable:SetOnIgniteFn(DefaultBurnFn) --点燃时不再能被保存
    -- inst.components.burnable:SetOnExtinguishFn(DefaultExtinguishFn) --熄灭时可以被保存
    inst.components.burnable:SetBurnTime(nil)
    inst.components.burnable:SetOnIgniteFn(OnIgniteFn)
    inst.components.burnable:SetOnExtinguishFn(OnExtinguishFn)

    MakeHauntableLaunchAndIgnite(inst)

    return inst
end

return Prefab("firecrackers_myth", fn, assets, prefabs)
