
local assets =
{
    Asset("ANIM", "anim/myth_plant_infantree_small.zip"),
}

local prefabs =
{
    "collapse_small",
}

local function onhammered(inst, worker)
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("metal")
    inst:Remove()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    inst.MiniMapEntity:SetIcon("myth_well.tex")

	MakeObstaclePhysics(inst, 1.5)

    inst:AddTag("antlion_sinkhole_blocker")
    inst:AddTag("birdblocker")

    inst.AnimState:SetBank("myth_plant_infantree_small")
    inst.AnimState:SetBuild("myth_plant_infantree_small")
    inst.AnimState:PlayAnimation("tree")

    inst:SetPrefabNameOverride("myth_well")

    inst:AddTag("infantree_small")
    
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    inst.dried = false

    inst:AddComponent("inspectable")

    inst:AddComponent("lootdropper")
	
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(3)
    inst.components.workable:SetOnFinishCallback(onhammered)

    inst.components.lootdropper:SetLoot( {"log","log","log","cutstone","cutstone"})

    MakeHauntable(inst)
    return inst
end

return Prefab("myth_plant_infantree_small", fn, assets, prefabs)
