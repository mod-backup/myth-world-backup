local assets =
{
    Asset("ANIM", "anim/myth_stool.zip"),
    Asset( "ATLAS", "images/inventoryimages/myth_stool.xml" ),
    Asset( "ATLAS", "images/inventoryimages/myth_stool_star.xml" ),
    Asset( "ATLAS", "images/inventoryimages/myth_stool_stone.xml" ),
    Asset( "ATLAS", "images/inventoryimages/myth_stool_bigstone.xml" ),
    Asset( "ATLAS", "images/inventoryimages/myth_stool_bear.xml" ),
    Asset( "ATLAS", "images/inventoryimages/myth_stool_tiger.xml" ),
    Asset( "ATLAS", "images/inventoryimages/myth_stool_golden.xml" ),
}

local prefabs =
{
}

local function onhammered(inst, worker)
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("metal")
    inst:Remove()
end

local function onhit(inst, worker)

end

local function onremove(inst)
    if inst.user and inst.user:IsValid() and not (inst.user.components.health and inst.user.components.health:IsDead())
        and inst.user.sg and inst.user.sg:HasStateTag("myth_stool_sit") then
        inst.user.sg.statemem.sittarget = nil
        inst.user:GoToState("idle")
    end
end

local function setuser(inst,doer)
    inst.user = doer
    inst:ListenForEvent("onremove",onremove)
    inst:RemoveFromScene()
end

local function loseuser(inst,doer)
    inst:ReturnToScene()
    inst.user = nil
    inst:RemoveEventCallback("onremove",onremove)
end

local function onuse(inst,doer)
    if inst and inst:IsValid() and doer and not inst.user then
        if doer.components.rider and doer.components.rider:IsRiding() then
            return false
        elseif doer.components.mk_flyer and doer.components.mk_flyer:IsFlying() then
            return false
        elseif doer._is_player_astral ~= nil and  doer._is_player_astral:value() then
            return false
        end
        doer.sg:GoToState("myth_stool_sit",inst)
        inst:SetUser(doer)
        return true
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("myth_stool")
    inst.AnimState:SetBuild("myth_stool")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("structure")

    inst.MYTH_USE_TYPE = "SIT"
    inst.onusesgname = "myth_stool_sit_pre"

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.user = nil

    inst:AddComponent("inspectable")

    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = false
	inst.components.myth_use_inventory.canusescene  = true
	inst.components.myth_use_inventory:SetOnUseFn(onuse)
    
    inst:AddComponent("lootdropper")
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(3)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetHauntValue(TUNING.HAUNT_TINY)

    MakeSmallBurnable(inst, TUNING.SMALL_BURNTIME)
    MakeSmallPropagator(inst)

    inst.SetUser = setuser
    inst.LoseUser = loseuser
    return inst
end

return Prefab("myth_stool", fn, assets),
    MakePlacer("myth_stool_placer", "myth_stool", "myth_stool", "idle")
