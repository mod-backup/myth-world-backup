local assets=
{
	Asset("ANIM", "anim/myth_plant_infantree_vine.zip"),
}

local prefabs =
{

}

local function onsave(inst, data)
    data.animnum = inst.animnum
end

local function onload(inst, data)
    if data then
        inst.animnum = data.animnum
        inst.AnimState:PlayAnimation("idle"..inst.animnum,true)
    end
end

local function commonfn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddNetwork()
	
	--inst.shadow = inst.entity:AddDynamicShadow()
	--inst.shadow:SetSize( 1.5, .75 )
    local s  = 1.4
    inst.Transform:SetScale(s, s, s)
	inst.AnimState:SetBank("myth_plant_infantree_vine")
    inst.AnimState:SetBuild("myth_plant_infantree_vine")
    inst.animnum = math.random(1,2)
	inst.AnimState:PlayAnimation("idle"..inst.animnum,true)

    inst:AddTag("NOBLOCK")
    inst:AddTag("NOCLICK")
    inst:AddTag("flying")

    if not TheNet:IsDedicated() then
    	inst:AddComponent("distancefade")
    	inst.components.distancefade:Setup(15,25)
    end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.OnSave = onsave
    inst.OnLoad = onload
	
	return inst
end

return Prefab("myth_plant_infantree_vine", commonfn, assets, prefabs)
