local assets =
{
    Asset("ANIM", "anim/myth_statue_pandaman.zip"),
}

local prefabs =
{

}
local function OnIsNight_npc(inst, isnight) --夜晚开始时，尝试生成熊猫人npc
    if TheWorld.state.isnight and BSHOPSTATE ~= nil then
        inst:DoTaskInTime(0.5+math.random(), function()

            local shop = FindEntity(inst, 32, nil, {"myth_shop_building"})
            if not shop then  --周围没有任何的小铺
                return
            end

            for i,num in pairs(BSHOPSTATE.npc) do
                if num ~= nil then
                    if num > 0 then --已经有npc了就不再产生新的（同时只存在一种）
                        return
                    end
                end
            end
            local types = {}
            for i,v in pairs(BSHOPSTATE.shops) do
                if v ~= nil then
                    if
                        v.num <= 0 --店铺还未存在
                        and BSHOPSTATE.npc[i] <= 0 --还没有产生过NPC
                    then
                        table.insert(types, i)
                    end
                end
            end

            --开始随机选择并产生一种店铺的npc
            if #types <= 0 then
                return
            end
            local thetype = types[math.random(#types)]
            local newnpc = SpawnPrefab("pandaman_myth")
            if newnpc ~= nil then
                newnpc:SetShopType(thetype)
                newnpc.components.knownlocations:RememberLocation("pandaman", inst:GetPosition())
                local pt = inst:GetPosition()
                local theta = math.random() * 2 * PI
                local radius = 4
                local offset = FindWalkableOffset(pt, theta, radius, 6, true)
                if offset ~= nil then
                    pt.x = pt.x + offset.x
                    pt.z = pt.z + offset.z
                end
                newnpc.Transform:SetPosition(pt.x,0,pt.z)
                BSHOPSTATE.npc[thetype] = BSHOPSTATE.npc[thetype] + 1
            end
        end)
    end
end

local function TrySpawnNpc(inst)
    inst:WatchWorldState("phase", OnIsNight_npc)
end

local function fn()
	local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
     
	MakeObstaclePhysics(inst, 1.25)
	
    inst.AnimState:SetBank("myth_statue_pandaman")
    inst.AnimState:SetBuild("myth_statue_pandaman")
    inst.AnimState:PlayAnimation("idle")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    TrySpawnNpc(inst)

    return inst
end

return Prefab("myth_statue_pandaman", fn, assets, prefabs)
