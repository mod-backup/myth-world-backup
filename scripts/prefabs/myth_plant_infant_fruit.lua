
local assets=
{
	Asset("ANIM", "anim/myth_plant_infant_fruit.zip"),
    Asset("ATLAS", "images/inventoryimages/myth_infant_fruit.xml"),
}

local prefabs =
{

}
local function onpicked(inst, picker, loot)
    local pos = inst:GetPosition()
    local fruit = SpawnPrefab("myth_infant_fruit")
    if picker and picker.components.inventory and picker.components.inventory:EquipHasTag("infant_fruit_picker") then
        picker.components.inventory:GiveItem(fruit,nil,pos)
        picker:PushEvent("picksomething", { object = inst, loot = fruit })
    else
        fruit.Transform:SetPosition(pos.x,2,pos.z)
        fruit:DoTaskInTime(0.3,function()
            if fruit:IsValid() and not fruit.components.inventoryitem.owner then
                fruit:OnDrop()
            end
        end)
        fruit.picker = picker
    end
    inst:Remove()
end

local function falldown(inst)
    inst.AnimState:PlayAnimation("spawn")
    inst.AnimState:PushAnimation("idle")
end

local function onburnt(inst)
    local theta = math.random() * 2 * PI
    local spd = math.random() * 2
    local ash = SpawnPrefab("charcoal")
    ash.Transform:SetPosition(inst:GetPosition():Get())
    ash.Physics:SetVel(math.cos(theta) * spd, 8 + math.random() * 4, math.sin(theta) * spd)
    inst:Remove()
end

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.shadow = inst.entity:AddDynamicShadow()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    inst.MiniMapEntity:SetIcon("myth_plant_infant_fruit.tex")

	inst.shadow:SetSize( 1.5, .75 )
    
	inst.AnimState:SetBank("myth_plant_infant_fruit")
    inst.AnimState:SetBuild("myth_plant_infant_fruit")
	inst.AnimState:PlayAnimation("idle", true)

    inst:AddTag("flying")
    inst:AddTag("NOBLOCK")
    inst:AddTag("myth_plant_infant_fruit")
    inst:AddTag("moistureimmunity")
    inst:AddTag("uppickable")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.AnimState:SetTime(math.random() * inst.AnimState:GetCurrentAnimationLength())

    inst.fall_down_fn = falldown

    --DebugSpawn"myth_plant_infant_fruit":fall_down_fn()

	inst:AddComponent("inspectable")
    
    inst:AddComponent("pickable")
    inst.components.pickable.picksound = "dontstarve/wilson/harvest_berries"
    inst.components.pickable.onpickedfn = onpicked
    inst.components.pickable.canbepicked  = true

    MakeSmallBurnable(inst, nil, nil, nil, "infant")
    inst.components.burnable.fxdata[1].prefab = "character_fire"
    inst.components.burnable.fxdata[1].followaschild = true
    inst.components.burnable:SetFXOffset(0, 1, 0)
    inst.components.burnable:SetBurnTime(0.2)
    inst.components.burnable:SetOnBurntFn(onburnt) -- Burning is handled differently, but if it ever gets to this point it's better to just remove the object
    MakeSmallPropagator(inst)

    MakeHauntable(inst)
    
	return inst
end

local function onperishchange(inst)
    if inst:GetIsWet() and not inst.wetperishable then
        inst.wetperishable = true
        inst:DoTaskInTime(1+math.random(2),inst.components.perishable:Perish())
    end
end

local function ondrop(inst)
    local x, y, z = inst:GetPosition():Get()
    if not TheWorld.Map:IsOceanTileAtPoint(x, y, z) then
        local fx = SpawnPrefab("waterplant_destroy")
        fx.Transform:SetPosition(x, 0, z)
        local ents = TheSim:FindEntities(x,y,z, 12,nil,{"playerghost"},{"player","myth_tudi"})
        for k,v in pairs(ents) do
            if (v == inst.picker or v.prefab == "myth_tudi") and not (v.components.health and v.components.health:IsDead()) and v.components.talker  then
                v.components.talker:Say( STRINGS.MYTH_INFANT_FRUIT_ONDROP[v.prefab]  or STRINGS.MYTH_INFANT_FRUIT_ONDROP.GENERIC)
            end
        end
        inst:Remove()
    end
end

local function onfruitburnt(inst)
    local ash = SpawnPrefab("charcoal")
    ash.Transform:SetPosition(inst:GetPosition():Get())
    inst:Remove()
end

local function Get_Myth_Food_Table(inst)
    return "myth_plant_infant_fruit","food"
end
local function food()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddNetwork()

	inst.AnimState:SetBank("myth_plant_infant_fruit")
    inst.AnimState:SetBuild("myth_plant_infant_fruit")
	inst.AnimState:PlayAnimation("fruit")

    inst:AddTag("preparedfood")
    MakeInventoryFloatable(inst, "med", nil, 0.68)

    MakeInventoryPhysics(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    inst.Get_Myth_Food_Table  = Get_Myth_Food_Table
	inst:AddComponent("inspectable")
    
    inst:AddComponent("edible")
    inst.components.edible.foodtype = FOODTYPE.GOODIES
    inst.components.edible.hungervalue = 888
    inst.components.edible.healthvalue = 888
    inst.components.edible.sanityvalue = 888
    inst.components.edible.oneaten = function(inst,eater)
        if eater.components.health ~= nil then
            eater.components.health:DoDelta(8888)
            if eater.components.sanity then
                eater.components.sanity:DoDelta(8888)
            end
            if eater.components.hunger then
                eater.components.hunger:DoDelta(8888)
            end
            if eater.components.debuffable ~= nil and eater.components.debuffable:IsEnabled() then
                eater.components.debuffable:AddDebuff("myth_infant_buff","myth_infant_buff")
            end
            eater:SpawnChild("mythinfant_fruitfx")
        end
    end
    inst:AddComponent("tradable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_infant_fruit.xml"

    inst:AddComponent("perishable")
    inst.components.perishable:SetPerishTime(TUNING.PERISH_ONE_DAY)
    inst.components.perishable.onperishreplacement = "spoiled_food"
    inst.components.perishable:StartPerishing()

    inst:ListenForEvent("ondropped", ondrop)
    inst:DoPeriodicTask(1,onperishchange,1)
    inst.OnDrop = ondrop
    MakeHauntableLaunch(inst)
    
    MakeSmallBurnable(inst)
    inst.components.burnable.onburnt =onfruitburnt
    inst.components.burnable:SetBurnTime(1)

	return inst
end

return Prefab("myth_plant_infant_fruit", fn, assets, prefabs),
    Prefab("myth_infant_fruit", food)