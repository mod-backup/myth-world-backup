local prefs = {}

--------------------------------------------------------------------------
--[[ 通用 ]]
--------------------------------------------------------------------------

local function MakeItem(data)
    table.insert(prefs, Prefab(
        data.name,
        function()
            local inst = CreateEntity()

            inst.entity:AddTransform()
            inst.entity:AddAnimState()
            inst.entity:AddNetwork()

            inst.AnimState:SetBank(data.name)
            inst.AnimState:SetBuild(data.name)
            inst.AnimState:PlayAnimation("idle")

            MakeInventoryPhysics(inst)

            if data.fn_common ~= nil then
                data.fn_common(inst)
            end

            if data.floatable ~= nil then
                MakeInventoryFloatable(inst, data.floatable[2], data.floatable[3], data.floatable[4])
                if data.floatable[1] ~= nil then
                    local OnLandedClient_old = inst.components.floater.OnLandedClient
                    inst.components.floater.OnLandedClient = function(self)
                        OnLandedClient_old(self)
                        self.inst.AnimState:SetFloatParams(data.floatable[1], 1, self.bob_percent)
                    end
                end
            end

            inst.entity:SetPristine()
            if not TheWorld.ismastersim then
                return inst
            end

            inst:AddComponent("inspectable")

            inst:AddComponent("inventoryitem")
            inst.components.inventoryitem.imagename = data.name
            inst.components.inventoryitem.atlasname = "images/inventoryimages/"..data.name..".xml"

            inst:AddComponent("equippable")

            MakeHauntableLaunch(inst)

            if data.fn_server ~= nil then
                data.fn_server(inst)
            end

            return inst
        end,
        data.assets,
        data.prefabs
    ))
end

--------------------------------------------------------------------------
--[[ 铸铁大刀 ]]
--------------------------------------------------------------------------

MakeItem({
    name = "myth_iron_broadsword",
    assets = {
        Asset("ANIM", "anim/myth_iron_broadsword.zip"),
        Asset("ATLAS", "images/inventoryimages/myth_iron_broadsword.xml"),
        Asset("IMAGE", "images/inventoryimages/myth_iron_broadsword.tex"),
    },
    prefabs = nil,
    floatable = {0.05, "small", 0.2, 0.6},
    fn_common = function(inst)
        inst:AddTag("sharp")

        --weapon (from weapon component) added to pristine state for optimization
        inst:AddTag("weapon")
    end,
    fn_server = function(inst)
        inst:AddComponent("weapon")
        inst.components.weapon:SetDamage(51)

        inst:AddComponent("finiteuses")
        inst.components.finiteuses:SetMaxUses(400)
        inst.components.finiteuses:SetUses(400)
        inst.components.finiteuses:SetOnFinished(inst.Remove)

        inst.components.equippable:SetOnEquip(function(inst, owner)
            owner.AnimState:OverrideSymbol("swap_object", "myth_iron_broadsword", "swap_object")
            owner.AnimState:Show("ARM_carry")
            owner.AnimState:Hide("ARM_normal")
        end)
        inst.components.equippable:SetOnUnequip(function(inst, owner)
            owner.AnimState:Hide("ARM_carry")
            owner.AnimState:Show("ARM_normal")
        end)
    end,
})

--------------------------------------------------------------------------
--[[ 铸铁头盔 ]]
--------------------------------------------------------------------------

local function SetWeakness(inst) --电系攻击能造成更多伤害
    inst.components.armor:AddWeakness("lightninggoat", TUNING.BEAVER_WOOD_DAMAGE) --电羊
    inst.components.armor:AddWeakness("electric", TUNING.BEAVER_WOOD_DAMAGE) --电标签（貌似没有对象有这个标签，但是为了兼容吧。而且官方的电攻设定都在变量里而不是标签里，没法用这个机制）
    inst.components.armor:AddWeakness("electrified", TUNING.BEAVER_WOOD_DAMAGE) --电气（棱镜的电气BOSS）
    inst.components.armor:AddWeakness("yj_spear", TUNING.BEAVER_WOOD_DAMAGE) --三尖两刃刀
    --晨星没有专属标签，没法做这个设定了
end

MakeItem({
    name = "myth_iron_helmet",
    assets = {
        Asset("ANIM", "anim/myth_iron_helmet.zip"),
        Asset("ATLAS", "images/inventoryimages/myth_iron_helmet.xml"),
        Asset("IMAGE", "images/inventoryimages/myth_iron_helmet.tex"),
    },
    prefabs = nil,
    floatable = {0.01, "small", 0.22, 1.2},
    fn_common = function(inst)
        --waterproofer (from waterproofer component) added to pristine state for optimization
        inst:AddTag("waterproofer")
    end,
    fn_server = function(inst)
        inst:AddComponent("armor")
        inst.components.armor:InitCondition(TUNING.ARMOR_RUINSHAT, TUNING.ARMOR_FOOTBALLHAT_ABSORPTION)
        SetWeakness(inst)

        inst:AddComponent("waterproofer")
        inst.components.waterproofer:SetEffectiveness(TUNING.WATERPROOFNESS_SMALL)

        inst.components.equippable.equipslot = EQUIPSLOTS.HEAD
        inst.components.equippable:SetOnEquip(function(inst, owner)
            owner.AnimState:OverrideSymbol("swap_hat", "myth_iron_helmet", "swap_hat")
            owner.AnimState:Show("HAT")
            owner.AnimState:Show("HAIR_HAT")
            owner.AnimState:Hide("HAIR_NOHAT")
            owner.AnimState:Hide("HAIR")

            if owner:HasTag("player") then
                owner.AnimState:Hide("HEAD")
                owner.AnimState:Show("HEAD_HAT")
            end
        end)
        inst.components.equippable:SetOnUnequip(function(inst, owner)
            owner.AnimState:ClearOverrideSymbol("swap_hat")
            owner.AnimState:Hide("HAT")
            owner.AnimState:Hide("HAIR_HAT")
            owner.AnimState:Show("HAIR_NOHAT")
            owner.AnimState:Show("HAIR")

            if owner:HasTag("player") then
                owner.AnimState:Show("HEAD")
                owner.AnimState:Hide("HEAD_HAT")
            end
        end)
    end,
})

--------------------------------------------------------------------------
--[[ 铸铁战甲 ]]
--------------------------------------------------------------------------

local function OnBlocked(owner)
    owner.SoundEmitter:PlaySound("dontstarve/wilson/hit_armour")
end

MakeItem({
    name = "myth_iron_battlegear",
    assets = {
        Asset("ANIM", "anim/myth_iron_battlegear.zip"),
        Asset("ATLAS", "images/inventoryimages/myth_iron_battlegear.xml"),
        Asset("IMAGE", "images/inventoryimages/myth_iron_battlegear.tex"),
    },
    prefabs = nil,
    floatable = {nil, "small", 0.08, 0.95},
    fn_common = function(inst)
        inst:AddTag("metal")

        inst.foleysound = "dontstarve/movement/foley/metalarmour"
    end,
    fn_server = function(inst)
        inst:AddComponent("armor")
        inst.components.armor:InitCondition(TUNING.ARMORRUINS, TUNING.ARMOR_FOOTBALLHAT_ABSORPTION)
        SetWeakness(inst)

        inst:AddComponent("waterproofer")
        inst.components.waterproofer:SetEffectiveness(TUNING.WATERPROOFNESS_SMALL)

        inst.components.equippable.equipslot = EQUIPSLOTS.BODY
        inst.components.equippable:SetOnEquip(function(inst, owner)
            owner.AnimState:OverrideSymbol("swap_body", "myth_iron_battlegear", "swap_body")
            inst:ListenForEvent("blocked", OnBlocked, owner)
        end)
        inst.components.equippable:SetOnUnequip(function(inst, owner)
            owner.AnimState:ClearOverrideSymbol("swap_body")
            inst:RemoveEventCallback("blocked", OnBlocked, owner)
        end)
    end,
})

--------------------
--------------------

return unpack(prefs)
