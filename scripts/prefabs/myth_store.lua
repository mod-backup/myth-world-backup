local prefs = {}

--------------------------------------------------------------------------
--[[ 通用 ]]
--------------------------------------------------------------------------

local function OnIsNight(inst, isnight)
    if TheWorld.state.isnight then --夜间进行补货
        inst.components.bambooshop_myth:InitShop({
            time_delay = 1+math.random()*4,
            buy = { isnew = nil, add_max = nil, spl_num = nil, },
            sell = { isnew = nil, add_max = nil, spl_num = nil, },
        })
    else --天亮了，开店！
        inst.components.bambooshop_myth:TriggerShop(false)
    end
end

local function OnIsDay(inst, isday)
    if TheWorld.state.isday then
        inst.components.bambooshop_myth:TriggerLight(false)
    else
        inst.components.bambooshop_myth:TriggerLight(true)
    end
end

local function getstatus(inst)
    return inst.components.bambooshop_myth and inst.components.bambooshop_myth.isclosed and "CLOSED" or "OPEN"
end

local function MakeStore(data)
    local basename = "myth_shop_"..data.name
    table.insert(prefs, Prefab(
        basename,
        function()
            local inst = CreateEntity()

            inst.entity:AddTransform()
            inst.entity:AddAnimState()
            inst.entity:AddLight()
            inst.entity:AddNetwork()

            inst.Light:SetFalloff(1)
            inst.Light:SetIntensity(.5)
            inst.Light:SetRadius(2.5)
            inst.Light:SetColour(180/255, 195/255, 50/255)
            inst.Light:Enable(false)

            MakeObstaclePhysics(inst, 1.1)

            inst.AnimState:SetBank(data.build)
            inst.AnimState:SetBuild(data.build)
            inst.AnimState:PlayAnimation("idle", true)

            inst:AddTag("myth_shop")

            if data.fn_common ~= nil then
                data.fn_common(inst)
            end
            inst.entity:SetPristine()
            if not TheWorld.ismastersim then
                return inst
            end

            inst:AddComponent("inspectable")
            inst.components.inspectable.getstatus = getstatus

            inst:AddComponent("bambooshop_myth")

            if data.fn_server ~= nil then
                data.fn_server(inst)
            end

            inst:WatchWorldState("isday", OnIsDay)
            inst:DoTaskInTime(0, function() --产生时登记自己所属店铺
                BSHOPSTATE.shops[data.name].num = BSHOPSTATE.shops[data.name].num + 1
                OnIsDay(inst, nil)
            end)
            inst:ListenForEvent("onremove", function(inst) --移除时注销自己所属店铺
                BSHOPSTATE.shops[data.name].num = BSHOPSTATE.shops[data.name].num - 1
            end)

            return inst
        end,
        data.assets,
        data.prefabs
    ))
end

--------------------------------------------------------------------------
--[[ 珍奇小摊 ]]
--------------------------------------------------------------------------

MakeStore({
    name = "rareitem",
    assets = {
        Asset("ANIM", "anim/myth_store_pearl.zip"),
    },
    build = "myth_store_pearl" ,
    prefabs = nil,
    fn_common = function(inst)
        inst.bbshoptype = "rareitem"
        inst.AnimState:AddOverrideBuild("pandaman_myth_"..inst.bbshoptype)
    end,
    fn_server = function(inst)
        inst.components.bambooshop_myth:InitShop({
            time_delay = 0.4, 
            --旧版 math.random(3, 4) math.random(4, 5)
            buy = { isnew = true, add_max = math.random(3, 4), spl_num = 0, },
            sell = { isnew = true, add_max = math.random(4, 5), spl_num = 0, },
        })

        inst:WatchWorldState("isnight", OnIsNight)
    end,
})

--------------------------------------------------------------------------
--[[ 菜市小铺 ]]
--------------------------------------------------------------------------

MakeStore({
    name = "ingredient",
    assets = {
        Asset("ANIM", "anim/myth_store_vegetables.zip"),
    },
    prefabs = nil,
    build = "myth_store_vegetables",
    fn_common = function(inst)
        inst.bbshoptype = "ingredient"
        inst.AnimState:AddOverrideBuild("pandaman_myth_"..inst.bbshoptype)
    end,
    fn_server = function(inst)
        inst.components.bambooshop_myth:InitShop({
            time_delay = 0.4,
            buy = { isnew = true, add_max = math.random(3, 4), spl_num = 0, },
            sell = { isnew = true, add_max = math.random(4, 5), spl_num = 0, },
        })

        inst:WatchWorldState("isnight", OnIsNight)
    end,
})

--------------------------------------------------------------------------
--[[ 花鸟小铺 ]]
--------------------------------------------------------------------------

MakeStore({
    name = "animals",
    assets = {
        Asset("ANIM", "anim/myth_store_flowerbird.zip"),
    },
    build = "myth_store_flowerbird",
    prefabs = nil,
    fn_common = function(inst)
        inst.bbshoptype = "animals"
        inst.AnimState:AddOverrideBuild("pandaman_myth_"..inst.bbshoptype)
    end,
    fn_server = function(inst)
        inst.components.bambooshop_myth:InitShop({
            time_delay = 0.4,
            buy = { isnew = true, add_max = math.random(3, 4), spl_num = 0, },
            sell = { isnew = true, add_max = math.random(4, 5), spl_num = 0, },
        })

        inst:WatchWorldState("isnight", OnIsNight)
    end,
})

--------------------------------------------------------------------------
--[[ 禾种小铺 ]]
--------------------------------------------------------------------------

MakeStore({
    name = "plants",
    assets = {
        Asset("ANIM", "anim/myth_store_seeds.zip"),
    },
    prefabs = nil,
    build = "myth_store_seeds",
    fn_common = function(inst)
        inst.bbshoptype = "plants"
        inst.AnimState:AddOverrideBuild("pandaman_myth_"..inst.bbshoptype)
    end,
    fn_server = function(inst)
        inst.components.bambooshop_myth:InitShop({
            time_delay = 0.4,
            buy = { isnew = true, add_max = math.random(3, 4), spl_num = 0, },
            sell = { isnew = true, add_max = math.random(4, 5), spl_num = 0, },
        })

        inst:WatchWorldState("isnight", OnIsNight)
    end,
})

--------------------------------------------------------------------------
--[[ 茶肴小铺 ]]
--------------------------------------------------------------------------

MakeStore({
    name = "foods",
    assets = {
        Asset("ANIM", "anim/myth_store_teadishes.zip"),
    },
    prefabs = nil,
    build = "myth_store_teadishes",
    fn_common = function(inst)
        inst.bbshoptype = "foods"
        inst.AnimState:AddOverrideBuild("pandaman_myth_"..inst.bbshoptype)
    end,
    fn_server = function(inst)
        inst.components.bambooshop_myth.countkind_buy = 12
        inst.components.bambooshop_myth.countkind_sell = 4
        inst.components.bambooshop_myth:InitShop({
            time_delay = 0.4,
            buy = { isnew = true, add_max = math.random(3, 4), spl_num = 0, },
            sell = { isnew = true, add_max = math.random(4, 5), spl_num = 0, },
        })

        inst:WatchWorldState("isnight", OnIsNight)
    end,
})

--------------------------------------------------------------------------
--[[ 铸匠小铺 ]]
--------------------------------------------------------------------------

MakeStore({
    name = "weapons",
    assets = {
        Asset("ANIM", "anim/myth_store_blacksmith.zip"),
    },
    build = "myth_store_blacksmith",
    prefabs = nil,
    fn_common = function(inst)
        inst.bbshoptype = "weapons"
        inst.AnimState:AddOverrideBuild("pandaman_myth_"..inst.bbshoptype)
    end,
    fn_server = function(inst)
        inst.components.bambooshop_myth.countkind_buy = 4
        inst.components.bambooshop_myth:InitShop({
            time_delay = 0.4,
            buy = { isnew = true, add_max = math.random(3, 4), spl_num = 0, },
            -- sell = { isnew = true, add_max = math.random(4, 5), spl_num = 0, },
        })

        inst:WatchWorldState("isnight", OnIsNight)
    end,
})

--------------------------------------------------------------------------
--[[ 算命小铺 ]]
--------------------------------------------------------------------------

MakeStore({
    name = "numerology",
    assets = {
        Asset("ANIM", "anim/myth_store_divination.zip"),
    },
    build = "myth_store_divination",
    prefabs = nil,
    fn_common = function(inst)
        inst.bbshoptype = "numerology"
        inst.AnimState:AddOverrideBuild("pandaman_myth_"..inst.bbshoptype)
    end,
    fn_server = function(inst)
        inst.components.bambooshop_myth.countkind_buy = 4
        inst.components.bambooshop_myth:InitShop({
            time_delay = 0.4,
            buy = { isnew = true, add_max = math.random(3, 4), spl_num = 0, },
            -- sell = { isnew = true, add_max = math.random(4, 5), spl_num = 0, },
        })

        inst:WatchWorldState("isnight", OnIsNight)
    end,
})

--------------------------------------------------------------------------
--[[ 材瓦小铺 ]]
--------------------------------------------------------------------------

MakeStore({
    name = "construct",
    assets = {
        Asset("ANIM", "anim/myth_store_craftsman.zip"),
    },
    build = "myth_store_craftsman",
    prefabs = nil,
    fn_common = function(inst)
        inst.bbshoptype = "construct"
        inst.AnimState:AddOverrideBuild("pandaman_myth_"..inst.bbshoptype)
    end,
    fn_server = function(inst)
        inst.components.bambooshop_myth.countkind_buy = 12
        inst.components.bambooshop_myth:InitShop({
            time_delay = 0.4,
            --5-7
            buy = { isnew = true, add_max = math.random(3, 4), spl_num = 0, },
            -- sell = { isnew = true, add_max = math.random(4, 5), spl_num = 0, },
        })

        inst:WatchWorldState("isnight", OnIsNight)
    end,
})

--------------------------------------------------------------------------
--[[ 打烊小店-旧 ]]
--------------------------------------------------------------------------

table.insert(prefs, Prefab(
    "myth_store",
    function()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeObstaclePhysics(inst, 2)

        inst.AnimState:SetBank("myth_store")
        inst.AnimState:SetBuild("myth_store")
        inst.AnimState:PlayAnimation("idle", true)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("inspectable")

        --把旧的打烊小铺全部换成建造中的小铺
        inst:DoTaskInTime(0.5, function()
            ReplacePrefab(inst, "myth_store_construction")
        end)

        return inst
    end,
    {
        Asset("ANIM", "anim/myth_store.zip"),
    },
    nil
))

--------------------------------------------------------------------------
--[[ 打烊小店-新版 ]]
--------------------------------------------------------------------------

local function onwenthome(inst,data)
    if data and data.doer and data.doer.bbshoptype ~= nil then
        local fx = SpawnPrefab("collapse_big")
        fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
        fx:SetMaterial("metal")
        ReplacePrefab(inst, "myth_shop_"..data.doer.bbshoptype)
    end
end

table.insert(prefs, Prefab(
    "myth_shop",
    function()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()

        MakeObstaclePhysics(inst, 2)

        inst.AnimState:SetBank("myth_store")
        inst.AnimState:SetBuild("myth_store")
        inst.AnimState:PlayAnimation("idle", true)

        inst:SetPrefabNameOverride("myth_store")

        inst:AddTag("myth_shop")
        inst:AddTag("myth_shop_building")
        inst:AddTag("panda_home")

        inst.entity:SetPristine()
        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("inspectable")

        inst:ListenForEvent("onwenthome",onwenthome)

        return inst
    end,
    {
        Asset("ANIM", "anim/myth_store.zip"),
    },
    nil
))

--------------------------------------------------------------------------
--[[ 建造中的小店 ]]
--------------------------------------------------------------------------

table.insert(prefs, Prefab(
    "myth_store_construction",
    function()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        MakeObstaclePhysics(inst, 2)

        inst.AnimState:SetBank("myth_store_construction")
        inst.AnimState:SetBuild("myth_store_construction")
        inst.AnimState:PlayAnimation("idle")
        inst.Transform:SetScale(0.75, 0.75, 0.75)

        inst:AddTag("constructionsite")
        inst:AddTag("myth_shop")
        inst:AddTag("myth_shop_building")

        inst.entity:SetPristine()
        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("inspectable")

        inst:AddComponent("constructionsite")
        inst.components.constructionsite:SetConstructionPrefab("construction_container")
        inst.components.constructionsite:SetOnConstructedFn(function(inst, doer)
            local concluded = true
            for _,v in ipairs(CONSTRUCTION_PLANS[inst.prefab] or {}) do
                if inst.components.constructionsite:GetMaterialCount(v.type) < v.amount then
                    concluded = false
                    break
                end
            end

            if concluded then
                local fx = SpawnPrefab("collapse_big")
                fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
                fx:SetMaterial("metal")
                local newshop = ReplacePrefab(inst, "myth_shop")
                if newshop then
                    newshop.SoundEmitter:PlaySound("dontstarve/halloween_2018/madscience_machine/place")
                end
            end
        end)

        return inst
    end,
    {
        Asset("ANIM", "anim/myth_store_construction.zip"),
    },
    nil
))

--------------------------------------------------------------------------
--[[ 宝藏（隐藏） ]]
--------------------------------------------------------------------------

table.insert(prefs, Prefab(
    "treasure_hide_myth",
    function()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddNetwork()

        inst:AddTag("myth_treasure")

        inst.entity:SetPristine()
        if not TheWorld.ismastersim then
            return inst
        end

        inst:DoTaskInTime(0.2, function() --作弊生成的无名宝藏是要主动删除的（缺数据）
            if inst.bs_components == nil or inst.bs_components.treasure == nil then
                inst:Remove()
            end
        end)

        inst.persists = false

        inst.OnEntitySleep = function(inst)
            if inst.task_check ~= nil then
                inst.task_check:Cancel()
                inst.task_check = nil
            end
        end
        inst.OnEntityWake = function(inst)
            if inst.task_check ~= nil then
                inst.task_check:Cancel()
            end
            if inst.bs_components ~= nil and inst.bs_components.treasure ~= nil then
                local trea = inst.bs_components.treasure
                inst.task_check = inst:DoPeriodicTask(3.5, function(inst)
                    --查看是否满足时间条件
                    if trea.time == 1 then
                        if not TheWorld.state.isday then
                            return
                        end
                    elseif trea.time == 2 then
                        if not TheWorld.state.isdusk then
                            return
                        end
                    elseif trea.time == 3 then
                        if not TheWorld.state.isnight then
                            return
                        end
                    elseif trea.time == 4 then
                        if not TheWorld.state.isfullmoon then
                            return
                        end
                    elseif trea.time == 5 then
                        if not TheWorld.state.isnewmoon then
                            return
                        end
                    end

                    --查看是否满足天气条件
                    if trea.weather == 1 then
                        if TheWorld.state.israining or TheWorld.state.issnowing then
                            return
                        end
                    elseif trea.weather == 2 then
                        if not (TheWorld.state.israining or TheWorld.state.issnowing) then
                            return
                        end
                    end

                    --宝藏显形
                    inst.bs_components.treasure = nil
                    inst.bs_components = nil
                    local bzshow = ReplacePrefab(inst, "treasure_show_myth")
                    if bzshow then
                        if trea.time == 4 or trea.time == 5 then
                            bzshow.quality = 1
                        end
                    end
                end, 1)
            end
        end

        return inst
    end,
    nil,
    nil
))

--------------------------------------------------------------------------
--[[ 宝藏（显现） ]]
--------------------------------------------------------------------------

local function DropRandomItems(inst, prefab, nummin, nummax, forcenum)
    local rand = forcenum or math.random(nummin, nummax)
    if rand <= 0 then
        return
    end
    for i = 1, rand, 1 do
        inst.components.lootdropper:SpawnLootPrefab(prefab)
    end
end
local function DropItems(inst, rands)
    local rand = math.random()
    if rand <= rands[4] then
        inst.components.lootdropper:SpawnLootPrefab("myth_plant_infantree_trunk")
    elseif rand <= rands[3] then
        inst.components.lootdropper:SpawnLootPrefab("krampus_sack")
        if math.random() <= 0.5 then
            inst.components.lootdropper:SpawnLootPrefab("opalpreciousgem")
        else
            DropRandomItems(inst, "redgem", 1, 3)
            DropRandomItems(inst, "orangegem", 1, 3)
            DropRandomItems(inst, "yellowgem", 1, 3)
            DropRandomItems(inst, "greengem", 1, 3)
            DropRandomItems(inst, "bluegem", 1, 3)
            DropRandomItems(inst, "purplegem", 1, 3)
        end

        local crops = {
            "asparagus",
            "garlic",
            "pumpkin",
            "corn",
            "onion",
            "potato",
            "dragonfruit",
            "pomegranate",
            "eggplant",
            "tomato",
            "watermelon",
            "pepper",
            "durian",
            "carrot",
            "gourd"
        }
        inst.components.lootdropper:SpawnLootPrefab(crops[math.random(#crops)].."_oversized")
    elseif rand <= rands[2] then
        if math.random() <= 0.5 then
            DropRandomItems(inst, "myth_coin_box", nil, nil, 2)
        else
            DropRandomItems(inst, "lucky_goldnugget", nil, nil, 10)
        end

        if math.random() <= 0.5 then
            DropRandomItems(inst, "goldnugget", 10, 20)
        else
            DropRandomItems(inst, "redgem", 0, 1)
            DropRandomItems(inst, "orangegem", 0, 1)
            DropRandomItems(inst, "yellowgem", 0, 1)
            DropRandomItems(inst, "greengem", 0, 1)
            DropRandomItems(inst, "bluegem", 0, 1)
            DropRandomItems(inst, "purplegem", 0, 1)
        end
    else
        if math.random() <= 0.5 then
            inst.components.lootdropper:SpawnLootPrefab("myth_coin_box")
        else
            inst.components.lootdropper:SpawnLootPrefab("wine_bottle_gourd")
        end

        if math.random() <= 0.5 then
            DropRandomItems(inst, "lucky_goldnugget", 1, 3)
        else
            DropRandomItems(inst, "goldnugget", 5, 10)
        end
    end
end

table.insert(prefs, Prefab(
    "treasure_show_myth",
    function()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        inst.AnimState:SetBank("myth_treasure")
        inst.AnimState:SetBuild("myth_treasure")
        inst.AnimState:PlayAnimation("idle")

        inst:AddTag("myth_treasure")

        inst.entity:SetPristine()
        if not TheWorld.ismastersim then
            return inst
        end

        inst.quality = nil

        inst:AddComponent("inspectable")

        inst:AddComponent("lootdropper")

        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.DIG)
        inst.components.workable:SetOnFinishCallback(function(inst, digger)
            if inst.quality == 1 then
                DropItems(inst, { 1, 0.6, 0.3, 0.1 }) --新月或者月圆之夜时，高级宝藏的几率变高（40%-30%-20%-10%）
            else
                DropItems(inst, { 1, 0.5, 0.2, 0.05 }) --（50%-30%-15%-5%）
            end
            inst:Remove()
        end)
        inst.components.workable:SetWorkLeft(math.random(3, 10))

        inst:ListenForEvent("onremove", function(inst) --被删除时，让世界发送一个事件，好让线索纸失效
            TheWorld:PushEvent("myth_treasure_remove")
        end)

        inst.OnSave = function(inst, data)
            if inst.quality ~= nil then
                data.quality = inst.quality
            end
        end
        inst.OnLoad = function(inst, data)
            if data ~= nil then
                inst.quality = data.quality
            end
        end

        return inst
    end,
    {
        Asset("ANIM", "anim/myth_treasure.zip"),
    },
    nil
))

--------------------------------------------------------------------------
--[[ 线索纸 ]]
--------------------------------------------------------------------------

table.insert(prefs, Prefab(
    "treasure_paper_myth",
    function()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        inst.AnimState:SetBank("myth_treasure")
        inst.AnimState:SetBuild("myth_treasure")
        inst.AnimState:PlayAnimation("idle_paper")

        inst:AddTag("cattoy")

        MakeInventoryPhysics(inst)
        MakeInventoryFloatable(inst, "small", 0.2, 0.8)
        local OnLandedClient_old = inst.components.floater.OnLandedClient
        inst.components.floater.OnLandedClient = function(self)
            OnLandedClient_old(self)
            self.inst.AnimState:SetFloatParams(0.03, 1, self.bob_percent)
        end

        inst.entity:SetPristine()
        if not TheWorld.ismastersim then
            return inst
        end

        inst.clues = nil

        inst:AddComponent("inspectable")
        inst.components.inspectable.descriptionfn = function(inst, doer)
            if inst.clues ~= nil and inst.clues.address ~= nil and inst.clues.time ~= nil and inst.clues.weather ~= nil then
                return '"'..subfmt(STRINGS.BBSHOP.CLUE[1], {
                    address = STRINGS.BBSHOP.CLUEADDRESS[inst.clues.address] or "???",
                    time = STRINGS.BBSHOP.CLUETIME[inst.clues.time] or "??",
                    weather = STRINGS.BBSHOP.CLUEWEATHER[inst.clues.weather] or "??"
                })..'"'
            end
            return nil
        end

        inst:AddComponent("inventoryitem")
        inst.components.inventoryitem.imagename = "treasure_paper_myth"
        inst.components.inventoryitem.atlasname = "images/inventoryimages/treasure_paper_myth.xml"

        inst:AddComponent("fuel")
        inst.components.fuel.fuelvalue = TUNING.SMALL_FUEL

        MakeSmallBurnable(inst, TUNING.SMALL_BURNTIME)
        MakeSmallPropagator(inst)

        MakeHauntableLaunchAndIgnite(inst)

        inst:ListenForEvent("myth_treasure_remove", function(world) --找到宝藏了，或者新开了线索，已有线索纸失效
            local paper = SpawnPrefab("papyrus")
            if paper ~= nil then
                paper.Transform:SetPosition(inst.Transform:GetWorldPosition())

                local owner = inst.components.inventoryitem.owner
                local holder = owner ~= nil and (owner.components.inventory or owner.components.container) or nil
                if holder ~= nil then
                    local slot = holder:GetItemSlot(inst)
                    inst:Remove()
                    holder:GiveItem(paper, slot)
                else
                    inst:Remove()
                end
            end
        end, TheWorld)

        inst.OnSave = function(inst, data)
            if inst.clues ~= nil then
                data.clues = {
                    address = inst.clues.address,
                    time = inst.clues.time,
                    weather = inst.clues.weather
                }
            end
        end
        inst.OnLoad = function(inst, data)
            if data ~= nil then
                if data.clues ~= nil then
                    inst.clues = {
                        address = data.clues.address,
                        time = data.clues.time,
                        weather = data.clues.weather
                    }
                end
            end
        end

        return inst
    end,
    {
        Asset("ANIM", "anim/myth_treasure.zip"),
        Asset( "IMAGE", "images/inventoryimages/treasure_paper_myth.tex" ),
		Asset( "ATLAS", "images/inventoryimages/treasure_paper_myth.xml" ),
    },
    nil
))

--------------------
--------------------

return unpack(prefs)
