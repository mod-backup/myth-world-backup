local function setshoptype(inst,type)
	inst.bbshoptype = type
	inst.AnimState:SetBuild("pandaman_myth_"..inst.bbshoptype)
end

local function onload(inst, data)
    if data ~= nil and data.bbshoptype ~= nil then
        setshoptype(inst,data.bbshoptype)
    end
end

local function onsave(inst, data)
    data.bbshoptype = inst.bbshoptype
end

local types =  {
	"rareitem",
	"ingredient",
	"animals",
	"plants",
	"foods",
	"weapons",
	"numerology",
	"construct",
}

local function dotalk(inst)
	if not inst.components.homeseeker:HasHome() then
		inst.components.talker:Say(STRINGS.PANDAMAN_MYTH_TALKS[math.random(#STRINGS.PANDAMAN_MYTH_TALKS)])
	end
	inst:DoTaskInTime(math.random(10,25),dotalk)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

	inst.AnimState:SetBank("townspig")
	inst.AnimState:SetBuild("pandaman_myth_weapons")
	inst.AnimState:PlayAnimation("idle_loop", true)

    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	inst.AnimState:Hide("ARM_carry")

	inst:AddTag("character")
	inst:AddTag("scarytoprey")
	inst:AddTag("noauradamage")
	inst:AddTag("notraptrigger")
	inst:AddTag("NOBLOCK")
	inst:AddTag("companion")

	inst:AddComponent("talker")
	inst.components.talker.fontsize = 35
	inst.components.talker.font = TALKINGFONT
	inst.components.talker.offset = Vector3(0, -550, 0)
	inst.components.talker:MakeChatter()
	inst.components.talker.colour = Vector3(0.50, 0.90, .4, 1)

	MakeCharacterPhysics(inst, 50, .5)

    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst.bbshoptype = types[math.random(#types)]
	inst.AnimState:SetBuild("pandaman_myth_"..inst.bbshoptype)

	inst:AddComponent("follower")

	inst:AddComponent("locomotor")
	inst.components.locomotor.runspeed = 3
	inst.components.locomotor.walkspeed = 3
	inst.components.locomotor.pathcaps = { allowocean = true }

	inst:AddComponent("inspectable")

	inst:AddComponent("homeseeker")
	inst.components.homeseeker.removecomponent = false

	inst:AddComponent("knownlocations")

	inst:WatchWorldState("phase", function(src, phase) --晚上 找不到房子或者还来不及进去的熊猫人会直接消失
		if phase == "night" then
			SpawnPrefab("waterplant_destroy").Transform:SetPosition(inst:GetPosition():Get())
			inst:Remove()
		end
	end)

	inst:ListenForEvent("onremove", function(inst) --移除时注销自己所属Npc
		if inst.bbshoptype ~= nil and BSHOPSTATE.npc and BSHOPSTATE.npc[inst.bbshoptype] ~= nil then
			BSHOPSTATE.npc[inst.bbshoptype] = BSHOPSTATE.npc[inst.bbshoptype] - 1
		end
	end)

	inst:SetStateGraph("SGpandaman_myth")
	inst:SetBrain(require("brains/pandaman_mythbrain"))

	inst:DoTaskInTime(math.random(5,10),dotalk)

	inst.SetShopType = setshoptype
    inst.OnSave = onsave
    inst.OnLoad = onload

    return inst
end
return Prefab("pandaman_myth", fn)