
local assets =
{
    Asset("ANIM", "anim/myth_plant_infantree_trunk.zip"),
    Asset( "ATLAS", "images/inventoryimages/myth_plant_infantree_trunk.xml" ),
}

local prefabs = 
{

}

local function oversized_onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_body", "myth_plant_infantree_trunk", "swap_body")
end
local function oversized_onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_body")
end

local function oversized_onburnt(inst)
    inst.components.lootdropper:DropLoot()
    inst:Remove()
end

local OVERSIZED_PHYSICS_RADIUS = 0.1

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("myth_plant_infantree_trunk")
    inst.AnimState:SetBuild("myth_plant_infantree_trunk")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("heavy")
    inst:AddTag("irreplaceable")
    inst:AddTag("myth_plant_infantree_trunk")

    inst.gymweight = 4

    MakeHeavyObstaclePhysics(inst, OVERSIZED_PHYSICS_RADIUS)
    inst:SetPhysicsRadiusOverride(OVERSIZED_PHYSICS_RADIUS)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("heavyobstaclephysics")
    inst.components.heavyobstaclephysics:SetRadius(OVERSIZED_PHYSICS_RADIUS)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_plant_infantree_trunk.xml"
    inst.components.inventoryitem.cangoincontainer = false
    inst.components.inventoryitem:SetSinks(true)

    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.BODY
    inst.components.equippable:SetOnEquip(oversized_onequip)
    inst.components.equippable:SetOnUnequip(oversized_onunequip)
    inst.components.equippable.walkspeedmult = TUNING.HEAVY_SPEED_MULT

    MakeMediumBurnable(inst)
    inst.components.burnable:SetOnBurntFn(oversized_onburnt)
    MakeMediumPropagator(inst)

    MakeHauntable(inst)

    return inst
end
--d_ground(29)

return Prefab("myth_plant_infantree_trunk", fn, assets, prefabs)
