local assets =
{
    Asset("ANIM", "anim/cane_peach.zip"),
    Asset("ATLAS", "images/inventoryimages/cane_peach.xml"),
}

local function onequip(inst, owner)
    local skin_build = inst:GetSkinBuild()
    if skin_build ~= nil then
        owner:PushEvent("equipskinneditem", inst:GetSkinName())
        owner.AnimState:OverrideItemSkinSymbol("swap_object", skin_build, "swap", inst.GUID, "swap")
    else
    owner.AnimState:OverrideSymbol("swap_object", "cane_peach", "swap")
    end

    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    local skin_build = inst:GetSkinBuild()
    if skin_build ~= nil then
        owner:PushEvent("unequipskinneditem", inst:GetSkinName())
    end

    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function myth_aoespellsg(inst)
	return "throw"
end
local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("cane_peach")
    inst.AnimState:SetBuild("cane_peach")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("weapon")
	inst:AddTag("aoeweapon_lunge")

	inst.myth_aoespellsg = myth_aoespellsg

    local swap_data = {sym_build = "water"}
    MakeInventoryFloatable(inst, "med", 0.05, {0.85, 0.45, 0.85}, true, 1, swap_data)

	inst:AddComponent("aoetargeting")
	inst.components.aoetargeting:SetRange(8)
	inst.components.aoetargeting.reticule.reticuleprefab = "cane_peach_reticuleaoe"
	inst.components.aoetargeting.reticule.pingprefab = "cane_peach_reticuleaoeping"
	inst.components.aoetargeting.reticule.targetfn = function()
		local player = ThePlayer
		local ground = TheWorld.Map
		local pos = Vector3()
		for r = 5, 0, -.25 do
			pos.x, pos.y, pos.z = player.entity:LocalToWorldSpace(r, 0, 0)
			if ground:IsPassableAtPoint(pos:Get()) and not ground:IsGroundTargetBlocked(pos) then
				return pos
			end
		end
		return pos
	end
	inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
	inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
	inst.components.aoetargeting.reticule.ease = true
	inst.components.aoetargeting.reticule.mouseenabled = true

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.CANE_DAMAGE)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/cane_peach.xml"

    inst:AddComponent("equippable")

    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
    inst.components.equippable.walkspeedmult = TUNING.CANE_SPEED_MULT

    inst:AddComponent("fuel")
    inst.components.fuel.fuelvalue = TUNING.MED_FUEL

    inst:AddComponent("peach_complexprojectile")	
	inst.components.peach_complexprojectile:SetHorizontalSpeed(15)
    inst.components.peach_complexprojectile:SetGravity(-35)
    inst.components.peach_complexprojectile:SetLaunchOffset(Vector3(.25, 1, 0))
	inst.components.peach_complexprojectile:SetOnLaunch(function(inst)
		inst:AddTag("thrown")
		inst.Physics:SetFriction(0.2)
		inst.AnimState:PlayAnimation("spin")
	end)
	inst.components.peach_complexprojectile:SetOnHit(function(inst, wj, mb)
		local pt = inst.targetpos or inst:GetPosition()
		for a = 1, math.random(3,5) do
			local finaloffset = FindValidPositionByFan(math.random() * 2 * PI, math.random(3), 12, function(offset)
				local x, z = pt.x + offset.x, pt.z + offset.z
				return TheWorld.Map:IsAboveGroundAtPoint(x, 0, z)
					and not TheWorld.Map:IsPointNearHole(Vector3(x, 0, z))
			end)
			local tree = SpawnPrefab("peachtree_myth")
			local fx  = SpawnPrefab("halloween_moonpuff")
			if finaloffset ~= nil then
				finaloffset.x = finaloffset.x + pt.x
				finaloffset.z = finaloffset.z + pt.z
				tree.Transform:SetPosition(finaloffset:Get())
				fx.Transform:SetPosition(finaloffset:Get())
			else
				tree.Transform:SetPosition(pt:Get())
				fx.Transform:SetPosition(pt:Get())
			end
		end
		inst:Remove()
	end)
	
	inst:AddComponent("myth_aoespell")
	inst.components.aoespell = inst.components.myth_aoespell
	inst.components.aoespell:SetSpellFn(function(zj, doer, pos)
		local weapon  = doer.components.inventory:DropItem(zj, false)
		if weapon and weapon.components.peach_complexprojectile then
			weapon.components.peach_complexprojectile:Launch(pos, doer)
			weapon.targetpos = pos
		end
	end)
	inst:RegisterComponentActions("aoespell")

    MakeSmallBurnable(inst, TUNING.MED_BURNTIME)
    MakeSmallPropagator(inst)

    MakeHauntableLaunchAndIgnite(inst)

    return inst
end

local function growtotree(inst)
	local tree = SpawnPrefab("peachtree_myth")
	tree.Transform:SetPosition(inst:GetPosition():Get())
	tree.components.growable:SetStage(1)
	inst:Remove()
end

local function grow(inst)
	inst.AnimState:PlayAnimation("lv2_idle")
	inst:DoTaskInTime(1,growtotree)
end

local function treefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("myth_peachtree")
    inst.AnimState:SetBuild("myth_peachtree")
    inst.AnimState:PlayAnimation("lv1_idle")

    inst:AddTag("fx")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:DoTaskInTime(0.5,grow)
	return inst
end

local function makeaoe(name, fx, size)
    local function fn()
        local inst = SpawnPrefab(fx)
        inst.AnimState:SetScale(size, size)
        return inst
    end
    return Prefab(name, fn)
end

return Prefab("cane_peach", fn, assets),
	Prefab("cane_peach_fx", treefn),
	makeaoe("cane_peach_reticuleaoe", "reticuleaoe", 1.5),
	makeaoe("cane_peach_reticuleaoeping", "reticuleaoeping", 1.5)
