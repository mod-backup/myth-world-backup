local assets =
{
    Asset("ANIM", "anim/myth_gold_staff.zip"),
    Asset("ATLAS", "images/inventoryimages/myth_gold_staff.xml"),
}

local function onequip(inst, owner)
    local skin_build = inst:GetSkinBuild()
    if skin_build ~= nil then
        owner:PushEvent("equipskinneditem", inst:GetSkinName())
        owner.AnimState:OverrideItemSkinSymbol("swap_object", skin_build, "swap_spear", inst.GUID, "swap_spear")
    else
    owner.AnimState:OverrideSymbol("swap_object", "myth_gold_staff", "swap_spear")
    end

    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    local skin_build = inst:GetSkinBuild()
    if skin_build ~= nil then
        owner:PushEvent("unequipskinneditem", inst:GetSkinName())
    end

    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function SinkEntity(entity)
    if not entity:IsValid() then
        return
    end
    local px, py, pz = 0, 0, 0
    if entity.Transform ~= nil then
        px, py, pz = entity.Transform:GetWorldPosition()
    end

    if entity.components.inventory ~= nil then
        entity.components.inventory:DropEverything()
    end
    if entity.components.container ~= nil then
        entity.components.container:DropEverything()
    end
    local fx = SpawnPrefab((TheWorld.Map:IsValidTileAtPoint(px, py, pz) and "splash_sink") or "splash_ocean")
    fx.Transform:SetPosition(px, py, pz)

    local sx, sy, sz = FindRandomPointOnShoreFromOcean(px, py, pz)
    if sx ~= nil then
        entity.Transform:SetPosition(sx, sy, sz)
    else
        for k, v in pairs(Ents) do
            if v:IsValid() and v:HasTag("multiplayer_portal") then
                entity.Transform:SetPosition(v.Transform:GetWorldPosition())
            end
        end
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("myth_gold_staff")
    inst.AnimState:SetBuild("myth_gold_staff")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("weapon")
    inst:AddTag("quidkpick_peach")
    inst:AddTag("infant_fruit_picker")
    inst:AddTag("mk_nodrop")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(5)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem:SetSinks(true)
    inst.components.inventoryitem.TryToSink = function(self)
        if ShouldEntitySink(self.inst, self.sinks) then
            self.inst:DoTaskInTime(0, SinkEntity)
        end
    end
    inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_gold_staff.xml"

    inst:AddComponent("drownable")

    inst:AddComponent("equippable")

    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    MakeHauntableLaunch(inst)
    return inst
end

return Prefab("myth_gold_staff", fn, assets)
