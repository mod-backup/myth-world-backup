local prefs = {}

for k,v in pairs(MYTH_ITEMSKINS) do
    v.type = "item"
    table.insert(prefs, CreatePrefabSkin(k,v))
end
return unpack(prefs)