local CANOPY_SHADOW_DATA = require("prefabs/canopyshadows")

local assets =
{
    Asset("ANIM", "anim/myth_plant_infantree.zip"),
    Asset("ANIM", "anim/infantree_leaves_canopy.zip"),
    --Asset("SOUND", "sound/tentacle.fsb"),
    Asset("SCRIPT", "scripts/prefabs/canopyshadows.lua")
}

local prefabs = 
{
    --"oceantreenut",
    --"oceanvine_cocoon",
}

local small_ram_products =
{
    "twigs",
    "cutgrass",
    "foliage",
    "foliage",
}

local ram_products_to_refabs = {}
for _, v in ipairs(small_ram_products) do
    ram_products_to_refabs[v] = true
end
for k, v in pairs(ram_products_to_refabs) do
    table.insert(prefabs, k)
end

local MIN = TUNING.SHADE_CANOPY_RANGE
local MAX = MIN + TUNING.WATERTREE_PILLAR_CANOPY_BUFFER

local DROP_ITEMS_DIST_MIN = 8
local DROP_ITEMS_DIST_VARIANCE = 12

local NUM_DROP_SMALL_ITEMS_MIN = 10
local NUM_DROP_SMALL_ITEMS_MAX = 14


local function removecanopyshadow(inst)
    if inst.canopy_data ~= nil then
        for _, shadetile_key in ipairs(inst.canopy_data.shadetile_keys) do
            if TheWorld.shadetiles[shadetile_key] ~= nil then
                TheWorld.shadetiles[shadetile_key] = TheWorld.shadetiles[shadetile_key] - 1

                if TheWorld.shadetiles[shadetile_key] <= 0 then
                    if TheWorld.shadetile_key_to_leaf_canopy_id[shadetile_key] ~= nil then
                        DespawnLeafCanopy(TheWorld.shadetile_key_to_leaf_canopy_id[shadetile_key])
                        TheWorld.shadetile_key_to_leaf_canopy_id[shadetile_key] = nil
                    end
                end
            end
        end

        for _, ray in ipairs(inst.canopy_data.lightrays) do
            ray:Remove()
        end
    end
end

local function OnFar(inst, player)
    if player.canopytrees then   
        player.canopytrees = player.canopytrees - 1
        player:PushEvent("onchangecanopyzone", player.canopytrees > 0)
    end
    if player.under_infantreeleaves_num then   
        player.under_infantreeleaves_num = player.under_infantreeleaves_num - 1
        player:PushEvent("onchangeunderinfantreezone", player.under_infantreeleaves_num > 0)
    end
    inst.players[player] = nil
end

local function OnNear(inst,player)
    inst.players[player] = true
    player.canopytrees = (player.canopytrees or 0) + 1
    player.under_infantreeleaves_num = (player.under_infantreeleaves_num or 0) + 1
    player:PushEvent("onchangecanopyzone", player.canopytrees > 0)
    player:PushEvent("onchangeunderinfantreezone", player.under_infantreeleaves_num > 0)
end

local function OnTimerDone(inst, data)
    if data ~= nil then
        if data.name == "regrow_oceantreenut" then
            --inst.num_oceantreenuts = inst.num_oceantreenuts + 1
           -- if inst.num_oceantreenuts < NUM_OCEANTREENUTS_MAX then
            --    inst.components.timer:StartTimer("regrow_oceantreenut", TUNING.OCEANTREENUT_REGENERATE_TIME + math.random() * TUNING.OCEANTREENUT_REGENERATE_TIME_VARIANCE)
            --end
        elseif data.name == "cocoon_regrow_check" then
            --cocoon_regrow_check(inst)
        end
    end
end
--闪电
local function DropLightningItems(inst, items)
    local x, _, z = inst.Transform:GetWorldPosition()
    local num_items = #items

    for i, item_prefab in ipairs(items) do
        local dist = DROP_ITEMS_DIST_MIN + DROP_ITEMS_DIST_VARIANCE * math.random()
        local theta = 2 * PI * math.random()

        inst:DoTaskInTime(i * 5 * FRAMES, function(inst2)
            local item = SpawnPrefab(item_prefab)
            item.Transform:SetPosition(x + dist * math.cos(theta), 20, z + dist * math.sin(theta))

            if i == num_items then
                inst._lightning_drop_task:Cancel()
                inst._lightning_drop_task = nil
            end 
        end)
    end
end

local function OnLightningStrike(inst)
    if inst._lightning_drop_task ~= nil then
        return
    end

    local num_small_items = math.random(NUM_DROP_SMALL_ITEMS_MIN, NUM_DROP_SMALL_ITEMS_MAX)
    local items_to_drop = {}

    for i = 1, num_small_items do
        table.insert(items_to_drop, small_ram_products[math.random(1, #small_ram_products)])
    end

    inst._lightning_drop_task = inst:DoTaskInTime(20*FRAMES, DropLightningItems, items_to_drop)
end


local function OnSave(inst, data)
    if inst.hasvive ~= nil then
        data.hasvive = inst.hasvive
    end
end

local function OnLoad(inst, data)
    if data then
        if data.hasvive then
            inst.hasvive = data.hasvive
        end
    end
end

local function OnLoadPostPass(inst, ents, data)

end

local function removecanopy(inst)
    if inst.roots then
        inst.roots:Remove()
    end
    for _,v in ipairs(inst.vines) do
        v:Remove()
    end
    for player in pairs(inst.players) do
        if player:IsValid() then
            if player.canopytrees then
                player.canopytrees = player.canopytrees - 1
                player:PushEvent("onchangecanopyzone", player.canopytrees > 0)
            end
        end
    end
    inst._hascanopy:set(false)
end

local function OnRemove(inst)
    removecanopy(inst)
end
local FIREFLY_MUST = {"firefly"}
local function OnPhaseChanged(inst, phase)
   if phase == "day" then
        local x, y, z = inst.Transform:GetWorldPosition()
        if TheSim:CountEntities(x,y,z, TUNING.SHADE_CANOPY_RANGE, FIREFLY_MUST) < 10 then
            if math.random()<0.7 then
                local pos = nil
                local offset = nil
                local count = 0
                while offset == nil and count < 10 do
                    local angle = 2*PI*math.random()
                    local radius = math.random() * (TUNING.SHADE_CANOPY_RANGE -4)
                    offset = {x= math.cos(angle) * radius, y=0, z=math.sin(angle) * radius}   
                    count = count + 1

                    pos = {x=x+offset.x,y=0,z=z+offset.z}

                    if TheSim:CountEntities(pos.x, pos.y, pos.z, 5) > 0 then
                        offset = nil
                    end
                end

                if offset then
                    local firefly = SpawnPrefab("fireflies")
                    firefly.Transform:SetPosition(x+offset.x,0,z+offset.z)
                end
            end
        end
   end
end

local function spawnvine(inst)
    local x, _, z = inst.Transform:GetWorldPosition()
    for i= 1, math.random(2,4) do
        local vine = SpawnPrefab("myth_plant_infantree_vine")
        local theta = math.random() * PI * 2
        local offset = 1.5 + math.random(3) + math.random()
        vine.Transform:SetPosition(x + math.cos(theta) * offset, 0, z + math.sin(theta) * offset)
        vine.persists = false
        table.insert(inst.vines,vine)
    end
end

local function spawnoverride(inst)
    local pt = Vector3(inst.Transform:GetWorldPosition())

    local theta = math.random() * 2* PI
    local radius = math.random()* 16 + 4

    local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))

    return offset
end

local function onspawnchild( inst, child )
    child:fall_down_fn()
end

local function Empty()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 4, 2, 0.75)

    inst:SetDeployExtraSpacing(4)

    -- HACK: this should really be in the c side checking the maximum size of the anim or the _current_ size of the anim instead
    -- of frame 0
    inst.entity:SetAABB(60, 20)

    inst:AddTag("shadecanopy")
    inst:AddTag("prototyper")

    inst.Transform:SetScale(2, 2, 2)

    inst.MiniMapEntity:SetIcon("myth_plant_infantree.tex")

    inst.AnimState:SetBank("myth_plant_infantree")
    inst.AnimState:SetBuild("myth_plant_infantree")
    inst.AnimState:PlayAnimation("idle", true)

    if not TheNet:IsDedicated() then
        inst:AddComponent("distancefade")
        inst.components.distancefade:Setup(15,25)
    end

    inst._hascanopy = net_bool(inst.GUID, "infantree._hascanopy", "hascanopydirty")
    inst._hascanopy:set(true)  
    inst:ListenForEvent("hascanopydirty", function()
                if not inst._hascanopy:value() then 
                    removecanopyshadow(inst) 
                end
        end)

    inst:DoTaskInTime(0,function()
        inst.canopy_data = CANOPY_SHADOW_DATA.spawnshadow(inst, math.floor(TUNING.SHADE_CANOPY_RANGE/4))
    end)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.players = {}

    inst.vines = {}
   
    inst:AddComponent("playerprox")
    inst.components.playerprox:SetTargetMode(inst.components.playerprox.TargetModes.AllPlayers)
    inst.components.playerprox:SetDist(MIN, MAX)
    inst.components.playerprox:SetOnPlayerFar(OnFar)
    inst.components.playerprox:SetOnPlayerNear(OnNear)

    inst:AddComponent("prototyper")
    inst.components.prototyper.onturnon = Empty
    inst.components.prototyper.onturnoff = Empty
    inst.components.prototyper.onactivate = Empty
    inst.components.prototyper.trees = TUNING.PROTOTYPER_TREES.MYTH_TECH_INFANTREE

    --------------------
    inst:AddComponent("inspectable")

    inst:AddComponent("childspawner")
    inst.components.childspawner.childname = "myth_plant_infant_fruit"
    --inst.components.childspawner:SetRegenPeriod(50*480)  
    --inst.components.childspawner:SetSpawnPeriod(10)
    inst.components.childspawner:SetMaxChildren(20)

    inst.components.childspawner:SetRegenPeriod(49*480)  
    inst.components.childspawner:SetSpawnPeriod(49*480)

    inst.components.childspawner.overridespawnlocation = spawnoverride
    inst.components.childspawner:SetSpawnedFn(onspawnchild)
    inst.components.childspawner:StartSpawning()      

    inst:AddComponent("timer")

    --------------------
    inst:AddComponent("lightningblocker")
    inst.components.lightningblocker:SetBlockRange(TUNING.SHADE_CANOPY_RANGE)
    inst.components.lightningblocker:SetOnLightningStrike(OnLightningStrike)

    inst:ListenForEvent("timerdone", OnTimerDone)
    inst:ListenForEvent("onremove", OnRemove)
    inst:ListenForEvent("phasechanged", function(src, phase) OnPhaseChanged(inst,phase) end, TheWorld)

    inst.roots = SpawnPrefab("myth_plant_infantree_roots")
    inst.roots:DoTaskInTime(0,function()
        inst.roots.Transform:SetPosition(inst.Transform:GetWorldPosition())
    end)
    inst:DoTaskInTime(0,function()
        if next(inst.vines) == nil then
            spawnvine(inst)
        end
    end)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

local function roots_fn(data)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    inst:AddTag("NOBLOCK")

    inst.AnimState:SetBank("infantree_carpet")
    inst.AnimState:SetBuild("infantree_carpet")
    inst.AnimState:PlayAnimation("idle")
    
    inst.Transform:SetScale(1.2,1.2,1.2)
	inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(-2)

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    return inst
end

return Prefab("myth_plant_infantree", fn, assets, prefabs),
    Prefab("myth_plant_infantree_roots", roots_fn, assets)
