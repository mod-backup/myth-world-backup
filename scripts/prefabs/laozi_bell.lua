local assets = {
    Asset("ANIM", "anim/laozi_bell.zip"),
    Asset("ATLAS", "images/inventoryimages/laozi_bell.xml"),
    Asset("ATLAS", "images/inventoryimages/laozi_bell_broken.xml")
}

local nianassets = {
    Asset("ANIM", "anim/nian_bell.zip"),
    Asset("ATLAS", "images/inventoryimages/nian_bell.xml"),
}

local prefabs = {
    "spawn_fx_medium"
}

local TWEEN_TARGET = {0, 0, 0, 1}
local TWEEN_TIME = 13 * FRAMES
local function on_player_despawned(inst)
    for beef, _ in pairs(inst.components.leader.followers) do
        local fx = SpawnPrefab("spawn_fx_medium")
        fx.Transform:SetPosition(beef.Transform:GetWorldPosition())

        beef.components.colourtweener:StartTween(TWEEN_TARGET, TWEEN_TIME, beef.Remove)
        beef:PushEvent("despawn")
    end
end

local function get_other_player_linked_bell(inst, other)
    if other.components.inventory ~= nil then
        return other.components.inventory:FindItem(
            function(item)
                return (item ~= inst) and (item.prefab == "beef_bell") and item:_HasBeefalo()
            end
        )
    elseif other.components.container ~= nil then
        return other.components.container:FindItem(
            function(item)
                return (item ~= inst) and (item.prefab == "beef_bell") and item:_HasBeefalo()
            end
        )
    else
        return nil
    end
end

local function get_beefalo(inst)
    for beef, v in pairs(inst.components.leader.followers) do
        if v then
            return beef
        end
    end

    return nil
end

local function get_owner(inst,grand_owner)
    local beef = get_beefalo(inst)
    if beef and beef.components.rideable and beef.components.rideable.rider ~= nil  then
        return beef.components.rideable.rider ~= grand_owner
    end
    return false
end
local function on_put_in_inventory(inst, owner)
    local grand_owner = inst.components.inventoryitem:GetGrandOwner()
    if grand_owner ~= nil then
        --如果我自己有铃铛！
        if inst:_HasBeefalo() then
            --真有意思 如果我拿了别人的铃铛 那么也要丢下来
            local isnotcurrentowner = get_owner(inst, grand_owner)
            local other_bell = get_other_player_linked_bell(inst, grand_owner)
            if isnotcurrentowner then
                grand_owner:DoTaskInTime(0,function()
                    if grand_owner.components.inventory ~= nil then
                        grand_owner.components.inventory:DropItem(inst, true, true)
                    elseif grand_owner.components.container ~= nil then
                        grand_owner.components.container:DropItem(inst)
                    end
                end)
            elseif other_bell ~= nil then
                if grand_owner.components.inventory ~= nil then
                    grand_owner.components.inventory:DropItem(other_bell, true, true)
                elseif grand_owner.components.container ~= nil then
                    grand_owner.components.container:DropItem(other_bell)
                end
            end
        end
    end
end

local function on_beef_disappeared(inst, beef)
    inst:RemoveTag("nobundling")
    inst.components.useabletargeteditem:StopUsingItem()
end

local function on_nian_disappeared(inst, beef)
end

local function has_beefalo(inst)
    return inst.components.leader:CountFollowers() > 0
end

local function on_used_on_beefalo(inst, target, user)
    if target.SetBeefBellOwner ~= nil then
        --if user ~= nil and get_other_player_linked_bell(inst, user) ~= nil then
        --    return false, "BEEF_BELL_HAS_BEEF_ALREADY"
        --end
        local beef_set_successful, failreason = target:SetBeefBellOwner(inst, user)

        if beef_set_successful then
            inst:AddTag("nobundling")
        end
        if failreason == nil then
            return beef_set_successful
        else
            local full_failreason = string.upper(inst.prefab) .. "_" .. failreason
            return beef_set_successful, full_failreason
        end
    else
        return false, "BEEF_BELL_INVALID_TARGET"
    end
end

local function on_stop_use(inst)
    inst:DoTaskInTime(
        1.5,
        function()
            local beef = get_beefalo(inst)
            if beef and beef:IsValid() and not beef.components.health:IsDead() then
                if beef.components.hitchable and not beef.components.hitchable.canbehitched then
                    beef.components.hitchable:Unhitch()
                end
                if beef.components.combat.target ~= nil then
                    beef.components.combat:SetTarget(nil)
                    beef.components.combat:BlankOutAttacks(1)
                end
                local owner = inst.components.inventoryitem.owner
                if owner then
                    local pos = owner:GetPosition()
                    local offset = FindWalkableOffset(pos, math.random() * 2 * PI, 3, 10)
                    if offset ~= nil then
                        pos.x = pos.x + offset.x
                        pos.z = pos.z + offset.z	
                    end
                    beef.Transform:SetPosition(pos:Get())
                    local fx = SpawnPrefab('mk_cloudpuff')
                    fx.Transform:SetScale(0.8,0.8,0.8)
                    fx.Transform:SetPosition(pos:Get())
                end
            end
        end
    )
end

local function on_stop_usenian(inst)
    local beef = get_beefalo(inst)
    if beef and beef:IsValid() and not beef.components.health:IsDead() then
        on_stop_use(inst)
    elseif inst.components.hunger:GetPercent() >= 1 then
        local beef = SpawnPrefab("nian_mount")
        local pos = inst:GetPosition()
        local targpos = pos + Vector3(GetRandomMinMax(-4,4), 0, GetRandomMinMax(-4,4))
        beef.Transform:SetPosition(targpos:Get())
        inst.components.useabletargeteditem:StartUsingItem(beef)
        local fx = SpawnPrefab('mk_cloudpuff')
        fx.Transform:SetScale(1,1,1)
        fx.Transform:SetPosition(targpos:Get())
    else
        --
    end
end
local function onhungerdelta(inst)
    if inst.components.named then
        inst.components.named:SetName((STRINGS.NAMES[string.upper(inst.prefab)] or "No Name").."\n"..STRINGS.UI.COOKBOOK.SORT_HUNGER..": "..math.floor(inst.components.hunger.current).."/"..inst.components.hunger.max)
    end
end

local function on_bell_save(inst, data)
    for beef, _ in pairs(inst.components.leader.followers) do
        data.beef_record = beef:GetSaveRecord()
        break
    end
end

local function on_bell_load(inst, data)
    if data and data.beef_record then
        local beef = SpawnSaveRecord(data.beef_record)
        if beef ~= nil then
            inst.components.useabletargeteditem:StartUsingItem(beef)
        end
    end
end

local function ShouldAcceptItem(inst, item)
    return inst.components.eater:CanEat(item)
end

local function OnGetItemFromPlayer(inst, giver, item)
    if item.components.edible and inst.components.eater:CanEat(item) then
        local hunger_delta = item.components.edible:GetHunger(inst)
        if hunger_delta ~= 0 then
            inst.components.hunger:DoDelta(hunger_delta)
        end
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
    inst.entity:AddMiniMapEntity()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("laozi_bell")
    inst.AnimState:SetBuild("laozi_bell")
    inst.AnimState:PlayAnimation("laozi_bell", true)

    inst.MiniMapEntity:SetIcon("laozi_bell.tex")

    MakeInventoryFloatable(inst)

    inst:AddTag("bell")
    inst:AddTag("laozi_bell")
    inst:AddTag("irreplaceable")
    inst:AddTag("myth_bell")

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    inst.bell_sound = "yotb_2021/common/cow_bell"

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem:SetOnPutInInventoryFn(on_put_in_inventory)
    inst.components.inventoryitem.atlasname = "images/inventoryimages/laozi_bell.xml"

    inst:AddComponent("useabletargeteditem")
    inst.components.useabletargeteditem:SetTargetPrefab("laozi_qingniu")
    inst.components.useabletargeteditem:SetOnUseFn(on_used_on_beefalo)
    inst.components.useabletargeteditem:SetOnStopUseFn(on_stop_use)
    inst.components.useabletargeteditem.StopUsingItem = function(self)
        if self.onstopusefn then
            self.onstopusefn(self.inst)
        end
    end
    inst.components.useabletargeteditem:SetInventoryDisable(true)

    inst:AddComponent("tradable")

    inst:AddComponent("leader")
    inst.components.leader.onremovefollower = on_nian_disappeared

    inst._HasBeefalo = has_beefalo
    inst.GetBeefalo = get_beefalo
    inst.OnSave = on_bell_save
    inst.OnLoad = on_bell_load

    TheWorld.laozi_bell = inst

    inst:ListenForEvent("player_despawn", on_player_despawned)
    inst:ListenForEvent("onremove", function()
        TheWorld.laozi_bell = nil
    end)

    return inst
end

local function nianfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
    inst.entity:AddMiniMapEntity()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("nian_bell")
    inst.AnimState:SetBuild("nian_bell")
    inst.AnimState:PlayAnimation("idle")

    inst.MiniMapEntity:SetIcon("nian_bell.tex")

    MakeInventoryFloatable(inst)

    inst:AddTag("bell")
    inst:AddTag("nian_bell")
    --inst:AddTag("irreplaceable")
    inst:AddTag("nobundling")
    inst:AddTag("myth_bell")
    inst:AddTag("handfed")
    inst:AddTag("fedbyall")

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("hunger")
    inst.components.hunger:SetMax(TUNING.NIAN_BELL_HUNGER)
    inst.components.hunger:SetPercent(0)
    inst.components.hunger:Pause()

    inst.bell_sound = "dontstarve/creatures/together/klaus/chain_foley"
    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem:SetOnPutInInventoryFn(on_put_in_inventory)
    inst.components.inventoryitem.atlasname = "images/inventoryimages/nian_bell.xml"

    inst:AddComponent("useabletargeteditem")
    --inst.components.useabletargeteditem:SetTargetPrefab("nian_mount") --不能对着物品使用
    inst.components.useabletargeteditem:SetOnUseFn(on_used_on_beefalo)
    inst.components.useabletargeteditem:SetOnStopUseFn(on_stop_usenian)
    inst.components.useabletargeteditem.StartUsingItem = function(self, target, doer)
        local usesuccess = nil
        local usefailreason = nil
        if self.onusefn then
            usesuccess, usefailreason = self.onusefn(self.inst, target, doer)
        else
            usesuccess = true
        end
        return usesuccess, usefailreason
    end
    inst.components.useabletargeteditem.StopUsingItem = function(self)
        if self.onstopusefn then
            self.onstopusefn(self.inst)
        end
    end
    inst.components.useabletargeteditem.inventory_disableable = true
    inst.components.useabletargeteditem.inuse_targeted = true
    --inst.components.useabletargeteditem:SetInventoryDisable(false)

    inst:AddComponent("eater")

    --inst:AddComponent("trader")
    --inst.components.trader:SetAcceptTest(ShouldAcceptItem)
    --inst.components.trader.onaccept = OnGetItemFromPlayer
    --inst.components.trader.onrefuse = OnRefuseItem

    inst:AddComponent("tradable")

    inst:AddComponent("named")

    inst:AddComponent("leader")
    inst.components.leader.onremovefollower = on_beef_disappeared

    inst._HasBeefalo = has_beefalo
    inst.GetBeefalo = get_beefalo
    inst.OnSave = on_bell_save
    inst.OnLoad = on_bell_load

    inst:ListenForEvent("player_despawn", on_player_despawned)
    inst:ListenForEvent("hungerdelta", onhungerdelta)
    inst:DoTaskInTime(0,function()
        inst.components.hunger:DoDelta(0)
    end)
    return inst
end

local function broken_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
    inst.entity:AddMiniMapEntity()
    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("laozi_bell")
    inst.AnimState:SetBuild("laozi_bell")
    inst.AnimState:PlayAnimation("laozi_bell_broken", true)

    inst.MiniMapEntity:SetIcon("laozi_bell.tex")

    --inst:AddTag("irreplaceable")
    MakeInventoryFloatable(inst)

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/laozi_bell_broken.xml"

    inst:AddComponent("tradable")

    return inst
end

return Prefab("laozi_bell", fn, assets), 
    Prefab("nian_bell", nianfn, nianassets),
    Prefab("laozi_bell_broken", broken_fn)
