require "prefabutil"

local assets =
{

}

local prefabs =
{
    "collapse_small",
}

local function onhammered(inst, worker)
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("metal")
    inst:Remove()
end

local function onbuilt(inst)
	inst:Hide()
	inst:DoTaskInTime(0,function()
		local x,y,z = inst.Transform:GetWorldPosition()
		x, y, z = TheWorld.Map:GetTileCenterPoint(x, 0, z)
		inst.Transform:SetPosition(x,0, z)
		inst:Show()
		inst.AnimState:PlayAnimation("fill")
		inst.AnimState:PushAnimation("idle_full")
	end)
end

local function OnCheck(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 16, nil, { "FX", "NOCLICK", "DECOR", "INLIMBO", "playerghost" }, { "smolder" })
    for i, v in pairs(ents) do
        if v.components.burnable ~= nil then
			if v.components.burnable:IsSmoldering() then
				v.components.burnable:SmotherSmolder()
			end
        end	
    end
end

local function canuseinscene_myth(inst,doer,right)
    --print(doer.components.inventory and doer.components.inventory:EquipHasTag("myth_plant_infantree_trunk"))
    return doer.replica.inventory and doer.replica.inventory:EquipHasTag("myth_plant_infantree_trunk")
end

local function EquipHasTag(self,tag)
    for k, v in pairs(self.equipslots) do
        if v:HasTag(tag) then
            return v
        end
    end
end

local function onuse(inst,doer)
    if inst.dried then
        return false
    end
    if doer and doer.components.inventory ~= nil then
        doer.components.inventory:DropItem(inst)
        local old = EquipHasTag(doer.components.inventory,"myth_plant_infantree_trunk")
        if old then
            old:Remove()
            local new = ReplacePrefab(inst, "myth_plant_infantree_small")
            new.AnimState:PlayAnimation("plant")
            new.AnimState:PushAnimation("tree",false)   
            doer:DoTaskInTime(0.2,function()
                doer.SoundEmitter:PlaySound("dontstarve/creatures/pengull/splash")
            end)         
            return true
        end
    end
end

local function setdried(inst)
    inst.dried = true 
    if inst.check then
        inst.check:Cancel()
        inst.check = nil
    end
    inst.AnimState:PlayAnimation("idle_empty")
end

local function setwatersource(inst)
    inst.dried = false 
    if not  inst.check then
        inst.check = inst:DoPeriodicTask(1, OnCheck,1)
    end
    inst.AnimState:PlayAnimation("fill")
    inst.AnimState:PushAnimation("idle_full")
end

local function OnIsRaining(inst, israining)
    if israining and inst.dried then
        inst:DoTaskInTime(5,setwatersource)
    end
end

local function onload(inst, data)
    if data ~= nil and data.dried then
        setdried(inst)
    end
end

local function onsave(inst, data)
    data.dried = inst.dried
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    inst.MiniMapEntity:SetIcon("myth_well.tex")

	MakeObstaclePhysics(inst, 1.5)
	
    inst:AddTag("watersource")
    inst:AddTag("antlion_sinkhole_blocker")
    inst:AddTag("birdblocker")
	inst:AddTag("shelter")

    inst.AnimState:SetBank("myth_well")
    inst.AnimState:SetBuild("myth_well")
    inst.AnimState:PlayAnimation("idle_full",true)

    inst.canuseinscene_myth = canuseinscene_myth
    inst.MYTH_USE_TYPE = "PLANT"
    inst.onusesgname = "doshortaction"
	--inst:SetDeployExtraSpacing(2)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    inst.dried = false

    inst:AddComponent("inspectable")

    inst:AddComponent("lootdropper")
	
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(3)
    inst.components.workable:SetOnFinishCallback(onhammered)

    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = false
	inst.components.myth_use_inventory.canusescene  = false
	inst.components.myth_use_inventory:SetOnUseFn(onuse)

    inst:ListenForEvent("onbuilt", onbuilt)

	inst:AddComponent("watersource")

    inst.OnSave = onsave
    inst.OnLoad = onload

    inst.SetDried = setdried

	inst.check = inst:DoPeriodicTask(1, OnCheck,1)

    inst:WatchWorldState("israining", OnIsRaining)

    return inst
end
local function placerfn(inst)
    inst.components.placer.snap_to_tile = true

	inst.outline = SpawnPrefab("tile_outline")
	inst.outline.entity:SetParent(inst.entity)

	inst.components.placer:LinkEntity(inst.outline)
end

return Prefab("myth_well", fn, assets, prefabs),
    MakePlacer("myth_well_placer", "myth_well", "myth_well", "idle_full",nil,nil,nil,nil,nil,nil,placerfn)
