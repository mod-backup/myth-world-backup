local function flyaway(inst,isfire,erode)
	local pt = inst:GetPosition()
	for i, v in pairs(TheSim:FindEntities(pt.x, 0, pt.z, 5)) do
		if v.components.temperature ~= nil then
			v.components.temperature:DoDelta(isfire and 70 or -70 )
		end
	end
	if erode then
		ErodeAway(inst)
	else
		inst:Remove()
	end
end

local function onuse(inst,target,doer)
    if target.components.infantree_grower then
        target.components.infantree_grower:AddEssense(inst)
    end
end

local function needfn(inst,target,doer)
    return target:HasTag("essense_target")
end

local function makeitem(name)
    return Prefab("myth_"..name.."essense", function()
		--------------------------------------------------------------------------------------
        local inst = CreateEntity()
		--------------------------------------------------------------------------------------
        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()
		--------------------------------------------------------------------------------------
        MakeInventoryPhysics(inst)
		--------------------------------------------------------------------------------------
        inst.AnimState:SetBank("myth_essense")
        inst.AnimState:SetBuild("myth_essense")
        inst.AnimState:PlayAnimation(name)
		--------------------------------------------------------------------------------------
        inst.entity:SetPristine()
		--------------------------------------------------------------------------------------

		inst.myth_useitem_needfn = needfn
		inst.MYTH_USEITEM_TYPE  = "LAOZIPACK"

        if not TheWorld.ismastersim then
            return inst
        end
		--------------------------------------------------------------------------------------
        inst:AddComponent("inventoryitem")
        inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_"..name.."essense.xml"
		--------------------------------------------------------------------------------------
        inst:AddComponent("inspectable")
		--------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------
        inst:ListenForEvent("ondropped", function(inst)
			flyaway(inst,name == "fire",true)
		end)

		inst:AddComponent("myth_useitem")
		inst.components.myth_useitem.onuse = onuse
		--------------------------------------------------------------------------------------
        inst:ListenForEvent("onputininventory", function(inst, owner)
            if owner and not owner:HasTag("essense_container") then
                inst:DoTaskInTime(0,function()
                    if owner.components.inventory and owner.components.inventory:GetActiveItem() ~= inst then
                        flyaway(inst,name == "fire")
                    end
                end)
            end
		end)
		--------------------------------------------------------------------------------------
        return inst
		--------------------------------------------------------------------------------------
    end, {
		Asset("ANIM", "anim/myth_essense.zip"),
		Asset( "ATLAS", "images/inventoryimages/myth_fireessense.xml" ),
		Asset( "ATLAS", "images/inventoryimages/myth_coldessense.xml" ),
	}, {
	})
end

return makeitem("fire"), makeitem("cold")