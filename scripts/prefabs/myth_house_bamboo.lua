require "prefabutil"

local assets =
{
	Asset("ANIM", "anim/myth_house_bamboo.zip"),
	Asset("ATLAS", "images/inventoryimages/myth_house_bamboo.xml"),

}

local prefabs =
{
	"collapse_small",
}

SetSharedLootTable('myth_house_bamboo',
	{
		{ 'livinglog', 1.00 },
		{ 'livinglog', 1.00 },
		{ 'livinglog', 1.00 },
		{ 'red_cap',   1.00 },
		{ 'red_cap',   1.00 },
		{ 'red_cap',   1.00 },
		{ 'boards',    1.00 },
		{ 'boards',    1.00 },
		{ 'boards',    1.00 },
	})

local function OnDoneTeleporting(inst, obj)
	if obj ~= nil and obj:HasTag("player") then
	end
end

local function OnActivate(inst, doer)
	if doer:HasTag("player") then
	elseif inst.SoundEmitter ~= nil then
		--inst.SoundEmitter:PlaySound("dontstarve/common/teleportworm/swallow") 音效待定
	end
end

local function onhit(inst, worker)

end

local function onhammered(inst, worker)
	inst.components.lootdropper:DropLoot()
	local fx = SpawnPrefab("collapse_big") --这个是拆除动画
	fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
	fx:SetMaterial("metal")
	inst:Remove()
end


local function onbuilt(inst, data)
	if not TheWorld.components.mythhouse or TheWorld.components.mythhouse:IsMax() then
		return
	end

	local x, z = TheWorld.components.mythhouse:GetPosition()

	--[[local removeents = TheSim:FindEntities(x, 0, z, 15)
	for i,v in ipairs(removeents) do
		v:Remove()
	end]]

	local myth_house = SpawnPrefab("myth_house1")
	myth_house.Transform:SetPosition(x, 0, z)
	myth_house:SetHasLight(true)

	local exit = SpawnPrefab("myth_door_exit_3")
	exit.Transform:SetPosition(x + 5, 0, z + 0.2)

	local floor = SpawnPrefab("myth_floor_1")
	floor.Transform:SetPosition(x - 5.8, 0, z)
	floor:SetHouseWallSkin("myth_floorskin_bamboom")

	local window = SpawnPrefab("myth_window_round")
	window.Transform:SetPosition(x - 6.2, 0.8, z + 2.5)

	local wallpaper = SpawnPrefab("myth_wallpaper_1")
	wallpaper.Transform:SetPosition(x, 0, z)
	wallpaper:SetHouseWallSkin("myth_wallskin_bamboom")

	exit.components.myth_teleporter:Target(inst)
	inst.components.myth_teleporter:Target(exit)

	local function addwall(x, z)
		local wall = SpawnPrefab("myth_wall")
		if wall ~= nil then
			wall.Physics:SetCollides(false)
			wall.Physics:Teleport(x, 0, z)
			wall.Physics:SetCollides(true)
		end
	end
	--行1
	for b = -8, 7 do
		addwall(x - 6, z + b + 0.5)
	end

	--行2
	for b = -8, 7 do
		addwall(x + 5 + 0.5, z + b + 0.5)
	end
	--列1
	for b = -7, 6 do
		addwall(x + b + 0.5, z - 7)
	end

	for b = -7, 6 do
		addwall(x + b + 0.5, z + 7)
	end

	local myth_wall_decals1 = SpawnPrefab("myth_wall_decals_bamboom")
	myth_wall_decals1.Transform:SetPosition(x + 5, 0, z - 5.8)
	local myth_wall_decals2 = SpawnPrefab("myth_wall_decals_bamboom")
	myth_wall_decals2.Transform:SetPosition(x + 5, 0, z + 5.8)
	myth_wall_decals2.AnimState:SetScale(-1, 1, 1)
	myth_wall_decals2.eastside = true

	local myth_wall_decals3 = SpawnPrefab("myth_wall_decals_bamboom_in")
	myth_wall_decals3.Transform:SetPosition(x - 5.5, 0, z - 5.8)

	local myth_wall_decals4 = SpawnPrefab("myth_wall_decals_bamboom_in")
	myth_wall_decals4.Transform:SetPosition(x - 5.5, 0, z + 5.8)
	myth_wall_decals4.AnimState:SetScale(-1, 1, 1)
	myth_wall_decals4.eastside = true

	-- local bed = SpawnPrefab("myth_interiors_bed")
	-- bed.Transform:SetPosition(x - 3.59, 0, z - 4.98)

	-- local xl = SpawnPrefab("myth_interiors_xl")
	-- xl.Transform:SetPosition(x + 3.14, 0, z - 5.25)

	-- local zz = SpawnPrefab("myth_interiors_zz")
	-- zz.Transform:SetPosition(x - 3.66, 0, z + 0.14)

	-- local gz = SpawnPrefab("myth_interiors_gz")
	-- gz.Transform:SetPosition(x - 4.2, 0, z + 4.23)

	-- local pf = SpawnPrefab("myth_interiors_pf")
	-- pf.Transform:SetPosition(x + 2.94, 0, z + 3.86)

	-- local gh = SpawnPrefab("myth_interiors_gh")
	-- gh.Transform:SetPosition(x - 5.5, 0, z + 0.32)

	-- local gh_small = SpawnPrefab("myth_interiors_gh_small")
	-- gh_small.Transform:SetPosition(x - 3.76, 0, z + 7.12)

	inst.house = true

	TheWorld.components.mythhouse:BuildHouse()

	if data and data.builder and data.builder.userid ~= nil then
		inst._ownerid = data.builder.userid
	end
end

local lightcolour = { 255 / 255, 236 / 255, 0 / 255 }

local function updatelight(inst, phase)
	if phase == "night" then
		inst.Light:Enable(true)
	else
		inst.Light:Enable(false)
	end
end

local function updatewindow(inst, phase)
	if phase == "night" then
		if inst._window ~= nil then
			inst._window:Show()
		end
	else
		if inst._window ~= nil then
			inst._window:Hide()
		end
	end
end

local function MakeWindow()
	local inst = CreateEntity("Bambbonhouse.MakeWindow")

	inst.entity:AddTransform()
	inst.entity:AddAnimState()

	inst:AddTag("DECOR")
	inst:AddTag("NOCLICK")

	inst.persists = false

	inst.AnimState:SetBank("myth_house_bamboo")
	inst.AnimState:SetBuild("myth_house_bamboo")
	inst.AnimState:PlayAnimation("light")
	inst.AnimState:SetLightOverride(.4)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	inst.AnimState:SetFinalOffset(1)

	inst:Hide()

	return inst
end

local function chuansong(inst, x, y, z, kill)
	if inst:HasTag("player") then
		inst:ScreenFade(false)
		inst:DoTaskInTime(1, function()
			inst:SnapCamera()
			inst:ScreenFade(true, 0.5)
		end)
	end
	if inst.Transform ~= nil then
		inst.Transform:SetPosition(x, y, z)
	end
	if kill then -----------------实测生物没有被杀
		-- inst:DoTaskInTime(0.2, function(inst)
		if inst and inst:IsValid() and inst.components.health ~= nil and not inst.components.health:IsDead() then
			-- inst and inst:IsValid() and inst:HasTag("player") and inst.components.health ~= nil and not inst.components.health:IsDead() then
			--如果不在水里 就kill
			inst.components.health:Kill()
		end
		-- end)
	end
end
local function onsave(inst, data)
	data._ownerid = inst._ownerid or nil
end

local function onload(inst, data)
	if data and data._ownerid ~= nil then
		inst._ownerid = data._ownerid
	end
end

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddMiniMapEntity()
	inst.entity:AddLight() --给物体加光
	inst.entity:AddNetwork()

	inst.MiniMapEntity:SetIcon("myth_ghg.tex") --没有制作icon

	inst.AnimState:SetBank("myth_house_bamboo")
	inst.AnimState:SetBuild("myth_house_bamboo")
	inst.AnimState:PlayAnimation("idle")
	inst.Transform:SetScale(0.9, 0.9, 0.9)

	inst:AddTag("myth_door")
	inst:AddTag("shelter")
	inst:AddTag("antlion_sinkhole_blocker")
	inst:AddTag("nonpackable")

	MakeObstaclePhysics(inst, 1.2) --1.2 碰撞体积
	MakeSnowCoveredPristine(inst)

	inst.Light:SetFalloff(.5)
	inst.Light:SetIntensity(.85)
	inst.Light:SetRadius(1.5)
	inst.Light:SetColour(unpack(lightcolour))

	if not TheNet:IsDedicated() then --为什么不能是dedicated
		inst._window = MakeWindow()
		inst._window.entity:SetParent(inst.entity)
		inst:WatchWorldState("phase", updatewindow) --总感觉这两行重复了
		updatewindow(inst, TheWorld.state.phase)
	end

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst:AddComponent("inspectable")

	inst:AddComponent("myth_teleporter")
	inst.components.myth_teleporter.onActivate = OnActivate
	inst.components.myth_teleporter.offset = 0
	inst.components.myth_teleporter.travelcameratime = 1
	inst.components.myth_teleporter.travelarrivetime = 0.5

	inst:ListenForEvent("doneteleporting", OnDoneTeleporting)

	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetChanceLootTable("hua_player_house") --啥意思

	--[[if TUNING.HUA_HOUSE_HAMMER == true then
		inst:AddComponent("workable")
		inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
		inst.components.workable:SetWorkLeft(5)
		inst.components.workable:SetOnFinishCallback(onhammered)
		inst.components.workable:SetOnWorkCallback(onhit)
	--end]]

	-- inst:ListenForEvent("onbuilt", onbuilt) ---build的时候才会建立链接，所以直接生成不行
	inst:DoTaskInTime(0.2, function(inst)
		if not inst.house then
			onbuilt(inst)
		end
	end)

	MakeSnowCovered(inst)

	inst:WatchWorldState("phase", updatelight)

	updatelight(inst, TheWorld.state.phase)

	inst:ListenForEvent("onremove", function(inst)
		local x, y, z = inst.Transform:GetWorldPosition()
		if not x then
			for k, v in pairs(Ents) do
				if v:HasTag("multiplayer_portal") then
					x, y, z = v.Transform:GetWorldPosition()
				end
			end
		end
		if not x then
			x, y, z = 0, 0, 0
		end

		if inst.components.myth_teleporter and inst.components.myth_teleporter.targetTeleporter ~= nil then
			local target = inst.components.myth_teleporter.targetTeleporter
			local target_x, target_y, target_z = target.Transform:GetWorldPosition()
			local ents = TheSim:FindEntities(target_x, 0, target_z, 20, nil, { "INLIMBO" })


			for i, v in ipairs(ents) do
				if v:HasTag("player") or v:HasTag("irreplaceable") or v.components.health ~= nil then
					chuansong(v, x, y, z, true)
				elseif v.components.workable ~= nil then --工作
					v.components.workable:Destroy(v) --这里墙不会被摧毁,只会摧毁其他建筑
					-- elseif v.components.perishable ~= nil then --新鲜度
					-- 	v.components.perishable:LongUpdate(10000)
					-- elseif v.components.finiteuses ~= nil then --耐久
					-- 	v.components.finiteuses:Use(10000)
					-- elseif v.components.fueled ~= nil then --耐久
					-- 	v.components.fueled:DoUpdate(10000)
				end
			end

			TheWorld:DoTaskInTime(0.5, function(world)
				local ents2 = TheSim:FindEntities(target_x, 0, target_z, 20)
				for i, v in ipairs(ents2) do
					if v and v.components.inventoryitem ~= nil then
						chuansong(v, x, y, z)
					else
						v:Remove()
					end
				end
				local collapse = SpawnPrefab("collapse_big")
				collapse.Transform:SetPosition(x, 0, z)
			end)
		end
	end)

	inst.OnSave = onsave
	inst.OnLoad = onload

	return inst
end

local function placefn(inst)
	inst.Transform:SetScale(0.9, 0.9, 0.9)
	inst.AnimState:HideSymbol("snow")
end

return Prefab("myth_house_bamboo", fn, assets, prefabs),
	MakePlacer("myth_house_bamboo_placer", "myth_house_bamboo", "myth_house_bamboo", "idle", nil, nil, nil, nil, nil, nil,
		placefn)
