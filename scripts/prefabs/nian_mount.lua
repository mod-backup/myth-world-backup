local fns = {} -- a table to store local functions in so that we don't hit the 60 upvalues limit

local assets =
{
    Asset("ANIM", "anim/nian_mount.zip"),
    --Asset("ANIM", "anim/nian_mount_docile.zip"),
}

local brain = require("brains/nian_mountbrain")

local sounds = 
{
    walk = "",
    grunt = "",
    yell = "Myth_nian/sfx/myth_nian",
    swish = "",
    curious = "",
    angry = "Myth_nian/sfx/myth_nian",
    idle = "",
    howl = "",
    hit = "",
    attack = "",
    death = "Myth_nian/sfx/myth_nian",
    sleep = "",
}

fns.ClearBellOwner = function(inst)
    local bell_leader = inst.components.follower:GetLeader()
    inst:RemoveEventCallback("onremove", inst._BellRemoveCallback, bell_leader)

    inst.components.follower:SetLeader(nil)
    inst.components.rideable:SetShouldSave(true)

    inst.persists = true
end

fns.GetBeefBellOwner = function(inst)
    local leader = inst.components.follower:GetLeader()
    return (leader ~= nil
        and leader.components.inventoryitem ~= nil
        and leader.components.inventoryitem:GetGrandOwner())
        or nil
end

fns.SetBeefBellOwner = function(inst, bell, bell_user)
    if inst.components.follower:GetLeader() == nil
            and bell ~= nil and bell.components.leader ~= nil then
        bell.components.leader:AddFollower(inst)
        inst.components.rideable:SetShouldSave(false)

        inst:ListenForEvent("onremove", inst._BellRemoveCallback, bell)

        inst.persists = false

        return true
    else
        return false, "ALREADY_USED"
    end
end

local function ClearBuildOverrides(inst, animstate)
    local basebuild = "nian_mount"
    if animstate ~= inst.AnimState then
        animstate:ClearOverrideBuild(basebuild)
    end
    --animstate:ClearOverrideBuild("nian_mount_docile")
end

local function ApplyBuildOverrides(inst, animstate)
    local basebuild = "nian_mount"
    if animstate ~= nil and animstate ~= inst.AnimState then
        animstate:AddOverrideBuild(basebuild)
    else
        animstate:SetBuild(basebuild)
    end
   -- animstate:AddOverrideBuild("nian_mount_docile")
end


local MAX_CHASEAWAY_DIST_SQ = 16
local function KeepTarget(inst, target)
    return false
    --return inst.components.combat:CanTarget(target) and target:IsOnValidGround()
    --        and target:GetDistanceSqToPoint(inst.Transform:GetWorldPosition()) < MAX_CHASEAWAY_DIST_SQ
end

local function OnAttacked(inst, data)
    if inst._ridersleeptask ~= nil then
        inst._ridersleeptask:Cancel()
        inst._ridersleeptask = nil
    end
    inst._ridersleep = nil
    if data.attacker then 
        inst.components.combat:SetTarget(data.attacker)
    end
end

local function ShouldBeg(inst)
    return false
end

local function OnDeath(inst, data)
    if not inst.dead_already then
        inst.dead_already = true
        inst.persists = false
        inst:AddTag("NOCLICK")
        if inst.components.rideable:IsBeingRidden() then
            inst.components.rideable:Buck(true)
        end
        local leader = inst.components.follower:GetLeader()
        if  leader and leader.prefab == "nian_bell"  and leader:GetBeefalo() == inst then
            if leader:IsValid() then
                if leader and leader.components.hunger then
                    leader.components.hunger:SetPercent(0)
                end
            end
        end
        local newbeef = SpawnPrefab("nian_fly")
        newbeef.Transform:SetPosition(inst.Transform:GetWorldPosition())
        newbeef:FacePoint(inst.Transform:GetWorldPosition())
        newbeef:GoAway()
        inst:Remove()
    end
end

local function DoRiderSleep(inst, sleepiness, sleeptime)
    inst._ridersleeptask = nil
    inst.components.sleeper:AddSleepiness(sleepiness, sleeptime)
end

local function PotentialRiderTest(inst, potential_rider)
    local leader = inst.components.follower:GetLeader()
    if leader == nil or leader.components.inventoryitem == nil then
        return false
    end

    local leader_owner = leader.components.inventoryitem:GetGrandOwner()
    return (leader_owner ~= nil and leader_owner == potential_rider)
end

local function _OnRefuseRider(inst)
    if inst.components.sleeper:IsAsleep() and not inst.components.health:IsDead() then
        inst.components.sleeper:WakeUp()
    end
end

local function OnRefuseRider(inst, data)
    inst:DoTaskInTime(0, _OnRefuseRider)
end

local function OnRiderSleep(inst, data)
    inst._ridersleep = inst.components.rideable:IsBeingRidden() and {
        time = GetTime(),
        sleepiness = data.sleepiness,
        sleeptime = data.sleeptime,
    } or nil
end

local WAKE_TO_FOLLOW_DISTANCE = 15
local function ShouldWakeUp(inst)
    return DefaultWakeTest(inst)
        or (inst.components.follower.leader ~= nil
            and not inst.components.follower:IsNearLeader(WAKE_TO_FOLLOW_DISTANCE))
end

local SLEEP_NEAR_LEADER_DISTANCE = 10
local function MountSleepTest(inst)
    return not inst.components.rideable:IsBeingRidden()
        and DefaultSleepTest(inst)
        and not inst:HasTag("hitched")
        and (inst.components.follower.leader == nil
            or inst.components.follower:IsNearLeader(SLEEP_NEAR_LEADER_DISTANCE))
end

local function onwenthome(inst,data)
    if data.doer and data.doer.prefab == "carrat" then
        addcarrat(inst,data.doer)
        inst:PushEvent("carratboarded")
    end
end

fns.OnSave = function(inst, data)

end

fns.OnLoad = function(inst, data)

end

fns.OnLoadPostPass = function(inst,data)

end

local function beefalo()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 100, .5)

    inst.DynamicShadow:SetSize(6, 2)
    inst.Transform:SetSixFaced()

    local s  = 0.8
    inst.Transform:SetScale(s, s, s)

    inst.AnimState:SetBank("warg")
    inst.AnimState:SetBuild("myth_nian")
    inst.AnimState:PlayAnimation("idle_loop", true)
    inst.AnimState:Hide("HEAT")

    --inst.MiniMapEntity:SetIcon("beefalo_domesticated.png")
    --inst.MiniMapEntity:SetEnabled(true)
    inst.AnimState:HideSymbol("warg_eye")

    inst:AddTag("animal")
    inst:AddTag("largecreature")
    inst:AddTag("companion")
    inst:AddTag("nian_mount")
    inst:AddTag("scarytoprey")
    inst:AddTag("notraptrigger")
    inst:AddTag("noauradamage")
    inst:AddTag("nian")
    inst:AddTag("monster")

    inst.sounds = sounds

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("bloomer")

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TUNING.NIAN_MOUNT_DAMAGE)
    inst.components.combat:SetAreaDamage(3, 1)
    inst.components.combat:SetRange(TUNING.NIAN_MOUNT_ATTACKRANGE)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.NIAN_MOUNT_HEALTH)
    inst.components.health.nofadeout = true

    inst:ListenForEvent("death", OnDeath)

    inst:AddComponent("lootdropper")

    inst:AddComponent("inspectable")

    inst:AddComponent("knownlocations")

    inst:AddComponent("leader")
    inst:AddComponent("follower")
    inst.components.follower.canaccepttarget = false

    inst:ListenForEvent("attacked", OnAttacked)

    inst:AddComponent("rideable")
    inst.components.rideable:SetCustomRiderTest(PotentialRiderTest)
    inst.components.rideable.canride = true
    inst.components.rideable:SetShouldSave(false)

    inst:ListenForEvent("refusedrider", OnRefuseRider)

    MakeLargeBurnableCharacter(inst, "swap_fire")
    --MakeLargeFreezableCharacter(inst, "swap_fire")

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.walkspeed = TUNING.BEEFALO_WALK_SPEED
    inst.components.locomotor.runspeed = 9

    inst:AddComponent("sleeper")
    inst.components.sleeper:SetResistance(3)
    inst.components.sleeper.sleeptestfn = MountSleepTest
    inst.components.sleeper.waketestfn = ShouldWakeUp

    inst:AddComponent("timer")

    inst.ApplyBuildOverrides = ApplyBuildOverrides
    inst.ClearBuildOverrides = ClearBuildOverrides

    inst:ListenForEvent("ridersleep", OnRiderSleep)

    inst:ListenForEvent("stopfollowing", fns.ClearBellOwner)

    inst:AddComponent("uniqueid")

    inst:AddComponent("drownable")

    inst:AddComponent("debuffable")

    inst:AddComponent("colourtweener")

    inst:AddComponent("markable_proxy")

    inst.ShouldBeg = ShouldBeg
    inst.SetBeefBellOwner = fns.SetBeefBellOwner
    inst.GetBeefBellOwner = fns.GetBeefBellOwner
    inst.ClearBeefBellOwner = fns.ClearBellOwner

    inst._BellRemoveCallback = function(bell)
        fns.ClearBellOwner(inst)
    end

    inst:SetBrain(brain)
    inst:SetStateGraph("SGmyth_nian")

    inst.GetIsInMood = function(...)
        return false
    end
    --inst.OnSave = fns.OnSave
    --inst.OnLoad = fns.OnLoad
    --inst.OnLoadPostPass = fns.OnLoadPostPass

    return inst
end

local function goaway(inst)
    inst.Physics:SetMotorVel(12,4,0)
end

local function fnfx(Sim)
    local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    local sound = inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
    
    MakeGhostPhysics(inst, 1, 1.5)
    RemovePhysicsColliders(inst)

    local s  = 0.8
    inst.Transform:SetScale(s, s, s)
    
    inst.AnimState:SetBank("warg")
    inst.AnimState:SetBuild("myth_nian")
    inst.AnimState:PlayAnimation("run_loop", true)
    inst.AnimState:Hide("HEAT")
    inst.Transform:SetSixFaced()

    inst:AddTag("fx")
    inst:AddTag("flying")
    inst:SetPrefabNameOverride("nian")
    inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/howl")

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end 

    inst.persists = false
    inst:AddComponent("locomotor")
    inst.components.locomotor.walkspeed = 12
    inst.components.locomotor.runspeed = 12

    inst:AddComponent("inspectable")

    inst:AddComponent("mk_cloudfxspawner")
    inst.components.mk_cloudfxspawner.qn = true

    inst.GoAway = goaway

    inst:DoTaskInTime(5,ErodeAway)

    return inst
end

return Prefab("nian_mount", beefalo, assets),
    Prefab("nian_fly", fnfx)
