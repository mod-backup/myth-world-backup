local assets =
{
    Asset("ANIM", "anim/commissioner_book.zip"),
    Asset("ANIM", "anim/commissioner_tang.zip"),
    Asset("ANIM", "anim/myth_yinyangyu.zip"),
    Asset("ANIM", "anim/myth_penspell.zip"),
    Asset("ATLAS", "images/inventoryimages/commissioner_book.xml"),
    Asset("ATLAS", "images/inventoryimages/commissioner_mpt.xml"),
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "commissioner_book", "swap")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")

    local theta = math.random() * 2 * PI
    local pt = owner:GetPosition()
    local radius = 2
    local offset = FindWalkableOffset(pt, theta, radius, 6, true)
    if offset ~= nil then
        pt.x = pt.x + offset.x
        pt.z = pt.z + offset.z
    end
    inst.components.book_leash:SpawnPetAt(pt.x, pt.y, pt.z, "commissioner_flybook")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
    inst.components.book_leash:DespawnAllPets()
end


local function Container_WithTag(self,tag)
    local containers = {}
    for i = 1, self.numslots do
        local item = self.slots[i]
        if item ~= nil then
            if item:HasTag(tag) then
                item:Remove()
            elseif item.components.container ~= nil then
                table.insert(containers, item)
            end
        end
    end
    for i, v in ipairs(containers) do
        Container_WithTag(v.components.container,tag)
    end
end

local function RemoveWithTag(self,tag)
    local containers = {}

    if self.activeitem ~= nil then
        if self.activeitem:HasTag(tag) then
            self.activeitem:Remove()
        elseif self.activeitem.components.container ~= nil then
            table.insert(containers, self.activeitem)
        end
    end

    for k = 1, self.maxslots do
        local v = self.itemslots[k]
        if v ~= nil then
            if v:HasTag(tag) then
                v:Remove()
            elseif v.components.container ~= nil then
                table.insert(containers, v)
            end
        end
    end

    for k, v in pairs(self.equipslots) do
        if v:HasTag(tag) then
            v:Remove()
        elseif v.components.container ~= nil then
            table.insert(containers, v)
        end
    end
    for i, v in ipairs(containers) do
        Container_WithTag(v.components.container,tag)
    end
end

local function DoEffects(pet)
    SpawnPrefab("commissioner_teleport").Transform:SetPosition(pet.Transform:GetWorldPosition())
end

local function OnSpawnPet(inst, pet)
    --Delayed in case we need to relocate for migration spawning
    pet:DoTaskInTime(0, DoEffects)
    if pet.components.spawnfader ~= nil then
        pet.components.spawnfader:FadeIn()
    end
end
local function OnDespawnPet(inst, pet)
    DoEffects(pet)
    pet:Remove()
end

local function TakeSoul(inst,info)
    if info then
        table.insert(inst.creatures,1,info)
        local new = {}
        for k= 1, 15 do
            if inst.creatures[k] then
                new[k] = inst.creatures[k]
            end
        end
        inst.creatures = new
    end
end

local function getlefttiem(inst)
    local new = {}
    for i, v in ipairs(inst.creatures) do
        if v.targettime and v.targettime > GetTime() then --灵魂还存在
        end
    end
end
local function onload(inst, data)
    if data ~= nil and data.creatures ~= nil then
        for i, v in ipairs(data.creatures) do
            if v.targettime then
                v.targettime =  v.targettime + GetTime()
            end
        end
        inst.creatures = data.creatures
    end
end

local function onsave(inst, data)
    local new = {}
    for i, v in ipairs(inst.creatures) do
        if v.targettime and v.targettime > GetTime() then --灵魂还存在
            v.targettime = v.targettime - GetTime()
            new[i] = v
        end
    end
    data.creatures = new
end

local function canuse(inst,doer)
    return doer.prefab == "monkey_king" or (doer.prefab == "yama_commissioners")
end

local function onuse(inst,doer)
    local new = {}
    for i, v in ipairs(inst.creatures) do
        if v.targettime and v.targettime > GetTime() then --灵魂还存在
            table.insert(new,v)
        end
    end
    inst.creatures = new
	local res = {
		finiteuses = inst.components.finiteuses.current,
		creatures = new,
	}
	local success,result  = pcall(json.encode,res)
	if success then
		SendModRPCToClient(CLIENT_MOD_RPC["commissioner_book_rpc"]["commissioner_book_rpc"],doer.userid,result,inst)
	end
    inst.inonbusy = true
    return true
end

local function onusebook(inst,type,key,doer)
    if inst.creatures[key] ~= nil then
        if type == 2 and inst.creatures[key].prefab then
            local new = SpawnPrefab(inst.creatures[key].prefab)
            if new then
                local theta = math.random() * 2 * PI
                local pt = inst:GetPosition()
                local radius = 4
                local offset = FindWalkableOffset(pt, theta, radius, 6, true)
                if offset ~= nil then
                    pt.x = pt.x + offset.x
                    pt.z = pt.z + offset.z
                end
                if new.Physics ~= nil then
                    new.Physics:Teleport(pt.x, 0, pt.z)
                elseif new.Transform ~= nil then
                    new.Transform:SetPosition(pt.x, 0, pt.z)
                end
                DoEffects(new)
                if new.components.myth_hassoul then
                    new.components.myth_hassoul.hassoul = false
                end
                if new.components.health then
                    new.components.health:SetPercent(0.5)
                end
            end
        elseif type == 3 then
            doer.components.hunger:DoDelta(15)
            doer.components.health:DoDelta(5)
            doer.components.sanity:DoDelta(8)
        end
        table.remove(inst.creatures,key)
        inst.components.finiteuses:Use(1)
    end
    inst.inonbusy = false
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.shadow = inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    inst.shadow:SetSize( 1.2, .75 )
    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("commissioner_book")
    inst.AnimState:SetBuild("commissioner_book")
    inst.AnimState:PlayAnimation("pen",true)

    inst:AddTag("weapon")
    inst:AddTag("commissioner_book")
    inst:AddTag("nobundling")

    inst.MYTH_USE_TYPE = "BOOK"
    inst.onusesgname = "doshortaction"
    inst.canuseininventory_myth = canuse

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.creatures = {}

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.CANE_DAMAGE)
    inst.components.weapon.attackwear = 0

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/commissioner_book.xml"

    inst:AddComponent("equippable")
    inst.components.equippable.restrictedtag = "yama_commissioners"
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    inst:AddComponent("leader")
    inst:AddComponent("book_leash")
    inst.components.book_leash:SetOnSpawnFn(OnSpawnPet)
    inst.components.book_leash:SetOnDespawnFn(OnDespawnPet)

    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = false
	inst.components.myth_use_inventory.canusescene  = false
	inst.components.myth_use_inventory:SetOnUseFn(onuse)

    inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(10)
    inst.components.finiteuses:SetUses(10)

    inst.components.finiteuses:SetOnFinished(inst.Remove)

    inst.UseSoulBook  = onusebook

    --inst._onentitydeathfn = function(src, data) OnEntityDeath(inst, data) end
    --inst:ListenForEvent("entity_death", inst._onentitydeathfn, TheWorld)

    inst.OnSave = onsave
    inst.OnLoad = onload

    inst.TakeSoul = TakeSoul
    MakeHauntable(inst)

    return inst
end

local function CanTakeSoulFly(inst)
    return inst.components.follower.leader and not inst.components.follower.leader.inonbusy
end

local function fly()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    inst.shadow = inst.entity:AddDynamicShadow()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("commissioner_book")
	inst.AnimState:SetBuild("commissioner_book")
	inst.AnimState:PlayAnimation("book", true)

    inst.shadow:SetSize( 1.2, .75 )

	inst:AddTag("flying")
	inst:AddTag("noauradamage")
	inst:AddTag("notraptrigger")
	inst:AddTag("NOBLOCK")
    inst:AddTag("nobundling")
	MakeGhostPhysics(inst, 1, .5)

    inst:AddTag("commissioner_book")

	inst.entity:SetPristine()
	if not TheWorld.ismastersim then
		return inst
	end

    inst.CanTakeSoul = CanTakeSoulFly

	inst:AddComponent("follower")
	inst:AddComponent("locomotor")
	inst.components.locomotor.runspeed = 6
	inst.components.locomotor.walkspeed = 6
	inst.components.locomotor.pathcaps = { allowocean = true }
	inst:AddComponent("inspectable")

	inst.fn_cangetsoul = function(inst, soultype)
        return true
	end
	inst.fn_getsoul = function(inst, soultype, count,soul) --soultype=soul_ghast、soul_specter
        if soul and soul.sssbinfo and inst.components.follower.leader and inst.components.follower.leader.TakeSoul then
            inst.components.follower.leader:TakeSoul(soul.sssbinfo)
        end
	end

	inst:SetStateGraph("SGcommissioner_book")
	inst:SetBrain(require("brains/commissioner_bookbrain"))
	return inst
end

local function ConDropEverythingWithTag(tag, drop_pos)
    local containers = {}
    for i = 1, self.numslots do
        local item = self.slots[i]
        if item ~= nil then
            if item:HasTag(tag) or v.components.leader then
                self:DropItemBySlot(i, drop_pos)
            elseif item.components.container ~= nil then
                table.insert(containers, item)
            end
        end
    end

    for i, v in ipairs(containers) do
        ConDropEverythingWithTag(v.components.container,tag, drop_pos)
        --v.components.container:DropEverythingWithTag(tag, drop_pos)
    end
end

local function inventoryDropEverythingWithTag(self,tag)
    local containers = {}

    if self.activeitem ~= nil then
        if self.activeitem:HasTag(tag) or self.activeitem.components.leader  then
            self:DropItem(self.activeitem)
            self:SetActiveItem(nil)
        elseif self.activeitem.components.container ~= nil then
            table.insert(containers, self.activeitem)
        end
    end

    for k = 1, self.maxslots do
        local v = self.itemslots[k]
        if v ~= nil then
            if v:HasTag(tag) or v.components.leader then
                self:DropItem(v, true, true)
            elseif v.components.container ~= nil then
                table.insert(containers, v)
            end
        end
    end

    for k, v in pairs(self.equipslots) do
        if v:HasTag(tag) or v.components.leader then
            self:DropItem(v, true, true)
        elseif v.components.container ~= nil then
            table.insert(containers, v)
        end
    end

    for i, v in ipairs(containers) do
        ConDropEverythingWithTag(v.components.container,tag)
    end
end

local function onuse(inst,doer)
    if doer and doer:HasTag("player") then
        doer.mengpo_world =  
        {
            pos = doer:GetPosition(),
            world = TheShard:GetShardId(),
            recipes = doer.components.builder:OnSave(),
            reroll = doer.SaveForReroll ~= nil and doer:SaveForReroll() or nil,
        }

        inst:Remove()
        RemoveWithTag(doer.components.inventory,"myth_removebydespwn") --移除神话特殊物品
        inventoryDropEverythingWithTag(doer.components.inventory,"irreplaceable")
        --doer.components.inventory:DropEverythingWithTag("irreplaceable") --掉落特殊标签的
        doer.components.inventory:DropEquipped() --掉落装备
        local plant = SpawnPrefab("myth_higanbana_reborn")
        plant.Transform:SetPosition(doer.Transform:GetWorldPosition())

        plant.save_player = {
            id = doer.userid,
            inventory = doer.components.inventory:OnSave(), --保存数据
        }
        for k,v in pairs(doer.components.inventory.itemslots) do
            if v then
                v:Remove()
            end
        end
        doer:DoTaskInTime(0,function()
            TheWorld:PushEvent("ms_playerdespawnanddelete", doer)
        end)
        return true
    end
end

local  function mpt()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("commissioner_tang")
	inst.AnimState:SetBuild("commissioner_tang")
	inst.AnimState:PlayAnimation("idle",true)

    MakeInventoryPhysics(inst)
    
    --inst.myth_use_needtag = "wb_monster"
    inst.MYTH_USE_TYPE = "DRINK"
    inst.onusesgname = "mk_drink"
    inst.mk_drink_symbol = "tang"

	inst.entity:SetPristine()
	if not TheWorld.ismastersim then
		return inst
	end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/commissioner_mpt.xml"

    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = true
	inst.components.myth_use_inventory.canusescene  = false
	inst.components.myth_use_inventory:SetOnUseFn(onuse)

    MakeHauntable(inst)

    return inst
end

local  function light()
	local inst = CreateEntity()
	inst.entity:AddTransform()
    inst.entity:AddLight()
	inst.entity:AddNetwork()

    inst.Light:SetRadius(.6)
    inst.Light:SetFalloff(1)
    inst.Light:SetIntensity(.5)
    inst.Light:SetColour(0/255, 185/255, 138/255)
    inst.Light:Enable(true)
    inst:AddTag("fx")

    inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

    inst:DoTaskInTime(1,inst.Remove)
    
    return inst
end

return Prefab("commissioner_book", fn, assets),
    Prefab("commissioner_booklight", light),
    Prefab("commissioner_flybook", fly),
    Prefab("commissioner_mpt", mpt)