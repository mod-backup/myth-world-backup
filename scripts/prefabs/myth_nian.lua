local prefabs_basic =
{
}

local easing = require("easing")

local brain = require("brains/myth_nianbrain")

local sounds =
{
    idle = "",
    howl = "Myth_nian/sfx/myth_nian",
    hit = "",
    attack = "Myth_nian/sfx/myth_nian",
    death = "",
    sleep = "",
}

SetSharedLootTable('myth_nian_loot',
{
    {'nian_bell',             1.00},

    {'myth_nian_fur',       1.00},
    {'myth_nian_fur',       1.00},
    {'myth_nian_fur',       1.00},
    {'myth_nian_fur',       1.00},

    {'myth_nian_fur',       1.00},
    {'myth_nian_fur',       1.00},
    {'myth_nian_fur',       1.00},
    {'myth_nian_fur',       1.00},

    {'lucky_goldnugget',       1.00},
    {'lucky_goldnugget',       1.00},
    {'lucky_goldnugget',       1.00},
    {'lucky_goldnugget',       1.00},
    {'lucky_goldnugget',       0.50},
    {'lucky_goldnugget',       0.50},
    {'lucky_goldnugget',       0.50},

    {'redgem',           1.00},
    {'bluegem',          1.00},
    {'purplegem',        1.00},
    {'orangegem',        1.00},
    {'yellowgem',        1.00},
    {'greengem',         1.00},

    {'redgem',           1.00},
    {'bluegem',          1.00},
    {'purplegem',        0.50},
    {'orangegem',        0.50},
    {'yellowgem',        0.50},
    {'greengem',         0.50},
})


-----==================仇恨与目标
local RETARGET_MUST_TAGS = { "_health","_combat" }
local RETARGET_CANT_TAGS = { "wall", "nian", }
local function RetargetFn(inst)
    return not (inst.sg:HasStateTag("hidden") or inst.sg:HasStateTag("statue"))
        and FindEntity(
                inst,
                TUNING.WARG_TARGETRANGE,
                function(guy)
                    return inst.components.combat:CanTarget(guy)
                end,
                RETARGET_MUST_TAGS,
                RETARGET_CANT_TAGS
            )
        or nil
end

local function KeepTargetFn(inst, target)
    return target ~= nil
        and not (inst.sg:HasStateTag("hidden") or inst.sg:HasStateTag("statue"))
        and inst:IsNear(target, 40)
        and inst.components.combat:CanTarget(target)
        and not target.components.health:IsDead()
end

local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
end

local function LaunchGooIcing(inst)
	local theta = math.random() * 2 * PI
	local r = inst:GetPhysicsRadius(0) + 0.25 + math.sqrt(math.random()) * TUNING.WARG_GINGERBREAD_GOO_DIST_VAR
	local x, y, z = inst.Transform:GetWorldPosition()
	local dest_x, dest_z = math.cos(theta) * r + x, math.sin(theta) * r + z

	local goo = SpawnPrefab("nian_projectile")
    goo.Transform:SetPosition(x, y, z)
	goo.Transform:SetRotation(theta / DEGREES)
    goo.components.entitytracker:TrackEntity("myth_nian", inst)
	Launch2(goo, inst, 1.5, 1.3, 3, 3)
end

local function OnHealthTrigger(inst)
    if not inst.already_sleepshield then
        inst:PushEvent("trytoshield")
    end
end

local function onload(inst, data)
    if data ~= nil and data.already_sleepshield ~= nil then
        inst.already_sleepshield = data.already_sleepshield
    end
end

local function onsave(inst, data)
    data.already_sleepshield = inst.already_sleepshield
end

local function dohowl(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    for i, v in ipairs(TheSim:FindEntities(x, y, z, 8, {"player"})) do
        if v:IsValid() and v.entity:IsVisible() and not (v.components.health ~= nil and v.components.health:IsDead()) then
            v:PushEvent("nian_dohowl")
        end
    end
    for i,v in ipairs(inst.nian_clouds) do
        if v then
            v:DoTaskInTime(math.random(),function(bom)
                if bom and bom:IsValid() and not bom.dispersed then
                    bom:DoDmage()
                    bom:DestroyByFan()
                end
            end)
        end
    end
    inst.nian_clouds = {}
end


local function flyfly(inst)
    for i,v in ipairs(inst.nian_clouds) do
        if v and v:IsValid() and not v.dispersed then
            v:DestroyByFan()
        end
    end

    local newbeef = SpawnPrefab("nian_fly")
    newbeef.Transform:SetPosition(inst.Transform:GetWorldPosition())
    newbeef.Transform:SetScale(1, 1, 1)
    newbeef:FacePoint(inst.Transform:GetWorldPosition())
    newbeef:GoAway()
    inst:Remove()
end
local function OnDeath(inst, data)
    if not inst.dead_already then
        inst.dead_already = true
        inst.persists = false
        inst:AddTag("NOCLICK")
        TheWorld:PushEvent("myth_nian_event",{type = "killed"})
        local loot = SpawnPrefab("myth_nian_loot")
        loot.Transform:SetPosition(inst.Transform:GetWorldPosition())
        loot.components.lootdropper:DropLoot(Vector3(loot.Transform:GetWorldPosition()))
        if TheWorld.state.season == "winter" then
            for i,v in ipairs(AllPlayers) do
                if v and v.components.myth_playernwd then
                    v.components.myth_playernwd:DoDelta("nian_nwd",50)
                end
            end
        end
        flyfly(inst)
    end
end

local function OnHitOther(inst, data)
    if data.target ~= nil and data.target:IsValid() and data.target:HasTag("player") then
        data.target:PushEvent("knockback", { knocker = inst, radius = 4})
    end
end

--TheWorld:PushEvent("myth_nian_event",{type = "killed"}) --leave  come  killed

local function OnSeasonChanged(inst, season)
    if not (inst.components.health:IsDead()) and season == SEASONS.SPRING then
        flyfly(inst)
        TheWorld:PushEvent("myth_nian_event",{type = "leave"})
    end
end

local function MakeWarg(name, bank, build, prefabs, tag)
    local assets =
    {
        Asset("SOUND", "sound/vargr.fsb"),
        Asset("ANIM", "anim/warg_actions.zip"),
        Asset("ANIM", "anim/myth_nian_actions.zip"),
        Asset("ANIM", "anim/myth_nian_facebase.zip"),
        Asset("ATLAS", "images/inventoryimages/myth_nian_fur.xml")
    }
    table.insert(assets, Asset("ANIM", "anim/"..build..".zip"))

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddDynamicShadow()
        inst.entity:AddNetwork()

        inst.DynamicShadow:SetSize(2.5, 1.5)

        inst.Transform:SetSixFaced()

        MakeCharacterPhysics(inst, 1000, 1)

        inst.Physics:ClearCollisionMask()
        inst.Physics:CollidesWith(COLLISION.WORLD)
        inst.Physics:CollidesWith(COLLISION.CHARACTERS)
        inst.Physics:CollidesWith(COLLISION.GIANTS)

        inst:AddTag("monster")
        inst:AddTag("nian")
        inst:AddTag("myth_nian")
        inst:AddTag("scarytoprey")
        inst:AddTag("largecreature")
        inst:AddTag("epic")
        inst:AddTag("canbestartled")

        if tag ~= nil then
            inst:AddTag(tag)
        end
        inst.AnimState:SetBank(bank)
        inst.AnimState:SetBuild(build)
        inst.AnimState:PlayAnimation("idle_loop", true)

        inst.AnimState:HideSymbol("warg_eye")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
        inst.nian_clouds = {}

        inst.already_sleepshield = false

        inst:AddComponent("inspectable")

        inst:AddComponent("leader")

        inst:AddComponent("locomotor")
        inst.components.locomotor.runspeed =  TUNING.MYTH_NIAN_RUNSPEED
        inst.components.locomotor:SetShouldRun(true)

        inst:AddComponent("combat")
        inst.components.combat:SetDefaultDamage(TUNING.MYTH_NIAN_DAMAGE)
        inst.components.combat.playerdamagepercent = TUNING.MYTH_NIAN_DAMAGE_PLAYER_PERCENT
        inst.components.combat:SetRange(TUNING.MYTH_NIAN_DAMAGE_RANGE,TUNING.MYTH_NIAN_DAMAGE_RANGE)
        --inst.components.combat:SetAttackPeriod(TUNING.WARG_ATTACKPERIOD)
        inst.components.combat:SetAreaDamage(3, 1)
        inst.components.combat:SetRetargetFunction(1, RetargetFn)
        inst.components.combat:SetKeepTargetFunction(KeepTargetFn)
        inst:ListenForEvent("attacked", OnAttacked)

        inst:AddComponent("health")
        inst.components.health.fire_damage_scale = 3
        inst.components.health:SetMaxHealth(TUNING.MYTH_NIAN_HEALTH)

        inst:AddComponent("lootdropper")
        --inst.components.lootdropper:SetChanceLootTable(name)

        inst:AddComponent("healthtrigger")
        inst.components.healthtrigger:AddTrigger(0.5, OnHealthTrigger)

        inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/vargr/hit")

        inst:AddComponent("sleeper")
        local old = inst.components.sleeper.AddSleepiness
        inst.components.sleeper.AddSleepiness = function(self,...)
            if inst.hasshield then
                return
            end
            old(self,...)
        end

        inst:AddComponent("timer")

        inst.LaunchGooIcing = LaunchGooIcing--function() end

        inst.sounds = sounds

        MakeLargeBurnableCharacter(inst,"swap_fire")

        MakeLargeFreezableCharacter(inst)

        inst:SetStateGraph("SGmyth_nian")

        MakeHauntableGoToState(inst, "howl", TUNING.HAUNT_CHANCE_OCCASIONAL, TUNING.HAUNT_COOLDOWN_MEDIUM, TUNING.HAUNT_CHANCE_LARGE)

        inst.components.timer:StartTimer("bomb_cd", TUNING.MYTH_NIAN_BOMBCD)
        inst.components.timer:StartTimer("bombboom", TUNING.MYTH_NIAN_BOOMCD)
        inst.components.timer:StartTimer("charge_cd", TUNING.MYTH_NIAN_CHARGECD)

        inst.mushroombomb_prefab = "nian_projectile"
        inst.DoHowl = dohowl

        inst:ListenForEvent("death", OnDeath)
        inst:ListenForEvent("onhitother", OnHitOther)
        inst:WatchWorldState("season", OnSeasonChanged)
        OnSeasonChanged(inst,TheWorld.state.season)

        inst:SetBrain(brain)

        inst.OnSave = onsave
        inst.OnLoad = onload

        return inst
    end

    return Prefab(name, fn, assets, prefabs)
end

local function dropfn(inst)
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst:AddTag("CLASSIFIED")

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("myth_nian_loot")

    inst.persists = false
    inst:DoTaskInTime(0.2, inst.Remove)
    return inst
end

local function furfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("myth_nianhat")
    inst.AnimState:SetBuild("myth_nianhat")
    inst.AnimState:PlayAnimation("item")

    MakeInventoryFloatable(inst, "med", nil, 0.77)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/myth_nian_fur.xml"

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_LARGEITEM

    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("myth_nian_loot",dropfn),
    Prefab("myth_nian_fur",furfn), 
     MakeWarg("myth_nian", "warg", "myth_nian", prefabs_basic)
