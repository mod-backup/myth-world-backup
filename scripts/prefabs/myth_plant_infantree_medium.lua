
local assets =
{
    Asset("ANIM", "anim/myth_plant_infantree_medium.zip"),
    Asset("ANIM", "anim/myth_plant_infantree_medium_actions.zip"),
}

local prefabs =
{
    "collapse_small",
}

local function IsPosWithin(x, z, positions, dist)
    dist = dist * dist
    for i, v in ipairs(positions) do
        local distance = VecUtil_DistSq(x, z, v.x, v.z)
        if distance < dist then
            return true
        end
    end
    return false
end

local fxpos = {
    {-180,-400,0},
    {200,-380,0},
    {0,-350,0},
    {0,-650,0},
}
local function ongrowfn(inst)
    SpawnPrefab("halloween_moonpuff").Transform:SetPosition(inst.Transform:GetWorldPosition())
    inst:DoTaskInTime(0.5,function()
        for i,v in ipairs(fxpos) do
            inst:DoTaskInTime(math.random(),function()
                local fx = SpawnPrefab("crab_king_shine")
                fx.entity:AddFollower()
                fx.Follower:FollowSymbol(inst.GUID, "unique_01", v[1], v[2], 0)
            end)
        end
    end)
    local x, y, z = inst.Transform:GetWorldPosition()
    local world = TheWorld
    local map = world.Map
	for k1 = -4,4,4 do
		for k2 = -4,4,4 do
            if TheWorld.Map:CanPlowAtPoint(x+k1, 0, z+k2) then
                local original_tile_type = map:GetTileAtPoint(x+k1, 0, z+k2)
                local x, y = map:GetTileCoordsAtPoint(x+k1, 0, z+k2)
                local below_soil_turf
                if original_tile_type == GROUND.FARMING_SOIL and TheWorld.components.farming_manager then
                    below_soil_turf = TheWorld.components.farming_manager:GetTileBelowSoil(x, y)
                end
                local turf = GROUND.FARMING_SOIL or below_soil_turf or GROUND.DIRT
                map:SetTile(x, y, turf)
                map:RebuildLayer(original_tile_type, x, y)
                map:RebuildLayer(turf, x, y)
                world.minimap.MiniMap:RebuildLayer(original_tile_type, x, y)
                world.minimap.MiniMap:RebuildLayer(turf, x, y)
                --[[if k1 ~= 0 and k2 ~= 0  then
                    local cx, cy, cz = map:GetTileCenterPoint(x+k1, 0, z+k2)
                    local TILE_EXTENTS = TILE_SCALE * 0.9
                    local spawned_positions = {}
                    for i = 1, math.random(TUNING.FARM_PLOW_DRILLING_DEBRIS_MIN, TUNING.FARM_PLOW_DRILLING_DEBRIS_MAX) do
                        local x = cx + (math.random() * TILE_EXTENTS) - TILE_EXTENTS/2
                        local z = cz + (math.random() * TILE_EXTENTS) - TILE_EXTENTS/2
                        if IsPosWithin(x, z, spawned_positions, 1) then
                            table.insert(spawned_positions, {x = x, z = z})
                            map:CollapseSoilAtPoint(x, cy, z)
                            SpawnPrefab("farm_soil_debris").Transform:SetPosition(x, cy, z)
                        end
                    end
                end]]
                world.components.farming_manager:AddTileNutrients(x, y, -100, -100, -100)
            end
		end
	end
end

local function ontreefn(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local world = TheWorld
    local map = world.Map
	for k1 = -4,4,4 do
		for k2 = -4,4,4 do
            local original_tile_type = map:GetTileAtPoint(x+k1, 0, z+k2)
            local x, y = map:GetTileCoordsAtPoint(x+k1, 0, z+k2)
            if original_tile_type == GROUND.FARMING_SOIL then
                local turf =  GROUND.DIRT
                map:SetTile(x, y, turf)
                map:RebuildLayer(original_tile_type, x, y)
                map:RebuildLayer(turf, x, y)
                world.minimap.MiniMap:RebuildLayer(original_tile_type, x, y)
                world.minimap.MiniMap:RebuildLayer(turf, x, y)
            end
		end
	end
    --inst.AnimState:PlayAnimation("mediumtotall")
    --inst:ListenForEvent("animover",function()
    --    local new = ReplacePrefab(inst, "myth_plant_infantree")
    --end)
    SpawnPrefab("halloween_moonpuff").Transform:SetPosition(inst.Transform:GetWorldPosition())
    local new = ReplacePrefab(inst, "myth_plant_infantree")
    --new.AnimState:PlayAnimation("plant")
    --new.AnimState:PushAnimation("idle",false)        
end

local function onburnt(inst)
    local new = ReplacePrefab(inst, "myth_well")
    new:SetDried()
end

local function canuseinscene_myth(inst,doer,right)
    return inst:HasTag("canbe_takecare")
end

local function onuse(inst,doer)
	inst:DoTaskInTime(0.5 + math.random() * 0.5, function()
		local fx = SpawnPrefab("farm_plant_happy")
		fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
	end)
    inst.components.infantree_grower:OnTakecare()
    return true
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    inst.MiniMapEntity:SetIcon("myth_well.tex")

	MakeObstaclePhysics(inst, 1.5)

    inst:AddTag("antlion_sinkhole_blocker")
    inst:AddTag("birdblocker")
    inst:AddTag("shelter")
    inst:AddTag("essense_target")

    inst.AnimState:SetBank("myth_plant_infantree_medium")
    inst.AnimState:SetBuild("myth_plant_infantree_medium")
    inst.AnimState:PlayAnimation("idle",true)
    
    inst.canuseinscene_myth = canuseinscene_myth
    inst.MYTH_USE_TYPE = "TAKECARE"
    inst.onusesgname = "dolongaction"

    MakeSnowCoveredPristine(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    inst.dried = false

    inst:AddComponent("inspectable")

    inst:AddComponent("lootdropper")

    inst:AddComponent("myth_use_inventory")
    inst.components.myth_use_inventory.canuse = false
	inst.components.myth_use_inventory.canusescene  = false
	inst.components.myth_use_inventory:SetOnUseFn(onuse)

    inst.OnGrowFn = ongrowfn
    inst.OnTreeFn = ontreefn

    MakeSnowCovered(inst)

    MakeMediumBurnable(inst)
    MakeSmallPropagator(inst)

    inst.components.burnable.onburnt = onburnt

    inst:AddComponent("infantree_grower")

    MakeHauntable(inst)
    return inst
end

--TheInput:GetWorldEntityUnderMouse():OnTreeFn()

return Prefab("myth_plant_infantree_medium", fn, assets, prefabs)
