require "behaviours/wander"
require "behaviours/faceentity"
require "behaviours/chaseandattack"
require "behaviours/panic"
require "behaviours/follow"
require "behaviours/attackwall"
require "behaviours/nian_eat"
local MIN_FOLLOW_DIST = 1
local MAX_FOLLOW_DIST = 12
local TARGET_FOLLOW_DIST = 6

local function GetFaceTargetFn(inst)
    return inst.components.com.leader
end

local function KeepFaceTargetFn(inst, target)
    return inst.components.follower.leader == target
end

local BeefaloBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local function TryChargeAttack(inst)
    if not inst.components.timer:TimerExists("charge_cd") then
        local target = inst.components.combat.target
        if target ~= nil then
            local dsq_to_target = inst:GetDistanceSqToInst(target)
            if dsq_to_target > TUNING.EYEOFTERROR_CHARGEMINDSQ and dsq_to_target < TUNING.EYEOFTERROR_CHARGEMAXDSQ then
                return true
            end
        end
    end
    return false
end

function BeefaloBrain:OnStart()
    local root = PriorityNode(
    {
        --WhileNode(function() return self.inst.components.hauntable ~= nil and self.inst.components.hauntable.panic end, "PanicHaunted", Panic(self.inst)),
        --WhileNode(function() return self.inst.components.health.takingfiredamage end, "OnFire",
        --    Panic(self.inst)),
        Nian_Eat(self.inst),
        IfNode(function() return TryChargeAttack(self.inst) end, "Charge",
            ActionNode(function() self.inst:PushEvent("charge") end)),
        ChaseAndAttack(self.inst,10),
        --FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn), 
        StandStill(self.inst),  
    }, .25)

    self.bt = BT(self.inst, root)
end

return BeefaloBrain
