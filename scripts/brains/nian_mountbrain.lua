require "behaviours/wander"
require "behaviours/faceentity"
require "behaviours/chaseandattack"
require "behaviours/panic"
require "behaviours/follow"
require "behaviours/attackwall"
require "behaviours/nian_eat"
local MIN_FOLLOW_DIST = 1
local MAX_FOLLOW_DIST = 12
local TARGET_FOLLOW_DIST = 6

local function GetFaceTargetFn(inst)
    return inst.components.follower.leader
end

local function KeepFaceTargetFn(inst, target)
    return inst.components.follower.leader == target
end

local BeefaloBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

function BeefaloBrain:OnStart()
    local root = PriorityNode(
    {
        WhileNode(function() return self.inst.components.hauntable ~= nil and self.inst.components.hauntable.panic end, "PanicHaunted", Panic(self.inst)),
        WhileNode(function() return self.inst.components.health.takingfiredamage end, "OnFire",
            Panic(self.inst)),
  
        Follow(self.inst, function() return self.inst.components.follower.leader end, --跟随
            MIN_FOLLOW_DIST, 4, 8, true),
        Nian_Eat(self.inst),
        ChaseAndAttack(self.inst, 0),
        FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn),   
    }, .25)

    self.bt = BT(self.inst, root)
end

return BeefaloBrain
