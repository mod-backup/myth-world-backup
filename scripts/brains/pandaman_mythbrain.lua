local pandaman_myth = Class(Brain, function(self, inst)
	Brain._ctor(self, inst)
end)

local function HasValidHome(inst)
	if inst.components.homeseeker:HasHome() then
		return true
	end
	local home = FindEntity(inst, 32, nil, {"panda_home"})
	if home and home:IsValid() then
		inst.components.homeseeker:SetHome(home)
		return true
	end
    return false
end

local function GoHomeAction(inst)
	if HasValidHome(inst) then
        return BufferedAction(inst, inst.components.homeseeker.home, ACTIONS.GOHOME)
    end
end

function pandaman_myth:OnStart()
	local root = PriorityNode(
	{
		WhileNode( function() return not TheWorld.state.isnight end, "GOGOGO",
		DoAction(self.inst, GoHomeAction, "go home", true )),
		Wander(self.inst, function() return self.inst.components.knownlocations:GetLocation("pandaman") end, 12)
	}, 0.25)
	self.bt = BT(self.inst, root)
end

return pandaman_myth