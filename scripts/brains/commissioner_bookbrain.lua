local commissioner_book = Class(Brain, function(self, inst)
	Brain._ctor(self, inst)
end)

function commissioner_book:OnStart()
	local root = PriorityNode(
	{
		--------------------------------------------------------------------------------------
		Follow(self.inst, function()
			return self.inst.components.follower.leader
		end, 0, 4, 8),
		--------------------------------------------------------------------------------------
	}, 0.25)
	self.bt = BT(self.inst, root)
end

return commissioner_book