local Widget = require "widgets/widget"
local Image = require "widgets/image"

local FlareOver = Class(Widget, function(self, owner)
    self.owner = owner
    Widget._ctor(self, "FlareOver")

    self._hide_task = nil
    self._alpha = 0.0
    self._alpha_target = 0.0
    self._alpha_speed = 0.5 -- rate of change from alpha=1 to alpha=0

    self:SetClickable(false)

    self.bg = self:AddChild(Image("images/fx4.xml", "flare_over.tex"))
    self.bg:SetVRegPoint(ANCHOR_TOP)
    self.bg:SetHRegPoint(ANCHOR_MIDDLE)
    self.bg:SetVAnchor(ANCHOR_TOP)
    self.bg:SetHAnchor(ANCHOR_MIDDLE)
    self.bg:SetScaleMode(SCALEMODE_FIXEDPROPORTIONAL)

    self:Hide()

    self.inst:ListenForEvent("startflareoverlay_myth", function(o) self:StartFlare() end, owner)
end)

local colours = {
    {255/255,69/255,69/255,1},
    {255/255,128/255,29/255,1},
    {255/255,129/255,11/255,1},
    {255/255,11/255,101/255,1},
    {103/255,193/255,56/255,1},
}

function FlareOver:SetColour()
    self.bg:SetTint(unpack(colours[math.random(#colours)]))
end

function FlareOver:StartFlare()
    self:Show()
    self._alpha = 1.0
    self:SetColour()
    self:StartUpdating()
end

function FlareOver:OnUpdate(dt)
    local delta = dt * self._alpha_speed
    self._alpha = (1 - delta) * self._alpha

    self.bg:SetFadeAlpha((self._alpha > 0.5 and 1) or self._alpha / 0.5)
    if self._alpha <= 0.01 then
        self:Hide()
        self:StopUpdating()
    else
        self:Show()
    end
end

return FlareOver
