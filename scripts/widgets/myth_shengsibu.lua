local Widget = require "widgets/widget"
local UIAnim = require "widgets/uianim"
local Text = require "widgets/text"
local Spinner = require "widgets/spinner"
local ImageButton = require "widgets/imagebutton"
local Menu = require "widgets/menu"
local LoadoutSelect = require "widgets/redux/loadoutselect"
local TEMPLATES = require "widgets/redux/templates"
local Puppet = require "widgets/skinspuppet"
local ClothingExplorerPanel = require "widgets/redux/clothingexplorerpanel"
local Subscreener = require "screens/redux/subscreener"
local SkinPresetsPopup = require "screens/redux/skinpresetspopup"
local AccountItemFrame = require "widgets/redux/accountitemframe"
local UIAnimButton = require "widgets/uianimbutton"
local ItemImage = require "widgets/itemimage"
local TextButton = require "widgets/textbutton"
local Button = require "widgets/button"

local function MakeDetailsLine(details_root, x, y, scale1, scale2)
	local value_title_line = details_root:AddChild(Image("images/myth_bookinfo_bg.xml", "line_long.tex"))
	value_title_line:SetScale(scale1, scale2)
	value_title_line:SetPosition(x, y)
end

local Myth_Ssbui =Class(Widget,function(self, owner)
    Widget._ctor(self, "Myth_Ssbui")
    self.owner = owner
	self.back = self:AddChild(ImageButton("images/global.xml", "square.tex"))
    self.back.image:SetVAnchor(ANCHOR_MIDDLE)
	self.back.image:SetVRegPoint(ANCHOR_MIDDLE)
	self.back.image:SetHRegPoint(ANCHOR_MIDDLE)
	self.back.image:SetVAnchor(ANCHOR_MIDDLE)
	self.back.image:SetHAnchor(ANCHOR_MIDDLE)
	self.back.image:SetScaleMode(SCALEMODE_FILLSCREEN)
	self.back.image:SetTint(0,0,0,0.2)
	self.back:MoveToBack()
	self.back:SetOnClick(function() self:OnClose() end)

	self.root = self:AddChild(Widget("ROOT"))
    self.root:SetVAnchor(ANCHOR_MIDDLE)
    self.root:SetHAnchor(ANCHOR_MIDDLE)
    --之后在优化好了
	--self.root:SetScaleMode(SCALEMODE_PROPORTIONAL)
    --self.root:SetScale(0.5)

    self.bg = self.root:AddChild(Image("images/myth_ssb_back.xml", "myth_ssb_back.tex"))
    --self.bg:SetScaleMode(SCALEMODE_FILLSCREEN)
    MakeDetailsLine(self.bg, 0, 270, 2.4,1)
    MakeDetailsLine(self.bg, 0, -270, 2.4,1)
    --self:SetScale(0.5)

    self.lines = {}
    for k = 1,16 do 
        self.lines[k] = self.bg:AddChild(Image("images/myth_ssb_line.xml", "myth_ssb_line.tex"))
        self.lines[k]:SetPosition( 480 - (k- 1)*63, 0 )
    end
    self.yinyangyu = {}
    self.creature = {}

    self.showtext = self:AddChild(Text(UIFONT, 30),"")
    self.showtext:SetPosition(0,0,0)
    self:Hide()
    self:FollowMouseConstrained()
 
    self.inst:ListenForEvent("open_commissioner_book",function(doer,data)
        self:DoUpdate(data)
    end,self.owner)
    --self:DoUpdate()
end)

local function getname(str)
    local t = {}
    for i=1, str:utf8len() do
        table.insert(t, ( str:utf8sub(i,i) ) )
    end
    return table.concat(t,"\n")
end

local function NewControl(self,control, down)
    if Button._base.OnControl(self, control, down) then return true end
    if not self:IsEnabled() or not self.focus then return false end
    if self:IsSelected() and not self.AllowOnControlWhenSelected then return false end
    if (control == CONTROL_ACCEPT or control == CONTROL_SECONDARY) and (not self.mouseonly or TheFrontEnd.isprimary) then
        if down then
            if not self.down then
                TheFrontEnd:GetSound():PlaySound("dontstarve/HUD/click_move")
                self.o_pos = self:GetLocalPosition()
                self:SetPosition(self.o_pos + self.clickoffset)
                self.down = true
                if self.whiledown then
                    self:StartUpdating()
                end
                if self.ondown then
                    self.ondown()
                end
            end
        else
            if self.down then
                self.down = false
                self:ResetPreClickPosition()
                if control == CONTROL_ACCEPT then
                    if self.onclick then
                        self.onclick(false)
                    end	
                elseif control == CONTROL_SECONDARY and (ThePlayer and ThePlayer.prefab == "yama_commissioners") then
                    if self.onclick then
                        self.onclick(true)
                    end
                end
                self:StopUpdating()
            end
        end
        return true
    end
end

local function getclickstr(self,isplayer)
    return (isplayer or self.owner.prefab == "monkey_king") and STRINGS.LMB..": 还魂"  or STRINGS.LMB..": 还魂\n"..STRINGS.RMB..": 轮回"
end
local function addbihua(self,colour,root)
    if root.yinyangyu[#root.yinyangyu] ~= nil then
        local anim = root.yinyangyu[#root.yinyangyu].yinyang  or "yin"
        root.yinyangyu[#root.yinyangyu]:GetAnimState():PlayAnimation(anim.."_broke")
        root.inst:DoTaskInTime(root.yinyangyu[#root.yinyangyu]:GetAnimState():GetCurrentAnimationLength(),function(inst)
            root.yinyangyu[#root.yinyangyu]:Kill()
            root.yinyangyu[#root.yinyangyu] = nil
        end)
    else
        return
    end
    local child = self:AddChild(UIAnim())
    child:GetAnimState():SetBuild("myth_penspell")
    child:GetAnimState():SetBank("myth_penspell")
    child:GetAnimState():PlayAnimation("write_"..colour)
    child:GetAnimState():PushAnimation("idle_"..colour)
    child:SetPosition(0,-60)
end

function Myth_Ssbui:DoUpdate(data)
    local data = data
    for k, v in ipairs(self.yinyangyu) do
        if v then 
            v:Kill()
        end
    end
    for k, v in ipairs(self.creature) do
        if v then 
            v:Kill()
        end
    end
    if not data then
        return 
    end
    self.isbusy = false
    if data.book then
        self.book = data.book
    end
    if data.finiteuses then
        local yin = false
        for k= 1, data.finiteuses do
            local anim = yin and "yin" or "yang"
            self.yinyangyu[k] = self.bg:AddChild(UIAnim())
            self.yinyangyu[k]:GetAnimState():SetBuild("myth_yinyangyu")
            self.yinyangyu[k]:GetAnimState():SetBank("myth_yinyangyu")
            self.yinyangyu[k]:GetAnimState():PlayAnimation(anim)
            self.yinyangyu[k]:SetPosition( (yin and -500 or -430) + (k- 1)*65, 334 )
            self.yinyangyu[k].yinyang = anim
            yin = not yin
        end
    end

    local ClientObjs = TheNet:GetClientTable() or {}
    for i,v in ipairs(ClientObjs) do
        if v.name  and v.userid and v.prefab and v.userflags ~= 0 then
            table.insert(data.creatures,1,{prefab = v.prefab, name = v.name,isplayer = true, userid = v.userid})
        end
    end

    if data.creatures then
        local num = 1
        for i, v in ipairs(data.creatures) do
            self.creature[i] = self.bg:AddChild(TextButton())
            self.creature[i]:SetPosition( 450 - (i- 1)*63, 60 )
            self.creature[i]:SetText(getname(v.name))
            self.creature[i]:SetTextSize(60)
            self.creature[i]:SetFont(HEADERFONT)
            self.creature[i]:SetTextColour(0, 0, 0, 1)
            self.creature[i]:SetTextFocusColour(0.9,0.8,0.6,1)
            self.creature[i].player = self.owner
            self.creature[i].OnControl = NewControl
            if not v.isplayer then
                self.creature[i].listnum = num
                num = num + 1
            end
			self.creature[i]:SetOnClick(function(right)
                if self.isbusy then
                    return
                end
				if right then
                    if v.isplayer then
                        return
                    elseif self.owner ~= "monkey_king" then
                        SendModRPCToServer( MOD_RPC["commissioner_book"]["commissioner_book"],self.book,3,self.creature[i].listnum)
                        addbihua(self.creature[i],"red",self)
                        self.isbusy = true
                        self.inst:DoTaskInTime(0.4,function()
                            self:OnClose()
                        end)
                    end
				else
                    if v.isplayer then
                        SendModRPCToServer( MOD_RPC["commissioner_book"]["commissioner_book"],self.book,1,v.userid)
                    else
                        --转身
                        SendModRPCToServer( MOD_RPC["commissioner_book"]["commissioner_book"],self.book,2,self.creature[i].listnum)
                    end
                    self.isbusy = true
                    self.inst:DoTaskInTime(0.4,function()
                        self:OnClose()
                    end)
					addbihua(self.creature[i],"black",self)
				end
			end)
            self.creature[i].showtext =  self.creature[i]:AddChild(Text(UIFONT, 45,getclickstr(self,v.isplayer)))
            self.creature[i].showtext:SetPosition(0,-60)
            self.creature[i].showtext:Hide()
            self.creature[i].showtext:SetColour({ 0.9,0.8,0.6,1 })
            self.creature[i].showtext:SetClickable(false)

            self.creature[i].ongainfocus = function(ongain)
                if ongain then
                    self.showtext_fuqin = self.creature[i]
                    self.showtext = self.creature[i].showtext
                    self.creature[i].showtext.inst.showtask = self.creature[i].showtext.inst:DoTaskInTime(0.2,function()
                        self.creature[i].showtext:Show()
                        self.creature[i].showtext.inst.showtask = nil
                    end)
                end
            end

            self.creature[i].onlosefocus = function(losegain)
                if losegain then
                    if self.creature[i].showtext.inst.showtask then
                        self.creature[i].showtext.inst.showtask:Cancel()
                        self.creature[i].showtext.inst.showtask = nil
                    end
                    self.creature[i].showtext:Hide()
                    self.showtext_fuqin = nil
                    self.showtext = nil
                end
            end
        end
    end
    self:Show()
end

function Myth_Ssbui:OnClose()
    if self.book then
        SendModRPCToServer( MOD_RPC["commissioner_book"]["commissioner_book"],self.book,4)
        self.book = nil
    end
    self:Hide()
end

function Myth_Ssbui:FollowMouseConstrained()
    if self.followhandler == nil then
        self.followhandler = TheInput:AddMoveHandler(function(x, y) 
            if self.showtext and self.showtext_fuqin then
                local pos = TheInput:GetScreenPosition()
                local uipos = self.showtext_fuqin:GetWorldPosition()
                local new = pos - uipos
                self.showtext:SetPosition( new.x,new.y-60 )
            end
        end)
    end
end

return Myth_Ssbui
