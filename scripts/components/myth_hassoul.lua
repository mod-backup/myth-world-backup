

local myth_hassoul = Class(function(self, inst)
    self.inst = inst
    self.hassoul = true
end)

function myth_hassoul:OnSave()
    return { hassoul = self.hassoul }
end

function myth_hassoul:OnLoad(data)
    if data and data.hassoul ~= nil then
        self.hassoul = data.hassoul
    end
end

return myth_hassoul
