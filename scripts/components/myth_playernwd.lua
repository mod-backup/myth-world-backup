
local maxnwd = {
    winterboss_nwd = 20,
    food_nwd = 20,
    foodtable_nwd = 8,
    firecrackers_nwd = 10,
    miniflare_nwd = 10,
    lantern_nwd = 10 ,
    nian_nwd = 50 ,
}

local myth_playernwd =Class(function(self, inst)
    self.inst = inst
    self.current = 0
    self.winterboss_nwd = 0 --击杀冬天相关boss增加的
    self.food_nwd = 0 --吃带有年维度的食物增加的
    self.foodtable_nwd = 0 --餐桌相关的
    self.firecrackers_nwd = 0 --爆竹
    self.miniflare_nwd = 0 --窜天猴
    self.lantern_nwd = 0 --灯笼相关
    self.nian_nwd = 0 --年兽相关

    self.inst:WatchWorldState("cycles",function()
        if TheWorld.state.season == "spring" and ((TheWorld.state.springlength - TheWorld.state.remainingdaysinseason) == 3) then
            self:CheckNwd()
        elseif TheWorld.state.season == "winter" and (TheWorld.state.remainingdaysinseason == TheWorld.state.winterlength) then
            self:ClearNwd()
        end
    end)
end)

function myth_playernwd:ClearNwd()
    for k, v in pairs(maxnwd) do
        self[k] = 0
    end
end

--print(ThePlayer.components.myth_playernwd:GetNwd())
--ThePlayer.components.myth_playernwd:SetMax()

function myth_playernwd:DoDelta(type,value)
    if self[type] ~= nil then
        if  self[type] >= maxnwd[type] then 
            return
        end
        local change = math.min(maxnwd[type] -self[type], value)
        self[type] = self[type] + change
    end
end

function myth_playernwd:GetNwd()
    local num = 0
    for k, v in pairs(maxnwd) do
        num =  num + self[k]
    end
    return num
end

function myth_playernwd:OnSave()
    local data = {}
    for k, v in pairs(maxnwd) do
        data[k] = self[k] or nil
    end
    return data
end

function myth_playernwd:OnLoad(data)
    if data then
        for k, v in pairs(maxnwd) do
            if data.k then
                self[k] = data.k
            end
        end  
    end
end

function myth_playernwd:SetMax()
    for k, v in pairs(maxnwd) do
        self[k] = maxnwd[k]
    end
end

function myth_playernwd:CheckNwd()
    local nwd = self:GetNwd()
    local x, y, z = self.inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 20, {"myth_nianweidu"})
    for i, v in ipairs(ents) do
        if v:HasTag("myth_toys") then
            nwd = nwd + math.min(12,v.components.stackable:StackSize() * 2)
        end
    end
    if nwd > 1 then
        local give = 1
        local redpouch = "myth_playerredpouch_normal"
        local item = "lucky_goldnugget"
        if nwd >= 100 then
            give = 20
            redpouch = "myth_playerredpouch_super"
        elseif nwd >= 61 then
            give = math.random(16,20)
        elseif nwd >= 41 then
            give = math.random(8,12)
        elseif nwd >= 21 then
            give = math.random(4,8)
        else
            item = "myth_coin"
        end
        local pt = self.inst:GetPosition()
        local pouch = SpawnPrefab(redpouch)
        local prize_items = {}
        table.insert(prize_items,{ prefab = item, data = ( give > 1 and { stackable = {stack = give}} or nil )})
        pouch.components.unwrappable.itemdata =prize_items

        local theta = math.random() * 2 * PI
        local radius = 3
        local offset = FindWalkableOffset(pt, theta, radius, 6, true)
        if offset ~= nil then
            pt.x = pt.x + offset.x
            pt.z = pt.z + offset.z
        end
        local fx = SpawnPrefab('mk_cloudpuff')
        fx.Transform:SetScale(1.2,1.2,1.2)
        fx.Transform:SetPosition(pt:Get())
        pouch.Transform:SetPosition(pt:Get())
    end
end

return myth_playernwd
