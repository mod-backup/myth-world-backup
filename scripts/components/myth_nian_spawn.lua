
local myth_nian_spawn = Class(function(self, inst)
    self.inst = inst
    self.cd = 20*480

    self.inst:WatchWorldState("phase", function(inst,phase)
		self:TrySpawn(phase)
	end)

	self.inst:DoTaskInTime(0,function()
		self:TrySpawn(TheWorld.state.phase)
	end)
end)

local function findanyshop(inst)
	if BSHOPSTATE and BSHOPSTATE.shops then
		for k, v in pairs(BSHOPSTATE.shops) do 
			if v and v.num and v.num > 0 then
				return true
			end
		end
	end
	return false
end

function myth_nian_spawn:TrySpawn(phase)
	if TheWorld.state.season == "winter" and TheWorld.state.remainingdaysinseason == 2 and phase == "dusk" then
		if self.inst.components.timer:TimerExists("myth_nian_timer") then
			return
		end
		if not findanyshop(self.inst) then
			return
		end
		local pt = self.inst:GetPosition()
		local boss = SpawnPrefab("myth_nian")
		local theta = math.random() * 2 * PI
		local radius = 12
		local offset = FindWalkableOffset(pt, theta, radius, 6, true)
		if offset ~= nil then
			pt.x = pt.x + offset.x
			pt.z = pt.z + offset.z
		end
		boss.Transform:SetPosition(pt.x, 0, pt.z)
		local fx = SpawnPrefab('mk_cloudpuff')
		fx.Transform:SetScale(2.2,2.2,2.2)
		fx.Transform:SetPosition(pt:Get())
		TheWorld:PushEvent("myth_nian_event",{type = "come"})
		self.inst.components.timer:StartTimer('myth_nian_timer', self.cd)
	end
end


return myth_nian_spawn
