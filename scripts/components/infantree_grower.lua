
local function ontakecare(self,takecare)
    if takecare ~= 0  then
        self.inst:RemoveTag("canbe_takecare")
    else
        self.inst:AddTag("canbe_takecare")
    end
end

local infantree_grower = Class(function(self, inst)
    self.inst = inst
    self.provisioned = 0
    self.takecare = 0
    self.max = 9 --最大次数
    self.current = 0 --当前次数
    self.essense = {}

    self.inst:WatchWorldState("cycles", function()
        self:CheckGrow()
    end)

end,
 nil,
    {
        takecare = ontakecare,
    }
)

--每天 早上营养够  + 当天照顾一次    都满足第二天就是+ 1

local SHADECANOPY_MUST_TAGS = {"shadecanopy"}
local SHADECANOPY_SMALL_MUST_TAGS = {"shadecanopysmall"}

local function undershadecanopy(self)
    local x, y, z = self.inst.Transform:GetWorldPosition()
    local canopy = TheSim:FindEntities(x,y,z, TUNING.SHADE_CANOPY_RANGE, SHADECANOPY_MUST_TAGS)
    local canopy_small = TheSim:FindEntities(x,y,z, TUNING.SHADE_CANOPY_RANGE_SMALL, SHADECANOPY_SMALL_MUST_TAGS)
    if #canopy > 0 or #canopy_small > 0 then
        return true
    end
end

local function anyplantnearby(self)
    local x, y, z = self.inst.Transform:GetWorldPosition()
    local world = TheWorld
    local map = world.Map
	for k1 = -4,4,4 do
		for k2 = -4,4,4 do
            local tile = world.Map:GetTileAtPoint(x+k1, 0, z+k2)
            if tile == GROUND.FARMING_SOIL then
                local ents = world.Map:GetEntitiesOnTileAtPoint(x+k1, 0, z+k2)
                for _, ent in ipairs(ents) do
                    if ent ~= self.inst and not (ent:HasTag("NOBLOCK") or ent:HasTag("locomotor") or ent:HasTag("NOCLICK") or ent:HasTag("FX") or ent:HasTag("DECOR")) then
                        if ent.components.growable or ent.components.pickable then --有可生长的物品
                            return true
                        elseif ent.components.workable and not ent.components.inventoryitem then --可被工作的 不包括物品
                            return true
                        end
                    end
                end
                local x1, y1 = world.Map:GetTileCoordsAtPoint(x+k1, 0, z+k2)
                local n1, n2, n3 = TheWorld.components.farming_manager:GetTileNutrients(x1,y1)
                if (n1 < 1) or (n2 < 1) or (n3 < 1) then
                    return true
                else
                    world.components.farming_manager:AddTileNutrients(x1, y1, -1, -1, -1)
                end
                if world.components.farming_manager:IsSoilMoistAtPoint(x+k1, 0, z+k2) then
                    world.components.farming_manager:AddSoilMoistureAtPoint(x+k1, 0, z+k2, -2)
                else
                    return true
                end
            else
                return true 
            end
        end
    end
end

function infantree_grower:CheckGrow()
    if self.provisioned > 0 and self.takecare > 0  then
        self.current = self.current + 1
    end
    self:Grow()
    self.provisioned = 0
    self.takecare = 0 

    if TheWorld:HasTag("cave") then --在洞穴
        return
    elseif undershadecanopy(self) then --树荫下
        return
    elseif anyplantnearby(self) then --有别的植物或者营养不满足！
        return 
    end
    self.provisioned = 1
end

function infantree_grower:Grow()
    if self.current >= self.max and #self.essense > 1 then
        self.inst:OnTreeFn()
    end
end

function infantree_grower:OnTakecare()
    self.takecare = 1
end

function infantree_grower:AddEssense(item)
    if not table.contains(self.essense,item.prefab) then
        table.insert(self.essense,item.prefab)
        item:Remove()
        self:Grow()
        return true
    end
    return false
end

function infantree_grower:OnSave()
    return
    {
        provisioned = self.provisioned,
        takecare = self.takecare,
        max = self.max,
		current = self.current,
        essense = self.essense,
    }
end

function infantree_grower:OnLoad(data)
    if data then
        self.provisioned = data.provisioned
        self.takecare = data.takecare
		self.max = data.max
        self.current = data.current
        self.essense = data.essense
    end
end

return infantree_grower
