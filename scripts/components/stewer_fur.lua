

local function ondone(self, done)
    if done then
        self.inst:AddTag("donecooking_fur")
    else
        self.inst:RemoveTag("donecooking_fur")
    end
end

local function oncheckready(inst)
    if inst.components.container ~= nil and
        not inst.components.container:IsOpen() and
        not inst.components.container:IsEmpty() then
        inst:AddTag("readytocook_fur")
    end
end

local function onnotready(inst)
    inst:RemoveTag("readytocook_fur")
end

local Stewer_Fur = Class(function(self, inst)
    self.inst = inst

    self.done = nil
    self.targettime = nil
    self.task = nil
	self.puff = nil
    self.product = nil
    self.spoiledproduct = "spoiled_food"
    self.num = 1

    inst:ListenForEvent("itemget", oncheckready)
    inst:ListenForEvent("onclose", oncheckready)

    inst:ListenForEvent("onopen", onnotready)

    inst:DoTaskInTime(0,function() --延迟判定人物mod开启 
        if TUNING.MYTH_CHARACTER_MOD_OPEN then
            for k,v in pairs(TUNING.MYTH_PLAYER_PILL_RECIPES) do
                TUNING.MYTH_PILL_RECIPES[k] = v
            end
        end
    end)
end,
nil,
{
    done = ondone,
})

function Stewer_Fur:OnRemoveFromEntity()
    self.inst:RemoveTag("donecooking_fur")
    self.inst:RemoveTag("readytocook_fur")
end

local function cookcook(inst,doer)
    for k, v in pairs(TUNING.MYTH_PILL_RECIPES) do 
        if v.recipe then
            local cando = true
            for k1,v1 in pairs(v.recipe) do 
                if not inst:Has(k1, v1) then
                    cando = false
                end
            end
            if cando and (not v.prefn or v.prefn(inst,doer)) then 
                return k,v.time,v.num,v.recipe
            end 
        end
    end
    return nil , 1.5, nil,nil
end

local function dostew(inst, self) --over
    self.task = nil
    if self.puff ~= nil then
        self.puff:Cancel()
		self.puff = nil
    end
    if self.smoke ~= nil then
        self.smoke:Remove()
		self.smoke = nil
    end	
    self.targettime = nil
    
    if self.ondonecooking ~= nil then
        self.ondonecooking(inst)
    end
    self.done = true
end

local function dobad(inst, self) --over
    self.task = nil
    if self.puff ~= nil then
        self.puff:Cancel()
		self.puff = nil
    end	
    if self.smoke ~= nil then
        self.smoke:Remove()
		self.smoke = nil
    end	
    self.targettime = nil
    
    if self.ondobad ~= nil then
        self.ondobad(inst)
    end
    self.done = nil
	if inst.components.container then
		inst.components.container.canbeopened = true
	end
end

local function dopuff(inst, self)
    local skin = self.inst.AnimState:GetBuild()
	if skin ~= "alchmy_fur_ruins" then
		local pt = Vector3(inst.Transform:GetWorldPosition())
		local mk_cloudpuff = SpawnPrefab( "mk_cloudpuff" )
		mk_cloudpuff.Transform:SetPosition( pt.x , pt.y + 1, pt.z)
	end
	self.puff = inst:DoTaskInTime(math.random(5,8), dopuff, self)    
end

function Stewer_Fur:IsDone()
    return self.done
end

function Stewer_Fur:CheckSmoke()
	local skin = self.inst.AnimState:GetBuild()
	if skin == "alchmy_fur_ruins" then
		if not self.smoke then
			self.smoke = SpawnPrefab("torchfire_rag")
			self.smoke.entity:SetParent(self.inst.entity)
			self.smoke.Transform:SetPosition(0,0.9,0)
		end
	elseif self.smoke ~= nil then
		self.smoke:Remove()
		self.smoke = nil
	end
end

function Stewer_Fur:DoneCooking()
	if self.product == nil then
		return
	end
    if self.task ~= nil then
        self.task:Cancel()
		self.task = nil
    end
    if self.puff ~= nil then
        self.puff:Cancel()
		self.puff = nil
    end	
    if self.smoke ~= nil then
        self.smoke:Remove()
		self.smoke = nil
    end	
    self.targettime = nil

	if self.ondonecooking ~= nil then
		self.ondonecooking(self.inst)
	end
	self.done = true
end

function Stewer_Fur:IsCooking()
    return not self.done and self.targettime ~= nil
end

function Stewer_Fur:GetTimeToCook()
    return not self.done and self.targettime ~= nil and self.targettime - GetTime() or 0
end

function Stewer_Fur:CanCook()
    return self.inst.components.container ~= nil and not  self.inst.components.container:IsEmpty()
end

function Stewer_Fur:StartCooking(doer)
    if self.targettime == nil then
        self.done = nil
        if self.onstartcooking ~= nil then 
            self.onstartcooking(self.inst)
        end
        local cooktime = nil
        local num = nil
        local cost = nil
		self.product, cooktime, num, cost  = cookcook(self.inst.components.container)
        if self.product ~= nil and cooktime and cost then
            self.num = num or 1
		    self.targettime = GetTime() + cooktime
            if self.task ~= nil then
                self.task:Cancel()
            end
            self.task = self.inst:DoTaskInTime(cooktime, dostew, self)

            if self.puff ~= nil then
                self.puff:Cancel()
            end
            self.puff = self.inst:DoTaskInTime(math.random(5,8), dopuff, self)
		    self:CheckSmoke()
            self.inst.components.container:Close()
            for k,v in pairs(cost) do
                self.inst.components.container:ConsumeByName(k, v)
            end
            self.inst.components.container:DropEverything()
            self.inst.components.container.canbeopened = false		
		else
		    self.targettime = 1.5
		    self.product = nil
            if self.task ~= nil then
                self.task:Cancel()
            end
		
            if self.puff ~= nil then
                self.puff:Cancel()
            end
		    self:CheckSmoke()
            self.task = self.inst:DoTaskInTime(1.5, dobad, self)
		
            self.inst.components.container:Close()
            --self.inst.components.container:DestroyContents()
            self.inst.components.container.canbeopened = false
		end
    end
end

local function StopProductPhysics(prod)
    prod.Physics:Stop()
end

function Stewer_Fur:OnSave()
    local remainingtime = self.targettime ~= nil and self.targettime - GetTime() or 0
    return
    {
        done = self.done,
        product = self.product,
        remainingtime = remainingtime > 0 and remainingtime or nil,
        num = self.num,
    }
end

function Stewer_Fur:OnLoad(data)
    if data.product ~= nil then
        self.done = data.done or nil
        self.product = data.product or nil
        self.num  = data.num or 1
        if self.task ~= nil then
            self.task:Cancel()
            self.task = nil
        end
        if self.puff ~= nil then
            self.puff:Cancel()
            self.puff = nil
        end
		if self.smoke ~= nil then
			self.smoke:Remove()
			self.smoke = nil
		end	
        self.targettime = nil

        if data.remainingtime ~= nil then
            self.targettime = GetTime() + math.max(0, data.remainingtime)
            if self.done then
                --self.task = self.inst:DoTaskInTime(data.remainingtime, dospoil, self)
                if self.oncontinuedone ~= nil then
                    self.oncontinuedone(self.inst)
                end
            else
                self.task = self.inst:DoTaskInTime(data.remainingtime, dostew, self)
				self.puff = self.inst:DoTaskInTime(math.random(5,8), dopuff, self)
				self:CheckSmoke()
                if self.oncontinuecooking ~= nil then
                    self.oncontinuecooking(self.inst)
                end
            end
        elseif self.product ~= self.spoiledproduct  then
            self.targettime = GetTime()
            self.task = self.inst:DoTaskInTime(0, dostew, self)
            if self.oncontinuecooking ~= nil then
                self.oncontinuecooking(self.inst)
            end
        elseif self.oncontinuedone ~= nil then
            self.oncontinuedone(self.inst)
        end

        if self.inst.components.container ~= nil then
            self.inst.components.container.canbeopened = false
        end
    end
end

function Stewer_Fur:Harvest(harvester)
    if self.done then
        if self.onharvest ~= nil then
            self.onharvest(self.inst)
        end
        if self.product ~= nil then
            local pillrecipe =  TUNING.MYTH_PILL_RECIPES[self.product]
            if pillrecipe ~= nil and pillrecipe.pruductfn then
                self.product = pillrecipe.pruductfn(self.inst)
            end
            for k = 1, self.num do 
                local loot = SpawnPrefab(self.product)
                if loot ~= nil then
                    if harvester ~= nil and harvester.components.inventory ~= nil then
                        harvester.components.inventory:GiveItem(loot, nil, self.inst:GetPosition())
                    else
                        LaunchAt(loot, self.inst, nil, 1, 1)
                    end
                end
            end
            self.product = nil
        end

        if self.task ~= nil then
            self.task:Cancel()
            self.task = nil
        end
        if self.puff ~= nil then
            self.puff:Cancel()
            self.puff = nil
        end
		if self.smoke ~= nil then
			self.smoke:Remove()
			self.smoke = nil
		end	
        self.targettime = nil
        self.done = nil
        self.num = 1

        if self.inst.components.container ~= nil then      
            self.inst.components.container.canbeopened = true
        end
        return true
    end
end

function Stewer_Fur:LongUpdate(dt)
    if self:IsCooking() then
        if self.task ~= nil then
            self.task:Cancel()
        end
        if self.task ~= nil then
            self.task:Cancel()
        end
        if self.targettime - dt > GetTime() then
            self.targettime = self.targettime - dt
            self.task = self.inst:DoTaskInTime(self.targettime - GetTime(), dostew, self)
			
			if self.targettime - GetTime() >= 5 then
				self.puff = self.inst:DoTaskInTime(math.random(5,8), dopuff, self)
			end
			self:CheckSmoke()
            dt = 0            
        else
            dt = dt - self.targettime + GetTime()
            dostew(self.inst, self)
        end
    end
end

return Stewer_Fur
