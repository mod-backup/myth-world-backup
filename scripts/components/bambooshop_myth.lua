local function onclosed(self)
    if self.isclosed then
        self.inst:AddTag("shopclosed")
    else
        self.inst:RemoveTag("shopclosed")
    end
end

local function DealNian(inst, data)
    if data == nil or data.type == nil then
        return
    end

    local self = inst.components.bambooshop_myth
    inst.components.timer:StopTimer("nian_leave")
    inst.components.timer:StopTimer("nian_killed")
    inst.components.timer:StopTimer("nian_noclose")
    if data.type == "come" then --年兽到来
        self.isnian = true
    elseif data.type == "leave" then --年兽自行离开
        self.isnian = false
        inst.components.timer:StartTimer("nian_leave", TUNING.TOTAL_DAY_TIME*5)
        for i = 1, self.countkind_buy, 1 do --商店货物随机损失一半
            local item = self.idx_items_buy[i]
            if item ~= nil and item.count ~= nil and item.count > 1 and math.random() < 0.7 then
                item.count = math.ceil(item.count/2)
            end
        end
    elseif data.type == "killed" then --年兽被驱逐
        self.isnian = false
        inst.components.timer:StartTimer("nian_killed", TUNING.TOTAL_DAY_TIME*5)
        if TheWorld.state.isspring or TheWorld.state.iswinter then
            inst.components.timer:StartTimer("nian_noclose", TUNING.TOTAL_DAY_TIME*80)
        end
    else --默认，恢复状态
        self.isnian = false
    end

    self:CloseTradeDialog() --让正在进行的铺子停止交易
    self:TriggerShop(TheWorld.state.isnight)
end

local function DealRhino_spawn(inst)
    inst.components.timer:StopTimer("nian_noclose") --提前结束店铺的夜晚不打烊buff
    inst.components.bambooshop_myth:CloseTradeDialog()
    inst.components.bambooshop_myth:TriggerShop(TheWorld.state.isnight)
end
local function DealRhino_killed(inst)
    inst.components.timer:StopTimer("nian_leave") --提前结束店铺的停止进货debuff
end
local function OnIsSpring(inst, isspring)
    if isspring then
    else
        DealRhino_spawn(inst)
    end
end

local BambooShop_myth = Class(function(self, inst)
    self.inst = inst

    self.isclosed = true --是否打烊
    self.occupieddoer = nil --正在使用的人
    self.discount = 1 --折扣比例(0-直接免费、0.1-1折、0.9-9折、...)
    self.isnian = false --年兽是否存在（存在时，不开门）

    self.items_buy = { --已上架的货物
        -- redgem = {},
    }
    self.idx_items_buy = { --上架货物-下标的对应表
        -- {
        --     prefab = "redgem",
        --     count = 3, --剩余数量
        --     num_mix = nil, --买到后，给予玩家的数量；需要达到这个数量，才能卖一次
        --     value = 10,
        --     lasttime = nil, --玩家上次购买该货物的时间点/初次上架时间
        --     idx = 1,
        -- },
    }
    self.countkind_buy = 8 --上架物品种类的最大数量

    self.items_sell = {} --已加入订购单的货物
    self.idx_items_sell = {} --订购货物-下标的对应表
    self.countkind_sell = 8 --订购物品种类的最大数量

    --禾种小铺
    self.item_seasonseeds = nil --当季种子包数据

    --算命小铺
    self.treasure = nil --宝藏数据

    self.inst:AddComponent("timer")

    --这样写还是有问题，那就是年兽存在期间新建的店铺还是能用
    self.inst:ListenForEvent("myth_nian_event", function(world, ...)
        DealNian(self.inst, ...)
    end, TheWorld)
    self.inst:ListenForEvent("rhino_boss_spawn", function(world)
        DealRhino_spawn(self.inst)
    end, TheWorld)
    self.inst:ListenForEvent("rhino_boss_killed", function(world)
        DealRhino_killed(self.inst)
    end, TheWorld)
    self.inst:WatchWorldState("isspring", OnIsSpring)
end,
nil,
{
    isclosed = onclosed,
})

function BambooShop_myth:CanTrade(doer) --是否能进入商铺
    if self.isclosed then
        return false, "CLOSED"
    elseif self.occupieddoer ~= nil then
        return false, "OCCUPIED"
    end
    return true
end

local function SetClientItems(self, type, idx_items, data)
    for i = 1, self["countkind_"..type], 1 do
        if idx_items[i] ~= nil then --严格来说，中途不应该出现空值数据
            local item = {
                prefab = idx_items[i].prefab,
                count = idx_items[i].count,
                -- value = math.ceil(idx_items[i].value*discount), --给客户端的直接计算了折扣(向上取整)
                -- lasttime = , --客户端不需要
                -- idx = i, --客户端自己定
            }
            data[i] = item
        end
    end
end
function BambooShop_myth:GetDiscount(doer) --计算最终折扣
    --如果有多重折扣，只要最大力度的折扣
    local dis = self.discount

    if self.inst.components.timer:TimerExists("nian_killed") and dis > 0.8 then --五天内打8折
        dis = 0.8
    end

    if doer ~= nil then
        if doer:HasTag("mime") and dis > 0.9 then --韦斯的暴食彩蛋延续
            dis = 0.9
        end
        if doer.bshop_discount ~= nil and dis > doer.bshop_discount then --兼容！
            dis = doer.bshop_discount
        end
    end
    return dis
end
function BambooShop_myth:OpenTradeDialog(doer) --进入商铺
    self.occupieddoer = doer
    doer.bbshop_m = self

    local data = {}

    data.discount = self:GetDiscount(doer) --传入折扣值，最终价格将在客户端计算一遍才展示

    if
        self.inst.bbshoptype == "rareitem" or self.inst.bbshoptype == "ingredient" or self.inst.bbshoptype == "animals"
        or self.inst.bbshoptype == "foods"
    then
        data.idx_items_buy = {}
        data.idx_items_sell = {}
        SetClientItems(self, "buy", self.idx_items_buy, data.idx_items_buy)
        SetClientItems(self, "sell", self.idx_items_sell, data.idx_items_sell)
    elseif self.inst.bbshoptype == "plants" then
        data.idx_items_buy = {}
        data.idx_items_sell = {}
        SetClientItems(self, "buy", self.idx_items_buy, data.idx_items_buy)
        SetClientItems(self, "sell", self.idx_items_sell, data.idx_items_sell)

        data.item_seasonseeds = self.item_seasonseeds
    elseif self.inst.bbshoptype == "weapons" or self.inst.bbshoptype == "construct" then
        data.idx_items_buy = {}
        SetClientItems(self, "buy", self.idx_items_buy, data.idx_items_buy)
    elseif self.inst.bbshoptype == "numerology" then
        data.idx_items_buy = {}
        SetClientItems(self, "buy", self.idx_items_buy, data.idx_items_buy)

        if self.treasure ~= nil and self.treasure.trea ~= nil then
            data.treasure = {
                address = self.treasure.address,
                time = self.treasure.time,
                weather = self.treasure.weather
            }
        end
    end

	local success, result = pcall(json.encode, data)
	if success then
        doer:ShowPopUp(POPUPS.BAMBOOSHOP, true, self.inst)
		SendModRPCToClient(GetClientModRPC("BBShop_m", "handleShop"), doer.userid, 1, result)
	end
end

function BambooShop_myth:CloseTradeDialog() --退出商铺
    if self.occupieddoer ~= nil then
        self.occupieddoer:ShowPopUp(POPUPS.BAMBOOSHOP, false, self.inst)
        self.occupieddoer.bbshop_m = nil
        self.occupieddoer = nil
    end
end

function BambooShop_myth:TriggerLight(isopen) --控制光源
    if self.isclosed or TheWorld.state.isday then
        isopen = false
    end

    if isopen then
        self.inst.Light:Enable(true)
        self.inst.AnimState:ShowSymbol("lights")
    else
        self.inst.Light:Enable(false)
        self.inst.AnimState:HideSymbol("lights")
    end
end

function BambooShop_myth:TriggerShop(isclose) --控制商铺开门/关门
    if self.task_init ~= nil or self.isnian then --在进货 或 年兽存在
        isclose = true
    elseif self.inst.components.timer:TimerExists("nian_noclose") then --夜晚不打烊
        isclose = false
    elseif TheWorld.state.isnight then
        isclose = true
    end

    if isclose then
        if not self.isclosed then
            self:CloseTradeDialog()
            self.isclosed = true
        end
        if not self.inst.AnimState:IsCurrentAnimation("closed_pre") and not self.inst.AnimState:IsCurrentAnimation("closed") then
            self.inst.AnimState:PlayAnimation("closed_pre", true)
            self.inst.AnimState:PushAnimation("closed", false)
        end
    else
        if self.isclosed then
            self.isclosed = false
        end
        if not self.inst.AnimState:IsCurrentAnimation("idle") then
            --self.inst.AnimState:PlayAnimation("closed_pst", true)
            self.inst.AnimState:PlayAnimation("idle",true)
        end
    end
    self:TriggerLight(not isclose)
end

------

local function InitSeasonSeeds(self)
    if self.item_seasonseeds ~= nil then
        return
    end
    self.item_seasonseeds = {
        count = 0, --当前数量
        num_mix = 3, --每包三个种子
        value = 1, --价格
    }
end
local function InitSets_buy(self, sets)
    if sets.buy ~= nil then
        if sets.buy.isnew then
            self.items_buy = {}
            self.idx_items_buy = {}
        end
        if sets.buy.add_max == nil then
            sets.buy.add_max = 2 --默认增加两种新货物
        end
        if sets.buy.spl_num == nil then
            sets.buy.spl_num = math.random(2, 4) --默认选择2-4个已有货物进行数量补充
        end
        if sets.buy.add_max ~= 0 or sets.buy.spl_num ~= 0 then
            return true
        end
    end
    return false
end
local function InitSets_sell(self, sets)
    if sets.sell ~= nil then
        if sets.sell.isnew then
            self.items_sell = {}
            self.idx_items_sell = {}
        end
        if sets.sell.add_max == nil then
            sets.sell.add_max = math.random(2, 3)
        end
        if sets.sell.spl_num == nil then
            sets.sell.spl_num = math.random(3, 4)
        end
        if sets.sell.add_max ~= 0 or sets.sell.spl_num ~= 0 then
            return true
        end
    end
    return false
end
local function SetItems_add(self, type, data, items, idx_items, count) --加新货物
    local chancedata = {}
    local weightall = 0
    for k,v in pairs(data) do
        if
            v[type] and v[type].value ~= nil and v[type].chance ~= nil --能买卖
            and items[k] == nil --必须是新货
            and PrefabExists(k) --主要是防止开始有后面没的mod数据加进去
        then
            local chance = { min = weightall, max = weightall + v[type].chance, }
            weightall = chance.max
            chancedata[k] = chance
        end
    end
    if weightall <= 0 then
        return
    end

    local random = math.random() * weightall
    for k,v in pairs(chancedata) do
        if random >= v.min and random < v.max then
            local base = data[k][type]
            local countnew = nil
            if base.count_min == base.count_max then
                countnew = base.count_min
            else
                countnew = math.random(base.count_min, base.count_max)
            end

            local newitem = {
                prefab = k,
                count = countnew,
                value = base.value,
                lasttime = GetTime(),
                num_mix = base.num_mix or 1,
                -- idx = 1,
                tag = base.tag,
            }
            for i = 1, self["countkind_"..type], 1 do
                if idx_items[i] == nil then
                    newitem.idx = i
                    break
                end
            end
            if newitem.idx == nil then
                return --代表没有额外的位置加新货物了，直接结束
            else
                items[k] = newitem
                idx_items[newitem.idx] = newitem
                break
            end
        end
    end

    count = count - 1
    if count > 0 then
        SetItems_add(self, type, data, items, idx_items, count)
    end
end
local function SetItems_supply(self, type, data, items, count) --补充货物
    local chancedata = {}
    local weightall = 0
    for k,v in pairs(data) do
        local chance = { min = weightall, max = weightall + (v.chance or 10), }
        weightall = chance.max
        chancedata[k] = chance
    end
    if weightall <= 0 then
        return
    end

    local datanew = nil
    local random = math.random() * weightall
    for k,v in pairs(chancedata) do
        local base = data[k]
        if random >= v.min and random < v.max then
            if base.count_min == base.count_max then
                items[k].count = items[k].count + base.count_min
            else
                items[k].count = items[k].count + math.random(base.count_min, base.count_max)
            end
        else
            if datanew == nil then
                datanew = {}
            end
            datanew[k] = base
        end
    end

    count = count - 1
    if datanew ~= nil and count > 0 then
        SetItems_supply(self, type, datanew, items, count)
    end
end
local function BrandNewDay(self, type, sets)
    local items = self["items_"..type]
    local idx_items = self["idx_items_"..type]
    local countall = self["countkind_"..type]
    local DATA = BBSHOPDATA[self.inst.bbshoptype]

    local items_new = {} --记录新表，把空缺位置给清掉
    local idx_items_new = {}
    local itemcount = 0 --已有货物数量

    local timenow = GetTime()
    local itemssupply = nil --补货候选名单(存的元表数据)

    --先整理出目前还有效的货物
    for i = 1, countall, 1 do
        if idx_items[i] ~= nil then
            local item = idx_items[i]
            if
                item.count <= 0 or --数量没了
                (timenow - item.lasttime) >= TUNING.TOTAL_DAY_TIME*4 or --距离初次上货架过了4天
                not DATA[item.prefab] --无效的货物
            then
                --nothing
            else --有效的货物，加入新表
                local itemBase = DATA[item.prefab][type]
                if item.count < itemBase.stacksize then --数量还没超过最大数量，可以补货
                    if itemssupply == nil then
                        itemssupply = {}
                    end
                    itemssupply[item.prefab] = itemBase
                end

                itemcount = itemcount + 1
                item.idx = itemcount
                items_new[item.prefab] = item
                idx_items_new[itemcount] = item
            end
            items[item.prefab] = nil
            idx_items[i] = nil
        end
    end

    --上架新货物
    local itemsadd = countall - itemcount --目前的缺货数量
    if itemsadd > sets.add_max then --限制最大上新数量
        itemsadd = sets.add_max
    end
    if itemsadd > 0 then
        SetItems_add(self, type, DATA, items_new, idx_items_new, itemsadd)
    end

    --补充已有货物
    if itemssupply ~= nil and sets.spl_num > 0 then
        SetItems_supply(self, type, itemssupply, items_new, sets.spl_num)
    end

    --更新数据
    self["items_"..type] = items_new
    self["idx_items_"..type] = idx_items_new
end
local function SetItems_rareitem(self)
    local emptyidx = nil
    for i = 1, self.countkind_buy, 1 do
        if self.idx_items_buy[i] == nil then
            if emptyidx == nil then
                emptyidx = i
            end
        elseif self.idx_items_buy[i].tag == 1 then
            emptyidx = i
            break
        end
    end

    if TheWorld.state.iswinter then --冬季限定
        if emptyidx == nil then --没有位置了，直接占用第一个货架
            emptyidx = 1
        end
        local itemprefab = math.random() < 0.5 and "miniflare_myth" or "firecrackers_myth"
        local base = BBSHOPDATA[self.inst.bbshoptype][itemprefab]
        if base == nil or base.buy == nil or base.buy.value == nil then
            return
        end
        base = base.buy
        local countnew = nil
        if base.count_min == base.count_max then
            countnew = base.count_min
        else
            countnew = math.random(base.count_min, base.count_max)
        end
        local newitem = {
            prefab = itemprefab,
            count = countnew,
            value = base.value,
            lasttime = GetTime(),
            num_mix = base.num_mix or 1,
            idx = emptyidx,
            tag = base.tag,
        }
        self.items_buy[itemprefab] = newitem
        self.idx_items_buy[emptyidx] = newitem
    else --非冬季时，立即移除该货物
        if emptyidx ~= nil and self.idx_items_buy[emptyidx] ~= nil then
            local item = self.idx_items_buy[emptyidx]
            self.items_buy[item.prefab] = nil
            self.idx_items_buy[emptyidx] = nil
        end
    end
end
function BambooShop_myth:InitShop(sets) --商铺初始化/进货补货
    if sets == nil then
        return
    end
    if self.task_init ~= nil then
        self.task_init:Cancel()
        self.task_init = nil
    end

    if self.inst.components.timer:TimerExists("nian_leave") then --五天内停止进货
        self:TriggerShop(TheWorld.state.isnight)
        return
    end
    self.task_init = self.inst:DoTaskInTime(sets.time_delay or 0, function()
        if
            self.inst.bbshoptype == "weapons"
            or self.inst.bbshoptype == "numerology"
            or self.inst.bbshoptype == "construct"
        then
            --这些只有卖的
            if InitSets_buy(self, sets) then
                BrandNewDay(self, "buy", sets.buy)
            end
        elseif self.inst.bbshoptype == "plants" then
            if InitSets_buy(self, sets) then
                BrandNewDay(self, "buy", sets.buy)
            end
            if InitSets_sell(self, sets) then
                BrandNewDay(self, "sell", sets.sell)
            end

            --设置当季种子包
            InitSeasonSeeds(self)
            if self.item_seasonseeds.count < 20 then
                self.item_seasonseeds.count = self.item_seasonseeds.count + math.random(3, 6)
            end
        elseif self.inst.bbshoptype == "rareitem" then
            if InitSets_buy(self, sets) then
                --更新前，先设置季节专属货物
                SetItems_rareitem(self)
                BrandNewDay(self, "buy", sets.buy)
            end
            if InitSets_sell(self, sets) then
                BrandNewDay(self, "sell", sets.sell)
            end
        else
            if InitSets_buy(self, sets) then
                BrandNewDay(self, "buy", sets.buy)
            end
            if InitSets_sell(self, sets) then
                BrandNewDay(self, "sell", sets.sell)
            end
        end

        self.task_init = nil
        self:TriggerShop(false)
    end)
    self:TriggerShop(true) --开始进货，不许玩家进来
end

------

local function FindSlotsItems(slots, fn, itemsandnum)
    for k,v in pairs(slots) do
        if v ~= nil then
            local count = fn(v)
            if count > 0 then
                table.insert(itemsandnum.items, v)
                itemsandnum.num = itemsandnum.num + count
            end
            if v.components.container ~= nil then
                FindSlotsItems(v.components.container.slots, fn, itemsandnum)
            end
        end
    end
end
function BambooShop_myth:FindItems(doer, fn)
    local inv = doer.components.inventory
    local itemsandnum = { items = {}, num = 0 }

    FindSlotsItems(inv.itemslots, fn, itemsandnum) --物品栏物品
    FindSlotsItems(inv.equipslots, fn, itemsandnum) --装备栏物品
    if inv.activeitem ~= nil then --鼠标栏物品
        FindSlotsItems({ inv.activeitem }, fn, itemsandnum)
    end

    return itemsandnum
end

local function ComputNumber(numnow, maxnum)
    local numfull = 0
    local numleft = 0

    if numnow > maxnum then
        numfull = math.modf(numnow / maxnum)
        numleft = numnow % maxnum
    elseif numnow == maxnum then
        numfull = 1
    elseif numnow > 0 then
        numleft = numnow
    end

    return numfull, numleft
end
local function GiveItems(doer, itemname, num, x, y, z, numfull)
    local item = SpawnPrefab(itemname)
    if item == nil then
        return
    end

    if numfull == nil then
        if item.components.stackable ~= nil then
            numfull, num = ComputNumber(num, item.components.stackable.maxsize)
        else
            numfull = 0
        end
    end

    if numfull > 0 then
        item.components.stackable:SetStackSize(item.components.stackable.maxsize)
        numfull = numfull - 1
    elseif num > 1 then
        if item.components.stackable ~= nil then
            item.components.stackable:SetStackSize(num)
            num = 0
        else
            num = num - 1
        end
    elseif num > 0 then
        num = 0
    end

    if doer.components.inventory == nil or not doer.components.inventory:GiveItem(item) then
        if item.Physics ~= nil then
            item.Physics:Teleport(x, y, z)
        else
            item.Transform:SetPosition(x, y, z)
        end
    end

    if num > 0 or numfull > 0 then
        GiveItems(doer, itemname, num, x, y, z, numfull)
    end
end
function BambooShop_myth:ComputCoins(doer, value)
    if value == 0 then
        return true
    end

    local itemsandnum = self:FindItems(doer, function(item)
        if item.prefab == "myth_coin_box" then
            return item.components.stackable ~= nil and item.components.stackable:StackSize()*40 or 40
        elseif item.prefab == "myth_coin" then
            return item.components.stackable ~= nil and item.components.stackable:StackSize() or 1
        end
        return 0
    end)

    if value < 0 and (itemsandnum.num+value) < 0 then --花钱的话，判断钱是否够
        return false
    end
    itemsandnum.num = itemsandnum.num + value

    --先删除所有铜钱
    for k,v in pairs(itemsandnum.items) do
        v:Remove() --直接删除能行吗
    end

    --再重新给予铜钱
    if itemsandnum.num > 0 then
        local x, y, z = doer.Transform:GetWorldPosition()
        local numbox, num = ComputNumber(itemsandnum.num, 40)
        if numbox > 0 then
            GiveItems(doer, "myth_coin_box", numbox, x, y, z)
        end
        if num > 0 then
            GiveItems(doer, "myth_coin", num, x, y, z)
        end
    end

    return true
end

local function Discount(value, discount) --计算折扣(向上取整)
    return math.ceil(value*discount)
end
function BambooShop_myth:Buy(doer, data) --玩家买货物
    if doer == nil or data == nil or self.isclosed then
        return
    end
    local dis = self:GetDiscount(doer)
    local x, y, z = doer.Transform:GetWorldPosition()
    for _,v in pairs(data) do
        local iteminfo = self.items_buy[v.prefab]
        if iteminfo ~= nil and iteminfo.count > 0 and v.num > 0 then
            if v.num > iteminfo.count then
                v.num = iteminfo.count
            end
            if self:ComputCoins(doer, -v.num*Discount(iteminfo.value, dis)) then --先交钱
                GiveItems(doer, v.prefab, v.num*iteminfo.num_mix, x, y, z, nil) --再给货

                iteminfo.count = iteminfo.count - v.num
                iteminfo.lasttime = GetTime()
            end
        end
    end
end

local function CostItems(doer, items, num, x, y, z)
    for _,item in pairs(items) do
        if item ~= nil then
            if item.components.stackable == nil then
                item:Remove()
                num = num - 1
            else
                local item2 = nil
                local size = item.components.stackable:StackSize() or 1
                if size >= num then
                    item2 = item.components.stackable:Get(num)
                    num = 0
                else
                    item2 = item
                    num = num - size
                end
                item2:Remove()
            end
            if num <= 0 then
                return true
            end
        end
    end
    return false
end
function BambooShop_myth:Sell(doer, data) --玩家卖物品
    if doer == nil or data == nil or self.isclosed then
        return
    end

    local x, y, z = doer.Transform:GetWorldPosition()
    for _,v in pairs(data) do
        local iteminfo = self.items_sell[v.prefab]
        if iteminfo ~= nil and iteminfo.count >= iteminfo.num_mix and v.num >= iteminfo.num_mix then
            if v.num > iteminfo.count then
                v.num = iteminfo.count
            end

            local itemsandnum = self:FindItems(doer, function(item)
                if item.prefab == v.prefab then
                    return item.components.stackable ~= nil and item.components.stackable:StackSize() or 1
                end
                return 0
            end)

            if itemsandnum.num >= iteminfo.num_mix then
                if itemsandnum.num < v.num then
                    v.num = itemsandnum.num
                end
                local countgroup = math.floor(v.num/iteminfo.num_mix)
                v.num = countgroup * iteminfo.num_mix --根据组数计算出最大可卖数量
                CostItems(doer, itemsandnum.items, v.num, x, y, z) --先消耗物品

                --再给钱，防止钱掉地上
                if
                    doer.components.debuffable ~= nil and
                    doer.components.debuffable:HasDebuff("myth_nianbuff_lu") and --有“禄”buff时，有几率额外获得铜钱
                    math.random() <= 0.4 --40%几率
                then
                    local coins = countgroup*iteminfo.value
                    self:ComputCoins(doer, coins + math.ceil(coins*0.2)) --增加20%（向上取整）
                else
                    self:ComputCoins(doer, countgroup*iteminfo.value)
                end

                iteminfo.count = iteminfo.count - v.num
                iteminfo.lasttime = GetTime()
            end
        end
    end
end

local function GetPreciseDecimal(nNum, n) --保留小数点后n位
    local nDecimal = 10 ^ n
    local nTemp = math.floor(nNum * nDecimal)
    local nRet = nTemp / nDecimal
    return nRet
end
local function SecToDay(time) --秒为单位的时间转为游戏天数为单位
    return GetPreciseDecimal(time/TUNING.TOTAL_DAY_TIME, 1)
end
local function GetFateWords(words, param)
    return subfmt(GetRandomItem(words), {time = tostring(param)})
end
local function GetCalculatedPos(x, y, z, radius, angle)
    local rad = radius or math.random() * 3
    local ang = angle or math.random() * 2 * PI
    return x + rad * math.cos(ang), y, z - rad * math.sin(ang)
end
local function SpawnTreasure(self, doer, data) --删除已有的宝藏，生成新的宝藏
    --删除已有的
    if self.treasure ~= nil and self.treasure.trea ~= nil then
        self.treasure.trea:Remove()
        self.treasure = nil
    end

    self.treasure = data

    --生成新的
    if data.pos ~= nil then
        local bz = SpawnPrefab("treasure_hide_myth")
        if bz ~= nil then
            self.treasure.trea = bz
            bz.bs_components = self
            bz.Transform:SetPosition(data.pos.x, 0, data.pos.z)
            -- doer.player_classified.revealmapspot_worldx:set(pos.x)
            -- doer.player_classified.revealmapspot_worldz:set(pos.z)
            -- doer.player_classified.revealmapspotevent:push()
            -- doer:DoTaskInTime(4*FRAMES, function() --点亮地图
            --     doer.player_classified.MapExplorer:RevealArea(pos.x, 0, pos.z, true, true)
            -- end)

            if doer ~= nil then --给线索纸
                local paper = SpawnPrefab("treasure_paper_myth")
                if paper ~= nil then
                    paper.clues = {
                        address = data.address,
                        time = data.time,
                        weather = data.weather
                    }

                    if doer.components.inventory == nil or not doer.components.inventory:GiveItem(paper) then
                        local x, y, z = doer.Transform:GetWorldPosition()
                        if paper.Physics ~= nil then
                            paper.Physics:Teleport(x, y, z)
                        else
                            paper.Transform:SetPosition(x, y, z)
                        end
                    end
                end
            end
        end
    end
end
function BambooShop_myth:Serve(doer, data, data2) --店铺的特殊功能
    if doer == nil or data == nil or self.isclosed then
        return
    end
    local dis = self:GetDiscount(doer)
    local x, y, z = doer.Transform:GetWorldPosition()
    if data.servetype == 1 then --季节种子包
        if self.item_seasonseeds ~= nil and self.item_seasonseeds.count > 0 and data.num > 0 then
            if data.num > self.item_seasonseeds.count then
                data.num = self.item_seasonseeds.count
            end
            if self:ComputCoins(doer, -data.num*Discount(self.item_seasonseeds.value, dis)) then --先交钱
                local seeds = { "seeds" }
                if TheWorld.state.isautumn then
                    seeds = { "carrot_seeds", "corn_seeds", "potato_seeds", "tomato_seeds", "eggplant_seeds", "pumpkin_seeds",
                        "garlic_seeds", "onion_seeds", "pepper_seeds" }
                elseif TheWorld.state.iswinter then
                    seeds = { "carrot_seeds", "potato_seeds", "asparagus_seeds", "pumpkin_seeds", "garlic_seeds" }
                elseif TheWorld.state.isspring then
                    seeds = { "carrot_seeds", "corn_seeds", "potato_seeds", "tomato_seeds", "asparagus_seeds", "eggplant_seeds",
                        "watermelon_seeds", "dragonfruit_seeds", "durian_seeds", "garlic_seeds", "onion_seeds",
                        "pomegranate_seeds" }
                elseif TheWorld.state.issummer then
                    seeds = { "corn_seeds", "tomato_seeds", "watermelon_seeds", "dragonfruit_seeds", "garlic_seeds",
                        "onion_seeds", "pepper_seeds", "pomegranate_seeds" }
                end
                for i = 1, data.num*self.item_seasonseeds.num_mix, 1 do --再给货
                    GiveItems(doer, seeds[math.random(#seeds)], 1, x, y, z, nil)
                end
                self.item_seasonseeds.count = self.item_seasonseeds.count - data.num
            end
        end
    elseif data.servetype == 2 then --修复物品
        if data2 == nil then
            return
        end
        local itemsandnum = self:FindItems(doer, function(item)
            if item:IsValid() and item == data2 then
                if
                    (item.components.finiteuses ~= nil and item.components.finiteuses:GetPercent() < 1) or
                    (item.components.armor ~= nil and item.components.armor:GetPercent() < 1) or
                    (item.components.fueled ~= nil and item.components.fueled:GetPercent() < 1) or
                    (item.components.perishable ~= nil and item.components.perishable:GetPercent() < 1)
                then
                    return 1
                end
            end
            return 0
        end)

        if itemsandnum.num >= 1 and itemsandnum.items[1] ~= nil then
            local item = itemsandnum.items[1]
            local iteminfo = BBSHOPDATA[self.inst.bbshoptype] and BBSHOPDATA[self.inst.bbshoptype][item.prefab] or nil
            if iteminfo ~= nil and iteminfo.fix ~= nil then
                if self:ComputCoins(doer, -Discount(iteminfo.fix.value, dis)) then --先交钱
                    --再修理
                    if item.components.finiteuses ~= nil then
                        item.components.finiteuses:SetPercent(1)
                    elseif item.components.armor ~= nil then
                        item.components.armor:SetPercent(1)
                    elseif item.components.fueled ~= nil then
                        item.components.fueled:SetPercent(1)
                    elseif item.components.perishable ~= nil then
                        item.components.perishable:SetPercent(1)
                    end
                end
            end
        end
    elseif data.servetype == 3 then --藏宝图占卜
        local res = { words = nil, anim = nil }
        local newwords = STRINGS.BBSHOP.WORDS[self.inst.bbshoptype] or STRINGS.BBSHOP.WORDS["rareitem"]
        local clue_ents = {}
        local clue_key = nil
        local addfns = {
            function(ent)
                if ent:HasTag("multiplayer_portal") then
                    return { key = "multiplayer_portal", isend = true, }
                end
            end,
            function(ent)
                if ent.prefab == "pigking" then
                    return { key = "pigking", isend = true, }
                end
            end,
            function(ent)
                if ent:HasTag("lava") then
                    return { key = "lava_pond", isend = false, }
                end
                --其实也可以试试 dragonfly_spawner
            end,
            function(ent)
                if ent.prefab == "oasislake" then
                    return { key = "oasislake", isend = true, }
                end
            end,
            function(ent)
                if ent.prefab == "moonbase" then
                    return { key = "moonbase", isend = true, }
                end
            end,
            function(ent)
                if ent.prefab == "critterlab" then
                    return { key = "critterlab", isend = true, }
                end
            end,
            function(ent)
                if ent.prefab == "beequeenhive" or ent.prefab == "beequeenhivegrown" then
                    return { key = "beequeenhive", isend = true, }
                end
            end,
            function(ent)
                if ent.prefab == "moon_fissure" then
                    return { key = "moonland", isend = false, }
                end
            end,
            function(ent)
                if ent.prefab == "myth_ghg" then
                    return { key = "myth_ghg", isend = true, }
                end
            end,
            function(ent)
                if
                    ent.prefab == "hermithouse_construction1" or ent.prefab == "hermithouse_construction2"
                    or ent.prefab == "hermithouse_construction3" or ent.prefab == "hermithouse"
                then
                    return { key = "hermithouse", isend = true, }
                end
            end,
            function(ent)
                if
                    ent.prefab == "peachsprout_myth" or ent.prefab == "peachsapling_myth"
                    or ent.prefab == "peachstump_myth" or ent.prefab == "peachtreeburnt_myth"
                    or ent.prefab == "peachtree_myth"
                then
                    return { key = "peachtree_myth", isend = false, }
                end
            end,
            function(ent)
                if ent.prefab == "fangcunhill" then
                    return { key = "fangcunhill", isend = true, }
                end
            end,
            function(ent)
                if ent:HasTag("myth_plant_bamboo") then
                    return { key = "myth_plant_bamboo", isend = false, }
                end
            end,
            function(ent)
                if ent:HasTag("myth_shop") then
                    return { key = "myth_shop", isend = false, }
                end
            end,
            function(ent)
                if ent.prefab == "walrus_camp" then
                    return { key = "walrus_camp", isend = false, }
                end
            end
        }
        local fn_add = addfns[math.random(#addfns)]
        for _,v in pairs(Ents) do
            if v ~= nil and v:IsValid() and not v:IsInLimbo() then
                local res = fn_add(v)
                if res ~= nil then
                    clue_key = res.key
                    table.insert(clue_ents, v)
                    if res.isend then
                        break
                    end
                end
            end
        end

        if clue_key ~= nil then
            local px, py, pz = clue_ents[math.random(#clue_ents)].Transform:GetWorldPosition()
            local xx = nil
            local zz = nil
            for i = 1, 5, 1 do
                local x, y, z = GetCalculatedPos(px, py, pz, 5+math.random()*20, nil)
                if
                    TheWorld.Map:IsAboveGroundAtPoint(x, 0, z) and --排除海洋
                    #TheSim:FindEntities(x, 0, z, 2) <= 0 --很近的范围不能有东西
                then
                    xx = x
                    zz = z
                    break
                end
            end

            if xx ~= nil then
                if self:ComputCoins(doer, -Discount(40, dis)) then --先交钱
                    TheWorld:PushEvent("myth_treasure_remove") --让世界发送一个事件，让已有线索纸失效
                    SpawnTreasure(self, doer, {
                        address = clue_key,
                        time = math.random(#STRINGS.BBSHOP.CLUETIME),
                        weather = math.random(#STRINGS.BBSHOP.CLUEWEATHER),
                        pos = { x = xx, z = zz },
                        trea = nil
                    })
                    res.anim = "anim_happy"
                    res.words = GetRandomItem(newwords.say_treasure)
                    res.treasure = {
                        address = self.treasure.address,
                        time = self.treasure.time,
                        weather = self.treasure.weather
                    }
                else --没有钱了：提示失败
                    res.anim = "anim_error"
                    res.words = GetRandomItem(newwords.say_buy_nocoin)
                end
            end
        end

        if res.words == nil then
            res.anim = "anim_refuse"
            res.words = GetRandomItem(newwords.say_treasure_error)
        end

        local success, result = pcall(json.encode, res)
        if success then
            SendModRPCToClient(GetClientModRPC("BBShop_m", "handleShop"), doer.userid, 2, result)
        end
    elseif data.servetype == 4 then --命运占卜
        if self:ComputCoins(doer, -Discount(1, dis)) then --先交钱
            local res = { words = nil, anim = nil }
            local newwords = STRINGS.BBSHOP.WORDS[self.inst.bbshoptype] or STRINGS.BBSHOP.WORDS["rareitem"]
            if math.random() < 0.4 then --4/10几率特殊台词
                local way = math.random(1, 7)
                if way == 1 then --猎犬
                    if TheWorld.components.hounded ~= nil then
                        local time = TheWorld.components.hounded:GetTimeToAttack()
                        if time ~= nil and time >= 30 then
                            res.anim = "anim_error"
                            res.words = GetFateWords(newwords.say_fate_dog, SecToDay(time))
                        end
                    end
                elseif way == 2 then --BOSS
                    way = math.random(1, 2)
                    res.anim = "anim_error"
                    if way == 1 then --熊獾
                        if TheWorld.components.worldsettingstimer ~= nil then
                            local time = TheWorld.components.worldsettingstimer:GetTimeLeft("bearger_timetospawn")
                            if time ~= nil and time >= 60 then
                                res.words = GetFateWords(newwords.say_fate_bearger, SecToDay(time))
                            end
                        end
                    elseif way == 2 then --巨鹿
                        if TheWorld.components.worldsettingstimer ~= nil then
                            local time = TheWorld.components.worldsettingstimer:GetTimeLeft("deerclops_timetoattack")
                            if time ~= nil and time >= 60 then
                                res.words = GetFateWords(newwords.say_fate_deerclops, SecToDay(time))
                            end
                        end
                    end
                elseif way == 3 then --季节剩余天数
                    res.anim = "anim_happy"
                    res.words = GetFateWords(newwords.say_fate_season, TheWorld.state.remainingdaysinseason)
                    -- print("TheWorld.state.summerlength："..tostring(TheWorld.state.summerlength))
                elseif way == 4 then --月相
                    local moondes = STRINGS.BBSHOP.MOONSTATE[TheWorld.state.moonphase]
                    if moondes ~= nil then
                        res.anim = TheWorld.state.moonphase == "new" and "anim_error" or "anim_happy"
                        res.words = GetFateWords(newwords.say_fate_moon, moondes)
                    end
                elseif way == 5 then --潮湿度
                    if TheWorld.state.pop <= 0.2 then
                        way = 1
                        res.anim = "anim_happy"
                    elseif TheWorld.state.pop <= 0.4 then
                        way = 2
                        res.anim = "anim_happy"
                    elseif TheWorld.state.pop <= 0.6 then
                        way = 3
                        res.anim = "anim_happy"
                    elseif TheWorld.state.pop <= 0.8 then
                        way = 4
                        res.anim = "anim_error"
                    else
                        way = 5
                        res.anim = "anim_error"
                    end
                    res.words = newwords.say_fate_rain[way]
                elseif way == 6 then --淘气值（看面相）
                    local kramped = Myth_GetPLayer_Kramped(doer)
                    res.anim = "anim_happy"
                    if kramped > 30 then
                        res.words = STRINGS.BBSHOP.TAOQIZHI[3]
                    elseif kramped > 15 then
                        res.words = STRINGS.BBSHOP.TAOQIZHI[2]
                    else
                        res.words = STRINGS.BBSHOP.TAOQIZHI[1]
                    end
                elseif way == 7 then --年味度
                    if doer.components.myth_playernwd ~= nil then
                        local nwd = doer.components.myth_playernwd:GetNwd()
                        if nwd ~= nil then
                            if nwd <= 20 then
                                way = 1
                            elseif nwd <= 40 then
                                way = 2
                            elseif nwd <= 60 then
                                way = 3
                            elseif nwd <= 99 then
                                way = 4
                            else
                                way = 5
                            end
                            res.anim = "anim_happy"
                            res.words = STRINGS.BBSHOP.NIANWEIDU[way]
                        end
                    end
                end
            end

            if res.words == nil then
                res.anim = "anim_refuse"
                res.words = GetRandomItem(newwords.say_fate)
            end

            local success, result = pcall(json.encode, res)
            if success then
                SendModRPCToClient(GetClientModRPC("BBShop_m", "handleShop"), doer.userid, 2, result)
            end
        end
    end
end

------

local function SetLastTimes(self, type, idx_items, dt)
    local timenow = GetTime()
    for i = 1, self["countkind_"..type], 1 do
        if idx_items[i] ~= nil then
            local item = idx_items[i]
            if item.lasttime == nil then
                item.lasttime = timenow - dt
            else
                item.lasttime = item.lasttime - dt
            end
        end
    end
end
function BambooShop_myth:LongUpdate(dt)
    if dt == nil or dt <= 0 then
        return
    end
	if
        self.inst.bbshoptype == "rareitem" or self.inst.bbshoptype == "ingredient" or self.inst.bbshoptype == "animals"
        or self.inst.bbshoptype == "plants" or self.inst.bbshoptype == "foods"
    then
        if self.idx_items_buy ~= nil then
            SetLastTimes(self, "buy", self.idx_items_buy, dt)
        end
        if self.idx_items_sell ~= nil then
            SetLastTimes(self, "sell", self.idx_items_sell, dt)
        end
    elseif self.inst.bbshoptype == "weapons" or self.inst.bbshoptype == "numerology" or self.inst.bbshoptype == "construct" then
        if self.idx_items_buy ~= nil then
            SetLastTimes(self, "buy", self.idx_items_buy, dt)
        end
    end
end

function BambooShop_myth:OnSave()
    local data = {
        isnian = self.isnian,
    }

    --保存买卖货物
    local timenow = GetTime()
    for i = 1, self.countkind_buy, 1 do
        local item = self.idx_items_buy[i]
        if item ~= nil then
            if data.idx_items_buy == nil then
                data.idx_items_buy = {}
            end
            data.idx_items_buy[i] = {
                prefab = item.prefab,
                count = item.count,
                lasttime = timenow - item.lasttime,
            }
        end
    end
    for i = 1, self.countkind_sell, 1 do
        local item = self.idx_items_sell[i]
        if item ~= nil then
            if data.idx_items_sell == nil then
                data.idx_items_sell = {}
            end
            data.idx_items_sell[i] = {
                prefab = item.prefab,
                count = item.count,
                lasttime = timenow - item.lasttime,
            }
        end
    end

    --保存“禾种小铺”的当季种子包数据
    if self.item_seasonseeds ~= nil and self.item_seasonseeds.count >= 0 then
        data.item_seasonseeds = self.item_seasonseeds.count
    end

    --保存“算命小铺”的宝藏数据
    if self.treasure ~= nil and self.treasure.trea ~= nil then
        data.treasure = {
            address = self.treasure.address,
            time = self.treasure.time,
            weather = self.treasure.weather,
            pos = { x = self.treasure.pos.x, z = self.treasure.pos.z }
        }
    end

    return data
end

local function SetLoadItems(self, type, data) --加载时，更新数据
    local items = self["items_"..type]
    local idx_items = self["idx_items_"..type]
    local countall = self["countkind_"..type]
    local itemall = BBSHOPDATA[self.inst.bbshoptype]
    local itemcount = 0
    local timenow = GetTime()

    for i = 1, countall, 1 do
        if data[i] ~= nil and itemall[data[i].prefab] ~= nil and PrefabExists(data[i].prefab) then --再次检查总表格，更新数据
            local base = itemall[data[i].prefab][type]
            if base.value ~= nil then --能否买卖
                itemcount = itemcount + 1
                local item = {
                    prefab = data[i].prefab,
                    count = data[i].count or 1,
                    value = base.value,
                    -- lasttime = GetTime(),
                    num_mix = base.num_mix or 1,
                    idx = itemcount,
                    tag = base.tag,
                }
                if data[i].lasttime == nil then
                    item.lasttime = timenow
                else
                    item.lasttime = timenow - data[i].lasttime
                end

                items[item.prefab] = item
                idx_items[itemcount] = item
            end
        end
    end
end
function BambooShop_myth:OnLoad(data)
    if data == nil then
        return
    end

    self.isnian = data.isnian

    local shouldstop = false
    if
        self.inst.bbshoptype == "rareitem" or self.inst.bbshoptype == "ingredient" or self.inst.bbshoptype == "animals"
        or self.inst.bbshoptype == "foods"
    then
        if data.idx_items_buy ~= nil then
            shouldstop = true
            SetLoadItems(self, "buy", data.idx_items_buy)
        end
        if data.idx_items_sell ~= nil then
            shouldstop = true
            SetLoadItems(self, "sell", data.idx_items_sell)
        end
    elseif self.inst.bbshoptype == "plants" then
        if data.idx_items_buy ~= nil then
            shouldstop = true
            SetLoadItems(self, "buy", data.idx_items_buy)
        end
        if data.idx_items_sell ~= nil then
            shouldstop = true
            SetLoadItems(self, "sell", data.idx_items_sell)
        end
        InitSeasonSeeds(self)
        if data.item_seasonseeds ~= nil and data.item_seasonseeds >= 0 then
            self.item_seasonseeds.count = data.item_seasonseeds
        end
    elseif self.inst.bbshoptype == "weapons" or self.inst.bbshoptype == "construct" then
        if data.idx_items_buy ~= nil then --只有卖的东西
            shouldstop = true
            SetLoadItems(self, "buy", data.idx_items_buy)
        end
    elseif self.inst.bbshoptype == "numerology" then
        if data.idx_items_buy ~= nil then --只有卖的东西
            shouldstop = true
            SetLoadItems(self, "buy", data.idx_items_buy)
        end
        if data.treasure ~= nil and data.treasure.address ~= nil and data.treasure.pos ~= nil then
            SpawnTreasure(self, nil, {
                address = data.treasure.address,
                time = data.treasure.time or math.random(#STRINGS.BBSHOP.CLUETIME),
                weather = data.treasure.weather or math.random(#STRINGS.BBSHOP.CLUEWEATHER),
                pos = { x = data.treasure.pos.x, z = data.treasure.pos.z },
                trea = nil
            })
        end
    end

    if shouldstop and self.task_init ~= nil then
        self.task_init:Cancel()
        self.task_init = nil
        self:TriggerShop(false) --开门
    end
end

return BambooShop_myth
