
require("stategraphs/commonstates")
local events =
{
    CommonHandlers.OnLocomote(true, true),
}
local states =
{
    State{
        name = "idle",
        tags = { "idle", "canrotate", "canslide" },
        onenter = function(inst)
			inst.AnimState:PlayAnimation("book", true)
        end,
    },
}
CommonStates.AddSimpleWalkStates(states, "book")
CommonStates.AddSimpleRunStates(states, "book")
return StateGraph("commissioner_book", states, events, "idle")