require("stategraphs/commonstates")

local actionhandlers =
{
}


local function DoBombShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .35, .02, .3, inst, 40)
end
local function ChooseAttack(inst)
    if not inst.components.timer:TimerExists("bomb_cd") then
        inst.sg:GoToState("bomb")
        return true
    elseif not inst.components.timer:TimerExists("bombboom") then
        inst.sg:GoToState("bombboom")
        return true
    end
    inst.sg:GoToState("nian_attack")
end


local MAIN_SHIELD_CD = 1.2
local function PickShield(inst)
    local t = GetTime()
    if (inst.sg.mem.lastshieldtime or 0) + .2 >= t then
        return
    end

    inst.sg.mem.lastshieldtime = t

    --variation 3 or 4 is the main shield
    local dt = t - (inst.sg.mem.lastmainshield or 0)
    if dt >= MAIN_SHIELD_CD then
        inst.sg.mem.lastmainshield = t
        return math.random(3, 4)
    end

    local rnd = math.random()
    if rnd < dt / MAIN_SHIELD_CD then
        inst.sg.mem.lastmainshield = t
        return math.random(3, 4)
    end

    return rnd < dt / (MAIN_SHIELD_CD * 2) + .5 and 2 or 1
end

local function picknearplayer(inst)
    local pt = inst:GetPosition()
    local players = TheSim:FindEntities(pt.x, 0, pt.z, 12, {"player"}, {"playerghost","INLIMBO"})
    for i, v in ipairs(players) do
        if not (v.components.health and v.components.health:IsDead() ) then
            inst.components.combat:SetTarget(v)
            return v
        end
    end
    return nil
end

local function picknearplayers(inst)
    local pt = inst:GetPosition()
    local players = TheSim:FindEntities(pt.x, 0, pt.z, 12, {"player"}, {"INLIMBO"})
    return #players
end

local function sleeponanimover(inst)
    if inst.AnimState:AnimDone() then
        inst.sg:GoToState("sleeping")
    end
end

local function onwakeup(inst)
	if not inst.sg:HasStateTag("nowake") then
	    inst.sg:GoToState("wake")
	end
end

local function onentersleeping(inst)
    inst.AnimState:PlayAnimation("sleep_loop")
    inst.AnimState:OverrideSymbol("warg_facebase", "myth_nian_facebase", "warg_facebase")
end

local function idleonanimover(inst)
    if inst.AnimState:AnimDone() then
        inst.sg:GoToState("idle")
    end
end

local COLLIDE_TIME = 3*FRAMES
local FX_TIME = 5*FRAMES
local CHARGE_LOOP_TARGET_CANT_TAGS = {"INLIMBO", "fx", "nian","shadow", "ghost", "NOCLICK", "DECOR", "INLIMBO",}
local CHARGE_LOOP_TARGET_ONEOF_TAGS = {"_combat", "_health","workable"}

local function spawn_ground_fx(inst)
    if not TheWorld.Map:IsVisualGroundAtPoint(inst.Transform:GetWorldPosition()) then
        SpawnPrefab("boss_ripple_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
    else
        local fx = SpawnPrefab("slide_puff")
        fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
        fx.Transform:SetScale(2, 2, 2)
    end
end

local events =
{
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnFreeze(),
    CommonHandlers.OnAttack(),
    --CommonHandlers.OnAttacked(),
    CommonHandlers.OnDeath(),
    CommonHandlers.OnLocomote(true, false),
    EventHandler("doattack", function(inst, data)
		if inst.components.health ~= nil and not inst.components.health:IsDead()
			and (not inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("hit")) then
            if inst:HasTag("nian_mount") then
			    inst.sg:GoToState("attack")
            else
                ChooseAttack(inst)
            end
		end
	end),

    EventHandler("trytoshield", function(inst)
        if not inst.components.health:IsDead() then
            inst.sg:GoToState("forcefield")
        end
    end),

    EventHandler("startle", function(inst)
        if not inst.components.health:IsDead() then
            if (GetTime() - (inst.last_startletime or -21)) > 20 then
                if inst.sg:HasStateTag("forcefield") then
                    inst:PushEvent("forcefield_stop")
                else
                    inst.sg:GoToState("startled")
                end
                inst.last_startletime = GetTime()
            end
        end
    end),
    EventHandler("startle_byexplode", function(inst)
        if not inst.components.health:IsDead() then
            if (GetTime() - (inst.last_startletime or -21)) > 20 then
                if inst.sg:HasStateTag("forcefield") then
                    inst:PushEvent("forcefield_stop")
                else
                    inst.sg:GoToState("startled")
                end
                inst.last_startletime = GetTime()
            end
        end
    end),

    EventHandler("charge", function(inst)
        if not inst.components.health:IsDead()
                and not inst.components.freezable:IsFrozen()
                and not inst.components.sleeper:IsAsleep()
                and not inst.sg:HasStateTag("busy") then
            inst.sg:GoToState("charge_pre", inst.components.combat.target)
        end
    end),
    EventHandler("nian_eat", function(inst, target)
		if inst.components.health ~= nil and not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then
			inst.sg:GoToState("nian_eat",target)
		end
	end),
    EventHandler("attacked", function(inst, data)
        if not inst.components.health:IsDead() then
            if inst.hasshield then
                local shieldtype = PickShield(inst)
                if shieldtype ~= nil then
                    local fx = SpawnPrefab("nian_shield"..tostring(shieldtype))
                    fx.entity:SetParent(inst.entity)
                    if shieldtype < 3 and math.random() < .5 then
                        --fx.AnimState:SetScale(-2.36, 2.36, 2.36)
                    end
                end
            elseif not (inst.sg:HasStateTag("busy")
                    or inst.sg:HasStateTag("caninterrupt")
                    or inst.sg:HasStateTag("frozen")) then
                inst.sg:GoToState("hit")           
            end
        end
    end),
}

local states =
{
    State{
        name = "idle",
        tags = { "idle" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop")
            if not inst.noidlesound then
                inst.SoundEmitter:PlaySound(inst.sounds.idle)
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State{
        name = "nian_eat",
        tags = { "busy" },

        onenter = function(inst,target)
            if not target then
                inst.sg:GoToState("idle")
            end
            inst.Physics:Stop()
            inst:ForceFacePoint(target.Transform:GetWorldPosition())
            inst.AnimState:PlayAnimation("atk")
            inst.sg.statemem.target = target
            inst.sg.statemem.size = 0
            inst.sg.statemem.time = 0
        end,
        onupdate = function(inst,dt)
            inst.sg.statemem.time =  inst.sg.statemem.time + dt
            if  inst.sg.statemem.time < 0.4 then
                inst.sg.statemem.size = inst.sg.statemem.size + 1/40
            elseif inst.sg.statemem.time < 0.8 then
                inst.sg.statemem.size =  inst.sg.statemem.size + - 1/40
            else
                return
            end
            local s = 0.8 * (1+inst.sg.statemem.size)
            inst.Transform:SetScale(s, s, s)
        end,
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end),
            TimeEvent(12 * FRAMES, function(inst)
                local target =  inst.sg.statemem.target
                if target and target:IsValid() and target.components.health ~= nil and not  target.components.health:IsDead() then
                    local maxhealth = target.components.health.maxhealth
                    target:Remove()
                    inst.components.health:DoDelta(maxhealth)
                end
            end),
        },
        onexit = function(inst)
            inst.Transform:SetScale(0.8, 0.8, 0.8)
        end
    },

    State{
        name = "howl",
        tags = { "busy", "howling" },

        onenter = function(inst, count)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("howl")
            inst.SoundEmitter:PlaySound(inst.sounds.howl)
            inst.sg.statemem.count = count
        end,

        timeline =
        {
            TimeEvent(10 * FRAMES, function(inst)
                if inst.sg.statemem.count == nil then
                    --SpawnHound(inst)
                end
            end),
        },

        events =
        {
            EventHandler("heardwhistle", function(inst)
                inst.sg.statemem.count = 2
            end),
            EventHandler("animover", function(inst)
                if inst.sg.statemem.count ~= nil and inst.sg.statemem.count > 1 then
                    inst.sg:GoToState("howl", inst.sg.statemem.count - 1)
                else
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },

	State{
        name = "frozen",
        tags = {"busy", "frozen"},

        onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("frozen")
            inst.SoundEmitter:PlaySound("dontstarve/common/freezecreature")
            inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
        end,

        onexit = function(inst)
            inst.AnimState:ClearOverrideSymbol("swap_frozen")
        end,

        events=
        {
            EventHandler("onthaw", function(inst) inst.sg:GoToState("thaw") end ),
        },
    },

    State{
        name = "thaw",
        tags = {"busy", "thawing"},

        onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("frozen_loop_pst", true)
            inst.SoundEmitter:PlaySound("dontstarve/common/freezethaw", "thawing")
            inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("thawing")
            inst.AnimState:ClearOverrideSymbol("swap_frozen")
        end,

        events =
        {
            EventHandler("unfreeze", function(inst)
                if inst.sg.sg.states.hit then
                    inst.sg:GoToState("hit")
                else
                    inst.sg:GoToState("idle")
                end
            end ),
        },
    },

    State{
        name = "bomb",
        tags = { "attack", "busy", "bombing", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("nian_icing")
            inst.components.combat:StartAttack()
            inst.components.timer:StartTimer("bomb_cd", TUNING.MYTH_NIAN_BOMBCD)
            inst.sg.statemem.maxbombs = 3
            local max = picknearplayers(inst)
            if max >= 6 then
                inst.sg.statemem.maxbombs = 6
            elseif max > 3 then
                inst.sg.statemem.maxbombs = max
            end
        end,
        timeline =
		{
            TimeEvent(14 * FRAMES, function(inst) 
                inst:LaunchGooIcing() 
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
            end),
            TimeEvent(17 * FRAMES, function(inst) 
                inst:LaunchGooIcing() 
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
            end),
            TimeEvent(26 * FRAMES, function(inst) 
                inst:LaunchGooIcing() 
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
            end),
            TimeEvent(33 * FRAMES, function(inst) 
                if inst.sg.statemem.maxbombs > 3 then 
                    inst:LaunchGooIcing() 
                    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
                end
            end),
            TimeEvent(42 * FRAMES, function(inst) 
                if inst.sg.statemem.maxbombs > 4 then 
                    inst:LaunchGooIcing() 
                    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
                end
            end),
            TimeEvent(49 * FRAMES, function(inst) 
                if inst.sg.statemem.maxbombs > 5 then 
                    inst:LaunchGooIcing() 
                    inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
                end
            end),
		},
        events =
        {
            CommonHandlers.OnNoSleepAnimOver("idle"),
        },
    },

    State {
        name = "charge_pre",
        tags = {"busy", "canrotate", "charge"},

        onenter = function(inst, target)
            inst.Physics:Stop()
            inst.components.timer:StartTimer("charge_cd", TUNING.MYTH_NIAN_CHARGECD) --冲刺得cd
            inst.AnimState:SetBank("beefalo")
            inst.AnimState:SetBuild("nian_mount")
            inst.Transform:SetScale(1.2, 1.2, 1.2)
            inst.sg.statemem.target = target
            inst.sg.statemem.stopsteering = false
            inst.AnimState:PlayAnimation("mating_taunt1")
            inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/howl")
        end,

        onupdate = function(inst)
            local target = inst.sg.statemem.target
            if not inst.sg.statemem.stopsteering and target and target:IsValid() then
                inst:ForceFacePoint(target.Transform:GetWorldPosition())
            end
        end,

        timeline =
        {
            TimeEvent(1.3, function(inst)
                inst.sg.statemem.stopsteering = true
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("charge_loop", inst.sg.statemem.target)
            end),
        },
        onexit = function(inst)
            inst.Transform:SetScale(1, 1, 1)
            inst.AnimState:SetBank("warg")
            inst.AnimState:SetBuild("myth_nian")
        end
    },
    State {
        name = "charge_loop",
        tags = {"busy", "canrotate", "charge"},

        onenter = function(inst, target)
            inst.components.locomotor:Stop()
            --inst.Physics:ClearCollisionMask()
            --inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
            inst.AnimState:PlayAnimation("run_loop", true)
            inst.Physics:SetMotorVelOverride(TUNING.EYEOFTERROR_CHARGESPEED, 0, 0)
            inst.sg:SetTimeout(1.00)
            inst.sg.statemem.collisiontime = 0
            inst.sg.statemem.fxtime = 0
            inst.sg.statemem.target = target
        end,

        onupdate = function(inst, dt)
            inst.Physics:SetMotorVelOverride(TUNING.EYEOFTERROR_CHARGESPEED, 0, 0)
            if inst.sg.statemem.collisiontime <= 0 then
                local x,y,z = inst.Transform:GetWorldPosition()
                local ents = TheSim:FindEntities(x, y, z, 2, nil, CHARGE_LOOP_TARGET_CANT_TAGS)
                for _, ent in ipairs(ents) do
                    if ent ~= inst and ent:IsValid() then
                        if  ent.components.workable ~= nil and
                            ent.components.workable:CanBeWorked() and
                            ent.components.workable.action ~= ACTIONS.NET then
                            ent.components.workable:Destroy(inst)
                        end
                        if ent.components.health and not ent.components.health:IsDead() and ent.components.combat then
                            ent.components.combat:GetAttacked(inst,TUNING.MYTH_NIAN_CHARGEDANAGE,nil)
                        end
                    end
                end
                inst.sg.statemem.collisiontime = COLLIDE_TIME
            end
            inst.sg.statemem.collisiontime = inst.sg.statemem.collisiontime - dt

            if inst.sg.statemem.fxtime <= 0 then
                spawn_ground_fx(inst)

                inst.sg.statemem.fxtime = FX_TIME
            end
            inst.sg.statemem.fxtime = inst.sg.statemem.fxtime - dt
        end,
        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
            end),
            TimeEvent(12 * FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
            end),
            TimeEvent(24 * FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
            end),
        },
        onexit = function(inst)
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            --ChangeToCharacterPhysics(inst)
            inst.Physics:ClearMotorVelOverride()
            inst.components.locomotor:Stop()
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("charge_pst")
        end,
    },
    State {
        name = "charge_pst",
        tags = {"busy", "canrotate", "charge"},

        onenter = function(inst, target)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("run_pst")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
            --inst.SoundEmitter:PlaySound("terraria1/eyeofterror/charge_pst_sfx")
        end,
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    --TheInput:GetWorldEntityUnderMouse().sg:GoToState("forcefield")
    State {
        name = "forcefield",
        tags = {"busy", "canrotate", "forcefield"},

        onenter = function(inst, target)
            inst.already_sleepshield = true
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("sleep_pre")
            inst.AnimState:PushAnimation("sleep_loop")
            inst.components.health.externalabsorbmodifiers:SetModifier("nian_forcefield", TUNING.ABIGAIL_FORCEFIELD_ABSORPTION)
            inst.hasshield = true
        end,
        events =
        {
            EventHandler("healthdelta", function(inst,data)
                if inst.components.health:GetPercent() >= 1 then
                    inst.sg:GoToState("forcefield_pst")
                end
            end),
            EventHandler("firedamage", function(inst,data)
                inst.sg:GoToState("forcefield_pst")
            end),
            EventHandler("forcefield_stop", function(inst,data)
                local player  = picknearplayer(inst)
                if player then
                    inst.sg:GoToState("charge_pre", player)
                else
                    inst.sg:GoToState("forcefield_pst")
                end
            end),
        }, 
        timeline =
        {
            TimeEvent(0 * FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound(inst.sounds.sleep) 
            end),
            TimeEvent(0.8, function(inst) 
                inst.AnimState:OverrideSymbol("warg_facebase", "myth_nian_facebase", "warg_facebase")
            end),
            TimeEvent(1, function(inst) 
                inst.components.health:StartRegen(200, 1)
            end),
        },
        onexit = function(inst)
            inst.AnimState:ClearOverrideSymbol("warg_facebase")
            inst.hasshield = false
            inst.components.health.externalabsorbmodifiers:RemoveModifier("nian_forcefield")
            inst.components.health:StopRegen()
        end,
    },
    State {
        name = "forcefield_pst",
        tags = {"busy", "canrotate"},

        onenter = function(inst, target)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("sleep_pst")
        end,
        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
    ---睡觉！
    State {
        name = "sleep",
        tags = { "busy", "sleeping" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
        events =
        {
            EventHandler("animover", sleeponanimover),
            EventHandler("onwakeup", onwakeup),
        },
    },

    State {
        name = "sleeping",
        tags = { "busy", "sleeping" },
        onenter = onentersleeping,
        timeline =  {
            TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.sleep) end),
        },
        events =
        {
            EventHandler("animover", sleeponanimover),
            EventHandler("onwakeup", onwakeup),
            EventHandler("healthdelta", function(inst,data)
                if data and data.oldpercent < 1 and   data.newpercent >= 1 then
                    if inst.components.sleeper:IsAsleep() then
                        inst.components.sleeper:WakeUp()
                    end
                end
            end),
        },
        onexit = function(inst)
            inst.AnimState:ClearOverrideSymbol("warg_facebase")
        end
    },
    State {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
        events =
        {
            EventHandler("animover", idleonanimover),
        },
    },

    State {
        name = "startled",
        tags = { "busy", },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("frozen_loop_pst",true)
            inst.sg:SetTimeout(5)
        end,
        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,
    },
    State {
        name = "bombboom",
        tags = { "busy", },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("howl")
        end,
        timeline =  {
            TimeEvent(0.3, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.howl) end),
            TimeEvent(1, function(inst) 
                inst:DoHowl() 
                inst.components.timer:StartTimer("bombboom", TUNING.MYTH_NIAN_BOOMCD)
            end),
        },
        events =
        {
            EventHandler("animover", idleonanimover),
        },
    },

    State {
        name = "nian_attack",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:SetBank("beefalo")
            inst.AnimState:SetBuild("nian_mount")
            inst.AnimState:PlayAnimation("atk_pre")
            inst.AnimState:PushAnimation("atk", false)
            inst.components.combat:StartAttack()
            inst.sg.statemem.target = target
            inst.Transform:SetScale(1.2, 1.2, 1.2)
        end,
        timeline ={
            TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end),
            TimeEvent(12 * FRAMES, function(inst) 
                inst.components.combat:DoAttack()
            end),
        },
        onexit = function(inst)
            inst.AnimState:SetBank("warg")
            inst.AnimState:SetBuild("myth_nian")
            inst.Transform:SetScale(1, 1, 1)
        end,
        events =
        {
            EventHandler("animover", idleonanimover),
        },
    },
}

CommonStates.AddCombatStates(states,
{
    hittimeline =
    {
        TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.hit) end),
    },
    attacktimeline =
    {
        TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end),
        TimeEvent(12 * FRAMES, function(inst) 
            inst.components.combat:DoAttack()
        end),
    },
    deathtimeline =
    {
        TimeEvent(0 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.death)
        end),
    },
})
CommonStates.AddRunStates(states,
{
    starttimeline = {},
    runtimeline =
    {
        TimeEvent(5 * FRAMES, function(inst)
            PlayFootstep(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/deer/chain")
        end),
    },
    endtimeline = {},
})
--CommonStates.AddFrozenStates(states)

return StateGraph("SGmyth_nian", states, events, "idle", actionhandlers)
