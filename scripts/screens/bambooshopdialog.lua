local Widget = require "widgets/widget"
local Screen = require "widgets/screen"
local Button = require "widgets/button"
local AnimButton = require "widgets/animbutton"
local Menu = require "widgets/menu"
local Text = require "widgets/text"
local Image = require "widgets/image"
local ImageButton = require "widgets/imagebutton"
local UIAnim = require "widgets/uianim"
local Widget = require "widgets/widget"
local TEMPLATES = require "widgets/templates"
local TEMPLATES2 = require "widgets/redux/templates"
local ItemImage = require "widgets/redux/itemimage"
local ClothingExplorerPanel = require "widgets/redux/clothingexplorerpanel"
local AccountItemFrame = require "widgets/redux/accountitemframe"
local ScrollableList = require "widgets/scrollablelist"
local PopupDialogScreen = require "screens/redux/popupdialog"
local HoverText = require "widgets/hoverer"
local TextEdit = require "widgets/textedit"

-- (破绽多) hungry: 饥饿而抚摸肚子
-- (破绽多) eat: 吃东西
-- (破绽多) emote_bow: 行礼（女）
-- (破绽多) idle_scared: 恐惧中
-- (破绽少) idle_happy: 在快乐地摇摆
-- (无破绽) idle_angry: 很做作地生气
-- (无破绽) idle_creepy: 被吓到了（拒绝状）
-- (无破绽) idle_loop: 默认站立着
-- (破绽少) emote_hat: 行礼（男）
-- (无破绽) hit: 被攻击时
-- (无破绽) interact: 给予玩家东西，看起来也像在拒绝
-- (破绽少) pig_pickup: 从地上捡起东西
-- (破绽少) sleep_pst: 睡醒起来
-- (无破绽) transform_pig_were: 很像在憋屎
-- (破绽少) abandon：有点像在赶玩家走的动作

local SHOPUIDATA = {
    rareitem = {
        name = "珍奇小摊",
        countkind_buy = 8,
        countkind_sell = 8,
        npc = {
            bank = "townspig",
            build = "pandaman_myth_rareitem",
            anim = "idle_loop",
            anim_refuse = "idle_angry", --这个动作只有在被玩家调戏时使用
            anim_happy = "interact", --很简单地给东西
            anim_error = "idle_angry", --他很自大
            anim_welcom = "pig_pickup", --进门时，他正在把秘密玩意收起来
            color_word = UICOLOURS.RED,
        }
    },
    ingredient = {
        name = "菜市小铺",
        countkind_buy = 8,
        countkind_sell = 8,
        npc = {
            bank = "townspig",
            build = "pandaman_myth_ingredient",
            anim = "idle_loop",
            anim_refuse = "abandon",
            anim_happy = "idle_happy", --很高兴有人来买东西
            anim_error = "idle_creepy",
            anim_welcom = "idle_happy", --进门时，很热情
            color_word = WEBCOLOURS.SPRINGGREEN,
        }
    },
    animals = {
        name = "花鸟小铺",
        countkind_buy = 8,
        countkind_sell = 8,
        npc = {
            bank = "townspig",
            build = "pandaman_myth_animals",
            anim = "idle_loop",
            anim_refuse = "abandon",
            anim_happy = "idle_happy",
            anim_error = "idle_creepy",
            anim_welcom = "sleep_pst", --进门时，在打盹呢
            color_word = WEBCOLOURS.GREEN,
        }
    },
    plants = {
        name = "禾种小铺",
        countkind_buy = 8,
        countkind_sell = 8,
        npc = {
            bank = "townspig",
            build = "pandaman_myth_plants",
            anim = "idle_loop",
            anim_refuse = "abandon",
            anim_happy = "idle_happy",
            anim_error = "idle_creepy",
            anim_welcom = "pig_pickup", --进门时，在整理植株
            color_word = WEBCOLOURS.TURQUOISE,
        }
    },
    foods = {
        name = "茶肴小铺",
        countkind_buy = 12,
        countkind_sell = 4,
        npc = {
            bank = "townspig",
            build = "pandaman_myth_foods",
            anim = "idle_loop",
            anim_refuse = "interact",
            anim_happy = "idle_happy",
            anim_error = "interact",
            anim_welcom = "idle_happy", --进门时，热情
            color_word = WEBCOLOURS.KHAKI,
        }
    },
    weapons = {
        name = "铸匠小铺",
        countkind_buy = 4,
        -- countkind_sell = 8,
        npc = {
            bank = "townspig",
            build = "pandaman_myth_weapons",
            anim = "idle_loop",
            anim_refuse = "abandon",
            anim_happy = "interact",
            anim_error = "idle_angry", --凶巴巴的
            anim_welcom = "transform_pig_were", --进门时，在憋气
            color_word = WEBCOLOURS.BURLYWOOD,
        }
    },
    numerology = {
        name = "算命小铺",
        countkind_buy = 4,
        -- countkind_sell = 8,
        npc = {
            bank = "townspig",
            build = "pandaman_myth_numerology",
            anim = "idle_loop",
            anim_refuse = "emote_hat",
            anim_happy = "idle_happy",
            anim_error = "idle_creepy", --这生意赚不了几个钱
            anim_welcom = "sleep_pst", --进门时，在打盹，生意太差了
            color_word = WEBCOLOURS.MEDIUMPURPLE,
        }
    },
    construct = {
        name = "材瓦小铺",
        countkind_buy = 12,
        -- countkind_sell = 8,
        npc = {
            bank = "townspig",
            build = "pandaman_myth_construct",
            anim = "idle_loop",
            anim_refuse = "interact",
            anim_happy = "idle_happy",
            anim_error = "interact",
            anim_welcom = "idle_happy", --好热情的大叔
            color_word = WEBCOLOURS.DARKRED,
        }
    },
}

local function InitMyCoins(self, oversize, offset_x, offset_y)
    self.coin_icon = self.proot:AddChild(Image("images/bbshop/ui_bs_coinbag.xml", "ui_bs_coinbag.tex"))
    self.coin_icon:ScaleToSize(184*oversize, 164*oversize)
    self.coin_icon:SetPosition(offset_x+218, offset_y-133)
    self.coin_value = self.proot:AddChild(Text(CHATFONT_OUTLINE, 24, "0", { 255/255, 243/255, 161/255, 1 }))
    self.coin_value:SetPosition(offset_x+226, offset_y-143)
    self.coin_value:SetHAlign(ANCHOR_LEFT)
end

local BambooShopDialog = Class(Screen, function(self, owner, shop)
	Screen._ctor(self, "BambooShopDialog")

    self.owner = owner
    if shop then
        self.shop = shop
        self.shoptype = shop.bbshoptype or "rareitem"
    else
        self.shoptype = "rareitem"
    end
    self.data_allitems = BBSHOPDATA[self.shoptype] or BBSHOPDATA["rareitem"]
    self.coins = 0

    self.data_shopui = SHOPUIDATA[self.shoptype] or SHOPUIDATA["rareitem"]
    if self.data_shopui.npc.say_welcom == nil then
        local newwords = STRINGS.BBSHOP.WORDS[self.shoptype] or STRINGS.BBSHOP.WORDS["rareitem"]
        for k,v in pairs(newwords) do
            if v ~= nil then
                self.data_shopui.npc[k] = v
            end
        end
    end

    --总体透明背景：玩家点击时就关闭整体界面
    local blackall = self:AddChild(ImageButton("images/global.xml", "square.tex"))
    blackall.image:SetVRegPoint(ANCHOR_MIDDLE)
    blackall.image:SetHRegPoint(ANCHOR_MIDDLE)
    blackall.image:SetVAnchor(ANCHOR_MIDDLE)
    blackall.image:SetHAnchor(ANCHOR_MIDDLE)
    blackall.image:SetScaleMode(SCALEMODE_FILLSCREEN)
    blackall.image:SetTint(0,0,0,.5)
    blackall:SetOnClick(function() TheFrontEnd:PopScreen() end)
    blackall:SetHelpTextMessage("")

    self.proot = self:AddChild(Widget("ROOT"))
    self.proot:SetScaleMode(SCALEMODE_PROPORTIONAL)
    self.proot:SetHAnchor(ANCHOR_MIDDLE)
    self.proot:SetVAnchor(ANCHOR_MIDDLE)
	self.proot:SetPosition(0, -25)

    local offset_x = 130
    local offset_y = -40
    local oversize = 0.6
    if self.shoptype == "rareitem" then
        --背景
        self.bg = self.proot:AddChild(Image("images/bbshop/ui_bs_bg.xml", "ui_bs_bg.tex"))
        self.bg:ScaleToSize(757*oversize, 550*oversize)
        self.bg:SetPosition(offset_x, offset_y)

        --牌子：售
        self.ui_buy = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_buy.xml", "ui_bs_tag_buy.tex"))
        self.ui_buy:ScaleToSize(61*oversize, 127*oversize)
        self.ui_buy:SetPosition(offset_x-200, offset_y+70)

        --牌子：收
        self.ui_sell = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_sell.xml", "ui_bs_tag_sell.tex"))
        self.ui_sell:ScaleToSize(61*oversize, 127*oversize)
        self.ui_sell:SetPosition(offset_x-200, offset_y-62)

        --灯笼
        self.ui3 = self.proot:AddChild(Image("images/bbshop/ui_bs_1_3.xml", "ui_bs_1_3.tex"))
        self.ui3:ScaleToSize(265*oversize, 415*oversize)
        self.ui3:SetPosition(offset_x+227, offset_y+32)

        --屋顶
        self.ui1 = self.proot:AddChild(Image("images/bbshop/ui_bs_1_1.xml", "ui_bs_1_1.tex"))
        self.ui1:ScaleToSize(1046*oversize, 283*oversize)
        self.ui1:SetPosition(offset_x+5, offset_y+208)

        --屋顶：龙灯笼
        self.ui4 = self.proot:AddChild(Image("images/bbshop/ui_bs_1_4.xml", "ui_bs_1_4.tex"))
        self.ui4:ScaleToSize(796*oversize, 311*oversize)
        self.ui4:SetPosition(offset_x+62, offset_y+245)

        --摆设
        self.ui2 = self.proot:AddChild(Image("images/bbshop/ui_bs_1_2.xml", "ui_bs_1_2.tex"))
        self.ui2:ScaleToSize(227*oversize, 270*oversize)
        self.ui2:SetPosition(offset_x-213, offset_y-125)
    elseif self.shoptype == "ingredient" then
        --屋顶：灯笼杆
        self.ui2 = self.proot:AddChild(Image("images/bbshop/ui_bs_2_2.xml", "ui_bs_2_2.tex"))
        self.ui2:ScaleToSize(108*oversize, 400*oversize)
        self.ui2:SetPosition(offset_x+225, offset_y+210)

        --背景
        self.bg = self.proot:AddChild(Image("images/bbshop/ui_bs_bg.xml", "ui_bs_bg.tex"))
        self.bg:ScaleToSize(757*oversize, 550*oversize)
        self.bg:SetPosition(offset_x, offset_y)

        --牌子：售
        self.ui_buy = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_buy.xml", "ui_bs_tag_buy.tex"))
        self.ui_buy:ScaleToSize(61*oversize, 127*oversize)
        self.ui_buy:SetPosition(offset_x-200, offset_y+70)

        --牌子：收
        self.ui_sell = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_sell.xml", "ui_bs_tag_sell.tex"))
        self.ui_sell:ScaleToSize(61*oversize, 127*oversize)
        self.ui_sell:SetPosition(offset_x-200, offset_y-52)

        --屋顶：玉米
        self.ui4 = self.proot:AddChild(Image("images/bbshop/ui_bs_2_4.xml", "ui_bs_2_4.tex"))
        self.ui4:ScaleToSize(123*oversize, 247*oversize)
        self.ui4:SetPosition(offset_x-244, offset_y+103)

        --屋顶：辣椒
        self.ui5 = self.proot:AddChild(Image("images/bbshop/ui_bs_2_5.xml", "ui_bs_2_5.tex"))
        self.ui5:ScaleToSize(90*oversize, 221*oversize)
        self.ui5:SetPosition(offset_x+188, offset_y+88)

        --屋顶
        self.ui1 = self.proot:AddChild(Image("images/bbshop/ui_bs_2_1.xml", "ui_bs_2_1.tex"))
        self.ui1:ScaleToSize(1030*oversize, 305*oversize)
        self.ui1:SetPosition(offset_x+5, offset_y+210)

        --屋顶：灯笼
        self.ui3 = self.proot:AddChild(Image("images/bbshop/ui_bs_2_3.xml", "ui_bs_2_3.tex"))
        self.ui3:ScaleToSize(257*oversize, 640*oversize)
        self.ui3:SetPosition(offset_x+250, offset_y+120)

        --摆设1
        self.ui7 = self.proot:AddChild(Image("images/bbshop/ui_bs_2_7.xml", "ui_bs_2_7.tex"))
        self.ui7:ScaleToSize(222*oversize, 212*oversize)
        self.ui7:SetPosition(offset_x-241, offset_y-125)

        --摆设2
        self.ui6 = self.proot:AddChild(Image("images/bbshop/ui_bs_2_6.xml", "ui_bs_2_6.tex"))
        self.ui6:ScaleToSize(227*oversize, 170*oversize)
        self.ui6:SetPosition(offset_x-211, offset_y-163)
    elseif self.shoptype == "animals" then
        --背景
        self.bg = self.proot:AddChild(Image("images/bbshop/ui_bs_bg.xml", "ui_bs_bg.tex"))
        self.bg:ScaleToSize(757*oversize, 550*oversize)
        self.bg:SetPosition(offset_x, offset_y)

        --牌子：售
        self.ui_buy = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_buy.xml", "ui_bs_tag_buy.tex"))
        self.ui_buy:ScaleToSize(61*oversize, 127*oversize)
        self.ui_buy:SetPosition(offset_x-200, offset_y+70)

        --牌子：收
        self.ui_sell = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_sell.xml", "ui_bs_tag_sell.tex"))
        self.ui_sell:ScaleToSize(61*oversize, 127*oversize)
        self.ui_sell:SetPosition(offset_x-200, offset_y-62)

        --灯笼
        self.ui2 = self.proot:AddChild(Image("images/bbshop/ui_bs_3_2.xml", "ui_bs_3_2.tex"))
        self.ui2:ScaleToSize(242*oversize, 440*oversize)
        self.ui2:SetPosition(offset_x+257, offset_y+40)

        --屋顶
        self.ui1 = self.proot:AddChild(Image("images/bbshop/ui_bs_3_1.xml", "ui_bs_3_1.tex"))
        self.ui1:ScaleToSize(1007*oversize, 271*oversize)
        self.ui1:SetPosition(offset_x+5, offset_y+186)

        --屋顶：鸟笼-蓝
        self.ui4 = self.proot:AddChild(Image("images/bbshop/ui_bs_3_4.xml", "ui_bs_3_4.tex"))
        self.ui4:ScaleToSize(175*oversize, 265*oversize)
        self.ui4:SetPosition(offset_x+90, offset_y+249)

        --屋顶：鸟笼-红
        self.ui3 = self.proot:AddChild(Image("images/bbshop/ui_bs_3_3.xml", "ui_bs_3_3.tex"))
        self.ui3:ScaleToSize(157*oversize, 245*oversize)
        self.ui3:SetPosition(offset_x+168, offset_y+232)

        --屋顶：鸟笼-黄
        self.ui5 = self.proot:AddChild(Image("images/bbshop/ui_bs_3_5.xml", "ui_bs_3_5.tex"))
        self.ui5:ScaleToSize(166*oversize, 274*oversize)
        self.ui5:SetPosition(offset_x-223, offset_y-115)
    elseif self.shoptype == "plants" then
        --背景
        self.bg = self.proot:AddChild(Image("images/bbshop/ui_bs_bg.xml", "ui_bs_bg.tex"))
        self.bg:ScaleToSize(757*oversize, 550*oversize)
        self.bg:SetPosition(offset_x, offset_y)

        --牌子：售
        self.ui_buy = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_buy.xml", "ui_bs_tag_buy.tex"))
        self.ui_buy:ScaleToSize(61*oversize, 127*oversize)
        self.ui_buy:SetPosition(offset_x-200, offset_y+70)

        --牌子：收
        self.ui_sell = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_sell.xml", "ui_bs_tag_sell.tex"))
        self.ui_sell:ScaleToSize(61*oversize, 127*oversize)
        self.ui_sell:SetPosition(offset_x-200, offset_y-52)

        --灯笼
        self.ui2 = self.proot:AddChild(Image("images/bbshop/ui_bs_4_2.xml", "ui_bs_4_2.tex"))
        self.ui2:ScaleToSize(252*oversize, 351*oversize)
        self.ui2:SetPosition(offset_x+255, offset_y+44)

        --屋顶
        self.ui1 = self.proot:AddChild(Image("images/bbshop/ui_bs_4_1.xml", "ui_bs_4_1.tex"))
        self.ui1:ScaleToSize(1022*oversize, 242*oversize)
        self.ui1:SetPosition(offset_x+5, offset_y+196)

        --屋顶：种子盘左
        self.ui3 = self.proot:AddChild(Image("images/bbshop/ui_bs_4_3.xml", "ui_bs_4_3.tex"))
        self.ui3:ScaleToSize(234*oversize, 165*oversize)
        self.ui3:SetPosition(offset_x-2, offset_y+182)

        --屋顶：种子盘右
        self.ui4 = self.proot:AddChild(Image("images/bbshop/ui_bs_4_4.xml", "ui_bs_4_4.tex"))
        self.ui4:ScaleToSize(270*oversize, 190*oversize)
        self.ui4:SetPosition(offset_x+154, offset_y+222)

        --摆设
        self.ui5 = self.proot:AddChild(Image("images/bbshop/ui_bs_4_5.xml", "ui_bs_4_5.tex"))
        self.ui5:ScaleToSize(232*oversize, 237*oversize)
        self.ui5:SetPosition(offset_x-243, offset_y-135)
    elseif self.shoptype == "foods" then
        --旗子
        self.ui3 = self.proot:AddChild(Image("images/bbshop/ui_bs_5_3.xml", "ui_bs_5_3.tex"))
        self.ui3:ScaleToSize(280*oversize, 472*oversize)
        self.ui3:SetPosition(offset_x+277, offset_y+134)

        --背景
        self.bg = self.proot:AddChild(Image("images/bbshop/ui_bs_bg7.xml", "ui_bs_bg7.tex"))
        self.bg:ScaleToSize(757*oversize, 550*oversize)
        self.bg:SetPosition(offset_x, offset_y)

        --牌子：售
        self.ui_buy = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_buy.xml", "ui_bs_tag_buy.tex"))
        self.ui_buy:ScaleToSize(61*oversize, 127*oversize)
        self.ui_buy:SetPosition(offset_x-200, offset_y+70)

        --牌子：收
        self.ui_sell = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_sell.xml", "ui_bs_tag_sell.tex"))
        self.ui_sell:ScaleToSize(61*oversize, 127*oversize)
        self.ui_sell:SetPosition(offset_x-200, offset_y-96)

        --灯笼
        self.ui2 = self.proot:AddChild(Image("images/bbshop/ui_bs_5_2.xml", "ui_bs_5_2.tex"))
        self.ui2:ScaleToSize(226*oversize, 395*oversize)
        self.ui2:SetPosition(offset_x+233, offset_y+47)

        --屋顶
        self.ui1 = self.proot:AddChild(Image("images/bbshop/ui_bs_5_1.xml", "ui_bs_5_1.tex"))
        self.ui1:ScaleToSize(911*oversize, 230*oversize)
        self.ui1:SetPosition(offset_x+3, offset_y+196)

        --摆设
        self.ui4 = self.proot:AddChild(Image("images/bbshop/ui_bs_5_4.xml", "ui_bs_5_4.tex"))
        self.ui4:ScaleToSize(184*oversize, 292*oversize)
        self.ui4:SetPosition(offset_x-250, offset_y-105)
    elseif self.shoptype == "weapons" then
        --火炉
        self.ui2 = self.proot:AddChild(Image("images/bbshop/ui_bs_6_2.xml", "ui_bs_6_2.tex"))
        self.ui2:ScaleToSize(330*oversize, 798*oversize)
        self.ui2:SetPosition(offset_x+224, offset_y+68)

        --背景
        self.bg = self.proot:AddChild(Image("images/bbshop/ui_bs_bg6.xml", "ui_bs_bg6.tex"))
        self.bg:ScaleToSize(757*oversize, 550*oversize)
        self.bg:SetPosition(offset_x, offset_y)

        --牌子：修
        self.ui_fix = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_fix.xml", "ui_bs_tag_fix.tex"))
        self.ui_fix:ScaleToSize(61*oversize, 127*oversize)
        self.ui_fix:SetPosition(offset_x-200, offset_y+12)

        --牌子：售
        self.ui_buy = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_buy.xml", "ui_bs_tag_buy.tex"))
        self.ui_buy:ScaleToSize(61*oversize, 127*oversize)
        self.ui_buy:SetPosition(offset_x-200, offset_y+101)

        --字牌
        self.ui3 = self.proot:AddChild(Image("images/bbshop/ui_bs_6_3.xml", "ui_bs_6_3.tex"))
        self.ui3:ScaleToSize(87*oversize, 206*oversize)
        self.ui3:SetPosition(offset_x+250, offset_y+80)

        --铁链
        self.ui4 = self.proot:AddChild(Image("images/bbshop/ui_bs_6_4.xml", "ui_bs_6_4.tex"))
        self.ui4:ScaleToSize(95*oversize, 198*oversize)
        self.ui4:SetPosition(offset_x+206, offset_y+82)

        --屋顶
        self.ui1 = self.proot:AddChild(Image("images/bbshop/ui_bs_6_1.xml", "ui_bs_6_1.tex"))
        self.ui1:ScaleToSize(934*oversize, 238*oversize)
        self.ui1:SetPosition(offset_x-6, offset_y+180)

        --铁毡
        self.ui5 = self.proot:AddChild(Image("images/bbshop/ui_bs_6_5.xml", "ui_bs_6_5.tex"))
        self.ui5:ScaleToSize(281*oversize, 150*oversize)
        self.ui5:SetPosition(offset_x-248, offset_y-152)

        --火钳
        self.ui6 = self.proot:AddChild(Image("images/bbshop/ui_bs_6_6.xml", "ui_bs_6_6.tex"))
        self.ui6:ScaleToSize(285*oversize, 219*oversize)
        self.ui6:SetPosition(offset_x-178, offset_y-138)
    elseif self.shoptype == "construct" then
        --摆设：砖
        self.ui5 = self.proot:AddChild(Image("images/bbshop/ui_bs_7_5.xml", "ui_bs_7_5.tex"))
        self.ui5:ScaleToSize(152*oversize, 136*oversize)
        self.ui5:SetPosition(offset_x-243, offset_y-114)

        --背景
        self.bg = self.proot:AddChild(Image("images/bbshop/ui_bs_bg7.xml", "ui_bs_bg7.tex"))
        self.bg:ScaleToSize(757*oversize, 550*oversize)
        self.bg:SetPosition(offset_x, offset_y)

        --牌子：售
        self.ui_buy = self.proot:AddChild(Image("images/bbshop/ui_bs_tag_buy.xml", "ui_bs_tag_buy.tex"))
        self.ui_buy:ScaleToSize(61*oversize, 127*oversize)
        self.ui_buy:SetPosition(offset_x-200, offset_y+70)

        --绳子
        self.ui2 = self.proot:AddChild(Image("images/bbshop/ui_bs_7_2.xml", "ui_bs_7_2.tex"))
        self.ui2:ScaleToSize(78*oversize, 244*oversize)
        self.ui2:SetPosition(offset_x-237, offset_y+113)

        --灯笼
        self.ui3 = self.proot:AddChild(Image("images/bbshop/ui_bs_7_3.xml", "ui_bs_7_3.tex"))
        self.ui3:ScaleToSize(242*oversize, 423*oversize)
        self.ui3:SetPosition(offset_x+250, offset_y+32)

        --屋顶
        self.ui1 = self.proot:AddChild(Image("images/bbshop/ui_bs_7_1.xml", "ui_bs_7_1.tex"))
        self.ui1:ScaleToSize(1114*oversize, 252*oversize)
        self.ui1:SetPosition(offset_x-3, offset_y+201)

        --摆设：瓦
        self.ui4 = self.proot:AddChild(Image("images/bbshop/ui_bs_7_4.xml", "ui_bs_7_4.tex"))
        self.ui4:ScaleToSize(345*oversize, 173*oversize)
        self.ui4:SetPosition(offset_x-197, offset_y-165)
    elseif self.shoptype == "numerology" then
        --背景
        self.bg = self.proot:AddChild(Image("images/bbshop/ui_bs_bg8.xml", "ui_bs_bg8.tex"))
        self.bg:ScaleToSize(825*oversize, 705*oversize)
        self.bg:SetPosition(offset_x, offset_y+40)

        --摆设
        self.ui3 = self.proot:AddChild(Image("images/bbshop/ui_bs_8_3.xml", "ui_bs_8_3.tex"))
        self.ui3:ScaleToSize(241*oversize, 190*oversize)
        self.ui3:SetPosition(offset_x-216, offset_y-145)

        return
    end

    ------拥有的金钱
    InitMyCoins(self, oversize, offset_x, offset_y)
end)

------

local function DoRpc(type, data, data2)
    local success, result  = pcall(json.encode, data)
	if success then
        SendModRPCToServer(GetModRPC("BBShop_m", "handleTrade"), type, result, data2)
	end
end

------

local function SetTaskInvItems(self)
    if self.task_invitems ~= nil then
        self.task_invitems:Cancel()
    end
    self.task_invitems = self.owner:DoPeriodicTask(1, function()
        self:GetInvItems(self.owner)
    end, 1)
end
local function AddInvItem(items, item)
    local num = items[item.prefab] or 0
    if item.replica.stackable ~= nil then
        num = num + (item.replica.stackable:StackSize() or 1)
    else
        num = num + 1
    end
    items[item.prefab] = num
end
local function GetContItems(items, item)
    local cont = {}
    if item.replica.container.classified ~= nil then
        cont = item.replica.container.classified
    else
        return
    end

    if cont._itemspreview ~= nil then
        for k, v in pairs(cont._itemspreview) do
            if v ~= nil then
                AddInvItem(items, v)
                if v.replica.container ~= nil then
                    GetContItems(items, v)
                end
            end
        end
    elseif cont._items ~= nil then
        for i, v in ipairs(cont._items) do
            if v ~= nil and v:value() ~= nil then
                local item = v:value()
                AddInvItem(items, item)
                if item.replica.container ~= nil then
                    GetContItems(items, item)
                end
            end
        end
    end
end

local function SetCoins(self)
    if self.items_inv ~= nil then
        local coins = 0
        if self.items_inv.myth_coin_box ~= nil then
            coins = coins + self.items_inv.myth_coin_box * 40
        end
        if self.items_inv.myth_coin ~= nil then
            coins = coins + self.items_inv.myth_coin
        end

        self.coins = coins
    else
        self.coins = 0
    end
end

function BambooShopDialog:GetInvItems(doer) --获取玩家携带的物品数量数据【客户端环境】
    local items = {}
    local inv = {}
    if doer.replica.inventory ~= nil and doer.replica.inventory.classified ~= nil then
        inv = doer.replica.inventory.classified
    end

    --鼠标栏物品
    if inv._activeitem ~= nil then
        AddInvItem(items, inv._activeitem)
        if inv._activeitem.replica.container ~= nil then
            GetContItems(items, inv._activeitem)
        end
    end

    --物品栏物品
    if inv._itemspreview ~= nil then
        for k, v in pairs(inv._itemspreview) do
            if v ~= nil then
                AddInvItem(items, v)
                if v.replica.container ~= nil then
                    GetContItems(items, v)
                end
            end
        end
    elseif inv._items ~= nil then
        for i, v in ipairs(inv._items) do
            if v ~= nil and v:value() ~= nil then
                local item = v:value()
                AddInvItem(items, item)
                if item.replica.container ~= nil then
                    GetContItems(items, item)
                end
            end
        end
    end

    --装备栏物品
    if inv._equipspreview ~= nil then
        for k, v in pairs(inv._equipspreview) do
            if v ~= nil then
                AddInvItem(items, v)
                if v.replica.container ~= nil then
                    GetContItems(items, v)
                end
            end
        end
    elseif inv._equips ~= nil then
        for k, v in pairs(inv._equips) do
            if v ~= nil and v:value() ~= nil then
                local item = v:value()
                AddInvItem(items, item)
                if item.replica.container ~= nil then
                    GetContItems(items, item)
                end
            end
        end
    end

    self.items_inv = items

    --更新钱
    SetCoins(self)
    if self.coin_value ~= nil then
        self.coin_value:SetString(tostring(self.coins))
    end

    --更新订购物品
    if self.items_sell ~= nil then
        for i = 1, self.data_shopui.countkind_sell, 1 do
            if self.items_sell[i] ~= nil then
                local item = self.items_sell[i]
                local count = self.items_inv[item.prefab] or 0
                if item.count ~= count then
                    item.count = count
                    if item.count_all > 0 and item.widget ~= nil and item.widget.cell_count_txt ~= nil then
                        item.widget.cell_count_txt:SetString(tostring(count))
                    end
                end
            end
        end
    end
end

local function SetTaskInvItems_weapons(self)
    if self.task_invitems ~= nil then
        self.task_invitems:Cancel()
    end
    self.task_invitems = self.owner:DoPeriodicTask(1, function()
        self:GetInvItems_weapons(self.owner)
    end, 1)
end
local function GetPercentText(value)
    -- value = value*100
    if value > 0 and value < 1 then
        value = 1
    elseif value > 100 then
        value = 100
    end
    return string.format("%2.0f%%", value)
end
local function GetItemPercentused(item)
    --这两种获取方式都是可以的
    -- local classified = item.replica and item.replica._ and
    --                        item.replica._.inventoryitem and
    --                        item.replica._.inventoryitem.classified
    local classified = item.replica and item.replica.inventoryitem and item.replica.inventoryitem.classified
    if classified ~= nil and classified.percentused ~= nil then
        return classified.percentused:value()
    end
    return nil
end
local function AddInvItem_weapons(self, items, coins, item)
    if self.data_allitems[item.prefab] ~= nil then
        if item.GUID ~= nil then
            local itemdata = self.data_allitems[item.prefab]
            if itemdata.fix ~= nil then
                local percentused = GetItemPercentused(item)
                if percentused ~= nil then
                    local newdata = items[item.GUID]
                    if newdata == nil then
                        if percentused >= 0 and percentused < 100 then --只添加需要维修的新数据
                            newdata = {
                                item = item, --把实体自己也存下来，为了当参数传给服务器
                                guid = item.GUID,
                                prefab = item.prefab,
                                percent = percentused,
                                value = itemdata.fix.value or 5,
                                idx = nil,
                                name = STRINGS.NAMES[string.upper(item.prefab)] or
                                            subfmt(STRINGS.UI.COOKBOOK.UNKNOWN_FOOD_NAME, {food = item.prefab or "SDF"})
                            }
                            newdata.img_tex = itemdata.img_tex or (item.prefab..".tex")
                            newdata.img_atlas = itemdata.img_atlas or GetInventoryItemAtlas(newdata.img_tex, true)
                            items[item.GUID] = newdata
                            table.insert(self.items_fix_idx, newdata)
                        end
                    else --对于旧数据，只更新数据，为了界面而不直接删除
                        newdata.percent = percentused
                        newdata.value = itemdata.fix.value or 5

                        --直接在这里先更新一下界面吧
                        if newdata.widget ~= nil then
                            if newdata.widget.cell_price_txt ~= nil then
                                if percentused >= 100 then
                                    newdata.widget.cell_price_txt:SetString("--")
                                    newdata.widget.cell_img_noitem:Show()
                                else
                                    newdata.widget.cell_price_txt:SetString(tostring(newdata.value))
                                    newdata.widget.cell_img_noitem:Hide()
                                end
                            end
                            if newdata.widget.cell_count_txt ~= nil then
                                newdata.widget.cell_count_txt:SetString(GetPercentText(percentused))
                            end
                        end
                    end
                end
            end
        end
    elseif item.prefab == "myth_coin_box" or item.prefab == "myth_coin" then
        AddInvItem(coins, item)
        return
    end

    if item.replica.container ~= nil and item.replica.container.classified ~= nil then
        local cont = item.replica.container.classified
        if cont._itemspreview ~= nil then
            for k, v in pairs(cont._itemspreview) do
                if v ~= nil then
                    AddInvItem_weapons(self, items, coins, v)
                end
            end
        elseif cont._items ~= nil then
            for i, v in ipairs(cont._items) do
                if v ~= nil and v:value() ~= nil then
                    AddInvItem_weapons(self, items, coins, v:value())
                end
            end
        end
    end
end
function BambooShopDialog:GetInvItems_weapons(doer) --获取玩家携带的物品guid以及耐久度数据【客户端环境】
    if self.items_fix == nil then --携带的待维修物品
        self.items_fix = {}
        self.items_fix_idx = {}
    end
    local inv = {}
    local coins = {} --携带的钱
    if doer.replica.inventory ~= nil and doer.replica.inventory.classified ~= nil then
        inv = doer.replica.inventory.classified
    end

    --鼠标栏物品
    if inv._activeitem ~= nil then
        AddInvItem_weapons(self, self.items_fix, coins, inv._activeitem)
    end

    --物品栏物品
    if inv._itemspreview ~= nil then
        for k, v in pairs(inv._itemspreview) do
            if v ~= nil then
                AddInvItem_weapons(self, self.items_fix, coins, v)
            end
        end
    elseif inv._items ~= nil then
        for i, v in ipairs(inv._items) do
            if v ~= nil and v:value() ~= nil then
                AddInvItem_weapons(self, self.items_fix, coins, v:value())
            end
        end
    end

    --装备栏物品
    if inv._equipspreview ~= nil then
        for k, v in pairs(inv._equipspreview) do
            if v ~= nil then
                AddInvItem_weapons(self, self.items_fix, coins, v)
            end
        end
    elseif inv._equips ~= nil then
        for k, v in pairs(inv._equips) do
            if v ~= nil and v:value() ~= nil then
                AddInvItem_weapons(self, self.items_fix, coins, v:value())
            end
        end
    end

    self.items_inv = coins
    local idx = 1
    for _,item in pairs(self.items_fix_idx) do
        if item ~= nil then
            item.idx = idx
            idx = idx + 1
        end
    end

    --更新钱
    SetCoins(self)
    if self.coin_value ~= nil then
        self.coin_value:SetString(tostring(self.coins))
    end
end

------

local function CanAddHoverPre(context)
    --滑动列表时不该生成hover
    if context ~= nil and context.grid and context.grid.current_scroll_pos == context.grid.target_scroll_pos then
        return true
    else
        return false
    end
end
local function NpcSayDesc(self, data, type)
    if data == nil then
        return
    end

    local words = data.desc
    if words == nil then
        if type == "fix" then
            if data.percent ~= nil then
                if data.percent < 100 then
                    words = STRINGS.BBSHOP.DESC.FIX
                else
                    words = STRINGS.BBSHOP.DESC.FIXED
                end
            end
        elseif type == "trea" then
            words = self.cluewords or STRINGS.BBSHOP.DESC.TREA
        else
            if type == "buy" then
                if data.count == nil or data.count <= 0 then
                    words = STRINGS.BBSHOP.DESC.BUY_NONE
                else
                    words = STRINGS.BBSHOP.DESC.BUY
                end
            elseif type == "sell" then
                if data.count_all == nil or data.count_all <= 0 then
                    words = STRINGS.BBSHOP.DESC.SELL_NONE
                else
                    words = STRINGS.BBSHOP.DESC.SELL
                end
            end
        end
    end

    if words ~= nil then
        self:NpcSay(words, 2, nil, true)
    end
end

function BambooShopDialog:NpcSay(words, wordtime, animkey, forcesay)
    if words == nil then
        return
    end

    if self.task_npcsay ~= nil then
        if forcesay then
            self.task_npcsay:Cancel()
        else
            return
        end
    end

    self.word_npc:SetMultilineTruncatedString(words, 3, 300, 65, true)
    --undo：声音呢！
    --ThePlayer.SoundEmitter:PlaySound("dontstarve/characters/winnie/talk_LP")
    --TheFrontEnd:GetSound():PlaySound("dontstarve/characters/wilton/talk_LP")
    --TheFrontEnd:GetSound():PlaySound("dontstarve/HUD/click_move")
    if self.owner and self.owner.SoundEmitter then
        --self.owner.SoundEmitter:PlaySound("dontstarve/characters/wilton/talk_LP")
    end
    if animkey then --不要互动了
        --self.anim_npc:GetAnimState():PlayAnimation(self.data_shopui.npc[animkey])
        --self.anim_npc:GetAnimState():PushAnimation(self.data_shopui.npc.anim, true)
    end
    self.task_npcsay = self.owner:DoTaskInTime(wordtime or 1, function()
        self.word_npc:SetString("")
        self.task_npcsay = nil
    end)
end

local function Discount(value, discount) --计算折扣(向上取整)
    return math.ceil(value*discount)
end
local function InitItems(self, type, resultdata, data, discount)
    for i = 1, self.data_shopui["countkind_"..type], 1 do
        local item = nil
        if data[i] ~= nil then
            local v = data[i]
            local itembase = self.data_allitems[v.prefab]
            if itembase ~= nil then
                item = resultdata[i] or {}
                if v.prefab ~= item.prefab then
                    item.prefab = v.prefab
                    item.name = STRINGS.NAMES[string.upper(v.prefab)] or
                                (
                                    itembase.nameoverride ~= nil and STRINGS.NAMES[string.upper(itembase.nameoverride)] or
                                    subfmt(STRINGS.UI.COOKBOOK.UNKNOWN_FOOD_NAME, {food = v.prefab or "SDF"})
                                )
                    item.img_tex = itembase.img_tex or (v.prefab..".tex")
                    item.img_atlas = itembase.img_atlas or GetInventoryItemAtlas(item.img_tex, true)
                    item.num_mix = itembase[type].num_mix or 1
                    item.desc = itembase.desc
                end

                item.value = Discount(itembase[type].value or 99, discount)

                if type == "sell" then --对于订购来说，count代表玩家拥有的，count_all才是订购数量
                    item.count_all = v.count or 0 --但是服务器传来的v.count是指订购数量
                    if self.items_inv ~= nil then
                        item.count = self.items_inv[item.prefab] or 0 --items_inv中的数量才是玩家拥有数量
                    else
                        item.count = 0
                    end
                else --对于销售来说，count_all无意义，count代表剩余库存
                    item.count_all = 0
                    item.count = v.count or 0
                end
                item.idx = i
                -- print("----"..tostring(v.prefab))
            end
        end
        resultdata[i] = item
    end
end
local function InitUISeasonSeeds(self, data)
    if data.item_seasonseeds ~= nil then
        self.item_seasonseeds = {
            count = data.item_seasonseeds.count or 0, --当前数量
            count_all = 0,
            value = Discount(data.item_seasonseeds.value or 1, self.discount), --价格
            num_mix = 1, --因为是只能买的，所以直接默认1即可
            prefab = "m_seasonseeds",
            servetype = 1, --特殊服务类型
            name = STRINGS.BBSHOP.BUTTON_SEASONSEEDS,
            desc = STRINGS.BBSHOP.DESC.SEASONSEEDS,
        }

        local w = self.proot:AddChild(Widget("w_seasonseeds"))
        w.data = self.item_seasonseeds
        self.w_seasonseeds = w
        w:SetPosition(-1, 130)
        w.cell_root = w:AddChild(ImageButton(
            "images/bbshop/ui_bs_seasonseeds.xml", "ui_bs_seasonseeds.tex", "ui_bs_seasonseeds.tex"
        ))
        local btnsize = 0.7
		w.cell_root:SetFocusScale(btnsize, btnsize)
		w.cell_root:SetNormalScale(btnsize, btnsize)
        w.cell_root.move_on_click = false --为了不触发按钮被点击时的下落效果
        w.cell_root.ongainfocusfn = function()
            w.hoverstay = true --鼠标从货物弹窗移入当前对应货物时，不应该删除弹窗
            if w.cell_root:IsEnabled() then
                local pos_y = -35
                if w.data.count > 0 and w.data.value <= self.coins then
                    self:AddSetableHover(w, "hover_seasonseeds", {
                        idx = -1, pos_x = -2, pos_y = pos_y,
                        fn_content = function(hover)
                            self:AddCounter(hover, w, -1, true) --数量调节器
                        end
                    })
                else
                    pos_y = pos_y - 16 --如果不能产生弹窗时，名字的位置会靠近一些
                end
                self:AddNameHover(w, "hover_name_root", { idx = -1, pos_x = -2, pos_y = pos_y+53 })
            end
        end
        w.cell_root.onlosefocusfn = function()
            w.hoverstay = nil
            if not w.mouseonhover then --鼠标移出货物时，如果没有移回对应弹窗，则删除弹窗
                if self.hover_seasonseeds ~= nil then
                    self.hover_seasonseeds:Kill()
                    self.hover_seasonseeds = nil
                end
                if self.hover_name_root ~= nil then
                    self.hover_name_root:Kill()
                    self.hover_name_root = nil
                end
            end
        end
        w.cell_root:SetOnClick(function()
            NpcSayDesc(self, w.data, "seasonseeds")
		end)

        --图片：无货物时的遮挡
        w.cell_img_noitem = w.cell_root:AddChild(Image("images/bbshop/ui_bs_slot_noitem.xml", "ui_bs_slot_noitem.tex"))
        w.cell_img_noitem:ScaleToSize(58, 54)
        -- w.cell_img_noitem:SetPosition(sets.offset_image.x, sets.offset_image.y)

        --文字：剩余数量
        w.cell_count_txt = w.cell_root:AddChild(Text(BODYTEXTFONT, 24, "1", UICOLOURS.WHITE))
        w.cell_count_txt:SetHAlign(ANCHOR_RIGHT)
        w.cell_count_txt:SetRegionSize(20, 33)
        w.cell_count_txt:SetPosition(25, -4)

        self:AddPriceTag(w, nil, { offset = { x=-8, y=-24 } })

        --更新界面函数（给数量调节器用的）
        w.ScrollWidgetSetData = function(context, widget, data, index)
            if data.count <= 0 then
                widget.cell_count_txt:SetString("")
                widget.cell_price_txt:SetString("--")
                widget.cell_img_noitem:Show()
            else
                widget.cell_count_txt:SetString(tostring(data.count))
                widget.cell_price_txt:SetString(tostring(data.value))
                widget.cell_img_noitem:Hide()
                if self.hover_seasonseeds ~= nil then
                    self.hover_seasonseeds.input_count:SetEditing(false) --重新判定数量
                end
            end
        end
        w.ScrollWidgetSetData(nil, w, w.data, -1)
    end
end
local function InitUINumerology_fate(self, data)
    local price = Discount(1, self.discount)

    local w = self.proot:AddChild(Widget("w_numerology_fate"))
    self.w_numerology_fate = w
    w:SetPosition(-59, -54)
    w.data = { name = STRINGS.BBSHOP.BUTTON_FATE, desc = STRINGS.BBSHOP.DESC.FATE, }
    w.cell_root = w:AddChild(ImageButton(
        "images/bbshop/ui_bs_fate.xml", "ui_bs_fate.tex", "ui_bs_fate.tex"
    ))
    local btnsize = 0.6
    w.cell_root:SetFocusScale(btnsize, btnsize)
    w.cell_root:SetNormalScale(btnsize, btnsize)
    w.cell_root.move_on_click = false --为了不触发按钮被点击时的下落效果
    w.cell_root.ongainfocusfn = function()
        w.hoverstay = true --鼠标从货物弹窗移入当前对应货物时，不应该删除弹窗
        if w.cell_root:IsEnabled() then
            local pos_y = -53
            if self.coins >= price then
                self:AddSetableHover(w, "hover_numerology_fate", {
                    idx = -1, pos_x = 0, pos_y = pos_y, bg_width = 66, bg_height = 36,
                    fn_content = function(hover)
                        hover.button = hover:AddChild(
                            ImageButton("images/global_redux.xml", "button_carny_long_normal.tex",
                                "button_carny_long_hover.tex", "button_carny_long_disabled.tex", "button_carny_long_down.tex")
                        )
                        hover.button.image:SetScale(.2, .4)
                        hover.button:SetFont(CHATFONT)
                        hover.button.text:SetColour(0,0,0,1)
                        hover.button:SetTextSize(20)
                        hover.button:SetText(STRINGS.BBSHOP.BUTTON_DO_FATE)
                        hover.button:SetPosition(0, 0)
                        hover.button:SetOnClick(function()
                            if self.coins >= price then
                                DoRpc(3, { servetype = 4 })

                                --延迟更新
                                SetTaskInvItems(self)
                                --更新钱（暂时的）
                                self.coins = self.coins - price
                                if self.coin_value ~= nil then
                                    self.coin_value:SetString(tostring(self.coins))
                                end
                                --更新hover
                                if self.coins < price and self.hover_numerology_fate ~= nil then
                                    self.hover_numerology_fate:Kill()
                                    self.hover_numerology_fate = nil
                                    w.mouseonhover = nil
                                end
                            end
                        end)
                    end
                })
            -- else
            --     pos_y = pos_y - 16 --如果不能产生弹窗时，名字的位置会靠近一些
            end
            self:AddNameHover(w, "hover_name_root", { idx = -1, pos_x = 0, pos_y = pos_y+53-16 })
        end
    end
    w.cell_root.onlosefocusfn = function()
        w.hoverstay = nil
        if not w.mouseonhover then --鼠标移出货物时，如果没有移回对应弹窗，则删除弹窗
            if self.hover_numerology_fate ~= nil then
                self.hover_numerology_fate:Kill()
                self.hover_numerology_fate = nil
            end
            if self.hover_name_root ~= nil then
                self.hover_name_root:Kill()
                self.hover_name_root = nil
            end
        end
    end
    w.cell_root:SetOnClick(function()
        NpcSayDesc(self, w.data, "fate")
    end)

    self:AddPriceTag(w, tostring(price), { offset = { x=1, y=-54 } })
end
local function InitUINumerology_trea(self)
    local price = Discount(40, self.discount)

    local w = self.proot:AddChild(Widget("w_numerology_trea"))
    self.w_numerology_trea = w
    w:SetPosition(127, 76)
    w.data = { name = STRINGS.BBSHOP.BUTTON_TREA, desc = nil, }
    w.cell_root = w:AddChild(ImageButton(
        "images/bbshop/ui_bs_trea.xml", "ui_bs_trea.tex", "ui_bs_trea.tex"
    ))

    local offset_x = 130
    local offset_y = -40
    local oversize = 0.6
    w.cell_root:SetFocusScale(oversize, oversize)
    w.cell_root:SetNormalScale(oversize, oversize)
    w.cell_root.move_on_click = false --为了不触发按钮被点击时的下落效果
    w.cell_root.ongainfocusfn = function()
        w.hoverstay = true --鼠标从货物弹窗移入当前对应货物时，不应该删除弹窗
        if w.cell_root:IsEnabled() then
            local pos_y = -45
            if self.coins >= price then
                self:AddSetableHover(w, "hover_numerology_trea", {
                    idx = -1, pos_x = 0, pos_y = -45, bg_width = 66, bg_height = 36,
                    fn_content = function(hover)
                        hover.button = hover:AddChild(
                            ImageButton("images/global_redux.xml", "button_carny_long_normal.tex",
                                "button_carny_long_hover.tex", "button_carny_long_disabled.tex", "button_carny_long_down.tex")
                        )
                        hover.button.image:SetScale(.2, .4)
                        hover.button:SetFont(CHATFONT)
                        hover.button.text:SetColour(0,0,0,1)
                        hover.button:SetTextSize(20)
                        hover.button:SetText(STRINGS.BBSHOP.BUTTON_DO_TREA)
                        hover.button:SetPosition(0, 0)
                        hover.button:SetOnClick(function()
                            if self.coins >= price then
                                DoRpc(3, { servetype = 3 })

                                --延迟更新
                                SetTaskInvItems(self)
                                --更新钱（暂时的）
                                self.coins = self.coins - price
                                if self.coin_value ~= nil then
                                    self.coin_value:SetString(tostring(self.coins))
                                end
                                --更新hover
                                if self.coins < price and self.hover_numerology_trea ~= nil then
                                    self.hover_numerology_trea:Kill()
                                    self.hover_numerology_trea = nil
                                    w.mouseonhover = nil
                                end

                                self:NpcSay(GetRandomItem(self.data_shopui.npc.say_treasure), 2, "anim_happy", false)
                            end
                        end)
                    end
                })
            -- else
            --     pos_y = pos_y - 16 --如果不能产生弹窗时，名字的位置会靠近一些
            end
            self:AddNameHover(w, "hover_name_root", { idx = -1, pos_x = 0, pos_y = pos_y+53-16 })
        end
    end
    w.cell_root.onlosefocusfn = function()
        w.hoverstay = nil
        if not w.mouseonhover then --鼠标移出货物时，如果没有移回对应弹窗，则删除弹窗
            if self.hover_numerology_trea ~= nil then
                self.hover_numerology_trea:Kill()
                self.hover_numerology_trea = nil
            end
            if self.hover_name_root ~= nil then
                self.hover_name_root:Kill()
                self.hover_name_root = nil
            end
        end
    end
    w.cell_root:SetOnClick(function()
        NpcSayDesc(self, w.data, "trea")
    end)

    self:AddPriceTag(w, tostring(price), { offset = { x=0, y=-69 } })

    --因为藏宝图在屋顶层级之下，所以只能后加载屋顶层级之上的界面
    --屋顶
    self.ui1 = self.proot:AddChild(Image("images/bbshop/ui_bs_8_1.xml", "ui_bs_8_1.tex"))
    self.ui1:ScaleToSize(911*oversize, 230*oversize)
    self.ui1:SetPosition(offset_x-5, offset_y+184)
    --旗子
    self.ui2 = self.proot:AddChild(Image("images/bbshop/ui_bs_8_2.xml", "ui_bs_8_2.tex"))
    self.ui2:ScaleToSize(231*oversize, 825*oversize)
    self.ui2:SetPosition(offset_x+270, offset_y+80)
    --拥有的金钱
    InitMyCoins(self, oversize, offset_x, offset_y)
end
function BambooShopDialog:GetClueWords(data)
    if data ~= nil and data.address ~= nil and data.time ~= nil and data.weather ~= nil then
        self.cluewords = subfmt(STRINGS.BBSHOP.CLUE[1], {
            address = STRINGS.BBSHOP.CLUEADDRESS[data.address] or "???",
            time = STRINGS.BBSHOP.CLUETIME[data.time] or "??",
            weather = STRINGS.BBSHOP.CLUEWEATHER[data.weather] or "??"
        })
        return
    end

    self.cluewords = nil
end
function BambooShopDialog:InitShop(data)
    ------店主的动画
    self.anim_npc = self.proot:AddChild(UIAnim())
	self.anim_npc:GetAnimState():SetScale(70, 70)
    self.anim_npc:GetAnimState():SetBank(self.data_shopui.npc.bank)
    self.anim_npc:GetAnimState():SetBuild(self.data_shopui.npc.build)
	self.anim_npc:GetAnimState():PlayAnimation(self.data_shopui.npc.anim, true)
	self.anim_npc:SetFacing(FACING_DOWN)
	self.anim_npc:SetPosition(-230, -240)
	self.anim_npc:SetClickable(true)
    self.anim_npc:Hide()
    self.inst:DoTaskInTime(0,function() --延迟显示
        self.anim_npc:Show()
    end)
    self.anim_npc.OnControl = function(down, control) --down是uianim自己，不是字符串
        if control == CONTROL_ACCEPT then --鼠标按下与弹回时
            if not self.anim_down then
                self.anim_down = true
                TheFrontEnd:GetSound():PlaySound("dontstarve/HUD/click_move")
            else
                self.anim_down = nil
                --拒绝的互动
                --self:NpcSay(GetRandomItem(self.data_shopui.npc.say_refuse), 1.3, "anim_refuse", false)
            end
        end
    end

    ------店主的话语
    self.word_npc = self.proot:AddChild(Text(CHATFONT_OUTLINE, 35, nil, self.data_shopui.npc.color_word))
    self.word_npc:SetPosition(-230, -10)
    self:NpcSay(GetRandomItem(self.data_shopui.npc.say_welcom), 1, "anim_welcom", false)

    ------注入数据
    if data == nil then
        return
    end

    self.discount = data.discount or 1

    if self.shoptype == "weapons" then
        self:GetInvItems_weapons(self.owner) --更新货物数据前，先更新物品栏数据
        SetTaskInvItems_weapons(self)

        if data.idx_items_buy ~= nil then
            self.items_buy = {}
            InitItems(self, "buy", self.items_buy, data.idx_items_buy, self.discount)
            self:InitItemListBuy({ pos = { x = 128, y = 6 }, })
        end
        self:InitItemListFix()
    elseif self.shoptype == "numerology" then
        self:GetClueWords(data.treasure) --生成线索台词
        InitUINumerology_trea(self)

        self:GetInvItems(self.owner) --更新货物数据前，先更新物品栏数据
        SetTaskInvItems(self)

        if data.idx_items_buy ~= nil then
            self.items_buy = {}
            InitItems(self, "buy", self.items_buy, data.idx_items_buy, self.discount)
            self:InitItemListBuy({ num_visible_rows = 0.8, pos = { x = 172, y = -92 }, offset_hover = { x = 38, y = -106 }, })
        end
        InitUINumerology_fate(self, data)
    elseif self.shoptype == "construct" then
        self:GetInvItems(self.owner) --更新货物数据前，先更新物品栏数据
        SetTaskInvItems(self)

        if data.idx_items_buy ~= nil then
            self.items_buy = {}
            InitItems(self, "buy", self.items_buy, data.idx_items_buy, self.discount)
            self:InitItemListBuy({ num_visible_rows = 2.8, pos = { x = 128, y = -26 }, })
        end
    elseif self.shoptype == "foods" then
        self:GetInvItems(self.owner) --更新货物数据前，先更新物品栏数据
        SetTaskInvItems(self)

        if data.idx_items_buy ~= nil then
            self.items_buy = {}
            InitItems(self, "buy", self.items_buy, data.idx_items_buy, self.discount)
            self:InitItemListBuy({ num_visible_rows = 2.8, pos = { x = 128, y = -22 }, })
        end
        if data.idx_items_sell ~= nil then
            self.items_sell = {}
            InitItems(self, "sell", self.items_sell, data.idx_items_sell, 1)
            self:InitItemListSell({ num_visible_rows = 0.8, pos = { x = 128, y = -158 }, offset_hover = { x = -8, y = -177 }, })
        end
    else
        self:GetInvItems(self.owner) --更新货物数据前，先更新物品栏数据
        SetTaskInvItems(self)

        if data.idx_items_buy ~= nil then
            self.items_buy = {}
            InitItems(self, "buy", self.items_buy, data.idx_items_buy, self.discount)
            self:InitItemListBuy()
        end
        if data.idx_items_sell ~= nil then
            self.items_sell = {}
            InitItems(self, "sell", self.items_sell, data.idx_items_sell, 1)
            self:InitItemListSell()
        end

        --季节种子包
        InitUISeasonSeeds(self, data)
    end

    --简单介绍
    -- self.w_desc = self.proot:AddChild(Widget("w_desc"))
    -- self.w_desc:SetPosition(-228, 133)
    -- self.w_desc_bg = self.w_desc:AddChild(Image("images/bbshop/ui_bbshop_btnbg.xml", "ui_bbshop_btnbg.tex"))
    -- self.w_desc_bg:ScaleToSize(180, 110)
    -- self.w_desc_name = self.w_desc:AddChild(Text(CHATFONT_OUTLINE, 28, "未知物品", self.data_shopui.npc.color_word))
    -- self.w_desc_name:SetPosition(0, 34)
    -- self.w_desc_desc = self.w_desc:AddChild(Text(CHATFONT_OUTLINE, 24, nil, nil))
    -- self.w_desc_desc:SetHAlign(ANCHOR_LEFT)
    -- self.w_desc_desc:SetPosition(0, -10)
    -- self.w_desc_desc:SetMultilineTruncatedString("这是介绍内容", 3, 170, 65, true)
end

------

local function CanHoverBuy(self, widget)
    if widget.data == nil or widget.data.count == nil or widget.data.count <= 0 then
        return false
    end
    if --判断能否买这个货物
        widget.data.value == nil or widget.data.value > self.coins
    then
        return false
    end

    return true
end
local function HandleTrade(self, isbuy, widget, hover, data, count)
    if isbuy then
        if data.value == nil or count <= 0 or data.count <= 0 then --卖光了
            --self:NpcSay(GetRandomItem(self.data_shopui.npc.say_buy_nocount), 1, "anim_error", false)
            return
        end
        if count > data.count then
            count = data.count
        end
        if data.value*count > self.coins then --钱不够
            --self:NpcSay(GetRandomItem(self.data_shopui.npc.say_buy_nocoin), 1, "anim_error", false)
            return
        end

        if data.servetype ~= nil then
            DoRpc(3, {
                num = count,
                servetype = data.servetype,
            })
        else
            DoRpc(1, {{ --之所以{{}}，是因为可以兼容一次性买多种物品
                prefab = data.prefab,
                num = count, --买的数量（排除了num_mix的，这个数量机制在服务器再弄）
            }})
        end

        --延迟更新
        SetTaskInvItems(self)
        --更新钱（暂时的）
        self.coins = self.coins - data.value*count
        if self.coin_value ~= nil then
            self.coin_value:SetString(tostring(self.coins))
        end
        --更新货架
        data.count = math.max(0, data.count-count)
        if widget.ScrollWidgetSetData ~= nil then
            widget.ScrollWidgetSetData(nil, widget, widget.data, hover.itemidx)
        end

        if math.random() < 0.3 then
            --self:NpcSay(GetRandomItem(self.data_shopui.npc.say_buy), 1, "anim_happy", false)
        end
    else
        if data.value == nil or data.count_all < data.num_mix then --不再需要了
            --self:NpcSay(GetRandomItem(self.data_shopui.npc.say_sell_noneed), 1, "anim_error", false)
            return
        end

        if count < data.num_mix then --物品不够
            --self:NpcSay(GetRandomItem(self.data_shopui.npc.say_sell_nocount), 1, "anim_error", false)
            return
        end

        if count > data.count then
            count = data.count
        end
        if count > data.count_all then
            count = data.count_all
        end
        count = math.floor(count/data.num_mix) * data.num_mix

        DoRpc(2, {{
            prefab = data.prefab,
            num = count, --卖的数量（加了num_mix的，服务器会再验证）
        }})

        --延迟更新
        SetTaskInvItems(self)
        --钱在卖东西这里不影响任何界面机制，所以不需要提前更新
        --更新货架
        data.count = math.max(0, data.count-count)
        data.count_all = math.max(0, data.count_all-count)
        if widget.ScrollWidgetSetData ~= nil then
            widget.ScrollWidgetSetData(nil, widget, widget.data, hover.itemidx)
        end

        if math.random() < 0.3 then
            --self:NpcSay(GetRandomItem(self.data_shopui.npc.say_sell), 1, "anim_happy", false)
        end
    end
end

local function GetHoverOffsetY(row, height, y)
    if row > 1 then
        local a1, a2 = math.modf(row)
        if a2 <= 0 then --是整数，就不变
            return y
        else
            return y + a2*height --带有小数，就把小数偏移量加上
        end
    end
    return y
end
function BambooShopDialog:InitItemListBuy(sets)
    local uiname = "scroll_list_buy"

    if self[uiname] ~= nil then
        self[uiname]:Kill()
    end
    self[uiname] = self.proot:AddChild(Widget("list_buy_root"))

    if sets == nil then
        sets = {
            row_w = 88, row_h = 68,
            num_visible_rows = 1.8,
            -- fn_getBgName = function(index)end,
            scale_bg = { focus_x = 1.05, focus_y = 1.2, normal_x = 1.05, normal_y = 1.2 },
            offset_hover = { x = -8, y = 26 },
            -- fn_initUI = function(widget, index)end,
            offset = { x = 5, y = 0 },
            offset_image = { x = 0, y = 5 },
            offset_count = { x = 18, y = 3 },
            offset_price = { x = -10, y = -18 },
            pos = { x = 128, y = 10 },
        }
    else
        if sets.row_w == nil then sets.row_w = 88 end
        if sets.row_h == nil then sets.row_h = 68 end
        if sets.num_visible_rows == nil then sets.num_visible_rows = 1.8 end
        if sets.scale_bg == nil then sets.scale_bg = { focus_x = 1.05, focus_y = 1.2, normal_x = 1.05, normal_y = 1.2 } end
        if sets.offset_hover == nil then sets.offset_hover = { x = -8, y = 26 } end
        if sets.offset == nil then sets.offset = { x = 5, y = 0 } end
        if sets.offset_image == nil then sets.offset_image = { x = 0, y = 5 } end
        if sets.offset_count == nil then sets.offset_count = { x = 18, y = 3 } end
        if sets.offset_price == nil then sets.offset_price = { x = -10, y = -18 } end
        if sets.pos == nil then sets.pos = { x = 128, y = 10 } end
    end

    --初始化格子时
    local function ScrollWidgetsCtor(context, index)
        local w = Widget("shelve_buy_".. index)

        local bgname = nil
        if sets.fn_getBgName ~= nil then
            bgname = sets.fn_getBgName(index)
        else
            bgname = "ui_bs_shelve1"
        end

        --图片按钮：货架背景
        w.bgname = bgname
		w.cell_root = w:AddChild(ImageButton(
            "images/bbshop/"..bgname..".xml", bgname..".tex", bgname..".tex"
        ))
		w.cell_root:SetFocusScale(sets.scale_bg.focus_x, sets.scale_bg.focus_y)
		w.cell_root:SetNormalScale(sets.scale_bg.normal_x, sets.scale_bg.normal_y)
        w.cell_root.move_on_click = false --为了不触发按钮被点击时的下落效果
        w.cell_root:SetPosition(sets.offset.x, sets.offset.y)

        --图片：货物图片
        w.cell_img = w.cell_root:AddChild(Image("images/global.xml", "square.tex"))
        w.cell_img:ScaleToSize(48, 48)
        w.cell_img:SetPosition(sets.offset_image.x, sets.offset_image.y)

        --图片：无货物时的遮挡
        w.cell_img_noitem = w.cell_root:AddChild(Image("images/bbshop/ui_bs_slot_noitem.xml", "ui_bs_slot_noitem.tex"))
        w.cell_img_noitem:ScaleToSize(58, 54)
        w.cell_img_noitem:SetPosition(sets.offset_image.x, sets.offset_image.y)

        if sets.fn_initUI ~= nil then
            sets.fn_initUI(w, index)
        end

        --文字：剩余数量
        w.cell_count_txt = w.cell_root:AddChild(Text(BODYTEXTFONT, 24, "1", UICOLOURS.WHITE))
        w.cell_count_txt:SetHAlign(ANCHOR_RIGHT)
        w.cell_count_txt:SetRegionSize(20, 33)
        w.cell_count_txt:SetPosition(sets.offset_count.x, sets.offset_count.y)

        self:AddPriceTag(w, nil, { offset = sets.offset_price })
        w.cell_price.ongainfocusfn = function()
            self[uiname].grid:OnWidgetFocus(w)
            w.hoverstay = true --鼠标从货物弹窗移入当前对应货物时，不应该删除弹窗
            if w.cell_root:IsEnabled() and CanAddHoverPre(self[uiname]) then
                local pos_y = GetHoverOffsetY(self[uiname].grid.current_scroll_pos or 1, sets.row_h, sets.offset_hover.y)
                if CanHoverBuy(self, w) then
                    self:AddSetableHover(w, "hover_buy_root", {
                        idx = index, pos_x = sets.offset_hover.x, pos_y = pos_y,
                        fn_content = function(hover)
                            self:AddCounter(hover, w, index, true) --数量调节器
                        end
                    })
                else
                    pos_y = pos_y - 16 --如果不能产生弹窗时，名字的位置会靠近一些
                end
                self:AddNameHover(w, "hover_name_root", { idx = index, pos_x = sets.offset_hover.x, pos_y = pos_y+53 })
            end
        end
        w.cell_price.onlosefocusfn = function()
            w.hoverstay = nil
            if not w.mouseonhover then --鼠标移出货物时，如果没有移回对应弹窗，则删除弹窗
                if self.hover_buy_root ~= nil then
                    self.hover_buy_root:Kill()
                    self.hover_buy_root = nil
                end
                if self.hover_name_root ~= nil then
                    self.hover_name_root:Kill()
                    self.hover_name_root = nil
                end
            end
        end

		w.cell_root:SetOnClick(function()
            NpcSayDesc(self, w.data, "buy")
		end)

		return w
    end

    --更新数据时
    local function ScrollWidgetSetData(context, widget, data, index)
        if widget.ScrollWidgetSetData == nil then
            widget.ScrollWidgetSetData = ScrollWidgetSetData
        end
		widget.data = data
        local hover = nil
        if CanHoverBuy(self, widget) then
            if self.hover_buy_root ~= nil and self.hover_buy_root.itemidx == index then
                hover = self.hover_buy_root
            end
        else
            if self.hover_buy_root ~= nil and self.hover_buy_root.itemidx == index then
                self.hover_buy_root:Kill()
                self.hover_buy_root = nil
                widget.mouseonhover = nil
            end
        end

		if data ~= nil then
            data.widget = widget
			widget.cell_root:Show()
            if data.img_atlas ~= nil then
                widget.cell_img:SetTexture(data.img_atlas, data.img_tex)
            end
            if data.count <= 0 then
                widget.cell_count_txt:SetString("")
                widget.cell_price_txt:SetString("--")
                widget.cell_img_noitem:Show()
            else
                widget.cell_count_txt:SetString(tostring(data.count))
                widget.cell_price_txt:SetString(tostring(data.value))
                widget.cell_img_noitem:Hide()
                if hover ~= nil then
                    hover.input_count:SetEditing(false) --重新判定数量
                end
            end
			widget:Enable()
		else
			widget:Disable()
			widget.cell_root:Hide()
		end
	end

    local grid = TEMPLATES2.ScrollingGrid(
        self.items_buy, {
        context = {},
        widget_width  = sets.row_w,
        widget_height = sets.row_h,
        force_peek    = true,
        num_visible_rows = sets.num_visible_rows, --竖排
        num_columns      = 4, --横排
        item_ctor_fn = ScrollWidgetsCtor,
        apply_fn     = ScrollWidgetSetData,
        scrollbar_offset = 8,
        scrollbar_height_offset = -60
    })

	grid.up_button:SetTextures("images/quagmire_recipebook.xml", "quagmire_recipe_scroll_arrow_hover.tex")
    grid.up_button:SetScale(0.4)

	grid.down_button:SetTextures("images/quagmire_recipebook.xml", "quagmire_recipe_scroll_arrow_hover.tex")
    grid.down_button:SetScale(-0.4)

	grid.scroll_bar_line:SetTexture("images/quagmire_recipebook.xml", "quagmire_recipe_scroll_bar.tex")
	grid.scroll_bar_line:SetScale(.25, 0.25)

	grid.position_marker:SetTextures("images/quagmire_recipebook.xml", "quagmire_recipe_scroll_handle.tex")
	grid.position_marker.image:SetTexture("images/quagmire_recipebook.xml", "quagmire_recipe_scroll_handle.tex")
    grid.position_marker:SetScale(.4)

    self[uiname].grid = self[uiname]:AddChild(grid)
    self[uiname]:SetPosition(sets.pos.x, sets.pos.y)
end

function BambooShopDialog:AddSetableHover(widget, uiname, sets)
    if uiname == nil then
        uiname = "hover_set"
    end
    if self[uiname] ~= nil then
        if self[uiname].itemidx == sets.idx then --已经有这个hover了，不需要重建
            return
        end
        self[uiname]:Kill() --移除之前的hover
    end

    --弹窗背景
    local hover_root = self.proot:AddChild(Image("images/bbshop/ui_bbshop_btnbg.xml", "ui_bbshop_btnbg.tex"))
    hover_root:SetSize(sets.bg_width or 70, sets.bg_height or 90)
    self[uiname] = hover_root
    hover_root.itemidx = sets.idx
    hover_root.ongainfocusfn = function()
        widget.mouseonhover = true --鼠标从目标组件移入当前对应弹窗时，不应该删除弹窗
    end
    hover_root.onlosefocusfn = function()
        widget.mouseonhover = nil
        if not widget.hoverstay then --鼠标移出弹窗时没有移入对应目标组件，应该删除弹窗
            if self.hover_name_root ~= nil then
                self.hover_name_root:Kill()
                self.hover_name_root = nil
            end
            hover_root:Kill()
            self[uiname] = nil
        end
    end

    --不清楚官方怎么算的，但是相对位置是正确的，所以我得自己调整偏移量
    local pos = widget:GetPosition()
    hover_root:SetPosition(pos.x + sets.pos_x, pos.y + sets.pos_y)

    --内容物自定义函数
    if sets.fn_content ~= nil then
        sets.fn_content(hover_root)
    end
end
function BambooShopDialog:AddNameHover(widget, uiname, sets)
    if uiname == nil then
        uiname = "hover_set"
    end
    if self[uiname] ~= nil then
        if self[uiname].itemidx == sets.idx then --已经有这个hover了，不需要重建
            return
        end
        self[uiname]:Kill() --移除之前的hover
    end
    local hover_root = self.proot:AddChild(Widget(uiname))
    hover_root:SetPosition(-228, 133)
    hover_root.item_name = hover_root:AddChild(Text(CHATFONT_OUTLINE, 24, "", WEBCOLOURS.KHAKI))
    if widget.data ~= nil and widget.data.name ~= nil then
        hover_root.item_name:SetString(widget.data.name)
    end
    hover_root.itemidx = sets.idx
    self[uiname] = hover_root

    --不清楚官方怎么算的，但是相对位置是正确的，所以我得自己调整偏移量
    local pos = widget:GetPosition()
    hover_root:SetPosition(pos.x + sets.pos_x, pos.y + sets.pos_y)
end

function BambooShopDialog:AddCounter(hover, widget, index, isbuy, stepnum)
    if widget.data == nil then
        return
    end

    if stepnum == nil then
        stepnum = 1
    end

    ------数量输入框（不知道为啥明明是文件编辑组件，也无法进行编辑）
    hover.input_count = hover:AddChild(TextEdit(BODYTEXTFONT, 22, "1", UICOLOURS.WHITE))
    hover.input_count:SetTextLengthLimit(3)
    hover.input_count:SetForceEdit(true)
    hover.input_count:EnableWordWrap(false)
    hover.input_count:EnableScrollEditWindow(true)
    hover.input_count:SetHAlign(ANCHOR_MIDDLE)
    hover.input_count:SetCharacterFilter("0123456789")
    hover.input_count:SetColour(unpack(UICOLOURS.WHITE))
    hover.input_count.idle_text_color = UICOLOURS.WHITE
    hover.input_count.edit_text_color = UICOLOURS.WHITE
    hover.input_count:SetEditCursorColour(unpack(UICOLOURS.WHITE))
    hover.input_count:SetHAlign(ANCHOR_LEFT)
    hover.input_count:SetPosition(2, 33)
    if not widget.data.count or widget.data.count < stepnum then
        hover.count = 0
    else
        hover.count = stepnum
    end
    hover.input_count:SetString(hover.count)

    local SetEditing_old = hover.input_count.SetEditing
    hover.input_count.SetEditing = function(...)
        SetEditing_old(...)
        if hover.input_count.editing then
            if hover.count <= stepnum then
                hover.input_count:SetString("") --初次点击时，删除已有数字，方便玩家输入
            end
        else
            if not widget.data.count or widget.data.count < stepnum then
                hover.count = 0
            else
                local txt = hover.input_count:GetString()
                if not txt or txt == "" then
                    hover.count = stepnum
                else
                    local number = tonumber(txt)
                    if number == nil or number <= stepnum then
                        hover.count = stepnum
                    else
                        if number > widget.data.count then
                            hover.count = math.floor(widget.data.count/stepnum) * stepnum
                        else
                            hover.count = math.floor(number/stepnum) * stepnum
                        end
                    end
                end
            end
            hover.input_count:SetString(hover.count)
        end
    end

    ------数量递减按钮
    local spinnersize_x = 0.25
    local spinnersize_y = 0.21
    hover.spinner_left = hover:AddChild(ImageButton(
        "images/myth_bookinfo_bg.xml", "arrow_left_disabled.tex", "arrow2_left_over.tex"
    ))
    hover.spinner_left:SetFocusScale(spinnersize_x, spinnersize_y)
    hover.spinner_left:SetNormalScale(spinnersize_x, spinnersize_y)
    hover.spinner_left:SetPosition(-22, 33)
    hover.spinner_left:SetOnClick(function()
        if not widget.data.count or widget.data.count < stepnum then
            hover.count = 0
        elseif hover.count <= stepnum then
            hover.count = math.floor(widget.data.count/stepnum) * stepnum
        else
            hover.count = hover.count - stepnum
        end
        hover.input_count:SetString(hover.count)
    end)

    ------数量递增按钮
    hover.spinner_right = hover:AddChild(ImageButton(
        "images/myth_bookinfo_bg.xml", "arrow_right_disabled.tex", "arrow2_right_over.tex"
    ))
    hover.spinner_right:SetFocusScale(spinnersize_x, spinnersize_y)
    hover.spinner_right:SetNormalScale(spinnersize_x, spinnersize_y)
    hover.spinner_right:SetPosition(22, 33)
    hover.spinner_right:SetOnClick(function()
        if not widget.data.count or widget.data.count < stepnum then
            hover.count = 0
        elseif hover.count >= math.floor(widget.data.count/stepnum)*stepnum then
            hover.count = stepnum
        else
            hover.count = hover.count + stepnum
        end
        hover.input_count:SetString(hover.count)
    end)

    ------购买/出售按钮
    hover.button = hover:AddChild(
        ImageButton("images/global_redux.xml", "button_carny_long_normal.tex",
            "button_carny_long_hover.tex", "button_carny_long_disabled.tex", "button_carny_long_down.tex")
    )
    hover.button.image:SetScale(.2, .4)
    hover.button:SetFont(CHATFONT)
    hover.button.text:SetColour(0,0,0,1)
    hover.button:SetTextSize(20)
    hover.button:SetText(isbuy and STRINGS.BBSHOP.BUTTON_DO_BUY or STRINGS.BBSHOP.BUTTON_DO_SELL)
    hover.button:SetPosition(0, 3)
    hover.button:SetOnClick(function()
        HandleTrade(self, isbuy, widget, hover, widget.data, hover.count)
    end)

    ------购买/出售全部按钮
    hover.button_all = hover:AddChild(
        ImageButton("images/global_redux.xml", "button_carny_long_normal.tex",
            "button_carny_long_hover.tex", "button_carny_long_disabled.tex", "button_carny_long_down.tex")
    )
    hover.button_all.image:SetScale(.2, .4)
    hover.button_all:SetFont(CHATFONT)
    hover.button_all.text:SetColour(0,0,0,1)
    hover.button_all:SetTextSize(20)
    hover.button_all:SetText(isbuy and STRINGS.BBSHOP.BUTTON_DO_BUYALL or STRINGS.BBSHOP.BUTTON_DO_SELLALL)
    hover.button_all:SetPosition(0, -27)
    hover.button_all:SetOnClick(function()
        HandleTrade(self, isbuy, widget, hover, widget.data, widget.data.count or 0)
    end)
end

------

local function CanHoverSell(self, widget)
    if widget.data == nil then
        return false
    end
    if --判断是否能卖这个货物
        widget.data.count_all == nil or widget.data.count_all <= 0 or --有订购需求
        widget.data.count == nil or widget.data.count < widget.data.num_mix --玩家有物品
    then
        return false
    end

    return true
end
function BambooShopDialog:InitItemListSell(sets)
    local uiname = "scroll_list_sell"

    if self[uiname] ~= nil then
        self[uiname]:Kill()
    end
    self[uiname] = self.proot:AddChild(Widget("list_sell_root"))

    if sets == nil then
        sets = {
            row_w = 88, row_h = 68,
            num_visible_rows = 1.8,
            -- fn_getBgName = function(index)end,
            scale_bg = { focus_x = 1.05, focus_y = 1.2, normal_x = 1.05, normal_y = 1.2 },
            offset_hover = { x = -8, y = -107 },
            -- fn_initUI = function(widget, index)end,
            offset = { x = 5, y = 0 },
            offset_image = { x = 0, y = 8 },
            offset_count = { x = 18, y = 18 },
            offset_price = { x = -10, y = -17 },
            pos = { x = 128, y = -126 },
        }
    else
        if sets.row_w == nil then sets.row_w = 88 end
        if sets.row_h == nil then sets.row_h = 68 end
        if sets.num_visible_rows == nil then sets.num_visible_rows = 1.8 end
        if sets.scale_bg == nil then sets.scale_bg = { focus_x = 1.05, focus_y = 1.2, normal_x = 1.05, normal_y = 1.2 } end
        if sets.offset_hover == nil then sets.offset_hover = { x = -8, y = -107 } end
        if sets.offset == nil then sets.offset = { x = 5, y = 0 } end
        if sets.offset_image == nil then sets.offset_image = { x = 0, y = 8 } end
        if sets.offset_count == nil then sets.offset_count = { x = 18, y = 18 } end
        if sets.offset_price == nil then sets.offset_price = { x = -10, y = -17 } end
        if sets.pos == nil then sets.pos = { x = 128, y = -126 } end
    end

    --初始化格子时
    local function ScrollWidgetsCtor(context, index)
        local w = Widget("shelve_sell_".. index)

        local bgname = nil
        if sets.fn_getBgName ~= nil then
            bgname = sets.fn_getBgName(index)
        else
            bgname = "ui_bs_shelve1"
        end

        --图片按钮：货架背景
        w.bgname = bgname
		w.cell_root = w:AddChild(ImageButton(
            "images/bbshop/"..bgname..".xml", bgname..".tex", bgname..".tex"
        ))
		w.cell_root:SetFocusScale(sets.scale_bg.focus_x, sets.scale_bg.focus_y)
		w.cell_root:SetNormalScale(sets.scale_bg.normal_x, sets.scale_bg.normal_y)
        w.cell_root.move_on_click = false --为了不触发按钮被点击时的下落效果
        w.cell_root:SetPosition(sets.offset.x, sets.offset.y)

        --图片：货物图片
        w.cell_img = w.cell_root:AddChild(Image("images/global.xml", "square.tex"))
        w.cell_img:ScaleToSize(48, 48)
        w.cell_img:SetPosition(sets.offset_image.x, sets.offset_image.y)

        --图片：无货物时的遮挡
        w.cell_img_noitem = w.cell_root:AddChild(Image("images/bbshop/ui_bs_slot_noitem.xml", "ui_bs_slot_noitem.tex"))
        w.cell_img_noitem:ScaleToSize(58, 54)
        w.cell_img_noitem:SetPosition(sets.offset_image.x, sets.offset_image.y)

        if sets.fn_initUI ~= nil then
            sets.fn_initUI(w, index)
        end

        --文字：拥有数量
        w.cell_count_txt = w.cell_root:AddChild(Text(BODYTEXTFONT, 24, "0", UICOLOURS.BRONZE))
        w.cell_count_txt:SetHAlign(ANCHOR_RIGHT)
        w.cell_count_txt:SetRegionSize(20, 33)
        w.cell_count_txt:SetPosition(sets.offset_count.x, sets.offset_count.y)

        --文字：剩余数量
        w.cell_count_all_txt = w.cell_root:AddChild(Text(BODYTEXTFONT, 24, "0", UICOLOURS.WHITE))
        w.cell_count_all_txt:SetHAlign(ANCHOR_RIGHT)
        w.cell_count_all_txt:SetRegionSize(20, 33)
        w.cell_count_all_txt:SetPosition(sets.offset_count.x, sets.offset_count.y-15)

        self:AddPriceTag(w, nil, { offset = sets.offset_price })
        w.cell_price.ongainfocusfn = function()
            self[uiname].grid:OnWidgetFocus(w)
            w.hoverstay = true --鼠标从货物弹窗移入当前对应货物时，不应该删除弹窗
            if w.cell_root:IsEnabled() and CanAddHoverPre(self[uiname]) then
                local pos_y = GetHoverOffsetY(self[uiname].grid.current_scroll_pos or 1, sets.row_h, sets.offset_hover.y)
                if CanHoverSell(self, w) then
                    self:AddSetableHover(w, "hover_sell_root", {
                        idx = index, pos_x = sets.offset_hover.x, pos_y = pos_y,
                        fn_content = function(hover)
                            self:AddCounter(hover, w, index, false, w.data.num_mix) --数量调节器
                        end
                    })
                else
                    pos_y = pos_y - 16 --如果不能产生弹窗时，名字的位置会靠近一些
                end
                self:AddNameHover(w, "hover_name_root", { idx = index, pos_x = sets.offset_hover.x, pos_y = pos_y+53 })
            end
        end
        w.cell_price.onlosefocusfn = function()
            w.hoverstay = nil
            if not w.mouseonhover then --鼠标移出货物时，如果没有移回对应弹窗，则删除弹窗
                if self.hover_sell_root ~= nil then
                    self.hover_sell_root:Kill()
                    self.hover_sell_root = nil
                end
                if self.hover_name_root ~= nil then
                    self.hover_name_root:Kill()
                    self.hover_name_root = nil
                end
            end
        end

		w.cell_root:SetOnClick(function()
            NpcSayDesc(self, w.data, "sell")
		end)

		return w
    end

    --更新数据时
    local function ScrollWidgetSetData(context, widget, data, index)
        if widget.ScrollWidgetSetData == nil then
            widget.ScrollWidgetSetData = ScrollWidgetSetData
        end
		widget.data = data
        local hover = nil
        if CanHoverSell(self, widget) then
            if self.hover_sell_root ~= nil and self.hover_sell_root.itemidx == index then
                hover = self.hover_sell_root
            end
            widget.cell_img_noitem:Hide()
        else
            if self.hover_sell_root ~= nil and self.hover_sell_root.itemidx == index then
                self.hover_sell_root:Kill()
                self.hover_sell_root = nil
                widget.mouseonhover = nil
            end
            widget.cell_img_noitem:Show()
        end

        if data ~= nil then
            data.widget = widget
			widget.cell_root:Show()
            if data.img_atlas ~= nil then
                widget.cell_img:SetTexture(data.img_atlas, data.img_tex)
            end
            if data.count_all <= 0 then --没需求了
                widget.cell_count_txt:SetString("")
                widget.cell_count_all_txt:SetString("")
                widget.cell_price_txt:SetString("--")
            else
                widget.cell_count_txt:SetString(data.count > 0 and tostring(data.count) or "") --玩家拥有的数量
                widget.cell_count_all_txt:SetString(tostring(data.count_all)) --剩余数量
                widget.cell_price_txt:SetString(tostring(data.value))
                if hover ~= nil then
                    hover.input_count:SetEditing(false) --重新判定数量
                end
            end
			widget:Enable()
		else
			widget:Disable()
			widget.cell_root:Hide()
		end
	end

    local grid = TEMPLATES2.ScrollingGrid(
        self.items_sell, {
        context = {},
        widget_width  = sets.row_w,
        widget_height = sets.row_h,
        force_peek    = true,
        num_visible_rows = sets.num_visible_rows, --竖排
        num_columns      = 4, --横排
        item_ctor_fn = ScrollWidgetsCtor,
        apply_fn     = ScrollWidgetSetData,
        scrollbar_offset = 8,
        scrollbar_height_offset = -60
    })

	grid.up_button:SetTextures("images/quagmire_recipebook.xml", "quagmire_recipe_scroll_arrow_hover.tex")
    grid.up_button:SetScale(0.4)

	grid.down_button:SetTextures("images/quagmire_recipebook.xml", "quagmire_recipe_scroll_arrow_hover.tex")
    grid.down_button:SetScale(-0.4)

	grid.scroll_bar_line:SetTexture("images/quagmire_recipebook.xml", "quagmire_recipe_scroll_bar.tex")
	grid.scroll_bar_line:SetScale(.25, 0.25)

	grid.position_marker:SetTextures("images/quagmire_recipebook.xml", "quagmire_recipe_scroll_handle.tex")
	grid.position_marker.image:SetTexture("images/quagmire_recipebook.xml", "quagmire_recipe_scroll_handle.tex")
    grid.position_marker:SetScale(.4)

    self[uiname].grid = self[uiname]:AddChild(grid)
    self[uiname]:SetPosition(sets.pos.x, sets.pos.y)
end

------

function BambooShopDialog:InitItemListFix()
    local uiname = "scroll_list_fix"

    if self[uiname] ~= nil then
        self[uiname]:Kill()
    end
    self[uiname] = self.proot:AddChild(Widget("list_fix_root"))

    local row_w = 88
    local row_h = 68

    --初始化格子时
    local function ScrollWidgetsCtor(context, index)
        local w = Widget("shelve_fix_".. index)

        --图片按钮：货架背景
		w.cell_root = w:AddChild(ImageButton(
            "images/bbshop/ui_bs_shelve1.xml", "ui_bs_shelve1.tex", "ui_bs_shelve1.tex"
        ))
		w.cell_root:SetFocusScale(1.05, 1.2)
		w.cell_root:SetNormalScale(1.05, 1.2)
        w.cell_root.move_on_click = false --为了不触发按钮被点击时的下落效果
        w.cell_root:SetPosition(5, 0)

        --图片：货物图片
        w.cell_img = w.cell_root:AddChild(Image("images/global.xml", "square.tex"))
        w.cell_img:ScaleToSize(48, 48)
        w.cell_img:SetPosition(0, 5)

        --图片：无货物时的遮挡
        w.cell_img_noitem = w.cell_root:AddChild(Image("images/bbshop/ui_bs_slot_noitem.xml", "ui_bs_slot_noitem.tex"))
        w.cell_img_noitem:ScaleToSize(58, 54)
        w.cell_img_noitem:SetPosition(0, 5)

        --文字：当前耐久度
        w.cell_count_txt = w.cell_root:AddChild(Text(BODYTEXTFONT, 24, "100%", UICOLOURS.WHITE))
        w.cell_count_txt:SetHAlign(ANCHOR_RIGHT)
        w.cell_count_txt:SetRegionSize(30, 33)
        w.cell_count_txt:SetPosition(18, 3)

        self:AddPriceTag(w, nil, { offset = { x=-10, y=-18 } })
        w.cell_price.ongainfocusfn = function()
            self[uiname].grid:OnWidgetFocus(w)
            w.hoverstay = true --鼠标从货物弹窗移入当前对应货物时，不应该删除弹窗
            if w.cell_root:IsEnabled() and CanAddHoverPre(self[uiname]) then
                local pos_y = GetHoverOffsetY(self[uiname].grid.current_scroll_pos or 1, row_h, -60)
                if w.data ~= nil and w.data.percent < 100 and w.data.value <= self.coins then
                    self:AddSetableHover(w, "hover_fix_root", {
                        idx = index, pos_x = -5, pos_y = pos_y, bg_width = 66, bg_height = 36,
                        fn_content = function(hover)
                            ------修理按钮
                            hover.button = hover:AddChild(
                                ImageButton("images/global_redux.xml", "button_carny_long_normal.tex",
                                    "button_carny_long_hover.tex", "button_carny_long_disabled.tex", "button_carny_long_down.tex")
                            )
                            hover.button.image:SetScale(.2, .4)
                            hover.button:SetFont(CHATFONT)
                            hover.button.text:SetColour(0,0,0,1)
                            hover.button:SetTextSize(20)
                            hover.button:SetText(STRINGS.BBSHOP.BUTTON_DO_FIX)
                            hover.button:SetPosition(0, 0)
                            hover.button:SetOnClick(function()
                                if w.data.percent < 100 and w.data.value <= self.coins then
                                    DoRpc(3, { servetype = 2 }, w.data.item)

                                    --延迟更新
                                    SetTaskInvItems_weapons(self)
                                    --更新钱（暂时的）
                                    self.coins = self.coins - w.data.value
                                    if self.coin_value ~= nil then
                                        self.coin_value:SetString(tostring(self.coins))
                                    end
                                    --更新货架（暂时的）
                                    w.data.percent = 100
                                    if w.ScrollWidgetSetData ~= nil then
                                        w.ScrollWidgetSetData(nil, w, w.data, hover.itemidx)
                                    end

                                    if math.random() < 0.3 then
                                        self:NpcSay(GetRandomItem(self.data_shopui.npc.say_fix), 1, "anim_happy", false)
                                    end
                                end
                            end)
                        end
                    })
                -- else
                --     pos_y = pos_y - 16 --如果不能产生弹窗时，名字的位置会靠近一些
                end
                self:AddNameHover(w, "hover_name_root", { idx = index, pos_x = -5, pos_y = pos_y+53-16 })
            end
        end
        w.cell_price.onlosefocusfn = function()
            w.hoverstay = nil
            if not w.mouseonhover then --鼠标移出货物时，如果没有移回对应弹窗，则删除弹窗
                if self.hover_fix_root ~= nil then
                    self.hover_fix_root:Kill()
                    self.hover_fix_root = nil
                end
                if self.hover_name_root ~= nil then
                    self.hover_name_root:Kill()
                    self.hover_name_root = nil
                end
            end
        end

		w.cell_root:SetOnClick(function()
            NpcSayDesc(self, w.data, "fix")
		end)

		return w
    end

    --更新数据时
    local function ScrollWidgetSetData(context, widget, data, index)
        if widget.ScrollWidgetSetData == nil then
            widget.ScrollWidgetSetData = ScrollWidgetSetData
        end
		widget.data = data

		if data ~= nil then
            data.widget = widget
			widget.cell_root:Show()
            if data.img_atlas ~= nil then
                widget.cell_img:SetTexture(data.img_atlas, data.img_tex)
            end
            if data.percent >= 100 then
                widget.cell_price_txt:SetString("--")
                widget.cell_img_noitem:Show()
                if self.hover_fix_root ~= nil then
                    self.hover_fix_root:Kill()
                    self.hover_fix_root = nil
                    widget.mouseonhover = nil
                end
            else
                widget.cell_price_txt:SetString(tostring(data.value))
                widget.cell_img_noitem:Hide()
            end
            widget.cell_count_txt:SetString(GetPercentText(data.percent))
			widget:Enable()
		else
			widget:Disable()
			widget.cell_root:Hide()
		end
	end

    local grid = TEMPLATES2.ScrollingGrid(
        self.items_fix_idx, {
        context = {},
        widget_width  = row_w,
        widget_height = row_h,
        force_peek    = true,
        num_visible_rows = 2.1, --竖排
        num_columns      = 4, --横排
        item_ctor_fn = ScrollWidgetsCtor,
        apply_fn     = ScrollWidgetSetData,
        scrollbar_offset = 8,
        scrollbar_height_offset = -60
    })

	grid.up_button:SetTextures("images/bbshop/ui_bs_shelve1.xml", "ui_bs_shelve1.tex")
    grid.up_button:SetScale(0.1)

	grid.down_button:SetTextures("images/bbshop/ui_bs_shelve1.xml", "ui_bs_shelve1.tex")
    grid.down_button:SetScale(-0.1)

	grid.scroll_bar_line:SetTexture("images/bbshop/ui_bs_shelve1.xml", "ui_bs_shelve1.tex")
	grid.scroll_bar_line:SetScale(.1, 0.1)

	grid.position_marker:SetTextures("images/bbshop/ui_bs_shelve1.xml", "ui_bs_shelve1.tex")
	grid.position_marker.image:SetTexture("images/bbshop/ui_bs_shelve1.xml", "ui_bs_shelve1.tex")
    grid.position_marker:SetScale(.1)

    self[uiname].grid = self[uiname]:AddChild(grid)
    -- self[uiname].grid:SetPosition(-45, 100)
    self[uiname]:SetPosition(128, -82)
end

------

function BambooShopDialog:AddPriceTag(w, value, sets)
    local offset = sets.offset or { x = 0, y = 0 }
    local offset_txt = sets.offset_txt or { x = 0, y = 0 }
    local oversize = 0.6

    w.cell_price = w.cell_root:AddChild(Widget("cell_price_root"))
    w.cell_price:SetPosition(offset.x, offset.y)

    --图片：价格图片背景
    w.cell_price_img = w.cell_price:AddChild(Image("images/bbshop/ui_bs_tag_price.xml", "ui_bs_tag_price.tex"))
    w.cell_price_img:ScaleToSize(sets.bg_width or (103*oversize), sets.bg_height or (45*oversize))

    --文字：价格
    w.cell_price_txt = w.cell_price:AddChild(Text(BUTTONFONT, 22, value or "--", UICOLOURS.BLACK))
    w.cell_price_txt:SetHAlign(ANCHOR_LEFT)
    w.cell_price_txt:SetRegionSize(35, 33)
    w.cell_price_txt:SetPosition(offset_txt.x+17, offset_txt.y-2)
end

------

function BambooShopDialog:OnDestroy()
    --取消循环查询物品task
    if self.task_invitems ~= nil then
        self.task_invitems:Cancel()
        self.task_invitems = nil
    end

    POPUPS.BAMBOOSHOP:Close(self.owner)

	BambooShopDialog._base.OnDestroy(self)
end

function BambooShopDialog:OnBecomeInactive()
    BambooShopDialog._base.OnBecomeInactive(self)
end

function BambooShopDialog:OnBecomeActive()
    BambooShopDialog._base.OnBecomeActive(self)
end

function BambooShopDialog:OnControl(control, down)
    if BambooShopDialog._base.OnControl(self, control, down) then return true end

    if not down and (control == CONTROL_MAP or control == CONTROL_CANCEL) then
		TheFrontEnd:GetSound():PlaySound("dontstarve/HUD/click_move")
        TheFrontEnd:PopScreen()
        return true
    end

	return false
end

return BambooShopDialog
