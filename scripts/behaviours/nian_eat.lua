Nian_Eat = Class(BehaviourNode, function(self, inst)
    BehaviourNode._ctor(self, "Nian_Eat")
    self.inst = inst
    self.radius = 4
end)

local NO_TAGS =
{
    "veggie",
    "player",
    "INLIMBO",
    "wall",
    "epic",
    "ghost",
    "structure",
    "balloon",
    "chess",
    "shadow",
    "shadowcreature",
    "shadowminion",
    "shadowchesspiece",
    "companion",
    "nian",
    "tentacle_pillar",
}

local ACT_TAGS =
{
    "_health",
    "_combat",
}
function Nian_Eat:Visit()
    if self.status == READY then
        self.status = RUNNING
    end
    if self.status == RUNNING then
        local x, y, z = self.inst.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x, y, z, self.radius, ACT_TAGS, NO_TAGS, ACT_TAGS)
        local can_eat = nil
        if #ents > 0 then
            for i, v in pairs(ents) do
                if v:IsValid() and v:IsOnValidGround() and v:GetTimeAlive() > 1 and not (v.components.health ~= nil and v.components.health:IsDead()) then
                    can_eat = v
                    break
                end
            end
        end
        if can_eat then
            self.inst:PushEvent("nian_eat",can_eat)
            self.status = SUCCESS
        else
            self.status = FAILED
        end
    end
end
