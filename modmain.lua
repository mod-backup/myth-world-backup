GLOBAL.setmetatable(env, { __index = function(t, k) return GLOBAL.rawget(GLOBAL, k) end })

TUNING.MYTH_WORDS_MOD_OPEN = true --全局变量方便别的mod获取

PrefabFiles = {
	"mk_items",
	"myth_itemskin_prefabs",
	"bananafan",
	"myth_fx",
	"purple_gourd",
	"myth_door_exit",
	"myth_door_enter",
	"myth_floor",
	"myth_housewall",
	"myth_zodiac",
	"myth_xingyue",
	"myth_interiors",
	"book_fly_myth",
	"myth_dengfire",
	"myth_smalllight",
	"myth_banana_tree",
	"myth_bundle",
	"myth_zongzi",
	"bananafan_big",
	"myth_flyskill",

	"myth_ghg",
	"myth_interiors_ghg",
	"myth_chang_e",
	"myth_redlantern",
	"myth_redlantern_ground",
	"myth_ruyi",
	"myth_fence",
	"myth_bbn",
	"myth_mooncake",
	"myth_ghg_snow",
	"myth_yylp",
	"myth_goldfrog",
	"myth_coin",
	"myth_treasure_bowl",
	"myth_cash_tree",
	"myth_redpouch",
	"myth_goldnugget_ingot",
	"myth_small_goldfrog",
	"myth_goldfrog_spawner",
	"myth_goldfrog_base",
	"myth_farm_spawn",
	"myth_yjp",
	"myth_granary",
	"myth_well",
	"myth_toys",
	"myth_tudi_shrines",
	"myth_tudi",
	"myth_food_table",
	"myth_honeypot",
	"myth_rock_ghgice",
	"laozi_qingniu",
	"saddle_qingniu",
	"laozi_bell",
	"myth_qxj",

	--荷花
	"myth_lotusleaf_hat",
	"myth_lotusleaf",
	"myth_siving",
	"armor_siving",
	"siving_hat",
	"myth_lotus_flower",
	"myth_plant_lotus",
	"myth_weapon_syf",
	"myth_weapon_syd",
	"myth_weapon_gtt",
	"krampussack_sealed",
	"myth_rhino_heart",
	"myth_siving_boss",
	"myth_fuchen",
	"myth_store",
	-- "myth_store_construction", ----青竹洲店铺(删除)

	"myth_statue_pandaman",
	"myth_bamboo",
	"myth_plant_bamboo",
	"myth_hotmeter",
	"myth_gale_sparkle_vfx",

	"myth_passcard_jie",
	"rhino_shoutfx",
	"myth_rhino_fx",
	"myth_rhino_firefx",

	"myth_huanhundan",
	"yama_fire_purple",
	"yama_fire_green",
	"yama_fire_yellow",
	"yama_fire_red",
	"myth_mooncake_box",
	"myth_coin_box",

	--特效
	"myth_theme_fxs",

	--青竹州更新
	"myth_walls",
	"myth_nian",
	"nian_shield",
	"nianbomb",
	"nian_sporecloud",
	"nian_mount",
	"myth_stool",
	"myth_rocktips",
	"cane_peach",
	"myth_gold_staff",
	"myth_nianhat",
	"infantree_carpet",
	"myth_essense",
	"myth_plant_infantree",
	"myth_plant_infantree_vine",
	"myth_plant_infantree_trunk",
	"myth_plant_infantree_small",
	"myth_plant_infantree_medium",
	"myth_plant_infant_fruit",
	"myth_house_bamboo",
	"myth_house_lights",
	"myth_house_lights_light",
	"myth_playerredpouch",

	--青竹洲店铺
	"myth_ironitems",  --铸铁装备
	"miniflare_myth",  --窜天猴
	"firecrackers_myth", --爆竹
	"commissioners_invs", --无常新修改
	"commissioner_book",
	"pandaman_myth",
}

Assets = {
	Asset("IMAGE", "images/monkey_king_item.tex"),
	Asset("ATLAS", "images/monkey_king_item.xml"),

	Asset("ATLAS_BUILD", "images/monkey_king_item.xml", 256),
	Asset("ATLAS", "images/inventoryimages/myth_banana_tree.xml"),
	Asset("ATLAS", "images/inventoryimages/myth_flyskill.xml"),
	Asset("ATLAS", "images/inventoryimages/myth_flyskill_mk.xml"),
	Asset("ATLAS", "images/inventoryimages/myth_flyskill_pg.xml"),
	Asset("ATLAS", "images/inventoryimages/myth_flyskill_nz.xml"),
	Asset("ATLAS", "images/inventoryimages/myth_flyskill_wb.xml"),
	Asset("ATLAS", "images/inventoryimages/myth_flyskill_yj.xml"),
	Asset("ATLAS", "images/inventoryimages/myth_flyskill_yt.xml"),
	Asset("ATLAS", "images/inventoryimages/myth_flyskill_ya.xml"),
	Asset("ATLAS", "images/hud/myth_tab.xml"),
	Asset("IMAGE", "images/hud/myth_tab.tex"),
	Asset("ATLAS", "images/hud/myth_tab_change.xml"),
	Asset("IMAGE", "images/hud/myth_tab_change.tex"),
	Asset("ATLAS", "images/hud/myth_tab_skill.xml"),
	Asset("IMAGE", "images/hud/myth_tab_skill.tex"),

	Asset("ATLAS", "images/myth_cloudfx.xml"),
	Asset("IMAGE", "images/myth_cloudfx.tex"),

	Asset("ANIM", "anim/gourd_items.zip"),

	Asset("ANIM", "anim/purple_gourd.zip"),
	Asset("ANIM", "anim/myth_peruse.zip"),
	Asset("ANIM", "anim/myth_wrap_bundle.zip"),
	Asset("ANIM", "anim/player_myth_stool_sit.zip"),

	Asset("SOUNDPACKAGE", "sound/buttons.fev"),
	Asset("SOUND", "sound/myth_jssound.fsb"),

	Asset("SOUNDPACKAGE", "sound/Myth_nian.fev"),
	Asset("SOUND", "sound/Myth_nian.fsb"),

	Asset("SOUNDPACKAGE", "sound/bianzhong.fev"),
	Asset("SOUND", "sound/bianzhong.fsb"),

	Asset("SOUNDPACKAGE", "sound/myth_icon_sound.fev"),
	Asset("SOUND", "sound/myth_icon_sound.fsb"),

	Asset("SOUNDPACKAGE", "sound/mythsound_rhino.fev"),
	Asset("SOUND", "sound/mythsound_rhino.fsb"),
	Asset("SOUNDPACKAGE", "sound/Laozi.fev"),
	Asset("SOUND", "sound/Laozi.fsb"),

	Asset("SOUNDPACKAGE", "sound/bamboo_clappers.fev"),
	Asset("SOUND", "sound/bamboo_clappers.fsb"),
	Asset("SOUNDPACKAGE", "sound/crane.fev"),
	Asset("SOUND", "sound/crane.fsb"),

	--青竹洲店铺
	Asset("IMAGE", "images/bbshop/ui_bs_shelve1.tex"), --本身没有内容，只是为了占位
	Asset("ATLAS", "images/bbshop/ui_bs_shelve1.xml"),
	Asset("IMAGE", "images/bbshop/ui_bbshop_btnbg.tex"),
	Asset("ATLAS", "images/bbshop/ui_bbshop_btnbg.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_bg.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_bg.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_bg6.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_bg6.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_bg7.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_bg7.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_bg8.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_bg8.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_coinbag.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_coinbag.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_tag_buy.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_tag_buy.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_tag_sell.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_tag_sell.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_tag_fix.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_tag_fix.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_tag_price.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_tag_price.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_slot_noitem.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_slot_noitem.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_1_1.tex"), --珍奇小摊
	Asset("ATLAS", "images/bbshop/ui_bs_1_1.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_1_2.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_1_2.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_1_3.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_1_3.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_1_4.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_1_4.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_2_1.tex"), --菜市小铺
	Asset("ATLAS", "images/bbshop/ui_bs_2_1.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_2_2.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_2_2.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_2_3.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_2_3.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_2_4.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_2_4.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_2_5.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_2_5.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_2_6.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_2_6.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_2_7.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_2_7.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_3_1.tex"), --花鸟小铺
	Asset("ATLAS", "images/bbshop/ui_bs_3_1.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_3_2.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_3_2.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_3_3.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_3_3.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_3_4.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_3_4.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_3_5.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_3_5.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_4_1.tex"), --禾种小铺
	Asset("ATLAS", "images/bbshop/ui_bs_4_1.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_4_2.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_4_2.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_4_3.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_4_3.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_4_4.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_4_4.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_4_5.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_4_5.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_seasonseeds.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_seasonseeds.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_5_1.tex"), --茶肴小铺
	Asset("ATLAS", "images/bbshop/ui_bs_5_1.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_5_2.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_5_2.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_5_3.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_5_3.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_5_4.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_5_4.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_6_1.tex"), --铸匠小铺
	Asset("ATLAS", "images/bbshop/ui_bs_6_1.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_6_2.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_6_2.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_6_3.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_6_3.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_6_4.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_6_4.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_6_5.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_6_5.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_6_6.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_6_6.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_7_1.tex"), --材瓦小铺
	Asset("ATLAS", "images/bbshop/ui_bs_7_1.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_7_2.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_7_2.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_7_3.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_7_3.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_7_4.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_7_4.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_7_5.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_7_5.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_8_1.tex"), --算命小铺
	Asset("ATLAS", "images/bbshop/ui_bs_8_1.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_8_2.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_8_2.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_8_3.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_8_3.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_fate.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_fate.xml"),
	Asset("IMAGE", "images/bbshop/ui_bs_trea.tex"),
	Asset("ATLAS", "images/bbshop/ui_bs_trea.xml"),

	Asset("IMAGE", "images/inventoryimages/miniflare_myth.tex"),
	Asset("ATLAS", "images/inventoryimages/miniflare_myth.xml"),
	Asset("IMAGE", "images/inventoryimages/firecrackers_myth.tex"),
	Asset("ATLAS", "images/inventoryimages/firecrackers_myth.xml"),
	Asset("ANIM", "anim/townspig_idle.zip"), --哈姆雷特的猪人动画模板
	Asset("ANIM", "anim/townspig_actions.zip"),
	Asset("ANIM", "anim/townspig_attacks.zip"),
	Asset("ANIM", "anim/townspig_basic.zip"),
	Asset("ANIM", "anim/townspig_sneaky.zip"),
	Asset("ANIM", "anim/pandaman_myth_animals.zip"), --熊猫人动画
	Asset("ANIM", "anim/pandaman_myth_construct.zip"),
	Asset("ANIM", "anim/pandaman_myth_foods.zip"),
	Asset("ANIM", "anim/pandaman_myth_ingredient.zip"),
	Asset("ANIM", "anim/pandaman_myth_numerology.zip"),
	Asset("ANIM", "anim/pandaman_myth_plants.zip"),
	Asset("ANIM", "anim/pandaman_myth_rareitem.zip"),
	Asset("ANIM", "anim/pandaman_myth_weapons.zip"),
	Asset("ATLAS", "images/inventoryimages/pass_commissioner.xml"), --无常新修改
	Asset("IMAGE", "images/inventoryimages/pass_commissioner.tex"),
	Asset("ANIM", "anim/pass_commissioner.zip"),

	Asset("ATLAS", "images/myth_ssb_back.xml"),
	Asset("ATLAS", "images/myth_ssb_line.xml"),
}

local mk_map_icons = {
	"peachtree",
	"mk_battle_flag",
	"alchmy_fur",
	"laozi",
	"myth_rhino_desk",
	"fangcunhill",
	"myth_banana_tree",
	"myth_ghg",
	"myth_yylp",
	"myth_treasure_bowl",
	"myth_cash_tree_ground",
	"myth_goldfrog_base",
	"myth_yjp",
	"myth_granary",
	"myth_well",
	"myth_tudi_shrines",
	"myth_food_table",
	"myth_honeypot",
	"laozi_bell",
	"myth_plant_bamboo",
	"myth_lotus_flower",
	"nian_bell",
	"myth_plant_infant_fruit",
	"myth_plant_infantree",
}

for k, v in pairs(mk_map_icons) do
	table.insert(Assets, Asset("IMAGE", "images/map_icons/" .. v .. ".tex"))
	table.insert(Assets, Asset("ATLAS", "images/map_icons/" .. v .. ".xml"))
	AddMinimapAtlas("images/map_icons/" .. v .. ".xml")
end

--为什么要转dyn呢？ 因为官方的也是dyn 看着好玩 = 。=
local mk_skin_assets = {
	"alchmy_fur", "alchmy_fur_copper", "alchmy_fur_ruins", "redlantern_myth_a", "redlantern_myth_b", "redlantern_myth_c",
	"redlantern_myth_d",
	"swap_redlantern_myth_a", "swap_redlantern_myth_b", "swap_redlantern_myth_c", "swap_redlantern_myth_d",
	"myth_granary", "myth_well",
	"redlantern_myth_e", "swap_redlantern_myth_e",
	"myth_food_table_star", "myth_food_table_stone",
	"myth_interiors_ghg_groundlight_bgz", "myth_interiors_ghg_groundlight_crh",
	"myth_interiors_ghg_groundlight_blz", "myth_interiors_ghg_groundlight_llt",
	"myth_interiors_ghg_groundlight_qzh", "myth_interiors_ghg_groundlight_ryx",
	"myth_interiors_ghg_groundlight_std", "myth_interiors_ghg_groundlight_zsz",
	"myth_interiors_ghg_groundlight_gxy",
	"myth_interiors_ghg_groundlight_yhy",
}
for _, v in ipairs(mk_skin_assets) do
	table.insert(Assets, Asset("DYNAMIC_ANIM", "anim/dynamic/" .. v .. ".zip"))
	table.insert(Assets, Asset("PKGREF", "anim/dynamic/" .. v .. ".dyn"))
end

function AddPrefabFiles(...)
	for _, v in ipairs({ ... }) do table.insert(PrefabFiles, v) end
end

--------------------------------------------------------------------------
--[[ 语言设置 ]]
--------------------------------------------------------------------------

AddReplicableComponent("myth_show_hotmeter")

local MK_MOD_LANGUAGE_SETTING = "CHINESE"

if GetModConfigData('Language') ~= 'A' then
	MK_MOD_LANGUAGE_SETTING = GetModConfigData('Language') --不是自动就是他了
else
	--检测系统语言
	local loc = require "languages/loc"
	local lan = loc and loc.GetLanguage and loc.GetLanguage()
	if lan == LANGUAGE.CHINESE_S or lan == LANGUAGE.CHINESE_S_RAIL then --自动的话找中文
		MK_MOD_LANGUAGE_SETTING = "CHINESE"
	else
		MK_MOD_LANGUAGE_SETTING = "ENGLISH"
	end
end
if type(MK_MOD_LANGUAGE_SETTING) == "boolean" then --旧版服务器的设置会保留以前的值
	MK_MOD_LANGUAGE_SETTING = "CHINESE"
end
GLOBAL.MK_MOD_LANGUAGE_SETTING = MK_MOD_LANGUAGE_SETTING

TUNING.MYTH_TREASURE_BOWL = {}

cache_fns = {}
function Myth_AddCachedStr(fn)
	table.insert(cache_fns, fn)
end

GLOBAL.Myth_AddCachedStr = Myth_AddCachedStr

function GLOBAL.Myth_RunCachedStr() --Called by character mod
	for _, fn in ipairs(cache_fns) do fn() end
end

--------------------------------------------------------------------------
--[[ 功能 ]]
--------------------------------------------------------------------------

local rwms = require "languages/strings_myth_theme_rwms"
if MK_MOD_LANGUAGE_SETTING == "CHINESE" then
	modimport("scripts/languages/strings_myth_theme_chs.lua")
	rwms("chs")
elseif MK_MOD_LANGUAGE_SETTING == "JAPAN" then
	modimport("scripts/languages/strings_myth_theme_ja.lua")
	rwms("ja")
elseif MK_MOD_LANGUAGE_SETTING == "VI" then
	modimport("scripts/languages/strings_myth_theme_vi.lua")
	rwms("vi")
else
	modimport("scripts/languages/strings_myth_theme_en.lua")
	rwms("en")
end

local function import(t)
	for _, v in ipairs(t) do modimport("main/" .. v) end
end

import {
	'sg',
	'actions',
	'containers',
	'equips',
	'foods',
	'tech',
	"widgets",

	'flag',
	'laozi',
	'peach',
	'pills',
	'plantable',
	"blackbear",
	"rhino",
	"components",
	'etc',
	'tuning',
	'mk_flyer',
	"mod_rpc",
	"prefabpostInit",
	"myth_bookinfo",
	"myth_lotus",
	"myth_newturf",
	"registerinventoryitematlas",
	"myth_pill_recipes", --炼丹
	--'hot',
	'myth_skins',
	"myth_bambooshop", --青竹洲店铺

	'recipes',      --配方要写在科技定义之后！
	'mythhouse',
}

if GetModConfigData('ShowBuff') then
	import { "myth_buff_ly_api" }
end
--modimport("main/logupload.lua")


-- GLOBAL.setmetatable(env, { __index = function(t, k) return GLOBAL.rawget(GLOBAL, k) end })


-- local function GetBuild(inst)
-- 	local strnn = ""
-- 	local str = inst.entity:GetDebugString()

-- 	if not str then
-- 		return nil
-- 	end
-- 	local bank, build, anim = str:match("bank: (.+) build: (.+) anim: .+:(.+) Frame")

-- 	if bank ~= nil and build ~= nil then
-- 		strnn = strnn .. "动画: anim/" .. bank .. ".zip"
-- 		strnn = strnn .. "\n" .. "贴图: anim/" .. build .. ".zip"
-- 	end
-- 	return strnn
-- end

-- AddClassPostConstruct("widgets/hoverer", function(self)
-- 	local old_SetString = self.text.SetString
-- 	self.text.SetString = function(text, str)
-- 		local target = TheInput:GetHUDEntityUnderMouse()
-- 		if target ~= nil then
-- 			target = target.widget ~= nil and target.widget.parent ~= nil and target.widget.parent.item
-- 		else
-- 			target = TheInput:GetWorldEntityUnderMouse()
-- 		end
-- 		if target and target.entity ~= nil then
-- 			if target.prefab ~= nil then
-- 				str = str .. "\n" .. "代码:" .. target.prefab
-- 			end
-- 			local build = GetBuild(target)
-- 			if build ~= nil then
-- 				str = str .. "\n" .. build
-- 			end
-- 		end
-- 		return old_SetString(text, str)
-- 	end
-- end)





-- local no_allow = {
-- 	BOOKS         = true,
-- 	SHADOW        = true,
-- 	ENGINEERING   = true,
-- 	ELIXIRBREWING = true,
-- 	BATTLESONGS   = true,
-- 	SPIDERCRAFT   = true,
-- 	NATURE        = true,
-- 	SLINGSHOTAMMO = true,
-- 	BALLOONOMANCY = true,
-- 	CLOCKMAKER    = true,
-- 	STRONGMAN     = true,
-- }
-- for k, v in pairs(CUSTOM_RECIPETABS) do
-- 	--这里其实应该再添加AddPrototyperDef的，但是他要求科技tab名字和科技站名字相同，就没法兼容了
-- 	if not no_allow[k] and v.icon_atlas and v.icon then
-- 		local filter_def = {
-- 			name = string.upper(v.str),
-- 			atlas = v.icon_atlas,
-- 			image = v.icon,
-- 			image_size = 64,
-- 			custom_pos = nil,
-- 			recipes = nil,
-- 		}
-- 		AddRecipeFilter(filter_def, nil)
-- 		-- STRINGS.UI.CRAFTING_FILTERS[filter_def.name]

-- 		if STRINGS.TABS[filter_def.name] and filter_def.name then
-- 			if #STRINGS.TABS[filter_def.name] < 1 and #filter_def.name < 1 then --和平鸽模组的骚操作emmm--名字为空字符串，导致点击就崩溃
-- 				STRINGS.UI.CRAFTING_FILTERS[filter_def.name] = "MISSINGNAME"
-- 			elseif #STRINGS.TABS[filter_def.name] < 1 then
-- 				STRINGS.UI.CRAFTING_FILTERS[filter_def.name] = filter_def.name or "MISSINGNAME"
-- 			elseif #filter_def.name < 1 then
-- 				STRINGS.UI.CRAFTING_FILTERS[filter_def.name] = STRINGS.TABS[filter_def.name] or "MISSINGNAME"
-- 			else
-- 				STRINGS.UI.CRAFTING_FILTERS[filter_def.name] = STRINGS.TABS[filter_def.name] or filter_def.name or
-- 					"MISSINGNAME"
-- 			end
-- 		else
-- 			STRINGS.UI.CRAFTING_FILTERS[filter_def.name] = STRINGS.TABS[filter_def.name] or filter_def.name or
-- 				"MISSINGNAME"
-- 		end
-- 	end
-- end


-- local no_allow = {
-- 	TOOLS               = true,
-- 	LIGHT               = true,
-- 	SURVIVAL            = true,
-- 	FARM                = true,
-- 	SCIENCE             = true,
-- 	WAR                 = true,
-- 	TOWN                = true,
-- 	SEAFARING           = true,
-- 	REFINE              = true,
-- 	MAGIC               = true,
-- 	DRESS               = true,

-- 	ANCIENT             = true,
-- 	CELESTIAL           = true,
-- 	MOON_ALTAR          = true,
-- 	CARTOGRAPHY         = true,
-- 	SCULPTING           = true,
-- 	ORPHANAGE           = true,
-- 	PERDOFFERING        = true,
-- 	MADSCIENCE          = true,
-- 	CARNIVAL_PRIZESHOP  = true,
-- 	CARNIVAL_HOSTSHOP   = true,
-- 	FOODPROCESSING      = true,
-- 	FISHING             = true,
-- 	WINTERSFEASTCOOKING = true,
-- 	HERMITCRABSHOP      = true,
-- 	TURFCRAFTING        = true,
-- }
-- for k, v in pairs(RECIPETABS) do
-- 	--这里其实应该再添加AddPrototyperDef的，但是他要求科技tab名字和名字相同，就没法兼容了
-- 	if not no_allow[k] and v.icon_atlas and v.icon then
-- 		local filter_def = {
-- 			name = string.upper(v.str),
-- 			atlas = v.icon_atlas,
-- 			image = v.icon,
-- 			image_size = 64,
-- 			custom_pos = nil,
-- 			recipes = nil,
-- 		}
-- 		AddRecipeFilter(filter_def, nil)
-- 		if STRINGS.TABS[filter_def.name] and filter_def.name then
-- 			if #STRINGS.TABS[filter_def.name] < 1 and #filter_def.name < 1 then --和平鸽模组的骚操作emmm--名字为空字符串，导致点击就崩溃
-- 				STRINGS.UI.CRAFTING_FILTERS[filter_def.name] = "MISSINGNAME"
-- 			elseif #STRINGS.TABS[filter_def.name] < 1 then
-- 				STRINGS.UI.CRAFTING_FILTERS[filter_def.name] = filter_def.name or "MISSINGNAME"
-- 			elseif #filter_def.name < 1 then
-- 				STRINGS.UI.CRAFTING_FILTERS[filter_def.name] = STRINGS.TABS[filter_def.name] or "MISSINGNAME"
-- 			else
-- 				STRINGS.UI.CRAFTING_FILTERS[filter_def.name] = STRINGS.TABS[filter_def.name] or filter_def.name or
-- 					"MISSINGNAME"
-- 			end
-- 		else
-- 			STRINGS.UI.CRAFTING_FILTERS[filter_def.name] = STRINGS.TABS[filter_def.name] or filter_def.name or
-- 				"MISSINGNAME"
-- 		end
-- 	end
-- end

-- local change = {
-- 	SURVIVAL = "RESTORATION", --生存栏--健康栏
-- 	FARM = "GARDENING",    --耕地
-- 	SCIENCE = "PROTOTYPERS", --科学--科技栏
-- 	WAR = "WEAPONS",       --战斗--武器
-- 	TOWN = "STRUCTURES",   --建筑到建筑栏
-- 	DRESS = "CLOTHING",    --服装到衣服
-- }
-- for k, v in pairs(AllRecipes) do
-- 	if v.tab then
-- 		local tab = nil
-- 		if type(v.tab) == "table" then
-- 			tab = string.upper(v.tab.str)
-- 		elseif type(v.tab) == "string" then
-- 			tab = string.upper(v.tab)
-- 		end
-- 		tab = change[tab] or tab
-- 		if CRAFTING_FILTERS[tab] then
-- 			AddRecipeToFilter(k, tab)
-- 		end
-- 		--科技站物品通过此添加，但是是通过现有信息不能很好的分辨是不是科技站物品，连带上面的科技站tab的问题，就不加了
-- 		-- for k,v in pairs(AllRecipes) do
-- 		--     AddRecipeToFilter(v,"CRAFTING_STATION")
-- 		-- end
-- 	end
-- end


-- setmetatable(STRINGS.UI.CRAFTING_FILTERS,{
--     __newindex  = function(tb,k,...)
--         if #STRINGS.TABS[k]<1 or #k<1 then--和平鸽模组的骚操作emmm--名字为空字符串，导致点击就崩溃
--             return "MISSINGNAME"
--         elseif #k1 then

--         end
--             return STRINGS.TABS[k] or k or "MISSINGNAME"
--         end
--     end
-- })
--[[
    下面是给解锁原版科技的模组科技站用的
    比如神话的月轮
    没实现就懒的改了
]]
--
--给客户端通知一下
-- AddPrefabPostInit("world", function (inst)
--     TheWorld.prototyper_net=net_string(TheWorld.GUID,"prototyper_net","prototyper_net")
--     TheWorld.prototyper_net:set("world_bm")
--     -- if not TheWorld.ismastersim then
--         print("这里有东西1")
--         TheWorld:ListenForEvent("prototyper_net",function (inst)
--             print("这里有东西")
--             local prefab=inst.prototyper_net:value()
--             if prefab and not PROTOTYPER_DEFS[prefab] then
--                 PROTOTYPER_DEFS[prefab]={icon_atlas = CRAFTING_ICONS_ATLAS, icon_image = "station_none.tex",	is_crafting_station = true,filter_text = STRINGS.NAMES[self.inst.prefab:upper()]or self.inst.prefab}
--             end
--         end)
--     -- end
-- end)
-- AddComponentPostInit("prototyper",function (self)
--     self.inst:DoTaskInTime(0.1,function (inst)
--         if self.inst.prefab and not PROTOTYPER_DEFS[self.inst.prefab] then
--             if TheWorld.prototyper_net then
--                 TheWorld.prototyper_net:set("")
--                 TheWorld.prototyper_net:set(self.inst.prefab)
--             end
--             PROTOTYPER_DEFS[self.inst.prefab]={icon_atlas = CRAFTING_ICONS_ATLAS, icon_image = "station_none.tex",	is_crafting_station = true,filter_text = STRINGS.NAMES[self.inst.prefab:upper()]or self.inst.prefab}
--         end
--     end)
-- end)
